<?php include 'filestobeincluded/header-top.php' ?>
<script src="https://kit.fontawesome.com/3212b33ef4.js" crossorigin="anonymous"></script>
<?php include 'filestobeincluded/header-bottom.php' ?>
<!-- Pre-loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div>
<!-- End Preloader-->
<?php include 'filestobeincluded/navigation.php' ?>


<?php

$all_leads = array();
$all_users = array();

if(!isset($_GET['leads'])){
	$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID =7 ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
}
?>


        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1 navbuttons">
                            </nav>
                            <h4 class="mb-1 mt-0">Re-Enquired Leads</h4>
                        </div>

                    </div>
                </div> <!-- container-fluid -->
													
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                            	<div class="row">
								<div class="col-lg-9" style="padding-top: 20px; padding-bottom:20px;">
								

								</div>
								<label class="col-lg-1 col-form-label"
                                        for="followup-date">Select Date</label>
                                    <div class="col-lg-2">
                                        <input class="form-control" id="lead_date" onchange="getList()" type="date" name="date">
                                    </div>
								</div>
								<div id="divLoader" class="col-12" style="display: none; height: 100%;">
									<center><div class="spinner-grow text-primary m-2" role="status">
									<span class="sr-only">Loading...</span>
								</div></center>
							</div>
								<div class="table-responsive">
								<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
								<form id="checkbox-form" method="POST">
                                <table id="" class="table table-striped table-hover nowrap" data-export-title="Blackboard_Leads_<?php echo date("Y-m-d h:i a"); ?>">
                                    
                                    <tbody id="reLeads">
                                    	
                                    		
                                    	
                                    </tbody>
								</table>
								</form>								
							</div>
							
<script type='text/javascript'>
$(document).ready(function () {
    $(".checkbox-selectall").click(function () {
		$(".checkbox-function").prop('checked', $(this).prop('checked'));
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide4").css({display: "none"});
		}
    });
    
    $(".checkbox-function").change(function(){
        if (!$(this).prop("checked")){
            $(".checkbox-selectall").prop("checked",false);
        }
	});
	
	$(".checkbox-function").click(function () {
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide4").css({display: "none"});
		}
    });

});           	
</script>

<!-------Refer Selected modal-------->
<div class="modal fade" id="selected_refer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Refer Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-refer-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Refer Selected modal-------->
<!-------Delete Selected modal-------->
<div class="modal fade" id="selected_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-xs">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Delete Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-delete-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Delete Selected modal-------->

<script type='text/javascript'>
	function deletelead(id) {
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/del_lead.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-delete').html(response); 

					// Display Modal
					$('#deletelead').modal('show'); 
				}
			});
		}
</script>	
<!-------Delete Lead----->
<div class="modal fade" id="deletelead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-delete">
				
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--------End Delete Lead---------->

<script type='text/javascript'>
	
		function referlead(id) {		
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/refer_lead.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-refer').html(response); 

					// Display Modal
					$('#referlead').modal('show'); 
				}
			});
		}
</script>								
<!--------refer lead---->
<div id="referlead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="referallleadsLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="referallleadsLabel">Refer Lead </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-refer">
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!------End refer lead------>





					</div>
				</div>
			</div>
	</div>
</div> <!-- content -->

<script type="text/javascript">
	$(document).ready(function() {
		$(document).ajaxStart(function(){
		    $('#divLoader').css("display", "block");
		});
		$(document).ajaxStop(function(){
		    $('#divLoader').css("display", "none");
		});
	});
</script>



<script>
    function deleteLeads(id) {
        var delete_lead_id = $('#delete_lead_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "ajax_leads/delete_lead.php",
          data: { "delete_lead_id": delete_lead_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
				toastr.success('Lead deleted successfully');
				window.location.reload();
            }
            else {
                toastr.error('Unable to delete lead');
            }
          }
        });
        return false;
    }
</script>

<script>  
      $(document).ready(function(){  
           $('#upload_csv').on("submit", function(e){  
                e.preventDefault(); //form will not submitted  
                $.ajax({  
					url: "ajax_leads/upload_lead.php",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){  
                     	console.log(data);
						$('.modal').modal('hide');
						if(data.match("true")) {
							toastr.success('Lead uploaded successfully');
							//window.location.reload();
						}
						else {
							toastr.error('Unable to upload lead');
						}  
                     }  
                })  
           });  
      });  
</script>

<script type='text/javascript'>
	function updateOwner(){
            var lead_id = $('#lead_id').val();
            var lead_owner_id = $('#lead_owner').val();
            var refer_lead_owner_id = $('#refer_lead_owner').val();
            var refer_comment = $('#referal_comment').val();
        $.ajax
        ({
          type: "POST",
          url: "ajax_leads/refer_lead_sql.php",
          data: { "lead_owner_id": lead_owner_id, "refer_lead_owner_id": refer_lead_owner_id, "refer_comment": refer_comment, "lead_id": lead_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
				toastr.success('Lead refered successfully');
				window.location.reload();
            }
            else {
                toastr.error('Unable to refer lead');
            }
          }
        });
        return false;
    }
</script>

<script type="text/javascript">
	function getList(vars="") {
			var searchstring = $('#searchbox').val();
			var lead_date = $('#lead_date').val();
	

			var itemPerPage = 25;
      var page =1;
      var totalPages = 0;
      if($("#pagination_info").attr("data-totalpages") != undefined ){
          totalPages = $("#pagination_info").attr("data-totalpages");
      }

      if($("#pagination_info").attr("data-currentpage") != undefined){
          page = $("#pagination_info").attr("data-currentpage");
      }

      if(vars == 'next' && page > 0 && totalPages != page ){
         page = parseInt(page)+1;
        // $('#next').attr('data-page',page);
         $("#pagination_info").attr("data-currentPage",page)
      }else{
        //$('#next').attr('data-page',1);
        $('#next').attr('disabled','disabled');
      }

      if(vars == 'pre' && page > 1 ){
         page = parseInt(page)-1 ;
         $('#pre').attr('data-page',page);
        // value='';
      }else{
        $('#pre').attr('data-page',1);
        $('#pre').attr('disabled','disabled');
      }

			$.ajax({
				url:"fetch_re_enquired.php",
				type: "GET",
				global: true, 
				data: {"lead_date":lead_date,"searchstring":searchstring,"page":page,"itemPerPage":itemPerPage},
				success: function(data) {
					//console.log(data);
					$("#reLeads").html(data);
					$('html, body').animate({ scrollTop: 0 }, 'slow');
					
				}
			});
		}
		getList();
</script>



<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>