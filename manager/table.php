<?php foreach ($all_leads as $lead) { ?>
										<tr>
											<td>
												<div class="row" style="padding-top: 10px;">
													<div class="col-lg-1">
														<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input checkbox-function" name="id[]" id="customCheck2<?php echo $lead['ID']; ?>" value="<?php echo $lead['ID']; ?>">
														<label class="custom-control-label" for="customCheck2<?php echo $lead['ID']; ?>"></label>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Name:</b> <?php echo $lead['Name']; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Email:</b> <?php echo substr($lead['Email'],0,15).'...'; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Mobile:</b> <a href="tel:<?php echo $lead['Mobile']; ?>"><?php echo $lead['Mobile']; ?></a></p>
															</div>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<i data-feather="user" class="icon-dual"></i>&nbsp;&nbsp;<i data-feather="user" class="icon-dual"></i>&nbsp;&nbsp;<i data-feather="user" class="icon-dual"></i>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">
																<?php
																	$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$lead['Counsellor_ID']."'");
																	$counsellor = mysqli_fetch_assoc($counsellor_query);
																	if($counsellor_query->num_rows > 0){
																		$couns = explode(' ',$counsellor['Name']);
																	}else{
																		$counsellor['Name'] = ' ';
																		$couns = $counsellor['Name'];
																	}
																?>
																<p><b>Counsellor:</b> <?php echo $couns[0]; ?></p>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>University:</b> <?php
																		$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$lead['Institute_ID']."'");
																		$univ = mysqli_fetch_assoc($univ_query);
																		if($univ_query->num_rows > 0){
																			echo $univ['Name'];
																		}else{
																			$course['Name'] = ' ';
																			echo $univ['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Course:</b> <?php
																		$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
																		$course = mysqli_fetch_assoc($course_query);
																		if($course_query->num_rows > 0){
																			echo $course['Name'];
																		}else{
																			$course['Name'] = ' ';
																			echo $course['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Specialization:</b> <?php
																		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
																		$specialization = mysqli_fetch_assoc($specialization_query);
																		if($specialization_query->num_rows > 0){
																			echo substr($specialization['Name'],0,27);
																		}else{
																			$specialization['Name'] = ' ';
																			echo $specialization['Name'];
																		}
																	?>
																</p>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
																	$stage = mysqli_fetch_assoc($stage_query);
																	if($stage_query->num_rows > 0){
																		$lead_stage = $stage['Name'];
																	}else{
																		$stage['Name'] = ' ';
																		$lead_stage = $stage['Name'];
																	}
																?>
																<p><b>Stage:</b> <?php echo $lead_stage; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
																	$reason = mysqli_fetch_assoc($reason_query);
																	if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
																		$badge = "success";
																	}
																	else if(strcasecmp($stage['Name'], "COLD")==0) {
																		$badge = "warning";
																	}
																	else {
																		$badge = "danger";
																	}
																?>
																<p><b>Reason:</b> <span class="badge badge-soft-<?php echo($badge); ?> py-1">
																		<?if($reason_query->num_rows > 0){
																			echo $reason['Name'];
																		}else{
																			$reason['Name'] = ' ';
																			echo $reason['Name'];
																		}?>
																	</span>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$lead['Subsource_ID']."'");
																	$subsource = mysqli_fetch_assoc($subsource_query);
																	if($subsource_query->num_rows > 0){
																		$lead_sub = $subsource['Name'];
																	}else{
																		$subsource['Name'] = ' ';
																		$lead_sub = $subsource['Name'];
																	}
																?>
																<p><b>Sub-Source:</b> <?php echo $lead_sub; ?></p>
															</div>
														</div>
													</div>
													<div class="col-lg-1">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<div class="btn-group">
																	<span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" style="cursor: pointer;" data-id="<?php echo $lead['ID']; ?>" aria-hidden="true"></i></p></span>&nbsp;&nbsp;
																	
																	<a href="tel:<?php echo $lead['Mobile']; ?>"><span data-toggle="tooltip" data-placement="top" data-original-title="Call" title=""><p style="font-size: 20px;"><i class="fa fa-phone" data-toggle="modal" style="cursor: pointer;" data-target="#leadcallmodal" aria-hidden="true"></i></p></span></a>&nbsp;&nbsp;
																	<span data-toggle="dropdown"><p style="font-size: 20px;"><i class="fa fa-ellipsis-v" style="cursor: pointer;" aria-hidden="true"></i></p></span>
																	<div class="dropdown-menu dropdown-menu-right">
																		<span class="dropdown-item"><i class="fa fa-user-plus"></i> <font class="addfollowupmodal" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Add Followup</font></span>
																		<span class="dropdown-item"><i class="fa fa-edit"></i> <font class="editlead" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Edit Lead</font></span>
																		<span class="dropdown-item"><i class="fa fa-history"></i> <font class="leadhistory" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">View History</font></span>
																		<span class="dropdown-item"><i class="fa fa-share-square-o"></i> <font class="referlead" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Refer Lead</font></span>
																		<span class="dropdown-item"><i class="fa fa-trash"></i> <font class="deletelead" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Delete</font></span>
																	</div>
																</div>
															</div>
														</div>
														
														
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
																	