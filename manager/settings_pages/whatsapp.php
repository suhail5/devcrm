<?php
    $all_whatsapp = array();
    $whatsapp_query_res = $conn->query("SELECT * FROM WhatsApp_Templates WHERE Institute_ID = '".$_SESSION['INSTITUTE_ID']."'");
    while ($row = $whatsapp_query_res->fetch_assoc()) {
        $all_whatsapp[] = $row;
    }
?>

<button class="btn btn-primary float-right" data-toggle="modal" data-target="#addwatempmodal">Add WhatsApp Template</button>
    <!----Add whatsapp Template Modal-------->
    <div class="modal fade" id="addwatempmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myCenterModalLabel">Add New Whatsapp Template</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form method="POST" action="">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label"
                                for="temp_name_whatsapp">Template Name</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="temp_name_whatsapp"
                                    placeholder="Template Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Institute</label>
                            <div class="col-lg-10">
                                <select class="form-control custom-select" id="whatsapp_institute_id">
                                    <option disabled selected>Select Institute</option>
                                    <?
                                    foreach ($all_institutes as $institute) {
                                        ?>
                                        <option value="<?php echo $institute['ID'] ?>"><?php echo $institute['Name']; ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Choose Variable</label>
                            <div class="col-lg-10">
                                <select class="form-control custom-select" id="choose_var_whatsapp">
                                    <option value="">Choose</option>
                                    <option value="$lead_name">Lead Name</option>
                                    <option value="$counsellor_name">Counsellor Name</option>
                                    <option value="$counsellor_contact_no">Counsellor Contact No</option>
                                    <option value="$counsellor_email">Counsellor Email</option>
                                </select>
                            </div>
                        </div>

                        
                        
                            <textarea class="form-control input-block-level" id="whatsapp_content" rows="15" required>

                            </textarea>
                        
                        <br><br>
                        
                        <button class="btn btn-primary float-right" type="button" id="savewhatsapptemplate">Publish</button>
                        
                    </form>
                    <script>
                        $(document).ready(function() {
                            
                            $('#choose_var_whatsapp').change(function() {
                                var cursorPos = $('#whatsapp_content').prop('selectionStart');
                                var v = $('#whatsapp_content').val();
                                var textBefore = v.substring(0,  cursorPos);
                                var textAfter  = v.substring(cursorPos, v.length);

                                $('#whatsapp_content').val(textBefore + $(this).val() + textAfter);
                            });
                            
                        });
                    </script>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <br><br>
    <div class="table-responsive" style="padding-top: 20px;" id="whatsapptable">
    <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
    <table class="table table-hover mb-0">
        <thead class="thead-light">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Template Name</th>
            <th scope="col">Message</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $wa_counter = 0;
                foreach($all_whatsapp as $wap){
                    $wa_counter++;
                    $ID_whatsapp = $wap['ID'];
                    $whatsapp_temp_name = $wap['wa_template_name'];
                    $whatsapp_temp_content = trim($wap['wa_template']);
                
            ?>
            <tr>
            <th scope="row"><?php echo $wa_counter ?></th>
            <td><?php echo $whatsapp_temp_name ?></td>
            <td><?php echo substr($whatsapp_temp_content, 0, 100); ?></td>
            <td>
                <i class="fa fa-edit" data-toggle="modal" data-target="#editwatempmodal<?php echo $ID_whatsapp?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                <!----Edit Source Modal-------->
                <div class="modal fade" id="editwatempmodal<?php echo $ID_whatsapp ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myCenterModalLabel">Edit WhatsApp Template</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <form method="POST" action="">
                                   <input type="hidden" value="<?php echo $ID_whatsapp ?>" id="temp_whatsapp_id<?php echo $ID_whatsapp ?>">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label"
                                            for="temp_name_whatsapp">Template Name</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="whatsapp_temp_name<?php echo $ID_whatsapp ?>"
                                                placeholder="Template Name" value="<?php echo $whatsapp_temp_name ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Institute</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select" id="whatsapp_institute_id<?php echo $ID_whatsapp ?>">
                                                <option disabled selected>Select Institute</option>
                                                <?
                                                foreach ($all_institutes as $institute) {
                                                    ?>
                                                    <option value="<?php echo $institute['ID'] ?>"><?php echo $institute['Name']; ?></option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Choose Variable</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select" id="choose_var_whatsapp<?php echo $ID_whatsapp ?>">
                                                <option value="">Choose</option>
                                                <option value="$lead_name">Lead Name</option>
                                                <option value="$counsellor_name">Counsellor Name</option>
                                                <option value="$counsellor_contact_no">Counsellor Contact No</option>
                                                <option value="$counsellor_email">Counsellor Email</option>
                                            </select>
                                        </div>
                                    </div>

                                    <br><br>
                                    <div id="whatsappEditor" style="border: 1px solid #e6e6e6; border-radius:5px;">
                                        <textarea class="form-control input-block-level" rows="15" id="whatsapp_content<?php echo $ID_whatsapp ?>" required>
                                            <?php echo $whatsapp_temp_content ?>
                                        </textarea>
                                    </div>
                                    <br><br>
                                    
                                    <button class="btn btn-primary float-right" type="button" onclick="updatewhatsapptemp(<?php echo $ID_whatsapp ?>)">Update</button>
                                    
                                </form>
                                <script>
                                    $(document).ready(function() {
                                        $('#choose_var_whatsapp<?php echo $ID_whatsapp ?>').change(function() {
                                            var cursorPos = $('#whatsapp_content<?php echo $ID_whatsapp ?>').prop('selectionStart');
                                            var v = $('#whatsapp_content<?php echo $ID_whatsapp ?>').val();
                                            var textBefore = v.substring(0,  cursorPos);
                                            var textAfter  = v.substring(cursorPos, v.length);

                                            $('#whatsapp_content<?php echo $ID_whatsapp ?>').val(textBefore + $(this).val() + textAfter);
                                        });
                                    });
                                </script>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletewatempmodal<?php echo $ID_whatsapp ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                <!----delete Source Modal-------->
                <div class="modal fade" id="deletewatempmodal<?php echo $ID_whatsapp ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <form method="post">
                                <input type="hidden" id="whatsapp_temp_id<?php echo $ID_whatsapp; ?>" value="<?php echo $ID_whatsapp ?>">
                                <center><button class="btn btn-danger textS-center" type="button" onclick="deletewhatsapptemp(<?php echo $ID_whatsapp; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                            </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>



<script>
  $(document).ready(function () {
    $('#savewhatsapptemplate').click(function (e) {
      e.preventDefault();
      var temp_name_whatsapp = $('#temp_name_whatsapp').val();
      var whatsapp_content = $('#whatsapp_content').val();
      console.log(whatsapp_content);

      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_whatsapp/add_whatsapp.php",
          data: { "temp_name_whatsapp": temp_name_whatsapp, "whatsapp_content":  whatsapp_content},
          success: function (data) {
            $('#addwatempmodal').modal('hide');
           
            if(data.match("true")) {
                $("#whatsapptable").load(location.href + " #whatsapptable");
                $('#addwatempmodal form')[0].reset();
                $('#whatsapp_content').summernote('reset');
                
                toastr.success('WhatsApp Template added successfully');
                
            }
            else {
                toastr.error('Unable to add whatsapp template');
            }
          }
        });
    });
  });
</script>

<script>
    function updatewhatsapptemp(id) {
        var temp_whatsapp_id = $('#temp_whatsapp_id'.concat(id)).val();
        var whatsapp_temp_name_id = $('#whatsapp_temp_name'.concat(id)).val();
        var whatsapp_temp_content_id = $('#whatsapp_content'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_whatsapp/update_whatsapp.php",
          data: { "temp_whatsapp_id": temp_whatsapp_id, "whatsapp_temp_name_id": whatsapp_temp_name_id, "whatsapp_temp_content_id":  whatsapp_temp_content_id},
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('WhatsApp Template updated successfully');
                //$("#whatsapptable").load(location.href + " #whatsapptable");
                                
            }
            else {
                toastr.error('Unable to update whatsapp template');
            }
          }
        });
        return false;
    }
</script>


<script>
    function deletewhatsapptemp(id) {
        var whatsapp_temp_id = $('#whatsapp_temp_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_whatsapp/delete_whatsapp.php",
          data: { "whatsapp_temp_id": whatsapp_temp_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('WhatsApp Template deleted successfully');
                $("#whatsapptable").load(location.href + " #whatsapptable");
            }
            else {
                toastr.error('Unable to delete whatsapp template');
            }
          }
        });
        return false;
    }
</script>