<?php
    $all_sms = array();
    $sms_query_res = $conn->query("SELECT * FROM SMS_Templates WHERE Institute_ID = '".$_SESSION['INSTITUTE_ID']."'");
    while ($row = $sms_query_res->fetch_assoc()) {
        $all_sms[] = $row;
    }
?>
<button class="btn btn-primary float-right" data-toggle="modal" data-target="#addsmstempmodal">Add SMS Template</button>
    <!----Add SMS Template Modal-------->
    <div class="modal fade" id="addsmstempmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myCenterModalLabel">Add New SMS Template</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label"
                                for="temp_name_sms">Template Name</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="temp_name_sms"
                                    placeholder="Template Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Institute</label>
                            <div class="col-lg-10">
                                <select class="form-control custom-select" id="sms_institute_id">
                                    <option disabled selected>Select Institute</option>
                                    <?
                                    foreach ($all_institutes as $institute) {
                                        ?>
                                        <option value="<?php echo $institute['ID'] ?>"><?php echo $institute['Name']; ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Choose Variable</label>
                            <div class="col-lg-10">
                                <select class="form-control custom-select" id="choose_var_sms">
                                    <option value="">Choose</option>
                                    <option value="$lead_name">Lead Name</option>
                                    <option value="$counsellor_name">Counsellor Name</option>
                                    <option value="$counsellor_contact_no">Counsellor Contact No</option>
                                    <option value="$counsellor_email">Counsellor Email</option>
                                </select>
                            </div>
                        </div>

                        <br><br>
                        <div id="smsEditor" style="border: 1px solid #e6e6e6; border-radius:5px;">
                            
                                <textarea class="form-control input-block-level" rows="15" id="sms_content" required>

                                </textarea>
                            
                        </div>
                        <br><br>
                        
                        <button class="btn btn-primary float-right" type="button" id="savesmstemplate">Publish</button>
                        
                    </form>
                    <script>
                        $(document).ready(function() {
                            $('#choose_var_sms').change(function() {
                                var cursorPos = $('#sms_content').prop('selectionStart');
                                var v = $('#sms_content').val();
                                var textBefore = v.substring(0,  cursorPos);
                                var textAfter  = v.substring(cursorPos, v.length);

                                $('#sms_content').val(textBefore + $(this).val() + textAfter);
                            });
                        });
                    </script>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <br><br>
    <div class="table-responsive" style="padding-top: 20px;" id="smstable">
    <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
    <table class="table table-hover mb-0">
        <thead class="thead-light">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Template Name</th>
            <th scope="col">SMS Body</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php
                $sm_counter = 0;
                foreach($all_sms as $sm){
                    $sm_counter++;
                    $ID_SMS = $sm['id'];
                    $sms_insti = $sm['Institute_ID'];
                    $sms_temp_name = $sm['sms_template_name'];
                    $sms_temp_content = trim($sm['sms_template']);
                
            ?>
            <tr>
            <th scope="row"><?php echo $sm_counter ?></th>
            <td><?php echo $sms_temp_name ?></td>
            <td><?php echo substr($sms_temp_content, 0, 100) ?></td>
            <td>
                <i class="fa fa-edit" data-toggle="modal" data-target="#editsmstempmodal<?php echo $ID_SMS ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                <!----Edit Source Modal-------->
                <div class="modal fade" id="editsmstempmodal<?php echo $ID_SMS ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myCenterModalLabel">Edit SMS Template</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="">
                                   <input type="hidden" value="<?php echo $ID_SMS ?>" id="temp_sms_id<?php echo $ID_SMS ?>">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label"
                                            for="sms_temp_name<?php echo $ID_SMS ?>">Template Name</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="sms_temp_name<?php echo $ID_SMS ?>"
                                                placeholder="Template Name" value="<?php echo $sms_temp_name ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Institute</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select" id="sms_institute_id<?php echo $ID_SMS ?>">
                                                <option disabled selected>Select Institute</option>
                                                <?
                                                foreach ($all_institutes as $institute) {
                                                    ?>
                                                    <option value="<?php echo $institute['ID'] ?>" <?php echo ($institute['ID'] ==  $sms_insti) ? ' selected="selected"' : '';?>><?php echo $institute['Name']; ?></option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Choose Variable</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select" id="choose_var_sms<?php echo $ID_SMS ?>">
                                                <option value="">Choose</option>
                                                <option value="$lead_name">Lead Name</option>
                                                <option value="$counsellor_name">Counsellor Name</option>
                                                <option value="$counsellor_contact_no">Counsellor Contact No</option>
                                                <option value="$counsellor_email">Counsellor Email</option>
                                            </select>
                                        </div>
                                    </div>

                                    <br><br>
                                    <div id="smsEditor" style="border: 1px solid #e6e6e6; border-radius:5px;">
                                        
                                            <textarea class="form-control input-block-level" rows="15" id="sms_content<?php echo $ID_SMS ?>" required>
                                                <?php echo $sms_temp_content ?>
                                            </textarea>
                                        
                                    </div>
                                    <br><br>
                                    
                                    <button class="btn btn-primary float-right" type="button" onclick="updatesmstemp(<?php echo $ID_SMS ?>)">Update</button>
                                    
                                </form>
                                <script>
                                    $(document).ready(function() {
                                        $('#choose_var_sms<?php echo $ID_SMS ?>').change(function() {
                                            var cursorPos = $('#sms_content<?php echo $ID_SMS ?>').prop('selectionStart');
                                            var v = $('#sms_content<?php echo $ID_SMS ?>').val();
                                            var textBefore = v.substring(0,  cursorPos);
                                            var textAfter  = v.substring(cursorPos, v.length);

                                            $('#sms_content<?php echo $ID_SMS ?>').val(textBefore + $(this).val() + textAfter);
                                        });
                                    });
                                </script>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletesmstempmodal<?php echo $ID_SMS ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                <!----delete Source Modal-------->
                <div class="modal fade" id="deletesmstempmodal<?php echo $ID_SMS ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <form method="post">
                                <input type="hidden" id="sms_temp_id<?php echo $ID_SMS; ?>" value="<?php echo $ID_SMS ?>">
                                <center><button class="btn btn-danger textS-center" type="button" onclick="deletesmstemp(<?php echo $ID_SMS; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                            </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                </td>
            </tr>
                <?php }?>
        </tbody>
    </table>
</div>



<script>
  $(document).ready(function () {
    $('#savesmstemplate').click(function (e) {
      e.preventDefault();
      var temp_name_sms = $('#temp_name_sms').val();
      var sms_content = $('#sms_content').val();
      var sms_institute_id = $('#sms_institute_id').val();
      console.log(sms_content);

      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_sms/add_sms.php",
          data: { "temp_name_sms": temp_name_sms, "sms_content":  sms_content, "sms_institute_id": sms_institute_id},
          success: function (data) {
            $('#addsmstempmodal').modal('hide');
           
            if(data.match("true")) {
                $("#smstable").load(location.href + " #smstable");
                $('#addsmstempmodal form')[0].reset();
                $('#sms_content').summernote('reset');
                
                toastr.success('SMS Template added successfully');
                
            }
            else {
                toastr.error('Unable to add SMS template');
            }
          }
        });
    });
  });
</script>

<script>
    function updatesmstemp(id) {
        var temp_sms_id = $('#temp_sms_id'.concat(id)).val();
        var sms_temp_name_id = $('#sms_temp_name'.concat(id)).val();
        var sms_temp_content_id = $('#sms_content'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_sms/update_sms.php",
          data: { "temp_sms_id": temp_sms_id, "sms_temp_name_id": sms_temp_name_id, "sms_temp_content_id":  sms_temp_content_id},
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('SMS Template updated successfully');
                //$("#smstable").load(location.href + " #smstable");
                                
            }
            else {
                toastr.error('Unable to update sms template');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deletesmstemp(id) {
        var sms_temp_id = $('#sms_temp_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_sms/delete_sms.php",
          data: { "sms_temp_id": sms_temp_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('SMS Template deleted successfully');
                $("#smstable").load(location.href + " #smstable");
            }
            else {
                toastr.error('Unable to delete sms template');
            }
          }
        });
        return false;
    }
</script>