<?php include 'filestobeincluded/header-top.php' ?>
<?php include 'filestobeincluded/header-bottom.php' ?>
<?php include 'filestobeincluded/navigation.php' ?>
        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page" id="updatesettings">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                                <!--<ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Shreyu</a></li>
                                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Starter</li>
                                    </ol>-->
                            </nav>
                            <h4 class="mb-1 mt-0">Settings</h4>
                        </div>
                    </div>

                    <div class="row">
                        
			
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body" id="settingstab">
                                    <ul class="nav nav-pills navtab-bg nav-justified" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-messages-tab" data-toggle="pill"
                                                href="#pills-emails" role="tab" aria-controls="pills-messages"
                                                aria-selected="false">
                                                Email Templates
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-projects-tab" data-toggle="pill"
                                                href="#pills-sms" role="tab" aria-controls="pills-projects"
                                                aria-selected="false">
                                                SMS Templates
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-tasks-tab" data-toggle="pill"
                                                href="#pills-whatsapp" role="tab" aria-controls="pills-tasks"
                                                aria-selected="false">
                                                WhatsApp Templates
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-files-tab" data-toggle="pill"
                                                href="#pills-users" role="tab" aria-controls="pills-files"
                                                aria-selected="false">
                                                Users
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-files-tab" data-toggle="pill"
                                                href="#pills-leadscore" role="tab" aria-controls="pills-files"
                                                aria-selected="false">
                                                Lead Score
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="pills-tabContent">
                                        

                                        
                                        <div class="tab-pane fade show active" id="pills-emails" role="tabpanel" aria-labelledby="pills-messages-tab">
						<?php include 'settings_pages/emails.php' ?>
                                        </div>

                                        <div class="tab-pane fade" id="pills-sms" role="tabpanel" aria-labelledby="pills-projects-tab">
												<?php include 'settings_pages/sms.php' ?>                                           
                                        </div>

                                        <div class="tab-pane fade" id="pills-whatsapp" role="tabpanel" aria-labelledby="pills-tasks-tab">
						<?php include 'settings_pages/whatsapp.php' ?>
                                        </div>

                                        <div class="tab-pane fade" id="pills-users" role="tabpanel" aria-labelledby="pills-files-tab">
                                            						<?php include 'settings_pages/users.php' ?>
                                        </div>

                                        <div class="tab-pane fade" id="pills-leadscore" role="tabpanel" aria-labelledby="pills-files-tab">
                                            
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <!-- end card -->
                        </div>
                    </div>


                </div> <!-- container-fluid -->

            </div> <!-- content -->


<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>