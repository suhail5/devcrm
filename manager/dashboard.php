<?php 
header("Location: leads"); 
include 'filestobeincluded/header-top.php'; ?>
<style>
.scroll{
    max-height: 250px;
    overflow-y: auto;
}
.scroll::-webkit-scrollbar {
    width: 0em;
}
.scrolls{
    max-height: 380px;
    overflow-y: auto;
}
.scrolls::-webkit-scrollbar {
    width: 0em;
}
</style>
<?php include 'filestobeincluded/header-bottom.php' ?>
<!-- Pre-loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div>
<!-- End Preloader-->
<?php include 'filestobeincluded/navigation.php' ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                            <button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Refresh data" onclick="window.location.reload();"><i data-feather="rotate-ccw" class="icon-sm"></i></button>
                            </nav>
                            <h4 class="mb-1 mt-0">Report</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->

                <div class="row">
                    <div  class="col-lg-2">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Last Updated on</p>
                                <font style="font-size: 16px;"><b><?php echo date("j M, h:i A"); ?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-2">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total Leads</p>
                                <font style="font-size: 16px;"><b><?php $get_all_leads = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID IN ($tree_ids)"); echo mysqli_num_rows($get_all_leads)?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-2">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total Emails Sent</p>
                                <font style="font-size: 16px;"><b><?php $email_logs = $conn->query("SELECT COUNT(ID) as email_count FROM Email_Logs WHERE Employee_ID IN ($tree_ids)"); $el = mysqli_fetch_assoc($email_logs); echo $el['email_count']; ?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-2">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total SMS Sent</p>
                                <font style="font-size: 16px;"><b><?php $get_all_leads = $conn->query("SELECT * FROM SMS_Logs WHERE Employee_ID IN ($tree_ids)"); echo mysqli_num_rows($get_all_leads)?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-2">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total Follow-ups</p>
                                <font style="font-size: 16px;"><b><?php $get_all_leads = $conn->query("SELECT * FROM Follow_Ups WHERE Counsellor_ID IN ($tree_ids)"); echo mysqli_num_rows($get_all_leads)?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-2">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total Counsellor</p>
                                <font style="font-size: 16px;"><b><?php $get_all_leads = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '".$_SESSION['useremployeeid']."'"); echo mysqli_num_rows($get_all_leads)?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-9">
                                        <div class="form-group mb-3">
                                            <label>Creation Date</label>
                                            <input type="text" class="form-control datepicker_range" id="lead_date" placeholder="Select Date">
                                            <input type="hidden" id="stage">
                                            <input type="hidden" id="course">
                                            <input type="hidden" id="counsellor">
                                            <input type="hidden" id="university">
                                            <input type="hidden" id="manager">
                                            <input type="hidden" id="source">
                                            <input type="hidden" id="state">
                                            <input type="hidden" id="month">
                                            <input type="hidden" id="attempt">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>&nbsp;</label>
                                        <button class="btn btn-primary" onclick="date_select();">Apply</button>
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="col-lg-9">
                                        <div class="form-group mb-3">
                                            <label>Updation Date</label>
                                            <input type="text" class="form-control datepicker_range" id="update_date" placeholder="Select Date">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label> &nbsp;</label>
                                        <button class="btn btn-primary" onclick="update_date_select();">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-12">
                        <div class="card">
                            <div class="card-body mb-0">
                            <center><p>Admission Done</p></center>
                            <div class="row mb-0">
                                <div class="col-md-6 text-left mb-0">
                                    <p>Previous Month</p>
                                    <p style="font-size: 22px;"><b>
                                    <?php 
                                        $start_date = date("Y-n-j", strtotime("first day of previous month"));
                                        $end_date = date("Y-n-j", strtotime("last day of previous month"));
                                        $get_preadmission_count = $conn->query("SELECT COUNT(ID) as lead_count FROM Leads WHERE Stage_ID = '5' AND (TimeStamp BETWEEN '$start_date. 00:00:00' AND '$end_date. 23:59:59') AND Counsellor_ID IN ($tree_ids)");
                                        $gpac = mysqli_fetch_assoc($get_preadmission_count);
                                        echo $gpac['lead_count'];
                                    ?>
                                    </b></p>
                                </div>
                                <div class="col-md-6 text-right mb-0">
                                    <p>This Month</p>
                                    <p style="font-size: 22px;"><b><?php 
                                        $start_dates = date('Y-m-01'); // hard-coded '01' for first day
                                        $end_dates = date('Y-m-t');
                                        $get_admission_count = $conn->query("SELECT COUNT(ID) as lead_count FROM Leads WHERE Stage_ID = '5' AND (TimeStamp BETWEEN'$start_dates. 00:00:00' AND '$end_dates. 23:59:59') AND Counsellor_ID IN ($tree_ids)");
                                        $gac = mysqli_fetch_assoc($get_admission_count);
                                        echo $gac['lead_count'];
                                    ?></b></p>
                                </div>
                                
                            </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mt-0 mb-0">Overall Leads</h6>
                                    <div id="month_chart" class="scrollate     = ' . date('Y-m-t')  . '<br />';" style="height: 200px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12" style="display: none;">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mt-0 mb-0">Total Leads</h6><br />
                                    <div id="manager_chart" class="scroll" style="padding-top:10px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mt-0 mb-0">Source wise Report</h6><br />
                                    <div id="source_chart" class="scroll" style="padding-top:10px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mt-0 mb-0">Location wise Leads</h6><br />
                                    <div id="state_chart" class="scroll" style="padding-top:10px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body pb-0">
                                <h6 class="card-title mb-0">Course wise Leads</h6>
                                    <div id="piechart" style="height: 320px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mt-0 mb-0">Stage wise Leads</h6>
                                    <div id="stage_chart" style="height: 300px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body pb-0">
                                <h6 class="card-title mt-0 mb-0">Counsellor wise Leads</h6>
                                <div id="counsellor_chart" style="height: 400px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body pb-0">
                                <h6 class="card-title mb-0">University Wise Leads</h6>
                                <div id="university_chart" style="height: 400px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <div class="card">
                            <div class="card-body pb-0">
                                <h6 class="card-title mb-0">Attempts Wise Leads</h6>
                                <div id="attempt_chart" style="height: 400px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card" style="height: 450px;">
                            <div class="card-body pb-0">
                                <h6 class="card-title mb-0">Logout Time</h6>
                                <div id="logout_chart" class="scrolls" style="height: 500px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    

                        
            
            
            
                </div>

            </div> <!-- content -->


<script src="charts/chart.js"></script>
<script></script>
<script>
function download_lead() {
    var manager = $('#manager').val();
    var counsellor = $('#counsellor').val();
    var course = $('#course').val();
    var stage = $('#stage').val();
    var source = $('#source').val();
    var university = $('#university').val();
    var date = $('#lead_date').val();
    var update_date = $('#update_date').val();
    var state = $('#state').val();
    var attempt = $('#attempt').val();  
    $.ajax({
        type: "POST",
        url: "charts/export_leads.php",
        data:{"manager":manager, "university":university, "counsellor":counsellor, "course":course, "source":source, "date":date, "state":state, "attempt":attempt, "stage":stage, "update_date":update_date},
        success: function (data) {
            var downloadLink = document.createElement("a");
            var fileData = ['\ufeff'+data];
            var blobObject = new Blob(fileData,{
                type: "text/csv;charset=utf-8;"
            });
            var url = URL.createObjectURL(blobObject);
            downloadLink.href = url;
            downloadLink.download = "Report_<?php echo date("d-m-Y h:i a"); ?>.csv";
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
    });
    return false;
}
</script>
            
<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>
