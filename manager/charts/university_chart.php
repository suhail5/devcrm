
            <?php 
            include '../filestobeincluded/db_config.php';

            if(session_status() === PHP_SESSION_NONE) session_start();

            if(!empty($_POST['manager'])){
                $manager = $_POST['manager'];
                $exp = explode('(', $manager);
                $manager_id = rtrim($exp[1], ')');
                $manager_query = "AND (U1.ID = '".$_SESSION['useremployeeid']."' OR Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."')";
            }else{
                $manager_query = "AND (U1.ID = '".$_SESSION['useremployeeid']."' OR Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."')";
            }

            if(!empty($_POST['counsellor'])){
                $couns = $_POST['counsellor'];
                $exp = explode('(', $couns);
                $couns_id = rtrim($exp[1], ')');
                $couns_query = "AND Leads.Counsellor_ID = '$couns_id'";
            }else{
                $couns_query = ' ';
            }

            if(!empty($_POST['stage'])){
                $stage = $_POST['stage'];
                $get_stage_id = $conn->query("SELECT ID FROM Stages WHERE Name like '$stage'");
                $gsi = mysqli_fetch_assoc($get_stage_id);
                $stage_query = "AND Leads.Stage_ID = '".$gsi['ID']."'";
            }else{
                $stage_query = ' ';
            }

            if(!empty($_POST['course'])){
                $course = $_POST['course'];
                $exp = explode('-', $course);
                $new_course = $exp[0];
                $new_univ = $exp[1];
                $get_newuniv_id = $conn->query("SELECT ID FROM Institutes WHERE Name like '$new_univ'");
                $nui = mysqli_fetch_assoc($get_newuniv_id);
                if($new_course=='Unknown'){
                    $course_query = "AND Courses.Name IS NULL AND Leads.Institute_ID = '".$nui['ID']."'";
                }else{
                    $get_course_id = $conn->query("SELECT ID FROM Courses WHERE Name like '$new_course' AND Institute_ID = '".$nui['ID']."'");
                    $gci = mysqli_fetch_assoc($get_course_id);
                    $course_query = "AND Leads.Course_ID = '".$gci['ID']."' AND Institutes.ID = '".$nui['ID']."'";
                }
            }else{
                $course_query = ' ';
            }

            if(!empty($_POST['source'])){
                $source = $_POST['source'];
                $get_source_id = $conn->query("SELECT ID FROM Sources WHERE Name like '$source'");
                $gsourcei = mysqli_fetch_assoc($get_source_id);
                $source_query = "AND Leads.Source_ID = '".$gsourcei['ID']."'";
            }else{
                $source_query = ' ';
            }

            if(!empty($_POST['state'])){
                $state = $_POST['state'];
                $get_state_id = $conn->query("SELECT ID FROM States WHERE Name like '$state'");
                if($get_state_id->num_rows>0){
                    $gstate = mysqli_fetch_assoc($get_state_id);
                    $state_query = "AND Leads.State_ID = '".$gstate['ID']."'";
                }else{
                    $state_query = "AND (Leads.State_ID = '' OR Leads.State_ID = ' ')";
                }
            }else{
                $state_query = ' ';
            }

            if(!empty($_POST['date'])){
                if(strpos($_POST['date'],'to')>0)
                {
                  $date = explode('to', $_POST['date']);
                  $from_date = date($date[0]);
                  $to_date = date($date[1]);
                  $date_query = "AND (Leads.Created_at BETWEEN '$from_date' AND '$to_date')";
                }
                else{
                  $from_date = date($_POST['date']);
                  $date_query = "AND (Leads.Created_at BETWEEN '".$from_date." 00:00:00' AND '".$from_date." 23:59:59')";
                }
            }else{
                $date_query = ' ';
            }

            if(!empty($_POST['update_date'])){
                if(strpos($_POST['update_date'],'to')>0)
                {
                  $date = explode('to', $_POST['update_date']);
                  $from_date = date($date[0]);
                  $to_date = date($date[1]);
                  $update_date_query = "AND (Leads.TimeStamp BETWEEN '$from_date' AND '$to_date')";
                }
                else{
                  $from_date = date($_POST['date']);
                  $update_date_query = "AND (Leads.TimeStamp BETWEEN '".$from_date." 00:00:00' AND '".$from_date." 23:59:59')";
                }
            }else{
                $update_date_query = ' ';
            }
            

            
                $data4[] = Array('Universities', 'Lead Count');       
                
                $get_reason_count = $conn->query("SELECT * FROM (SELECT COUNT(Leads.ID) as lead_count, Institutes.Name FROM Leads LEFT JOIN users ON Leads.Counsellor_ID = users.ID LEFT JOIN users as U1 ON users.Reporting_To_User_ID = U1.ID LEFT JOIN States ON Leads.State_ID = States.ID LEFT JOIN Institutes ON Leads.Institute_ID = Institutes.ID LEFT JOIN Courses ON Leads.Course_ID = Courses.ID LEFT JOIN Stages ON Leads.Stage_ID = Stages.ID LEFT JOIN Sources ON Leads.Source_ID = Sources.ID WHERE users.Role != 'Administrator' ".$manager_query.$course_query.$stage_query.$state_query.$source_query.$couns_query.$date_query.$update_date_query." GROUP BY Leads.Institute_ID ORDER BY lead_count DESC) Leads order by lead_count asc");
                    while($grc = $get_reason_count->fetch_assoc()){
                        if(is_null($grc['Name'])){
                            $grc['Name'] = 'Unknown';
                        }
                        $data4[] = Array($grc['Name'], $grc['lead_count']);
                    }
                    echo json_encode($data4);
            







            
            ?> 