<div class="row" id="all_leads_update">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
								<div id="all_leads">
                                <table id="datatable-buttons" class="table table-striped table-hover dt-responsive nowrap" data-export-title="Blacboard Leads">
                                    <thead>
                                        <tr>
											<th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Stage</th>
                                            <th>Course</th>
                                            <th>Specialization</th>
                                            <th>Next Action Date</th>
                                            <th>Reason</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php

                                    	$counter = 1;
                                    	foreach ($all_leads as $lead) {
                                    		
                                    		?>
                                    		<tr>
												<td><?php echo $counter++; ?></td>
                                    			<td><a href="#"><?php echo $lead['Name']; ?></a></td>
		                                        <td><?php echo $lead['Email']; ?></td>
		                                        <td><?php echo $lead['Mobile']; ?></td>
		                                        <td>
		                                        	<?php
		                                        		$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
		                                        		$stage = mysqli_fetch_assoc($stage_query);
		                                        		echo "<b>".$stage['Name']."</b>";
		                                        	?>
		                                        </td>
		                                        <td>
		                                        	<?php
		                                        		$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
		                                        		$course = mysqli_fetch_assoc($course_query);
		                                        		echo $course['Name'];
		                                        	?>
		                                        </td>
		                                        <td>
		                                        	<?php
		                                        		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
		                                        		$specialization = mysqli_fetch_assoc($specialization_query);
		                                        		echo $specialization['Name'];
		                                        	?>
		                                        </td>
		                                        <td>August 14, 2020</td>
		                                        <td>
		                                        	<?php
		                                        		$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
		                                        		$reason = mysqli_fetch_assoc($reason_query);
		                                        		if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
		                                        			$badge = "success";
		                                        		}
		                                        		else if(strcasecmp($stage['Name'], "COLD")==0) {
		                                        			$badge = "warning";
		                                        		}
		                                        		else {
		                                        			$badge = "danger";
		                                        		}
		                                        	?>
		                                        	<span class="badge badge-soft-<?php echo($badge); ?> py-1">
		                                        		<?php echo $reason['Name']; ?>
		                                        	</span>
		                                        </td>
		                                        <td style="font-size: 18px;">
		                                            
													<div class="btn-group">
														<span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><i data-feather="message-circle" class="icon-xs icon-dual" data-toggle="modal" style="cursor: pointer;" data-target="#whatsappmessage<?php echo $lead['ID']; ?>" aria-hidden="true"></i></span>&nbsp;&nbsp;
														
														<span data-toggle="tooltip" data-placement="top" data-original-title="Call" title=""><i data-feather="phone-call" class="icon-xs icon-dual" data-toggle="modal" style="cursor: pointer;" data-target="#call" aria-hidden="true"></i></span>&nbsp;&nbsp;
														<span data-toggle="dropdown"><i data-feather="more-vertical" class="icon-xs icon-dual" style="cursor: pointer;" aria-hidden="true"></i></span>
														<div class="dropdown-menu dropdown-menu-right">
															<a class="dropdown-item" href="" data-toggle="modal" data-target="#addfollowup<?php echo $lead['ID']?>"><i data-feather="plus-circle" class="icon-xs icon-dual"></i> Add Followup</a>
															<a class="dropdown-item" href="" data-toggle="modal" data-target="#editlead<?php echo $lead['ID']?>"><i data-feather="edit" class="icon-xs icon-dual"></i> Edit Lead</a>
															<a class="dropdown-item" href="#"><i data-feather="loader" class="icon-xs icon-dual"></i> View History</a>
															<a class="dropdown-item" href="" data-toggle="modal" data-target="#referlead<?php echo $lead['ID']?>"><i data-feather="share-2" class="icon-xs icon-dual"></i> Refer Lead</a>
															<a class="dropdown-item" href="" data-toggle="modal" data-target="#deletelead<?php echo $lead['ID']?>"><i data-feather="trash" class="icon-xs icon-dual"></i> Delete</a>
														</div>
													</div>
													
													
		                                            
		                                        
												</td>
													<!-------whatsapp modal-------->
													<div class="modal fade" id="whatsappmessage<?php echo $lead['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
														<div class="modal-dialog modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">Send WhatsApp Message</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">�</span>
																	</button>
																</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-12">
																		<form method="POST">
																			<div class="form-group row">
																				<div class="col-lg-12">
																					<input type="text" class="form-control" id="whatsapp_name"
																						value="<?php echo $lead['Name']; ?>">
																				</div>
																			</div>
																			<div class="form-group row">
																				<div class="col-lg-12">
																					<select data-plugin="customselect" class="form-control" id="whatsapp_template">
																						<option disabled selected>Choose Template</option>
																						<?php
																							$result_whatsapp_template = $conn->query("SELECT * FROM WhatsApp_Templates");
																							while($wa_temp = $result_whatsapp_template->fetch_assoc()) {
																						?>
																							<option value="<?php echo $wa_temp['ID']; ?>"><?php echo $wa_temp['wa_template_name']; ?></option>
																						<?php } ?>
																					</select>
																				</div>
																			</div>
																			<input type="hidden" value="<?php echo $lead['Mobile']; ?>" id="whatsapp_tel" required>
																			<div class="form-group row">
																				<div class="col-lg-12">
																					<div id="whatsappmessageshow" style="border: 1px solid #e6e6e6; border-radius:5px;">
																						<div class="controls">
																							<textarea class="summernote input-block-level" id="whatsapp_content" required>
																									
																							</textarea>
																						</div>
																					</div>
																				</div>
																			</div>
																		</form>	

																		</div>
																	</div>
																	
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary">Send Message</button>
																</div>
															</div>
														</div>
													</div>
												<!-------whatsapp modal-------->

												<!-------Add followup modal-------->
												<div class="modal fade" id="addfollowup<?php echo $lead['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
														<div class="modal-dialog modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">Add Followup for <?php echo $lead['Name']; ?></h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">�</span>
																	</button>
																</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-12">
																			<form method="POST">
																				<div class="form-group row">
																					<label class="col-lg-12 col-form-label">Add/Update Followup Type</label>
																					<div class="col-lg-12">
																						<select data-plugin="customselect" class="form-control" id="whatsapp_template">
																							<option disabled selected>Choose</option>
																							<option value="Followup Call">Followup Call</option>
																						</select>
																					</div>
																				</div>
																				<div class="form-group row">
																					<label class="col-lg-2 col-form-label">Select Date</label>
																					<div class="col-lg-10">
																						<input type="text" id="basic-datepicker" class="form-control" value="<?php echo date("Y-m-d", strtotime(' +1 day')) ?>">
																					</div>
																				</div>
																				<div class="form-group row">
																					<label class="col-lg-2 col-form-label">Select Time</label>
																					<div class="col-lg-10">
																						<input type="text" id="basic-timepicker" class="form-control" value="<?php echo date("h:i") ?>">
																					</div>
																				</div>
																				
																			</form>	
																		</div>
																	</div>
																	
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary">Add</button>
																</div>
															</div>
														</div>
													</div>
												<!-------Add Followup modal-------->

												<!--------Edit Lead----------->
												<div class="modal fade" id="editlead<?php echo $lead['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="myCenterModalLabel">Edit Details for <?php echo $lead['Name']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															
															<div class="modal-body">
																<!-----Form goes here --------->														
																<form method="POST">
																		<div class="row">
																			<input type="hidden" id="new_leadID" value="<?php echo $lead['ID']; ?>">
																			<div class="form-group col-lg-6">
																				<label>Stage<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_stage_select" name="new_stage_select">
																					<option selected value="<?php echo $lead['Stage_ID'] ?>"><?
																					$get_stage_name = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
																					$stage_name = mysqli_fetch_assoc($get_stage_name);
																					echo $stage_name['Name'];
																					?></option>
																					<?php
																						$result_stage = $conn->query("SELECT * FROM Stages WHERE Status = 'Y'");
																						while($stages = $result_stage->fetch_assoc()) {
																					?>
																						<option value="<?php echo $stages['ID']; ?>"><?php echo $stages['Name']; ?></option>
																					<?php } ?>
																				</select>                                                            
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Reason<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_reason_select">
																					<option selected value="<?php echo $lead['Reason_ID'] ?>"><?
																					$get_reason_name = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
																					$reason_name = mysqli_fetch_assoc($get_reason_name);
																					echo $reason_name['Name'];
																					?></option>
																				</select>
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Full Name<span style="color: #F64744; font-size: 12px">*</span></label>
																				<input type="text" id="new_full_name" name="full_name" class="form-control" placeholder="Full Name" required="required" value="<?php echo $lead['Name'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Email</label>
																				<input type="email" id="new_email_id" name="email_id" class="form-control" placeholder="Email" value="<?php echo $lead['Email'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Mobile Number<span style="color: #F64744; font-size: 12px">*</span></label>
																				<input type="number" id="new_mobile_number" name="mobile_number" maxlength="10" class="form-control" placeholder="Mobile Number" value="<?php echo $lead['Mobile'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Remarks<span style="color: #F64744; font-size: 12px">*</span></label>
																				<input type="text" id="new_remarks" name="remarks" class="form-control" placeholder="Remarks" value="<?php echo $lead['Remarks'] ?>">
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Address</label>
																				<input type="text" id="new_address" class="form-control" placeholder="Address" value="<?php echo $lead['Address'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label>State<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_state_select" name="state_select">
																					<option selected value="<?php echo $lead['State_ID'] ?>"><?
																					$get_state_name = $conn->query("SELECT * FROM States WHERE ID = '".$lead['State_ID']."'");
																					$state_name = mysqli_fetch_assoc($get_state_name);
																					echo $state_name['Name'];
																					?></option>
																					<?php
																						$result_state = $conn->query("SELECT * FROM States WHERE Country_ID = 101");
																						while($states = $result_state->fetch_assoc()) {
																					?>
																						<option value="<?php echo $states['ID']; ?>"><?php echo $states['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>City<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_city_select">
																					<option selected value="<?php echo $lead['City_ID'] ?>"><?
																					$get_city_name = $conn->query("SELECT * FROM Cities WHERE ID = '".$lead['City_ID']."'");
																					$city_name = mysqli_fetch_assoc($get_city_name);
																					echo $city_name['Name'];
																					?></option>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Pincode</label>
																				<input type="text" pattern="\d*" maxlength="6" id="new_pin_code" name="pin_code" class="form-control" placeholder="Pincode" value="<?php echo $lead['Pincode']?>">
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label>Source<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_source_select" name="source_select">
																					<option selected value="<?php echo $lead['Source_ID'] ?>"><?
																					$get_source_name = $conn->query("SELECT * FROM Sources WHERE ID = '".$lead['Source_ID']."'");
																					$source_name = mysqli_fetch_assoc($get_source_name);
																					echo $source_name['Name'];
																					?></option>
																					<?php
																						$result_source = $conn->query("SELECT * FROM Sources WHERE Status = 'Y'");
																						while($sources = $result_source->fetch_assoc()) {
																					?>
																						<option value="<?php echo $sources['ID']; ?>"><?php echo $sources['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Sub-Source<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_subsource_select">
																					<option selected value="<?php echo $lead['Subsource_ID'] ?>"><?
																					$get_subsource_name = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$lead['Subsource_ID']."'");
																					$subsource_name = mysqli_fetch_assoc($get_subsource_name);
																					echo $subsource_name['Name'];
																					?></option>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Campaign Name</label>
																				<input type="text" id="new_campaign_name" class="form-control" value="General Enquiry" value="<?php echo $lead['CampaignName'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Lead Owner</label>
																				<select data-plugin="customselect" class="form-control" id="new_lead_owner">
																					<?php
																						$result_user = $conn->query("SELECT * FROM users WHERE Status = 'Y'");
																						while($users = $result_user->fetch_assoc()) {
																					?>
																						<option value="<?php echo $users['ID']; ?>"><?php echo $users['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">School Name</label>
																				<input type="text" id="new_school_name" class="form-control" placeholder="School Name" value="<?php echo $lead['School'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-percentage">Percentage / Grade</label>
																				<input type="text" id="new_percentage" class="form-control" placeholder="Percentage / Grade" value="<?php echo $lead['Grade'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-qualification">Highest Qualification</label>
																				<input type="text" id="new_qualification" class="form-control" placeholder="Highest Qualification" value="<?php echo $lead['Qualification'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-refer">Refer By</label>
																				<input type="text" id="new_refer" class="form-control" placeholder="Refer by" value="<?php echo $lead['Refer'] ?>">
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label>University</label>
																				<select data-plugin="customselect" class="form-control" id="new_select_institute" name="select_institute">
																					<option selected value="<?php echo $lead['Institute_ID'] ?>"><?
																					$get_institute_name = $conn->query("SELECT * FROM Institutes WHERE ID = '".$lead['Institute_ID']."'");
																					$institute_name = mysqli_fetch_assoc($get_institute_name);
																					echo $institute_name['Name'];
																					?></option>
																					<?php
																						$result_institute = $conn->query("SELECT * FROM Institutes WHERE Status = 'Y' AND ID <> '0'");
																						while($institutes = $result_institute->fetch_assoc()) {
																					?>
																						<option value="<?php echo $institutes['ID']; ?>"><?php echo $institutes['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Course</label>
																				<select data-plugin="customselect" class="form-control" id="new_select_course">
																					<option selected value="<?php echo $lead['Course_ID'] ?>"><?
																					$get_course_name = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
																					$course_name = mysqli_fetch_assoc($get_course_name);
																					echo $course_name['Name'];
																					?></option>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Specialization</label>
																				<select data-plugin="customselect" class="form-control" id="new_select_specialization">
																					<option selected value="<?php echo $lead['Specialization_ID'] ?>"><?
																					$get_specialization_name = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
																					$specialization_name = mysqli_fetch_assoc($get_specialization_name);
																					echo $course_name['Name'];
																					?></option>
																				</select>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-lg-10"></div>
																			<div class="col-lg-2">
																				<button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
																				<button class="btn btn-primary" type="button" id="new_add_lead" onclick="updateLead(<?php echo $lead['ID']; ?>);" style="float: right;">Update</button>
																			</div>
																			
																		</div>

																	
																</form>
																<!-----Form end --------->
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>
												<!-- /.modal end-->
												<!--------Edit Lead End----------->
												
												<!--------refer lead---->
												<div id="referlead<?php echo $lead['ID']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="referallleadsLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="referallleadsLabel">Refer <?php echo $lead['Name']; ?> </h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="form-group row">
																	<label class="col-lg-2 col-form-label">Select Lead Owner</label>
																	<div class="col-lg-10">
																		<select data-plugin="customselect" class="form-control" id="refer_lead_owner">
																			<option disabled selected>Choose</option>
																			<?php
																				$result_refer_lead = $conn->query("SELECT * FROM users");
																				while($refer_leads = $result_refer_lead->fetch_assoc()) {
																			?>
																				<option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
																			<?php } ?>
																		</select>
																	</div>
																</div>
																<div class="form-group row">
																	<label class="col-lg-2 col-form-label">Add Comment</label>
																	<div class="col-lg-10">
																		<input type="text" class="form-control" id="referal_comment"
																			placeholder="">
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary" >&nbsp;&nbsp;Refer&nbsp;&nbsp;</button>
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div><!-- /.modal -->
												<!------End refer lead------>

												<!-------Delete Lead----->
												<div class="modal fade" id="deletelead<?php echo $lead['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-dialog-centered modal-sm">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form method="POST">
																	<input type="hidden" id="delete_source_id<?php echo $lead['ID']; ?>" value="<?php echo($lead['ID']); ?>">
																	<center><button class="btn btn-danger textS-center" type="button" onclick="deletelead(<?php echo $lead['ID']; ?>)">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
																</form>
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>
												<!-- /.modal -->
												<!--------End Delete Lead---------->



                                    		</tr>
                                    		<?php
                                    	}
                                    	?>
                                    </tbody>
								</table>
							</div>
                            </div>
                        </div>
                    </div>
                </div>