<!-------whatsapp modal-------->
													<div class="modal fade" id="whatsappmessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
														<div class="modal-dialog modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">Send WhatsApp Message</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">�</span>
																	</button>
																</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-12">
																		<form method="POST">
																			<div class="form-group row">
																				<div class="col-lg-12">
																					<input type="text" class="form-control" id="whatsapp_name"
																						value="<?php echo $lead['Name']; ?>">
																				</div>
																			</div>
																			<div class="form-group row">
																				<div class="col-lg-12">
																					<select data-plugin="customselect" class="form-control" id="whatsapp_template">
																						<option disabled selected>Choose Template</option>
																						<?php
																							$result_whatsapp_template = $conn->query("SELECT * FROM WhatsApp_Templates");
																							while($wa_temp = $result_whatsapp_template->fetch_assoc()) {
																						?>
																							<option value="<?php echo $wa_temp['ID']; ?>"><?php echo $wa_temp['wa_template_name']; ?></option>
																						<?php } ?>
																					</select>
																				</div>
																			</div>
																			<input type="hidden" value="<?php echo $lead['Mobile']; ?>" id="whatsapp_tel" required>
																			<div class="form-group row">
																				<div class="col-lg-12">
																					<div id="whatsappmessageshow" style="border: 1px solid #e6e6e6; border-radius:5px;">
																						<div class="controls">
																							<textarea class="summernote input-block-level" id="whatsapp_content" required>
																									
																							</textarea>
																						</div>
																					</div>
																				</div>
																			</div>
																		</form>	

																		</div>
																	</div>
																	
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary">Send Message</button>
																</div>
															</div>
														</div>
													</div>
												<!-------whatsapp modal-------->

												<!-----------Call Modal------------>
												<div id="leadcallmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addcampaignLabel" aria-hidden="true">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="addcampaignLabel">Call Lead</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<h6>Do you want to make a call to <?php echo $lead['Name']."'s" ?> number</h6>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmleadcallmodal<?php echo $lead['ID']; ?>" data-dismiss="modal">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div><!-- /.modal -->
												<!----------------------->
													<div class="modal fade" id="confirmleadcallmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
														<div class="modal-dialog modal-dialog-slideout modal-md" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel"></h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">�</span>
																	</button>
																</div>
																<div class="modal-body">
																	<center><h4>Calling <?php echo $lead['Name']; ?></h4>
																		<br><br><script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
																	<lottie-player src="https://assets2.lottiefiles.com/private_files/lf30_daep5h.json"  background="transparent"  speed="1"  style="width: 250px; height: 250px;"  loop  autoplay></lottie-player>
																	<br><br>
																	<style>
																		.ml12 {
																			font-weight: 400;
																			font-size: 1.2em;
																			text-transform: uppercase;
																			letter-spacing: 0.5em;
																		}

																		.ml12 .letter {
																			display: inline-block;
																			line-height: 1em;
																		}
																	</style>
																	<h2 class="ml12">Please wait...</h2>
																	<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
																	<script>
																		// Wrap every letter in a span
																		var textWrapper = document.querySelector('.ml12');
																		textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

																		anime.timeline({loop: true})
																		.add({
																			targets: '.ml12 .letter',
																			translateX: [40,0],
																			translateZ: 0,
																			opacity: [0,1],
																			easing: "easeOutExpo",
																			duration: 1200,
																			delay: (el, i) => 500 + 30 * i
																		}).add({
																			targets: '.ml12 .letter',
																			translateX: [0,-30],
																			opacity: [1,0],
																			easing: "easeInExpo",
																			duration: 1100,
																			delay: (el, i) => 100 + 30 * i
																		});
																	</script>

																
																	</center>
																</div>	
															</div>
														</div>
													</div>
												<!----------End Call Modal------------>

												<!-------Add followup modal-------->
												<div class="modal fade" id="addfollowup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
														<div class="modal-dialog modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">Add Followup for <?php echo $lead['Name']; ?></h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">�</span>
																	</button>
																</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-12">
																			<form method="POST">
																				<div class="form-group row">
																					<label class="col-lg-12 col-form-label">Add/Update Followup Type</label>
																					<div class="col-lg-12">
																						<select data-plugin="customselect" class="form-control" id="whatsapp_template">
																							<option disabled selected>Choose</option>
																							<option value="Followup Call">Followup Call</option>
																						</select>
																					</div>
																				</div>
																				<div class="form-group row">
																					<label class="col-lg-2 col-form-label">Select Date</label>
																					<div class="col-lg-10">
																						<input type="text" id="basic-datepicker" class="form-control" value="<?php echo date("Y-m-d", strtotime(' +1 day')) ?>">
																					</div>
																				</div>
																				<div class="form-group row">
																					<label class="col-lg-2 col-form-label">Select Time</label>
																					<div class="col-lg-10">
																						<input type="text" id="basic-timepicker" class="form-control" value="<?php echo date("h:i") ?>">
																					</div>
																				</div>
																				<div class="form-group row">
																					<label class="col-lg-2 col-form-label">Remark</label>
																					<div class="col-lg-10">
																						<input type="text" id="followup_remark" class="form-control" placeholder="Remark">
																					</div>
																				</div>
																				
																			</form>	
																		</div>
																	</div>
																	
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary">Add</button>
																</div>
															</div>
														</div>
													</div>
												<!-------Add Followup modal-------->

												<!--------Edit Lead----------->
												<div class="modal fade" id="editlead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="myCenterModalLabel">Edit Details for <?php echo $lead['Name']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															
															<div class="modal-body">
																<!-----Form goes here --------->														
																<form method="POST">
																		<div class="row">
																			<input type="hidden" id="new_leadID" value="<?php echo $lead['ID']; ?>">
																			<div class="form-group col-lg-6">
																				<label>Stage<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_stage_select" name="new_stage_select">
																					<option selected value="<?php echo $lead['Stage_ID'] ?>"><?
																					$get_stage_name = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
																					$stage_name = mysqli_fetch_assoc($get_stage_name);
																					echo $stage_name['Name'];
																					?></option>
																					<?php
																						$result_stage = $conn->query("SELECT * FROM Stages WHERE Status = 'Y'");
																						while($stages = $result_stage->fetch_assoc()) {
																					?>
																						<option value="<?php echo $stages['ID']; ?>"><?php echo $stages['Name']; ?></option>
																					<?php } ?>
																				</select>                                                            
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Reason<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_reason_select">
																					<option selected value="<?php echo $lead['Reason_ID'] ?>"><?
																					$get_reason_name = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
																					$reason_name = mysqli_fetch_assoc($get_reason_name);
																					echo $reason_name['Name'];
																					?></option>
																				</select>
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Full Name<span style="color: #F64744; font-size: 12px">*</span></label>
																				<input type="text" id="new_full_name" name="full_name" class="form-control" placeholder="Full Name" required="required" value="<?php echo $lead['Name'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Email</label>
																				<input type="email" id="new_email_id" name="email_id" class="form-control" placeholder="Email" value="<?php echo $lead['Email'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Mobile Number<span style="color: #F64744; font-size: 12px">*</span></label>
																				<input type="number" id="new_mobile_number" name="mobile_number" maxlength="10" class="form-control" placeholder="Mobile Number" value="<?php echo $lead['Mobile'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Remarks<span style="color: #F64744; font-size: 12px">*</span></label>
																				<input type="text" id="new_remarks" name="remarks" class="form-control" placeholder="Remarks" value="<?php echo $lead['Remarks'] ?>">
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Address</label>
																				<input type="text" id="new_address" class="form-control" placeholder="Address" value="<?php echo $lead['Address'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label>State<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_state_select" name="state_select">
																					<option selected value="<?php echo $lead['State_ID'] ?>"><?
																					$get_state_name = $conn->query("SELECT * FROM States WHERE ID = '".$lead['State_ID']."'");
																					$state_name = mysqli_fetch_assoc($get_state_name);
																					echo $state_name['Name'];
																					?></option>
																					<?php
																						$result_state = $conn->query("SELECT * FROM States WHERE Country_ID = 101");
																						while($states = $result_state->fetch_assoc()) {
																					?>
																						<option value="<?php echo $states['ID']; ?>"><?php echo $states['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>City<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_city_select">
																					<option selected value="<?php echo $lead['City_ID'] ?>"><?
																					$get_city_name = $conn->query("SELECT * FROM Cities WHERE ID = '".$lead['City_ID']."'");
																					$city_name = mysqli_fetch_assoc($get_city_name);
																					echo $city_name['Name'];
																					?></option>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Pincode</label>
																				<input type="text" pattern="\d*" maxlength="6" id="new_pin_code" name="pin_code" class="form-control" placeholder="Pincode" value="<?php echo $lead['Pincode']?>">
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label>Source<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_source_select" name="source_select">
																					<option selected value="<?php echo $lead['Source_ID'] ?>"><?
																					$get_source_name = $conn->query("SELECT * FROM Sources WHERE ID = '".$lead['Source_ID']."'");
																					$source_name = mysqli_fetch_assoc($get_source_name);
																					echo $source_name['Name'];
																					?></option>
																					<?php
																						$result_source = $conn->query("SELECT * FROM Sources WHERE Status = 'Y'");
																						while($sources = $result_source->fetch_assoc()) {
																					?>
																						<option value="<?php echo $sources['ID']; ?>"><?php echo $sources['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Sub-Source<span style="color: #F64744; font-size: 12px">*</span></label>
																				<select data-plugin="customselect" class="form-control" id="new_subsource_select">
																					<option selected value="<?php echo $lead['Subsource_ID'] ?>"><?
																					$get_subsource_name = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$lead['Subsource_ID']."'");
																					$subsource_name = mysqli_fetch_assoc($get_subsource_name);
																					echo $subsource_name['Name'];
																					?></option>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">Campaign Name</label>
																				<input type="text" id="new_campaign_name" class="form-control" value="General Enquiry" value="<?php echo $lead['CampaignName'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Lead Owner</label>
																				<select data-plugin="customselect" class="form-control" id="new_lead_owner">
																					<?php
																						$result_user = $conn->query("SELECT * FROM users WHERE Status = 'Y'");
																						while($users = $result_user->fetch_assoc()) {
																					?>
																						<option value="<?php echo $users['ID']; ?>"><?php echo $users['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-first-name">School Name</label>
																				<input type="text" id="new_school_name" class="form-control" placeholder="School Name" value="<?php echo $lead['School'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-percentage">Percentage / Grade</label>
																				<input type="text" id="new_percentage" class="form-control" placeholder="Percentage / Grade" value="<?php echo $lead['Grade'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-qualification">Highest Qualification</label>
																				<input type="text" id="new_qualification" class="form-control" placeholder="Highest Qualification" value="<?php echo $lead['Qualification'] ?>">
																			</div>
																			<div class="form-group col-lg-6">
																				<label for="sw-arrows-refer">Refer By</label>
																				<input type="text" id="new_refer" class="form-control" placeholder="Refer by" value="<?php echo $lead['Refer'] ?>">
																			</div>
																		</div>
																		<div class="row">
																			<div class="form-group col-lg-6">
																				<label>University</label>
																				<select data-plugin="customselect" class="form-control" id="new_select_institute" name="select_institute">
																					<option selected value="<?php echo $lead['Institute_ID'] ?>"><?
																					$get_institute_name = $conn->query("SELECT * FROM Institutes WHERE ID = '".$lead['Institute_ID']."'");
																					$institute_name = mysqli_fetch_assoc($get_institute_name);
																					echo $institute_name['Name'];
																					?></option>
																					<?php
																						$result_institute = $conn->query("SELECT * FROM Institutes WHERE Status = 'Y' AND ID <> '0'");
																						while($institutes = $result_institute->fetch_assoc()) {
																					?>
																						<option value="<?php echo $institutes['ID']; ?>"><?php echo $institutes['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Course</label>
																				<select data-plugin="customselect" class="form-control" id="new_select_course">
																					<option selected value="<?php echo $lead['Course_ID'] ?>"><?
																					$get_course_name = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
																					$course_name = mysqli_fetch_assoc($get_course_name);
																					echo $course_name['Name'];
																					?></option>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Specialization</label>
																				<select data-plugin="customselect" class="form-control" id="new_select_specialization">
																					<option selected value="<?php echo $lead['Specialization_ID'] ?>"><?
																					$get_specialization_name = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
																					$specialization_name = mysqli_fetch_assoc($get_specialization_name);
																					echo $course_name['Name'];
																					?></option>
																				</select>
																			</div>
																			<div class="form-group col-lg-6">
																				<label>Counsellor</label>
																				<select data-plugin="customselect" class="form-control" id="new_select_counsellor">
																					<option selected disabled>Choose</option>
																					<?php
																						$result_user = $conn->query("SELECT * FROM users WHERE Status = 'Y'");
																						while($users = $result_user->fetch_assoc()) {
																					?>
																						<option value="<?php echo $users['ID']; ?>"><?php echo $users['Name']; ?></option>
																					<?php } ?>
																				</select>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-lg-10"></div>
																			<div class="col-lg-2">
																				<button class="btn btn-light" data-dismiss="modal">Cancel</button>
																				<button class="btn btn-primary" type="button" id="new_add_lead" onclick="updateLead(<?php echo $lead['ID']; ?>);" style="float: right;">Update</button>
																			</div>
																			
																		</div>

																	
																</form>
																<!-----Form end --------->
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>
												<!-- /.modal end-->
												<!--------Edit Lead End----------->


												<!-------History modal-------->
												<div class="modal fade" id="viewhistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
														<div class="modal-dialog modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">History</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">�</span>
																	</button>
																</div>
																<div class="modal-body">
																	<div class="row">
																		<div class="col-12">
																			
																		</div>
																	</div>
																	
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
																</div>
															</div>
														</div>
													</div>
												<!-------End History modal-------->
												
												<!--------refer lead---->
												<div id="referlead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="referallleadsLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="referallleadsLabel">Refer <?php echo $lead['Name']; ?> </h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="form-group row">
																	<label class="col-lg-2 col-form-label">Select Lead Owner</label>
																	<div class="col-lg-10">
																		<select data-plugin="customselect" class="form-control" id="refer_lead_owner">
																			<option disabled selected>Choose</option>
																			<?php
																				$result_refer_lead = $conn->query("SELECT * FROM users");
																				while($refer_leads = $result_refer_lead->fetch_assoc()) {
																			?>
																				<option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
																			<?php } ?>
																		</select>
																	</div>
																</div>
																<div class="form-group row">
																	<label class="col-lg-2 col-form-label">Add Comment</label>
																	<div class="col-lg-10">
																		<input type="text" class="form-control" id="referal_comment"
																			placeholder="">
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary" >&nbsp;&nbsp;Refer&nbsp;&nbsp;</button>
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div><!-- /.modal -->
												<!------End refer lead------>

												<!-------Delete Lead----->
												<div class="modal fade" id="deletelead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-dialog-centered modal-sm">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form method="POST">
																	<input type="hidden" id="delete_lead_id<?php echo($lead['ID']); ?>" value="<?php echo($lead['ID']); ?>">
																	<center><button class="btn btn-danger textS-center" type="button" onclick="deleteLeads(<?php echo $lead['ID']; ?>)">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
																</form>
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>
												<!-- /.modal -->
												<!--------End Delete Lead---------->