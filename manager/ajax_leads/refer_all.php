<?php 
require '../filestobeincluded/db_config.php';

function session_error_function() {
    echo '<script language="javascript">';
    echo 'location.href="/";';
    echo '</script>';
  }
  
  set_error_handler('session_error_function');
  if(session_status() === PHP_SESSION_NONE) session_start();

  require '../filestobeincluded/db_config.php';
 
$all_array = array();
$coun_array = array();
$couns_array = array();
$get_tree = $conn->query("SELECT * FROM users WHERE ID = '". $_SESSION['USERS_ID'] ."'");
while($row = $get_tree->fetch_assoc()) {
    $all_array[] = $row;
}
foreach($all_array as $univ){
    $university = $univ['Institute_ID'];
    $get_counsellors = $conn->query("SELECT * FROM users WHERE Institute_ID = $university AND Role = 'Counsellor'");
    while($rows = $get_counsellors->fetch_assoc()) {
        $coun_array[] = $rows;
    }
    foreach($coun_array as $cuniv){
        $couns_array[]=$cuniv['ID'];
                                                            
    }
    $imp = "'" . implode( "','", ($couns_array) ) . "'";
    $tree_ids = $imp.",'".$_SESSION['USERS_ID']."'";
}


$all_leads=array();
$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
while($row = $leads_query_res->fetch_assoc()) {
    $all_leads[] = $row;
}
?>

<form method="POST" id="refer_new_counsellor_form">

<div class="form-group row">
    <label class="col-lg-2 col-form-label">Select Counsellor</label>
    <div class="col-lg-10">
        <select data-plugin="customselect" class="form-control" id="refer_lead_owner" name="refer_lead_owner">
            <option disabled selected>Choose</option>
            <?php
                $result_refer_lead = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '".$_SESSION['USERS_ID']."'");
                while($refer_leads = $result_refer_lead->fetch_assoc()) {
            ?>
                <option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Add Comment</label>
    <div class="col-lg-10">
        <input type="text" class="form-control" id="referal_comment" placeholder="">
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="referAllLeads()" >&nbsp;&nbsp;Refer&nbsp;&nbsp;</button>
</div>
</form>

<script>
    function referAllLeads() {
        if($('#refer_new_counsellor_form').valid()) {
            var new_counsellor = $('#refer_lead_owner').val();
            var all_selected_leads = '<?php foreach($all_leads as $r_leads){ echo $r_leads['ID'].',';} ?>';
            var comments = $('#referal_comment').val();

            $.ajax
                ({
                  type: "POST",
                  url: "ajax_leads/ajax_refer_all.php",
                  data: { "new_counsellor": new_counsellor, "all_selected_leads": all_selected_leads, "comments": comments },
                  success: function (data) {
                    console.log(data);
                    if(data.match("true")) {
                        toastr.success('All leads referred successfully');
                        location.reload();
                    }
                    else {
                        toastr.error('Unable to all refer leads');
                    }
                  }
                });
            }
        }
</script>

<script>
    $(document).ready(function() {
        $('#refer_new_counsellor_form').validate({
            rules: {
                refer_lead_owner: {
                    required: true
                }
            },
            messages: {
                refer_lead_owner: {
                    required: 'Please select a counsellor'
                }
            },
            highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
            }
        });
    })    
</script>
<?php
exit;
?>