<?php require "../filestobeincluded/db_config.php" ?>

<?php

$sms_temp_query = $conn->query("SELECT * FROM SMS_Templates WHERE sms_template_name = 'Welcome Message'");
$sms_body_res = mysqli_fetch_assoc($sms_temp_query);

$sms_body = $sms_body_res['sms_template'];

?>

<?php

require_once 'vendor/autoload.php';

use Twilio\Rest\Client;

if(isset($_POST['phoneNumber']) && isset($_POST['leadName'])) {

	$phoneNumber = "whatsapp:+91".$_POST['phoneNumber'];
	$leadName = $_POST['leadName'];

	$sid = "ACc6a12f1c186f142d10ef27482a5c74bc"; // Your Account SID from www.twilio.com/console
	$token = "031364793f406dfd471f00ae45452877"; // Your Auth Token from www.twilio.com/console

	$client = new Client($sid, $token);
	$message = $client->messages->create(
	  $phoneNumber,
	  array(
	    'from' => 'whatsapp:+14155238886',
	    'body' => sprintf($sms_body, $leadName)
	  )
	);

	echo "true";
}

else {
	echo "false";
}

?>
