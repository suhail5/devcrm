<?php 

    require "../filestobeincluded/db_config.php";
    if(session_status() === PHP_SESSION_NONE) session_start();

    $ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api-cloudphone.tatateleservices.com/v1/agents');

$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Authorization: '.$_SESSION['token'].'';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}else{
    print_r($result);   
}
curl_close($ch);
?>