<?php 

require "../filestobeincluded/db_config.php";

if(session_status() === PHP_SESSION_NONE) session_start();


$query = $conn->query("SELECT Dialer_EID FROM users where ID = '".$_SESSION['useremployeeid']."' ");
$eid = mysqli_fetch_row($query)[0];

if($eid){

  $curl = curl_init();
  
  curl_setopt_array($curl, [
    CURLOPT_URL => "https://api-cloudphone.tatateleservices.com/v1/live_calls?extension=$eid",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => [
      "Accept: application/json",
      'Authorization: '.$_SESSION['static_token'].''
    ],
  ]);
  
  $result = curl_exec($curl);
  $r = json_decode($result,true);
  
  
  if(!$r[0]['id'] && $r['success'] == false && count($r) !=0  ){
      
      include 'token_refresh_admin.php';
  }
  
  if (curl_errno($curl)) {
      echo 'Error:' . curl_error($curl);
  }else{
      print_r($result);   
  }
  
  curl_close($curl);
}

