<?php
require "../filestobeincluded/db_config.php";
if (session_status() === PHP_SESSION_NONE) session_start();
$agents = $_SESSION['eid'];
$destination = $_POST['destination'];
$call_type = $_POST['call_type'];
$operator = $_POST['operator'];
$direction = $_POST['direction'];
$duration = $_POST['duration'];
$ivr = $_POST['ivr'];
$department = $_POST['department'];
if($_POST['date']){
    $date = $_POST['date'];
    
    [$from_date, $to_date] = explode('-',$date);
    
    $to_date = date_format(date_create($to_date),"Y-m-d H:i:s");
    $from_date = date_format(date_create($from_date),"Y-m-d H:i:s");
    $to_date = str_replace(' ','%20',$to_date);
    $from_date = str_replace(' ','%20',$from_date);
}

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api-cloudphone.tatateleservices.com/v1/call/records?agents=' . $agents . '&destination=' . $destination . '&call_type=' . $call_type . '&operator=' . $operator . '&direction=' . $direction . '&duration=' . $duration . '&ivr=' . $ivr . '&department=' . $department . '&to_date='.$to_date.'&from_date='.$from_date.'&limit=25');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Authorization: ' . $_SESSION['token'] . '';
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
$r = json_decode($result, true);

if ($r['count'] == '' && $r['success'] == false) {
    include 'token_refresh.php';
}


if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
} else {
    print_r($result);
}
curl_close($ch);
