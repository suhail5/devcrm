<?php require 'filestobeincluded/db_config.php'; ?> 


<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require 'Mailer/vendor/autoload.php';
require "Mailer/sendgrid/sendgrid-php/sendgrid-php.php";

require_once 'DialerAPI/vendor/autoload.php';

// if(isset($_GET['initiateDripMarketing'])) {

// 	$stage_Id = $_GET['Stage_ID'];
// 	$institute_Id = $_GET['Institute_ID'];
// 	$course = $_GET['Course_Name'];
// 	$reason_Id = $_GET['Reason_ID'];
// 	$state_Id = $_GET['State_ID'];
// 	$lead_name = $_GET['Lead_Name'];
// 	$lead_email = $_GET['Lead_Email'];
// 	$lead_primary_number = $_GET['Lead_Primary_Number'];
// 	$lead_alt_number = $_GET['Lead_Alt_Number'];
// 	$counsellor_id = $_GET['Counsellor_ID'];
// 	$last_insert_id = $_GET['LEAD_ID'];

// 	if($state_Id == '') {
// 		$state_Id = "0";
// 	}

// 	initiateDripMarketing($conn, $stage_Id, $institute_Id, $course, $reason_Id, $state_Id, $lead_name, $lead_email, $lead_primary_number, $lead_alt_number, $counsellor_id, $last_insert_id);
// }

class MyDB extends SQLite3
{
	function __construct()
	{
		$dbpath = __DIR__ . '/drip.db';
		$this->open($dbpath);
	}
}

$db = new MyDB();
$ret = $db->query("SELECT * FROM drip_leads");
while ($row = $ret->fetchArray(SQLITE3_ASSOC)) {
	$stage_Id = $row['stage_ID'];
	$institute_Id = $row['institute_ID'];
	$course = $row['course_name'];
	$reason_Id = $row['reason_ID'];
	$state_Id = $row['state_ID'];
	$lead_name = $row['name'];
	$lead_email = $row['email'];
	$lead_primary_number = $row['mobile'];
	$lead_alt_number = $row['alt_mobile'];
	$counsellor_id = $row['counsellor_ID'];
	$last_insert_id = $row['lead_ID'];

	$res = initiateDripMarketing($conn, $stage_Id, $institute_Id, $course, $reason_Id, $state_Id, $lead_name, $lead_email, $lead_primary_number, $lead_alt_number, $counsellor_id, $last_insert_id);
	if($res == 'mail' || $res == 'sms' ){
		echo 'delete';
		$delete = $db->exec("DELETE FROM drip_leads WHERE ID='" . $row['ID'] . "'");
		if(!$delete) {
			echo $db->lastErrorMsg();
		 }
	}else{
		// print_r($res);
	}
}



?>

<?php

function initiateDripMarketing($conn, $stage_Id, $institute_Id, $course, $reason_Id, $state_Id, $lead_name, $lead_email, $lead_primary_number, $lead_alt_number, $counsellor_id, $last_insert_id)
{

	$leadProperties = array();

	if (strcasecmp($stage_Id, '') != 0) {
		$leadProperties[] = 'Stage_ID';
	}

	if (strcasecmp($institute_Id, '') != 0) {
		$leadProperties[] = 'Institute_ID';
	}

	if (strcasecmp($course, '') != 0) {
		$leadProperties[] = 'Course';
	}

	if (strcasecmp($reason_Id, '') != 0) {
		$leadProperties[] = 'Reason_ID';
	}

	if (strcasecmp($state_Id, '') != 0) {
		$leadProperties[] = 'State_ID';
	}


	$drip_marketing_rules = array();

	date_default_timezone_set("Asia/Kolkata");
	$time_now = date("yy-m-d H:i:s");

	$get_drip_rules = $conn->query("SELECT * FROM Drip_Marketing WHERE Start_Timestamp < '" . $time_now . "'");
	while ($row = $get_drip_rules->fetch_assoc()) {
		$drip_marketing_rules[] = $row;
	}

	foreach ($drip_marketing_rules as $rule) {
		$if_statement = $rule['If_Statements'];
		$comm_modes = $rule['Comm_Modes'];

		$break_char = '';

		if (strpos($if_statement, '&&') !== false) {
			$break_char = '&&';
		} else {
			$break_char = '||';
		}

		$condition_terms = explode($break_char, $if_statement);
		$condition_terms = array_map('trim', $condition_terms);

		$dripProperties = array();
		$drip_stage_id = '';
		$drip_institute_id = '';
		$drip_course_id = '';
		$drip_reason_id = '';
		$drip_state_id = '';

		foreach ($condition_terms as $term) {
			
			if (strpos($term, 'stage') !== false) {
				$dripProperties[] = 'Stage_ID';
				$drip_stage_id = explode("= ", $term)[1];
			}

			if (strpos($term, 'university') !== false) {
				$dripProperties[] = 'Institute_ID';
				$drip_institute_id = explode("= ", $term)[1];
			}

			if (strpos($term, 'course') !== false) {
				$dripProperties[] = 'Course';
				$drip_course_id = explode("= ", $term)[1];
			}

			if (strpos($term, 'reason') !== false) {
				$dripProperties[] = 'Reason_ID';
				$drip_reason_id = explode("= ", $term)[1];
			}

			if (strpos($term, 'state') !== false) {
				$dripProperties[] = 'State_ID';
				$drip_state_id = explode("= ", $term)[1];
			}
		}
	
		$common_terms = array_intersect($leadProperties, $dripProperties);
		
		$counter = 0;

		foreach ($common_terms as $ct) {
			if (strcasecmp($ct, 'Stage_ID') == 0) {
				if ($stage_Id == trim($drip_stage_id)) {
					$counter++;
				}
			}

			if (strcasecmp($ct, 'Institute_ID') == 0) {
				if ($institute_Id == trim($drip_institute_id)) {
					$counter++;
				}
			}

			if (strcasecmp($ct, 'Course') == 0) {

				$get_drip_course_name = $conn->query("SELECT * FROM Courses WHERE ID = '" . trim($drip_course_id) . "'");
				$drip_course_dets = mysqli_fetch_assoc($get_drip_course_name);
				$drip_course_name = $drip_course_dets['Name'];

				if (strcasecmp($course, $drip_course_name) == 0) {
					$counter++;
				}
			}

			if (strcasecmp($ct, 'Reason_ID') == 0) {
				if ($reason_Id == trim($drip_reason_id)) {
					$counter++;
				}
			}

			if (strcasecmp($ct, 'State_ID') == 0) {
				if ($state_Id == trim($drip_state_id)) {
					$counter++;
				}
			}
		}

		if (strcasecmp($break_char, '&&') == 0) {
			if ($counter == count($common_terms)) {
				if (strpos($comm_modes, 'SMS') !== false && strpos($comm_modes, 'EMAIL') !== false) {
					$sms_mode = explode('@', $comm_modes)[0];
					$email_mode = explode('@', $comm_modes)[1];
				} else if (strpos($comm_modes, 'SMS') !== false) {
					$sms_mode = $comm_modes;
					$email_mode = '';
				} else if (strpos($comm_modes, 'EMAIL') !== false) {
					$email_mode = $comm_modes;
					$sms_mode = '';
				} else {
					$sms_mode = '';
					$email_mode = '';
				}

				if (strcasecmp($email_mode, '') != 0) {
					$template_id = explode("~", $email_mode)[1];
					if($lead_email){
						if(filter_var($lead_email, FILTER_VALIDATE_EMAIL)){
							return sendEmail($conn, $template_id, $lead_name, $lead_email, $counsellor_id, $last_insert_id);
						}
					}
					
				}

				if (strcasecmp($sms_mode, '') != 0) {
					$template_id = explode("~", $sms_mode)[1];
			
					return sendSMS($conn, $template_id, $lead_name, $lead_primary_number, $lead_alt_number, $counsellor_id, $last_insert_id);
				}

			}
		} else if (strcasecmp($break_char, '||') == 0) {
			if ($counter > 0) {

				if (strpos($comm_modes, 'SMS') !== false && strpos($comm_modes, 'EMAIL') !== false) {
					$sms_mode = explode('@', $comm_modes)[0];
					$email_mode = explode('@', $comm_modes)[1];
				} else if (strpos($comm_modes, 'SMS') !== false) {
					$sms_mode = $comm_modes;
					$email_mode = '';
				} else if (strpos($comm_modes, 'EMAIL') !== false) {
					$email_mode = $comm_modes;
					$sms_mode = '';
				} else {
					$sms_mode = '';
					$email_mode = '';
				}

				if (strcasecmp($email_mode, '') != 0) {
					$template_id = explode("~", $email_mode)[1];
					if($lead_email){
						if(filter_var($lead_email, FILTER_VALIDATE_EMAIL)){
							return sendEmail($conn, $template_id, $lead_name, $lead_email, $counsellor_id, $last_insert_id);
						}
					}
				}

				if (strcasecmp($sms_mode, '') != 0) {
					$template_id = explode("~", $sms_mode)[1];
					
					
					return sendSMS($conn, $template_id, $lead_name, $lead_primary_number, $lead_alt_number, $counsellor_id, $last_insert_id);
				}
			}
		}
	}
}

function sendSMS($conn, $template_id, $lead_name, $lead_primary_number, $lead_alt_number, $counsellor_id, $last_insert_id)
{


	$sender_id_query = $conn->query("SELECT Sender_ID FROM Institutes LEFT JOIN users ON Institutes.ID = users.Institute_ID WHERE users.ID = '" . $counsellor_id . "'");

	$sender_id_dets = mysqli_fetch_assoc($sender_id_query);
	$sender_id = $sender_id_dets['Sender_ID'];

	$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '" . $counsellor_id . "'");
	$counsellor_dets = mysqli_fetch_assoc($counsellor_query);

	$body_query = $conn->query("SELECT * FROM SMS_Templates WHERE ID = '" . $template_id . "'");
	$body_dets = mysqli_fetch_assoc($body_query);
	$sms_temp_body = $body_dets['sms_template'];
	$sms_template_id = $body_dets['Template_ID'];

	$sms_temp_body = str_replace('$lead_name', $lead_name, $sms_temp_body);
	$sms_temp_body = str_replace('$counsellor_name', $counsellor_dets['Name'], $sms_temp_body);
	$sms_temp_body = str_replace('$counsellor_contact_no', $counsellor_dets['Mobile'], $sms_temp_body);
	$sms_temp_body = str_replace('$counsellor_email', $counsellor_dets['Email'], $sms_temp_body);

	// if (strcasecmp($sender_id, "") != 0) {

	// 	$key = "010TH37S00jcoqsW5hBKpgRe0mJEA";
	// 	$profile_id = "624897";

	// 	$response = $client->request('GET', 'http://sms.venetsmedia.com/shn/api/pushsms.php?usr=' . $profile_id . '&key=' . $key . '&sndr=' . $sender_id . '&ph=' . $lead_primary_number . ',' . $lead_alt_number . '&text=' . urlencode($sms_temp_body) . '&rpt=1');
	// 	$resp = $response->getBody();

	// 	if (strpos($resp, 'Message Sent.') !== false) {
	// 		$lead_query = $conn->query("SELECT * FROM Leads WHERE Name = '" . $lead_name . "' AND Mobile = '" . $lead_primary_number . "' AND Alt_Mobile = '" . $lead_alt_number . "'");
	// 		if ($lead_query->num_rows > 0) {
	// 			$lead_dets = mysqli_fetch_assoc($lead_query);
	// 			$create_log = $conn->query("INSERT INTO SMS_Logs(Lead_ID, Employee_ID, SMS_temp) VALUES ('" . $lead_dets['ID'] . "', '" . $counsellor_id . "', '" . $sms_temp_body . "')");
	// 		}
	// 	}
	// }
	
	$requestParams = array(

		'user' => 'blackboard',

		'authkey' => '92RemKUMn4aJw',

		'sender' => $sender_id,
		'mobile' => json_encode($lead_primary_number, JSON_NUMERIC_CHECK),
		

		'text' => $sms_temp_body,

		'rpt' => '1',

		'templateid' => $sms_template_id,

		'entityid' => '1301160404164682486'																																		

	);



	$apiUrl = "http://api.onex-ultimo.in/api/pushsms?";

	foreach($requestParams as $key => $val){

		$apiUrl .= $key.'='.urlencode($val).'&';

	}

   

	$apiUrl = rtrim($apiUrl, "&");

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $apiUrl);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);
	
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}else{
		$lead_query = $conn->query("SELECT * FROM Leads WHERE Name = '" . $lead_name . "' AND Mobile = '" . $lead_primary_number . "' AND Alt_Mobile = '" . $lead_alt_number . "'");
	 		if ($lead_query->num_rows > 0) {
	 			$lead_dets = mysqli_fetch_assoc($lead_query);
	 			$create_log = $conn->query("INSERT INTO SMS_Logs(Lead_ID, Employee_ID, SMS_temp) VALUES ('" . $lead_dets['ID'] . "', '" . $counsellor_id . "', '" . $sms_temp_body . "')");
	 		}
		return ('sms');
		
	}


	// $return = json_encode($result);
}

function sendEmail($conn, $template_id, $lead_name, $lead_email, $counsellor_id, $last_insert_id)
{
	$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '" . $counsellor_id . "'");
	$counsellor_dets = mysqli_fetch_assoc($counsellor_query);

	$comm_query = $conn->query("SELECT Name,Email FROM Communication_Mail WHERE Institute_ID = '" . $counsellor_dets['Institute_ID'] . "' ");
	if($counsellor_dets['Institute_ID'] == '0'){
		$comm_query = $conn->query("SELECT Name,Email FROM Communication_Mail");
	}
	$comm = mysqli_fetch_assoc($comm_query);
	if($comm['Email'] && strpos($lead_email,'@')!== false){
	$body_query = $conn->query("SELECT * FROM Email_Templates WHERE ID = '" . $template_id . "'");
	$body_dets = mysqli_fetch_assoc($body_query);
	$subject = $body_dets['subject'];
	$body = $body_dets['email_template'];

	$body = str_replace('$lead_name', $lead_name, $body);
	$body = str_replace('$counsellor_name', $counsellor_dets['Name'], $body);
	$body = str_replace('$counsellor_contact_no', $counsellor_dets['Mobile'], $body);
	$body = str_replace('$counsellor_email', $counsellor_dets['Email'], $body);

	$mail = new \SendGrid\Mail\Mail();
	$mail->setFrom($comm['Email'], $comm['Name']);
	$mail->setSubject($subject);
	$mail->addTo(strval($lead_email));
	$mail->addContent('text/html', $body);
	$key_id = "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";
	$sendgrid = new \SendGrid($key_id);



	if ($sendgrid->send($mail)) {
		$lead_query = $conn->query("SELECT * FROM Leads WHERE ID = '" . $last_insert_id . "'");
		if ($lead_query->num_rows > 0) {
			$lead_dets = mysqli_fetch_assoc($lead_query);
			$create_log = $conn->query("INSERT INTO Email_Logs(Lead_ID, Employee_ID, Email_temp, Email_Send_By, Sender_Mail) VALUES ('" . $lead_dets['ID'] . "', '" . $counsellor_id . "', '" . mysqli_real_escape_string($conn, $body) . "', '" . $counsellor_dets['Name'] . "', '" . $counsellor_dets['Email'] . "')");
		}
		if($create_log){
			return 'mail';
		}
	} else {
		echo 'Mailer Error: ' . $mail->ErrorInfo;
		print_r(mysqli_error($conn));
	}
}
}

?>