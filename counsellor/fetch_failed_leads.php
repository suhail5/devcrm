<?php require 'filestobeincluded/db_config.php';?>
<?php

	if(session_status() === PHP_SESSION_NONE) session_start();
        
	$itemPerPage =25;
      $page=1;
      if(!empty($_GET['itemPerPage']))
      $itemPerPage =$_GET['itemPerPage'];
      if(!empty($_GET['page']))
        $page =$_GET['page'];

        $offset = (($page-1) * $itemPerPage);
        $flag = 1;

        $str = '';

        if($_GET['searchstring']!=''){
         $str=$_GET['searchstring'];
       }

       
      //echo $request->bookmark; die;
      
      

     if($str!='')
      {
      	
      	$result = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Leads.Stage_ID =7 AND Leads.Counsellor_ID = '". $_SESSION['USERS_ID'] ."' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%') ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
      	$totRecords = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Leads.Stage_ID =7 AND Leads.Counsellor_ID = '". $_SESSION['USERS_ID'] ."' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%') ORDER BY TimeStamp DESC")->num_rows;
      } 
      else{
      	
      	$result = $conn->query("SELECT * FROM Leads WHERE Stage_ID =7 AND Counsellor_ID = '". $_SESSION['USERS_ID'] ."' ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
      	$totRecords = $conn->query("SELECT * FROM Leads WHERE Stage_ID =7 AND Counsellor_ID = '". $_SESSION['USERS_ID'] ."' ORDER BY TimeStamp DESC")->num_rows;
      }

      
      
      
      //$totRecords=$jobs1->get()->count();
      
      //$jobs=$jobs1->offset($offset)->limit($itemPerPage)->orderby('jobs.id', 'desc')->get();
      $totalPages = ceil($totRecords/$itemPerPage);




 //$result = $conn->query("SELECT * FROM Leads ORDER BY Timestamp DESC LIMIT ".$startLimit.", ".$endLimit."");
      if($totalPages > 1) { ?>
          <div id="pagination" class="candidates-list" style="float:right;margin-bottom:10px;">
                      <span id="pagination_info" data-totalpages ="<?php echo $totalPages;?>"  data-currentpage ="<?php echo $page;?>" >
                          Page <?php echo $page ?> out of <?php echo $totalPages;?>  Pages
                      </span>
                      <a href="javascript:void(0)" data-page="<?php echo $page; ?>" id="pre" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" onClick="getList(`pre`)";>
                          Previous
                      </a>
                      <a href="javascript:void(0)" id="next" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" data-page ="<?php echo $page; ?>" onClick="getList(`next`)";>
                          Next
                      </a>
                </div><br><br>
        <?php }
 while($lead = $result->fetch_assoc())
 { ?>
 
 <tr>
											<td>
												<div class="row" style="padding-top: 10px;">
													<div class="col-lg-1">
														<div class="custom-control custom-checkbox">
														<input type="checkbox" onclick="checkbox_function()" class="custom-control-input checkbox-function" name="id[]" id="customCheck2<?php echo $lead['ID']; ?>" value="<?php echo $lead['ID']; ?>">
														<label class="custom-control-label" for="customCheck2<?php echo $lead['ID']; ?>"></label>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Name:</b> <?php echo $lead['Name']; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Email:</b> <?php echo substr($lead['Email'],0,15).'...'; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Mobile:</b> <a href="tel:<?php echo $lead['Mobile']; ?>"><?php echo $lead['Mobile']; ?></a></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																	<?php 
																	$get_followup_remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' ORDER BY ID DESC LIMIT 1");
																	if($get_followup_remark->num_rows > 0){
																		$re_mark = mysqli_fetch_assoc($get_followup_remark);
																		if(strlen($re_mark['Remark'])>40){
																			echo '<p><b>Remark:</b>&nbsp;' . substr($re_mark['Remark'],0,40).'...<button type="button" onclick="pop();" class="btn btn-link btn-sm" data-container="body" title=""
																			data-toggle="popover" data-placement="left"
																			data-content= "'.$re_mark['Remark'].'"
																			data-original-title="Remark">
																			Read More
																		</button>';
																		}else{
																			echo '<p><b>Remark:</b>&nbsp;' . $re_mark['Remark'];
																		}
																		
																	}else{
																		echo '';
																	}

																?>
																
																
																
																</p>
															</div>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
															<ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu mb-0">
																	<li class="nav-link" role="button" aria-haspopup="false" aria-expanded="false">
																	<?php $fsql = "SELECT COUNT(Lead_ID) as Leadid FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' GROUP BY Lead_ID"; $fresult = $conn->query($fsql); if ($fresult->num_rows > 0) { while($frow = $fresult->fetch_assoc()) { $gfc = $frow["Leadid"]; }} else { $gfc = "0";} ?>
																		<font style="font-size: 24px; cursor:pointer;" onclick="followupmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-tie"></i></font>
																		<span><mark class="mark1">&nbsp;<?php echo $gfc; ?>&nbsp;</mark></span>
																	</a></li>
																	<li class="nav-link" role="button" aria-haspopup="false"
																		aria-expanded="false">
																		<?php $elsql = "SELECT COUNT(Lead_ID) as Leadid FROM Email_Logs WHERE Lead_ID = '".$lead['ID']."' GROUP BY Lead_ID"; $elresult = $conn->query($elsql); if ($elresult->num_rows > 0) { while($elrow = $elresult->fetch_assoc()) { $gelc = $elrow["Leadid"]; }} else { $gelc = "0";} 
																			$slsql = "SELECT COUNT(Lead_ID) as Leadid FROM SMS_Logs WHERE Lead_ID = '".$lead['ID']."' GROUP BY Lead_ID"; $slresult = $conn->query($slsql); if ($slresult->num_rows > 0) { while($slrow = $slresult->fetch_assoc()) { $gslc = $slrow["Leadid"]; }} else { $gslc = "0";} 
																			$add_both = $gelc + $gslc;?>
																		<font style="font-size: 24px; cursor:pointer;" onclick="responsesmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-graduate"></i></font>
																		<span><mark class="mark2">&nbsp;<?php echo $add_both ?>&nbsp;</mark></span>
																	</a></li>
																	<li class="nav-link" role="button" aria-haspopup="false"
																		aria-expanded="false">
																		<?php $rsql = "SELECT COUNT(ID) as Leadid FROM Re_Enquired WHERE Name = '".$lead['Name']."' AND Email = '".$lead['Email']."' AND Mobile = '".$lead['Mobile']."' AND Institute_ID = '".$lead['Institute_ID']."'"; $rresult = $conn->query($rsql); if ($rresult->num_rows > 0) { while($rrow = $rresult->fetch_assoc()) { $grc = $rrow["Leadid"]; }} else { $grc = "0";} ?>
																		<font style="font-size: 24px; cursor:pointer;" onclick="re_enquiredmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-tie"></i></font>
																		<span><mark class="mark3">&nbsp;<?php echo $grc; ?>&nbsp;</mark></span>
																	</a></li>
																</ul>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">
																<?php
																	$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$lead['Counsellor_ID']."'");
																	$counsellor = mysqli_fetch_assoc($counsellor_query);
																	if($counsellor_query->num_rows > 0){
																		$couns = explode(' ',$counsellor['Name']);
																	}else{
																		$counsellor['Name'] = ' ';
																		$couns = $counsellor['Name'];
																	}
																?>
																<p><b>Counsellor:</b> <?php echo $couns[0]; ?></p>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>University:</b> <?php
																		$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$lead['Institute_ID']."'");
																		$univ = mysqli_fetch_assoc($univ_query);
																		if($univ_query->num_rows > 0){
																			echo $univ['Name'];
																		}else{
																			$course['Name'] = ' ';
																			echo $univ['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Course:</b> <?php
																		$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
																		$course = mysqli_fetch_assoc($course_query);
																		if($course_query->num_rows > 0){
																			echo $course['Name'];
																		}else{
																			$course['Name'] = ' ';
																			echo $course['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Specialization:</b> <?php
																		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
																		$specialization = mysqli_fetch_assoc($specialization_query);
																		if($specialization_query->num_rows > 0){
																			echo substr($specialization['Name'],0,27);
																		}else{
																			$specialization['Name'] = ' ';
																			echo $specialization['Name'];
																		}
																	?>
																</p>
															</div>

															<div class="col-lg-12 col-md-3 col-sm-12">
																
																<?php 
																	$get_followup_date = $conn->query("SELECT Followup_Timestamp FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' ORDER BY ID DESC LIMIT 1");
																	if($get_followup_date->num_rows > 0){
																		$current_timestamp = date('Y-m-d h:i:s');
																		$date = mysqli_fetch_assoc($get_followup_date);
																		if($current_timestamp < $date['Followup_Timestamp']){
																			echo '<p><b>Next Follow-up Date:</b>&nbsp;'. date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
																		}else{
																			echo '<p><b>Previous Follow-up Date:</b>&nbsp;'. date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
																		}
																		
																	}else{
																		echo '';
																	}

																?>
																</p>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
																	$stage = mysqli_fetch_assoc($stage_query);
																	if($stage_query->num_rows > 0){
																		$lead_stage = $stage['Name'];
																	}else{
																		$stage['Name'] = ' ';
																		$lead_stage = $stage['Name'];
																	}
																?>
																<p><b>Stage:</b> <?php echo $lead_stage; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
																	$reason = mysqli_fetch_assoc($reason_query);
																	if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
																		$badge = "success";
																	}
																	else if(strcasecmp($stage['Name'], "COLD")==0) {
																		$badge = "warning";
																	}
																	else {
																		$badge = "danger";
																	}
																?>
																<p><b>Reason:</b> <span class="badge badge-soft-<?php echo($badge); ?> py-1">
																		<?if($reason_query->num_rows > 0){
																			echo $reason['Name'];
																		}else{
																			$reason['Name'] = ' ';
																			echo $reason['Name'];
																		}?>
																	</span>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$lead['Subsource_ID']."'");
																	$subsource = mysqli_fetch_assoc($subsource_query);
																	if($subsource_query->num_rows > 0){
																		$lead_sub = $subsource['Name'];
																	}else{
																		$subsource['Name'] = ' ';
																		$lead_sub = $subsource['Name'];
																	}
																?>
																<p><b>Sub-Source:</b> <?php echo $lead_sub; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Creation Date: </b>
																<?php 
																	$get_creation_date = $conn->query("SELECT TimeStamp FROM History WHERE Lead_ID = '".$lead['ID']."' ORDER BY ID ASC LIMIT 1");
																	if($get_creation_date->num_rows > 0){
																		$date = mysqli_fetch_assoc($get_creation_date);
																		echo date("F j, Y g:i a", strtotime($date["TimeStamp"]));
																	}else{
																		echo date("F j, Y g:i a", strtotime($lead["TimeStamp"]));
																	}

																?>
																</p>
															</div>
														</div>
													</div>
													<div class="col-lg-1">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<div class="btn-group">
																	<!-- <span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" style="cursor: pointer;" onclick="whatsapp('<?php echo $lead['ID']; ?>')" aria-hidden="true"></i></p></span>&nbsp;&nbsp;
																	
																	<a href="tel:<?php echo $lead['Mobile']; ?>"><span data-toggle="tooltip" data-placement="top" data-original-title="Call" title=""><p style="font-size: 20px;"><i class="fa fa-phone" data-toggle="modal" style="cursor: pointer;" data-target="#leadcallmodal" aria-hidden="true"></i></p></span></a>&nbsp;&nbsp; -->
																	<span data-toggle="dropdown"><p style="font-size: 20px;"><i class="fa fa-ellipsis-v" style="cursor: pointer;" aria-hidden="true"></i></p></span>
																	<div class="dropdown-menu dropdown-menu-right">
																		<!-- <span class="dropdown-item"><i class="fas fa-notes-medical" style="font-size: 16px; color: #6C757D;"></i> <font class="addfollowupmodal" onclick="addfollowupmodal('<?php echo $lead['ID']; ?>')" style="cursor: pointer;">Add Followup</font></span>
																		<span class="dropdown-item"><i class="fas fa-user-edit" style="font-size: 16px; color: #6C757D;"></i> <font class="editlead" onclick="editlead('<?php echo $lead['ID']; ?>')" style="cursor: pointer;">Edit Lead</font></span>
																		<span class="dropdown-item"><i class="fas fa-history" style="font-size: 16px; color: #6C757D;"></i> <font class="leadhistory" onclick="leadhistory('<?php echo $lead['ID']; ?>')" style="cursor: pointer;">View History</font></span> -->
																		<span class="dropdown-item"><i class="fas fa-share-alt" style="font-size: 16px; color: #6C757D;"></i> <font class="referlead" onclick="referlead('<?php echo $lead['ID']; ?>')" style="cursor: pointer;">Refer Lead</font></span>
																		<span class="dropdown-item"><i class="fas fa-trash" style="font-size: 16px; color: #6C757D;"></i> <font class="deletelead" onclick="deletelead('<?php echo $lead['ID']; ?>')"  style="cursor: pointer;">Delete</font></span>
																	</div>
																</div>
															</div>
														</div>
														
														
													</div>
												</div>
											</td>
										</tr>
 
 <?php }
 if($totalPages > 1) { ?>
                    <div id="pagination" class="candidates-list" style="float:right;margin-bottom:10px;">
                      <span id="pagination_info" data-totalpages ="<?php echo $totalPages;?>"  data-currentpage ="<?php echo $page;?>" >
                          Page <?php echo $page ?> out of <?php echo $totalPages;?>  Pages
                      </span>
                      <a href="javascript:void(0)" data-page="<?php echo $page; ?>" id="pre" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" onClick="getList(`pre`)";>
                          Previous
                      </a>
                      <a href="javascript:void(0)" id="next" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" data-page ="<?php echo $page; ?>" onClick="getList(`next`)";>
                          Next
                      </a>
                </div>
                           <?php }
?>
