<?php
    require '../filestobeincluded/db_config.php';
    $all_institutes = array();
    $institute_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> 0");
    while ($row = $institute_query_res->fetch_assoc()) {
        $all_institutes[] = $row;
    }
?>

<form method="POST" action="">
    <div class="form-group row">
        <label class="col-lg-2 col-form-label"
            for="temp_name_email">Template Name</label>
        <div class="col-lg-4">
            <input type="text" class="form-control" id="temp_name_email"
                placeholder="Template Name" required>
        </div>
        <div class="col-lg-1"></div>
        <label class="col-lg-1 col-form-label"
            for="temp_subject_email">Subject</label>
        <div class="col-lg-4">
            <input type="text" class="form-control" id="temp_subject_email"
                placeholder="Subject" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label">Institute</label>
        <div class="col-lg-10">
            <select class="form-control custom-select" id="email_institute_id">
                <option disabled selected>Select Institute</option>
                <?
                foreach ($all_institutes as $institute) {
                    ?>
                    <option value="<?php echo $institute['ID'] ?>"><?php echo $institute['Name']; ?></option>
                    <?
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label">Choose Variable</label>
        <div class="col-lg-10">
            <select class="form-control custom-select" id="choose_var_email">
                <option value="">Choose</option>
                <option value="$lead_name">Lead Name</option>
                <option value="$counsellor_name">Counsellor Name</option>
                <option value="$counsellor_contact_no">Counsellor Contact No</option>
                <option value="$counsellor_email">Counsellor Email</option>
            </select>
        </div>
    </div>
    <br>
    <div id="emailEditor" style="border: 1px solid #e6e6e6; border-radius:5px;">
        <div class="controls">
            <textarea class="summernote input-block-level" id="email_content" required>

            </textarea>
        </div>
    </div>
    <br><br>
    
    <button class="btn btn-primary float-right" type="button" id="saveemailtemplate">Publish</button>
    
</form>
<script>
    $(document).ready(function() {
        $("#choose_var_email").change(function(){
            var choose_variab = $('#choose_var_email option:selected').val();
            $('#email_content').summernote('editor.saveRange');
            $('#email_content').summernote('editor.focus');
            $('#email_content').summernote('editor.insertText', choose_variab);
        })
    });
</script>

<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 330,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
    });
</script>


<script>
  $(document).ready(function () {
    $('#saveemailtemplate').click(function (e) {
      e.preventDefault();
      var temp_name_email = $('#temp_name_email').val();
      var temp_subject_email = $('#temp_subject_email').val();
      var email_institute_id = $('#email_institute_id').val();
      var email_content = $('#email_content').val();
      console.log(email_content);

      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_email/add_email.php",
          data: { "temp_name_email": temp_name_email, "temp_subject_email": temp_subject_email, "email_content":  email_content, "email_institute_id": email_institute_id},
          success: function (data) {
            $('#addemailtempmodal').modal('hide');
           
            if(data.match("true")) {
                $("#emailtable").load(location.href + " #emailtable");
                $('#addemailtempmodal form')[0].reset();
                $('#email_content').summernote('reset');
                
                toastr.success('Email Template added successfully');
                
            }
            else {
                toastr.error('Unable to add email template');
            }
          }
        });
    });
  });
</script>