<?php 

if(session_status() === PHP_SESSION_NONE) if(session_status() === PHP_SESSION_NONE) session_start();

require '../filestobeincluded/db_config.php';

$userid = $_POST['userid'];
 
$sql = "select * from Leads where ID=".$userid;
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);
?>
<div class="row">
    <div class="col-12">
        <form method="POST">
            <div class="form-group row">
                <div class="col-lg-12">
                    <input type="text" class="form-control" id="whatsapp_name"
                        value="<?php echo $row['Name']; ?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <select data-plugin="customselect" class="form-control" id="whatsapp_number">
                        <option disabled selected>Choose Number</option>
                        <option value="<?php echo $row['Mobile']; ?>">Primary - <?php echo $row['Mobile']; ?></option>
                        <option value="<?php echo $row['Alt_Mobile']; ?>">Alternate - <?php echo $row['Alt_Mobile']; ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <select data-plugin="customselect" class="form-control" id="whatsapp_template">
                        <option disabled selected>Choose Template</option>
                        <?php
                            $result_whatsapp_template = $conn->query("SELECT * FROM WhatsApp_Templates WHERE Institute_ID = '".$_SESSION['INSTITUTE_ID']."'");
                            while($wa_temp = $result_whatsapp_template->fetch_assoc()) {
                        ?>
                            <option value="<?php echo $wa_temp['ID']; ?>"><?php echo $wa_temp['wa_template_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                <textarea class="form-control" rows="7" id="whatsapp_textarea">

                </textarea>
                </div>
            </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="sendWhatsAppMessage();">Send Message</button>
</div>
</form>

<script>
    function sendWhatsAppMessage() {
        var whatsapp_number = $('#whatsapp_number').val();
        var whatsapp_message = $('#whatsapp_textarea').val();

        window.open("https://api.whatsapp.com/send?phone=91"+whatsapp_number+"&text="+encodeURIComponent(whatsapp_message), "_blank");
    }
</script>

<script>
    $(document).ready(function() {
        $('#whatsapp_template').change(function() {

            $('#whatsapp_textarea').val("Select Template");

            var template_id = $('#whatsapp_template').val();
            var usr_id = '<?php echo $userid; ?>';

            $.ajax
            ({
                type: "POST",
                url: "ajax_leads/ajax_whatsapp.php",
                data: { "whatsapp_template_id": template_id, "user_id": usr_id },
                success: function(data) {
                    if(data != "") {
                        $('#whatsapp_textarea').val(data);
                    }
                    else {
                        $('#whatsapp_textarea').val("Select Template");
                    }
                }
            });
        });
    });
</script>

<?php
exit;
?>