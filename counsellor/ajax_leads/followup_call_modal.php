<!----------------------->
<?php

if(session_status() === PHP_SESSION_NONE) if(session_status() === PHP_SESSION_NONE) session_start();

require '../filestobeincluded/db_config.php';
$follow_id = $_POST['userid'];
$get_follow = $conn->query("SELECT * FROM Follow_Ups WHERE ID = '".$follow_id."'");
$follow = mysqli_fetch_assoc($get_follow);

$get_lead = $conn->query("SELECT * FROM Leads WHERE ID = '".$follow['Lead_ID']."'");
$lead = mysqli_fetch_assoc($get_lead);
?>
            <div class="row">
            <div class="col-md-12 text-center">

            <script type="text/javascript">

                var lead_number = '<?php echo $_POST['number']; ?>';
                var ip_address = '<?php echo $_SESSION['IP_ADDRESS'] ?>';
                var agent_id = '<?php echo $_SESSION['useremployeeid'] ?>';

                var d = new Date();
                var datestring = d.getFullYear()  + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes();

                $.ajax({
                    type: "GET",
                    url: "/DialerAPI/start_manual_call.php",
                    dataType: 'xml',
                    data: {"agent_id": agent_id, "customernumber": lead_number, "host": ip_address},
                    success: function(data){
                       console.log(data);

                       var status = $(data).find('STATUS').first().text();
                       var status_desc = $(data).find('STATUSDESC').first().text();

                       if(status==="0") {
                        toastr.success('Putting call through...');
                       }
                       else {
                        toastr.error(status_desc);
                        $("#confirmleadcallmodal").modal('hide');
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            html: 'Connection with <b>Dialer</b> has been terminated. Please login again.',
                            footer: '<a href="/logout">Why do I have this issue?</a>',
                            confirmButtonText: `OK`
                        }).then((result) => {
                            if(result.isConfirmed) {
                                window.location.href = "/logout";
                            }
                        });
                       }
                   }
               });
            </script>

            <center>
                <h4>Calling <?php echo $lead['Name']; ?></h4>
                    <br><br><script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                <lottie-player src="https://assets2.lottiefiles.com/private_files/lf30_daep5h.json"  background="transparent"  speed="1"  style="width: 250px; height: 250px;"  loop  autoplay></lottie-player>
                <br><br>
                <style>
                    .ml12 {
                        font-weight: 400;
                        font-size: 1.2em;
                        text-transform: uppercase;
                        letter-spacing: 0.5em;
                    }

                    .ml12 .letter {
                        display: inline-block;
                        line-height: 1em;
                    }
                </style>
                <h6 style="font-size: 12px;" class="ml12">Please accept call in X-Lite...</h6>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
                <script>
                    // Wrap every letter in a span
                    var textWrapper = document.querySelector('.ml12');
                    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

                    anime.timeline({loop: true})
                    .add({
                        targets: '.ml12 .letter',
                        translateX: [40,0],
                        translateZ: 0,
                        opacity: [0,1],
                        easing: "easeOutExpo",
                        duration: 1200,
                        delay: (el, i) => 500 + 30 * i
                    }).add({
                        targets: '.ml12 .letter',
                        translateX: [0,-30],
                        opacity: [1,0],
                        easing: "easeInExpo",
                        duration: 1100,
                        delay: (el, i) => 100 + 30 * i
                    });
                </script>
                <style>
                    #buttonfooter {
                        position: fixed;
                        bottom: 0;
                        width: 100%;
                    }
                </style>
                <div class="col-lg-12" style="padding-top: 150px;">
                    <center><button class="btn btn-light" id="end_call_btn" style="height: 80px; width:80px; border-radius:100px;" onclick="end_call();"><img src="/assets/phone-call-end.svg" width="40px;"></button></center>
                </div>
            </center>
            </div>
            </div>

            <script type="text/javascript">

                function end_call() {
                    $("#end_call_btn").prop('disabled', true); 

                    var ip_address = '<?php echo $_SESSION['IP_ADDRESS']; ?>';
                    var agent_id = '<?php echo $_SESSION['useremployeeid']; ?>';

                    $.ajax({
                        type: "GET",
                        url: "/DialerAPI/hangupcall.php",
                        dataType: 'xml',
                        data: {"agent_id": agent_id, "host": ip_address},
                        success: function(data) {
                            console.log(data);

                            var status = $(data).find('STATUS').first().text();
                            var status_desc = $(data).find('STATUSDESC').first().text();

                            if(status==="0") {
                                toastr.success('Call Ended Successfully. Saving call details. Please wait..');

                                $.ajax({
                                    type: "GET",
                                    url: "/DialerAPI/end_call.php",
                                    dataType: 'xml',
                                    data: {"agentid": agent_id, "host": ip_address},
                                    success: function(data){
                                       console.log(data);

                                       var status = $(data).find('STATUS').first().text();
                                       var status_desc = $(data).find('STATUSDESC').first().text();
                                       var call_pick_time = $(data).find('C_AATIME').first().text();
                                       var call_end_time = $(data).find('C_AHTIME').first().text();
                                       var audio_link = $(data).find('C_CALLURL').first().text();

                                       if(status==="0") {
                                        toastr.success('Details saved successfully.');
                                        $("#confirmleadcallmodal").modal('hide');

                                        var lead_id = '<?php echo $lead['ID']; ?>';

                                        $.ajax({
                                            type: "POST",
                                            url: "/ajax_calls/add_call_log.php",
                                            data: {"lead_id": lead_id, "employee_id": agent_id, "call_pick_time": call_pick_time, "call_end_time": call_end_time, "audio_link": audio_link, "call_time": datestring}
                                        });

                                        addfollowupmodal('<?php echo $follow_id; ?>');
                                       }
                                       else {
                                        toastr.error(status_desc);
                                        if(status == '-404') {
                                            $("#end_call_btn").prop('disabled', false);
                                        }
                                        else {
                                            $("#end_call_btn").prop('disabled', false);
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                html: 'Connection with <b>Dialer</b> has been terminated. Please login again.',
                                                footer: '<a href="/logout">Why do I have this issue?</a>',
                                                confirmButtonText: `OK`
                                            }).then((result) => {
                                                if(result.isConfirmed) {
                                                    window.location.href = "/logout";
                                                }
                                            });
                                        }
                                       }
                                   }
                               });
                            }
                            else {
                                toastr.error(status_desc);
                                $("#end_call_btn").prop('disabled', false);
                            }
                        }
                    });
                }
            </script>