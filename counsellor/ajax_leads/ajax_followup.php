<?php

class MyDB extends SQLite3
{
	function __construct()
	{
		$this->open('../../drip.db');
	}
}

require '../filestobeincluded/db_config.php';
require_once '../../DialerAPI/vendor/autoload.php';
$counsellor_id = $_POST['counsellor_id'];
$lead_id = $_POST['lead_id'];
$followup_type = $_POST['followup_type'];
$new_stage = $_POST['new_stage'];
$new_reason = mysqli_real_escape_string($conn, $_POST['new_reason']);
$follow_date = $_POST['follow_date'];
$follow_time = $_POST['follow_time'];
$followup_remark = mysqli_real_escape_string($conn, $_POST['followup_remark']);
date_default_timezone_set('Asia/Kolkata');
$follow_time = date("G:i", strtotime($follow_time));
$followup_final_time = $follow_date.' '.$follow_time;
$followup_check = $_POST['followup_check'];

if(!$_SESSION['User_Type']){
		if($followup_check=='off'){
		$add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`,`Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID`, `dob`,`urnno`) SELECT * FROM Leads WHERE ID = '$lead_id'");
		if($add_history){
			$update_stage = $conn->query("UPDATE Leads SET Stage_ID = '".$new_stage."', Reason_ID = '".$new_reason."', Remarks = '".$followup_remark."', TimeStamp = now() WHERE ID = '".$lead_id."'");
			$client = new \GuzzleHttp\Client();
			$get_course = $conn->query("SELECT Leads.Name as fullname, Leads.Institute_ID as institute, Leads.Stage_ID as stage, Leads.Reason_ID as reason, Leads.Email as email, Leads.State_ID as state, Leads.Mobile as mobile, Leads.Alt_Mobile as alt_mobile, Leads.Counsellor_ID as counsellor,Courses.Name as course FROM Leads LEFT JOIN Courses ON Leads.Course_ID = Courses.ID WHERE Leads.ID = '$lead_id'");
			$gc = mysqli_fetch_assoc($get_course);
			$db = new MyDB();
			$insert = $db->exec("INSERT INTO drip_leads (`lead_ID`,`counsellor_ID`,`stage_ID`,`mobile`,`alt_mobile`,`email`,`name`,`institute_ID`,`course_name`,`reason_ID`,`State_ID`) VALUES ($lead_id,'" . $gc["counsellor"] . "','" . $gc["stage"] . "','" . $gc["mobile"] . "','" . $gc["alt_mobile"] . "','" . $gc["email"] . "','" . $gc["fullname"] . "','" . $gc["institute"] . "','" . $gc["course"] . "','" . $gc["reason"] . "','" . $gc["state"] . "')");
			if (!$insert) {
				echo $db->lastErrorMsg();
			}
			// $promise = $client->request('GET', 'https://crm.collegevidya.com/initiateDripMarketing.php?initiateDripMarketing=ok&Stage_ID='.$gc["stage"].'&Institute_ID='.$gc["institute"].'&Course_Name='.$gc["course"].'&Reason_ID='.$gc["reason"].'&State_ID='.$gc["state"].'&Lead_Name='.$gc["fullname"].'&Lead_Email='.$gc["email"].'&Lead_Primary_Number='.$gc["mobile"].'&Lead_Alt_Number='.$gc["alt_mobile"].'&Counsellor_ID='.$gc["counsellor"].'&LEAD_ID='.$lead_id.'');
			if($update_stage){
				include 'fetch_lead_data.php';
				echo '.';
			}else{
				echo '';
			}
		}else{
			echo '';
		}
	}else{
		$add_history_follow_up = $conn->query("INSERT INTO History_Follow_Ups ( `Followup_ID`, `Counsellor_ID`, `Lead_ID`, `Followup_Type`, `Followup_Timestamp`, `Remark`, `Status`, Follow_Up_Status,Last_Updated_Timestamp) SELECT * FROM Follow_Ups WHERE Lead_ID = '$lead_id'");
		if($add_history_follow_up){
			$add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`,`Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID`, `dob`,`urnno`) SELECT * FROM Leads WHERE ID = '$lead_id'");
		}
		if($add_history){
			if(strcasecmp($followup_final_time, ' 05:30')==0) {
				$update_lead = $conn->query("UPDATE Leads SET Stage_ID = '".$new_stage."', Reason_ID = '".$new_reason."', TimeStamp = now() WHERE ID = '".$lead_id."'");
				$client = new \GuzzleHttp\Client();
				$get_course = $conn->query("SELECT Leads.Name as fullname, Leads.Institute_ID as institute, Leads.Stage_ID as stage, Leads.Reason_ID as reason, Leads.Email as email, Leads.State_ID as state, Leads.Mobile as mobile, Leads.Alt_Mobile as alt_mobile, Leads.Counsellor_ID as counsellor,Courses.Name as course FROM Leads LEFT JOIN Courses ON Leads.Course_ID = Courses.ID WHERE Leads.ID = '$lead_id'");
				$gc = mysqli_fetch_assoc($get_course);
				$db = new MyDB();
				$insert = $db->exec("INSERT INTO drip_leads (`lead_ID`,`counsellor_ID`,`stage_ID`,`mobile`,`alt_mobile`,`email`,`name`,`institute_ID`,`course_name`,`reason_ID`,`State_ID`) VALUES ($lead_id,'" . $gc["counsellor"] . "','" . $gc["stage"] . "','" . $gc["mobile"] . "','" . $gc["alt_mobile"] . "','" . $gc["email"] . "','" . $gc["fullname"] . "','" . $gc["institute"] . "','" . $gc["course"] . "','" . $gc["reason"] . "','" . $gc["state"] . "')");
				if (!$insert) {
					echo $db->lastErrorMsg();
				}
				// $promise = $client->request('GET', 'https://crm.collegevidya.com/initiateDripMarketing.php?initiateDripMarketing=ok&Stage_ID='.$gc["stage"].'&Institute_ID='.$gc["institute"].'&Course_Name='.$gc["course"].'&Reason_ID='.$gc["reason"].'&State_ID='.$gc["state"].'&Lead_Name='.$gc["fullname"].'&Lead_Email='.$gc["email"].'&Lead_Primary_Number='.$gc["mobile"].'&Lead_Alt_Number='.$gc["alt_mobile"].'&Counsellor_ID='.$gc["counsellor"].'&LEAD_ID='.$lead_id.'');
				if($update_lead) {
					$add_followup = $conn->query("INSERT INTO Follow_Ups(Counsellor_ID, Lead_ID, Remark, Follow_Up_Status,Last_Updated_Timestamp) VALUES ('".$counsellor_id."', '".$lead_id."', '".$followup_remark."', 'NO')");
					if($add_followup) {
						include 'fetch_lead_data.php';
						echo ".";
					}
					else {
						echo mysqli_error($conn);
					}
				}
				else {
					echo mysqli_error($conn);
				}
			}
			else {
				$update_lead = $conn->query("UPDATE Leads SET Stage_ID = '".$new_stage."', Reason_ID = '".$new_reason."', TimeStamp = now() WHERE ID = '".$lead_id."'");
				$client = new \GuzzleHttp\Client();
				$get_course = $conn->query("SELECT Leads.Name as fullname, Leads.Institute_ID as institute, Leads.Stage_ID as stage, Leads.Reason_ID as reason, Leads.Email as email, Leads.State_ID as state, Leads.Mobile as mobile, Leads.Alt_Mobile as alt_mobile, Leads.Counsellor_ID as counsellor,Courses.Name as course FROM Leads LEFT JOIN Courses ON Leads.Course_ID = Courses.ID WHERE Leads.ID = '$lead_id'");
				$gc = mysqli_fetch_assoc($get_course);
				$db = new MyDB();
			$insert = $db->exec("INSERT INTO drip_leads (`lead_ID`,`counsellor_ID`,`stage_ID`,`mobile`,`alt_mobile`,`email`,`name`,`institute_ID`,`course_name`,`reason_ID`,`State_ID`) VALUES ($lead_id,'" . $gc["counsellor"] . "','" . $gc["stage"] . "','" . $gc["mobile"] . "','" . $gc["alt_mobile"] . "','" . $gc["email"] . "','" . $gc["fullname"] . "','" . $gc["institute"] . "','" . $gc["course"] . "','" . $gc["reason"] . "','" . $gc["state"] . "')");
			if (!$insert) {
				echo $db->lastErrorMsg();
			}
				// $promise = $client->request('GET', 'https://crm.collegevidya.com/initiateDripMarketing.php?initiateDripMarketing=ok&Stage_ID='.$gc["stage"].'&Institute_ID='.$gc["institute"].'&Course_Name='.$gc["course"].'&Reason_ID='.$gc["reason"].'&State_ID='.$gc["state"].'&Lead_Name='.$gc["fullname"].'&Lead_Email='.$gc["email"].'&Lead_Primary_Number='.$gc["mobile"].'&Lead_Alt_Number='.$gc["alt_mobile"].'&Counsellor_ID='.$gc["counsellor"].'&LEAD_ID='.$lead_id.'');
				if($update_lead) {
					$add_followup = $conn->query("INSERT INTO Follow_Ups(Counsellor_ID, Lead_ID, Followup_Type, Followup_Timestamp, Remark, Follow_Up_Status) VALUES ('".$counsellor_id."', '".$lead_id."', '".$followup_type."', '".$followup_final_time."', '".$followup_remark."', 'NO')");
					if($add_followup) {
						include 'fetch_lead_data.php';
						echo ".";
					}
					else {
						echo mysqli_error($conn);
					}
				}
				else {
					echo mysqli_error($conn);
				}
			}
		}
	}
}else{
	if($followup_check=='off'){
		$add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`,`Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID`, `dob`,`urnno`,`exp`) SELECT * FROM Leads WHERE ID = '$lead_id'");
		if($add_history){
			$update_stage = $conn->query("UPDATE Leads SET Stage_ID = '".$new_stage."', Reason_ID = '".$new_reason."', Remarks = '".$followup_remark."', TimeStamp = now() WHERE ID = '".$lead_id."'");
			$client = new \GuzzleHttp\Client();
			$get_course = $conn->query("SELECT Leads.Name as fullname, Leads.Institute_ID as institute, Leads.Stage_ID as stage, Leads.Reason_ID as reason, Leads.Email as email, Leads.State_ID as state, Leads.Mobile as mobile, Leads.Alt_Mobile as alt_mobile, Leads.Counsellor_ID as counsellor,Courses.Name as course FROM Leads LEFT JOIN Courses ON Leads.Course_ID = Courses.ID WHERE Leads.ID = '$lead_id'");
			$gc = mysqli_fetch_assoc($get_course);
			// $promise = $client->request('GET', 'https://crm.collegevidya.com/initiateDripMarketing.php?initiateDripMarketing=ok&Stage_ID='.$gc["stage"].'&Institute_ID='.$gc["institute"].'&Course_Name='.$gc["course"].'&Reason_ID='.$gc["reason"].'&State_ID='.$gc["state"].'&Lead_Name='.$gc["fullname"].'&Lead_Email='.$gc["email"].'&Lead_Primary_Number='.$gc["mobile"].'&Lead_Alt_Number='.$gc["alt_mobile"].'&Counsellor_ID='.$gc["counsellor"].'&LEAD_ID='.$lead_id.'');
			if($update_stage){
				include 'fetch_lead_data.php';
				echo '.';
			}else{
				echo '';
			}
		}else{
			echo '';
		}
	}else{
		$add_history_follow_up = $conn->query("INSERT INTO History_Follow_Ups ( `Followup_ID`, `Counsellor_ID`, `Lead_ID`, `Followup_Type`, `Followup_Timestamp`, `Remark`, `Status`, Follow_Up_Status) SELECT * FROM Follow_Ups WHERE Lead_ID = '$lead_id'");
		if($add_history_follow_up){
			$add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`,`Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID`, `dob`,`urnno`,`exp`) SELECT * FROM Leads WHERE ID = '$lead_id'");
		}
		if($add_history){
			if(strcasecmp($followup_final_time, ' 05:30')==0) {
				$update_lead = $conn->query("UPDATE Leads SET Stage_ID = '".$new_stage."', Reason_ID = '".$new_reason."', TimeStamp = now() WHERE ID = '".$lead_id."'");
				$client = new \GuzzleHttp\Client();
				$get_course = $conn->query("SELECT Leads.Name as fullname, Leads.Institute_ID as institute, Leads.Stage_ID as stage, Leads.Reason_ID as reason, Leads.Email as email, Leads.State_ID as state, Leads.Mobile as mobile, Leads.Alt_Mobile as alt_mobile, Leads.Counsellor_ID as counsellor,Courses.Name as course FROM Leads LEFT JOIN Courses ON Leads.Course_ID = Courses.ID WHERE Leads.ID = '$lead_id'");
				$gc = mysqli_fetch_assoc($get_course);
				// $promise = $client->request('GET', 'https://crm.collegevidya.com/initiateDripMarketing.php?initiateDripMarketing=ok&Stage_ID='.$gc["stage"].'&Institute_ID='.$gc["institute"].'&Course_Name='.$gc["course"].'&Reason_ID='.$gc["reason"].'&State_ID='.$gc["state"].'&Lead_Name='.$gc["fullname"].'&Lead_Email='.$gc["email"].'&Lead_Primary_Number='.$gc["mobile"].'&Lead_Alt_Number='.$gc["alt_mobile"].'&Counsellor_ID='.$gc["counsellor"].'&LEAD_ID='.$lead_id.'');
				if($update_lead) {
					$add_followup = $conn->query("INSERT INTO Follow_Ups(Counsellor_ID, Lead_ID, Remark, Follow_Up_Status) VALUES ('".$counsellor_id."', '".$lead_id."', '".$followup_remark."', 'NO')");
					if($add_followup) {
						include 'fetch_lead_data.php';
						echo ".";
					}
					else {
						echo mysqli_error($conn);
					}
				}
				else {
					echo mysqli_error($conn);
				}
			}
			else {
				$update_lead = $conn->query("UPDATE Leads SET Stage_ID = '".$new_stage."', Reason_ID = '".$new_reason."', TimeStamp = now() WHERE ID = '".$lead_id."'");
				$client = new \GuzzleHttp\Client();
				$get_course = $conn->query("SELECT Leads.Name as fullname, Leads.Institute_ID as institute, Leads.Stage_ID as stage, Leads.Reason_ID as reason, Leads.Email as email, Leads.State_ID as state, Leads.Mobile as mobile, Leads.Alt_Mobile as alt_mobile, Leads.Counsellor_ID as counsellor,Courses.Name as course FROM Leads LEFT JOIN Courses ON Leads.Course_ID = Courses.ID WHERE Leads.ID = '$lead_id'");
				$gc = mysqli_fetch_assoc($get_course);
				// $promise = $client->request('GET', 'https://crm.collegevidya.com/initiateDripMarketing.php?initiateDripMarketing=ok&Stage_ID='.$gc["stage"].'&Institute_ID='.$gc["institute"].'&Course_Name='.$gc["course"].'&Reason_ID='.$gc["reason"].'&State_ID='.$gc["state"].'&Lead_Name='.$gc["fullname"].'&Lead_Email='.$gc["email"].'&Lead_Primary_Number='.$gc["mobile"].'&Lead_Alt_Number='.$gc["alt_mobile"].'&Counsellor_ID='.$gc["counsellor"].'&LEAD_ID='.$lead_id.'');
				if($update_lead) {
					$add_followup = $conn->query("INSERT INTO Follow_Ups(Counsellor_ID, Lead_ID, Followup_Type, Followup_Timestamp, Remark, Follow_Up_Status) VALUES ('".$counsellor_id."', '".$lead_id."', '".$followup_type."', '".$followup_final_time."', '".$followup_remark."', 'NO')");
					if($add_followup) {
						include 'fetch_lead_data.php';
						echo ".";
					}
					else {
						echo mysqli_error($conn);
					}
				}
				else {
					echo mysqli_error($conn);
				}
			}
		}
	}
}
