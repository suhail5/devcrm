<?php 
if(session_status() === PHP_SESSION_NONE) session_start();
if(isset($_POST['id'])){
	$lead_id = $_POST['id'];
	require '../filestobeincluded/db_config.php';
}

$get_lead = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp FROM (SELECT * FROM (SELECT *, max(Followup_Timestamp) FROM Follow_Ups WHERE Follow_Ups.ID IN (SELECT MAX(ID) FROM Follow_Ups GROUP BY Lead_ID) AND  Counsellor_ID = '".$_SESSION['useremployeeid']."' GROUP BY Followup_Timestamp, Lead_ID ORDER BY Followup_Timestamp DESC) as Follow_Ups ORDER BY Followup_Timestamp DESC) AS Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Follow_Ups.ID IN (SELECT MAX(ID) FROM Follow_Ups GROUP BY Lead_ID) AND  Follow_Ups.Counsellor_ID = '".$_SESSION['useremployeeid']."'  AND (Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."') AND Leads.ID = '$lead_id' GROUP BY Lead_ID DESC");
while($lead = $get_lead->fetch_assoc())
{ ?>

   <div class="row" style="padding-top: 10px;" id="row<?php echo $lead['ID'] ?>">
      <div class="col-lg-1">
         <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input checkbox-function" name="id[]" id="customCheck2<?php echo $lead['ID']; ?>" onclick="checkbox_function()" value="<?php echo $lead['ID']; ?>">
            <label class="custom-control-label" for="customCheck2<?php echo $lead['ID']; ?>"></label>
         </div>
      </div>
      <div class="col-lg-2">
         <div class="row">
            <div class="col-lg-12 col-md-3 col-sm-12">
               <p id="person_name<?php echo $lead['ID'] ?>" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy name"><b>Name:</b> <?php echo $lead['Name']; ?></p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <p id="person_email<?php echo $lead['ID'] ?>" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy email"><b>Email:</b> <?php echo $lead['Email']; ?></p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <p id="person_mobile<?php echo $lead['ID'] ?>" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy mobile"><b>Mobile:</b> <a href="tel:<?php echo $lead['Mobile']; ?>"><?php echo $lead['Mobile']; ?></a></p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <?php
                  if(strlen($lead['rem'])>50){
                     $comment = substr($lead['rem'],0,50).'...</p>'.'<button type="button" class="btn btn-link btn-sm" onclick="pop();" data-container="body" title=""
                     data-toggle="popover" data-placement="left"
                     data-content="'.$lead['rem'].'"
                     data-original-title="Remark">
                     See More
                  </button>';
                  }else{
                     $comment = $lead['rem'].'</p>';
                  }
                  
                  ?>
               <p><b>Followup Comment:</b>&nbsp;&nbsp;<?php echo $comment ?>
            </div>
         </div>
      </div>
      <div class="col-lg-2">
         <div class="row">
            <div class="col-lg-12 col-md-3 col-sm-12">
               <ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu mb-0">
                               <li class="nav-link" role="button" aria-haspopup="false" aria-expanded="false">
                               <?php $fsql = "SELECT COUNT(Lead_ID) as Leadid FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' AND Counsellor_ID = '".$_SESSION['useremployeeid']."' GROUP BY Lead_ID"; $fresult = $conn->query($fsql); if ($fresult->num_rows > 0) { while($frow = $fresult->fetch_assoc()) { $gfc = $frow["Leadid"]; }} else { $gfc = "0";} ?>
                                 <font style="font-size: 24px; cursor:pointer;" onclick="followupmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-tie"></i></font>
                                 <span><mark class="mark1">&nbsp;<?php echo $gfc; ?>&nbsp;</mark></span>
                               </a></li>
                               <li class="nav-link" role="button" aria-haspopup="false"
                                 aria-expanded="false">
                                 <?php $elsql = "SELECT COUNT(Lead_ID) as Leadid FROM Email_Logs WHERE Lead_ID = '".$lead['ID']."' AND Employee_ID = '".$_SESSION['useremployeeid']."' GROUP BY Lead_ID"; $elresult = $conn->query($elsql); if ($elresult->num_rows > 0) { while($elrow = $elresult->fetch_assoc()) { $gelc = $elrow["Leadid"]; }} else { $gelc = "0";} 
                                   $slsql = "SELECT COUNT(Lead_ID) as Leadid FROM SMS_Logs WHERE Lead_ID = '".$lead['ID']."' AND Employee_ID = '".$_SESSION['useremployeeid']."' GROUP BY Lead_ID"; $slresult = $conn->query($slsql); if ($slresult->num_rows > 0) { while($slrow = $slresult->fetch_assoc()) { $gslc = $slrow["Leadid"]; }} else { $gslc = "0";} 
                                   $clsql = "SELECT COUNT(Lead_ID) as Leadid FROM Call_Logs WHERE Lead_ID = '".$lead['ID']."' AND Employee_ID = '".$_SESSION['useremployeeid']."' GROUP BY Lead_ID"; $clresult = $conn->query($clsql); if ($clresult->num_rows > 0) { while($clrow = $clresult->fetch_assoc()) { $gclc = $clrow["Leadid"]; }} else { $gclc = "0";}
                                   $add_both = $gelc + $gslc + $gclc;?>                                    <font style="font-size: 24px; cursor:pointer;" onclick="responsesmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-graduate"></i></font>
                                 <span><mark class="mark2">&nbsp;<?php echo $add_both ?>&nbsp;</mark></span>
                               </a></li>
                               <li class="nav-link" role="button" aria-haspopup="false"
                                 aria-expanded="false">
                                 <?php $rsql = "SELECT COUNT(ID) as Leadid FROM Re_Enquired WHERE Name = '".$lead['Name']."' AND Email = '".$lead['Email']."' AND Mobile = '".$lead['Mobile']."' AND Institute_ID = '".$lead['Institute_ID']."' AND Counsellor_ID = '".$_SESSION['useremployeeid']."'"; $rresult = $conn->query($rsql); if ($rresult->num_rows > 0) { while($rrow = $rresult->fetch_assoc()) { $grc = $rrow["Leadid"]; }} else { $grc = "0";} ?>
                                 <font style="font-size: 24px; cursor:pointer;" onclick="re_enquiredmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-tie"></i></font>
                                 <span><mark class="mark3">&nbsp;<?php echo $grc; ?>&nbsp;</mark></span>
                               </a></li>
                             </ul>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">
               <?php
          
                  $counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$lead['couns']."'");
                  $counsellor = mysqli_fetch_assoc($counsellor_query);
                  if($counsellor_query->num_rows > 0){
                     $couns = explode(' ',$counsellor['Name']);
                  }else{
                     $counsellor['Name'] = ' ';
                     $couns = $counsellor['Name'];
                  }
                  ?>
               <p><b>Counsellor:</b> <?php echo $couns[0]; ?></p>
            </div>
         </div>
      </div>
      <div class="col-lg-3">
         <div class="row">
            <div class="col-lg-12 col-md-3 col-sm-12">
               <p><b>University:</b> <?php
                  $univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$lead['Institute_ID']."'");
                  $univ = mysqli_fetch_assoc($univ_query);
                  if($univ_query->num_rows > 0){
                     echo $univ['Name'];
                  }else{
                     $course['Name'] = ' ';
                     echo $univ['Name'];
                  }
                  ?>
               </p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <p><b>Course:</b> <?php
                  $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
                  $course = mysqli_fetch_assoc($course_query);
                  if($course_query->num_rows > 0){
                     echo $course['Name'];
                  }else{
                     $course['Name'] = ' ';
                     echo $course['Name'];
                  }
                  ?>
               </p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <p><b>Specialization:</b> <?php
                  $specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
                  $specialization = mysqli_fetch_assoc($specialization_query);
                  if($specialization_query->num_rows > 0){
                     echo substr($specialization['Name'],0,27);
                  }else{
                     $specialization['Name'] = ' ';
                     echo $specialization['Name'];
                  }
                  ?>
               </p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <p><b>Follow-up Date:</b> <?php echo date("F j, Y g:i a", strtotime($lead["Followup_Timestamp"])); ?></p>
            </div>
         </div>
      </div>
      <div class="col-lg-3">
         <div class="row">
            <div class="col-lg-12 col-md-3 col-sm-12">
               <?php
                  $stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
                  $stage = mysqli_fetch_assoc($stage_query);
                  if($stage_query->num_rows > 0){
                     $lead_stage = $stage['Name'];
                  }else{
                     $stage['Name'] = ' ';
                     $lead_stage = $stage['Name'];
                  }
                  ?>
               <p><b>Stage:</b> <?php echo $lead_stage; ?></p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <?php
                  $reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
                  $reason = mysqli_fetch_assoc($reason_query);
                  if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
                     $badge = "success";
                  }
                  else if(strcasecmp($stage['Name'], "COLD")==0) {
                     $badge = "warning";
                  }
                  else {
                     $badge = "danger";
                  }
                  ?>
               <p><b>Reason:</b> <span class="badge badge-soft-<?php echo($badge); ?> py-1">
                  <?if($reason_query->num_rows > 0){
                     echo $reason['Name'];
                     }else{
                     $reason['Name'] = ' ';
                     echo $reason['Name'];
                     }?>
                  </span>
               </p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <?php
                  $source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$lead['Source_ID']."'");
                  $source = mysqli_fetch_assoc($source_query);
                  if($source_query->num_rows > 0){
                     $lead_source = $source['Name'];
                  }else{
                     $source['Name'] = ' ';
                     $lead_source = $source['Name'];
                  }
                  ?>
               <p><b>Source:</b> <?php echo $lead_source; ?></p>
            </div>
            <div class="col-lg-12 col-md-3 col-sm-12">
               <?php
                  $subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$lead['Subsource_ID']."'");
                  $subsource = mysqli_fetch_assoc($subsource_query);
                  if($subsource_query->num_rows > 0){
                     $lead_sub = $subsource['Name'];
                  }else{
                     $subsource['Name'] = ' ';
                     $lead_sub = $subsource['Name'];
                  }
                  ?>
               <p><b>Sub-Source:</b> <?php echo $lead_sub; ?></p>
            </div>
         </div>
      </div>
      <div class="col-lg-1">
         <div class="row">
            <div class="col-lg-12 col-md-3 col-sm-12">
               <div class="btn-group">
                  <span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title="">
                     <p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" onclick="whatsapp('<?php echo $lead['ID']; ?>')" style="cursor: pointer;" aria-hidden="true"></i></p>
                  </span>


                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <?php
                   if($lead['Mobile'] == '' && $lead['Alt_Mobile'] == '') {
                     ?>
                     <span data-container="body" title="" data-toggle="popover" data-placement="bottom" onclick="toastr.error('No valid number to call');" data-content="" data-original-title=""><p style="font-size: 20px;"><i class="fa fa-phone" style="cursor: pointer;" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;
                     <?php
                   }
                   else {
                     if($lead['Mobile'] == '' && $lead['Alt_Mobile'] != '') {
                       ?>
                     <span data-container="body" title="" data-toggle="popover" data-placement="bottom" onclick="callLeads('<?php echo $lead['follow_id'] ?>', '<?php echo $lead['Alt_Mobile']?>');" data-content="" data-original-title=""><p style="font-size: 20px;"><i class="fa fa-phone" style="cursor: pointer;" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;
                     <?php
                     }
                     if($lead['Mobile'] != '' && $lead['Alt_Mobile'] == '') {
                       ?>
                     <span data-container="body" title="" data-toggle="popover" data-placement="bottom" onclick="callLeads('<?php echo $lead['follow_id'] ?>', '<?php echo $lead['Mobile']?>');" data-content="" data-original-title=""><p style="font-size: 20px;"><i class="fa fa-phone" style="cursor: pointer;" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;
                     <?php
                     }
                     if($lead['Mobile'] != '' && $lead['Alt_Mobile'] != '') {
                       ?>

                       <span data-container="body" title="" data-toggle="popover" data-placement="bottom" onclick="askNumber('<?php echo $lead['follow_id'] ?>', '<?php echo $lead['Mobile']?>', '<?php echo $lead['Alt_Mobile']?>');" data-content="" data-original-title=""><p style="font-size: 20px;"><i class="fa fa-phone" style="cursor: pointer;" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;

                       <?php
                     }
                   }
                   ?>
                  &nbsp;&nbsp;&nbsp;&nbsp;


                  <span data-toggle="dropdown">
                     <p style="font-size: 20px;"><i class="fa fa-ellipsis-v" style="cursor: pointer;" aria-hidden="true"></i></p>
                  </span>
                  <div class="dropdown-menu dropdown-menu-right">
                     <span class="dropdown-item"><i class="fa fa-user-plus"></i> <font class="addfollowupmodal" onclick="addfollowupmodal('<?php echo $lead['follow_id']; ?>')" style="cursor: pointer;">Schedule Next Followup</font></span>                                                                                                       
                     <span class="dropdown-item"><i class="fa fa-history"></i> <font class="leadhistory" onclick="leadhistory('<?php echo $lead['ID']; ?>')" style="cursor: pointer;">View History</font></span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

<?php }



?>