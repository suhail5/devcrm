<?php 
require '../filestobeincluded/db_config.php';

$userid = $_POST['userid'];
 
$sql = "select * from Leads where ID=".$userid;
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);
?>
<form method="POST">
    <div class="row col-lg-12">
        <p>Refer Lead <b><?php echo $row['Name'];?></b> to</p>
    </div><br>
<div class="form-group row">
    <input type="hidden" value="<?php echo $row['ID']; ?>" id="lead_id">
    <input type="hidden" value="<?php echo $row['Counsellor_ID']; ?>" id="lead_owner">
    <label class="col-lg-2 col-form-label">Select Lead Owner</label>
    <div class="col-lg-10">
        <select data-plugin="customselect" class="form-control" id="refer_lead_owner">
            <option disabled selected>Choose</option>
            <?php
                $result_refer_lead = $conn->query("SELECT * FROM users");
                while($refer_leads = $result_refer_lead->fetch_assoc()) {
            ?>
                <option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Add Comment</label>
    <div class="col-lg-10">
        <input type="text" class="form-control" id="referal_comment"
            placeholder="">
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="updateOwner();">&nbsp;&nbsp;Refer&nbsp;&nbsp;</button>
</div>
</form>

<?php
exit;
?>