<?php 

function session_error_function() {
    echo '<script language="javascript">';
    echo 'location.href="/";';
    echo '</script>';
  }
  
  set_error_handler('session_error_function');
  if(session_status() === PHP_SESSION_NONE) session_start();

require '../filestobeincluded/db_config.php';
$all_leads=array();
$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID = '". $_SESSION['USERS_ID'] ."' ORDER BY TimeStamp DESC");
while($row = $leads_query_res->fetch_assoc()) {
    $all_leads[] = $row;
}
?>
<form method="POST">

<?php
    foreach($all_leads as $sms_leads){
?>
<input type="hidden" name="mobile[]" value="<?php echo $sms_leads['Mobile']; ?>" />
<?php
    }
?>

<div class="form-group row">
    <div class="col-lg-12">
        <select data-plugin="customselect" class="form-control" id="sms_template">
            <option disabled selected>Choose Template</option>
            <?php
                $result_SMS_template = $conn->query("SELECT * FROM SMS_Templates WHERE Institute_ID = '".$_SESSION['INSTITUTE_ID']."'");
                while($sms_temp = $result_SMS_template->fetch_assoc()) {
            ?>
                <option value="<?php echo $sms_temp['id']; ?>"><?php echo $sms_temp['sms_template_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12">
        <textarea class="input-block-level form-control" rows="10" id="sms_textarea">
                
        </textarea>
    </div>
</div>
    

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">&nbsp;&nbsp;Send&nbsp;&nbsp;</button>
</div>
</form>

<script>
function sendSelectedSMS() {
    
    var sms_temp_body = $('#sms_textarea_selected').val();
    var mobile = '<?php foreach($all_leads as $r_leads){ echo $r_leads['Mobile'].','; } ?>';
    $.ajax
        ({
            type: "POST",
            url: "Twilio/ajax_selected_sms.php",
            data: { "sms_temp_body" :sms_temp_body, "mobile" :mobile },
            success: function (data) {
            console.log(data);
            if(data.match("true")) {
                toastr.success('SMS send successfully');
                location.reload();
            }
            else {
                toastr.error('Unable to send SMS');
            }
            }
        });
    
}
</script>

<script>
    $(document).ready(function() {
        $('#sms_template').change(function() {

            $('#sms_textarea').val("Select Template");
            var template_id = $('#sms_template').val();

            $.ajax
            ({
                type: "POST",
                url: "ajax_leads/ajax_sms.php",
                data: { "sms_template_id": template_id },
                success: function(data) {
                    if(data != "") {
                        $('#sms_textarea').val(data);
                    }
                    else {
                        $('#sms_textarea').val("Select Template");
                    }
                }
            });
        });
    });
</script>

<?php
exit;
?>