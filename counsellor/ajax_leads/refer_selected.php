<?php 
require '../filestobeincluded/db_config.php';
$selected_leads = $_POST['data_id'];

$selected_leads = str_replace("%5B", "", $selected_leads);
$selected_leads = str_replace("%5D", "", $selected_leads);
$selected_leads = str_replace("id=", "", $selected_leads);
$selected_leads = str_replace("&", ",", $selected_leads);

?>
<form method="POST" id="refer_new_counsellor_form">

<div class="form-group row">
    <label class="col-lg-2 col-form-label">Select Counsellor</label>
    <div class="col-lg-10">
        <select data-plugin="customselect" class="form-control" id="refer_lead_owner" name="refer_lead_owner">
            <option disabled selected>Choose</option>
            <?php
                $result_refer_lead = $conn->query("SELECT * FROM users");
                while($refer_leads = $result_refer_lead->fetch_assoc()) {
            ?>
                <option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Add Comment</label>
    <div class="col-lg-10">
        <input type="text" class="form-control" id="referal_comment" placeholder="">
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="referSelectedLeads()" >&nbsp;&nbsp;Refer&nbsp;&nbsp;</button>
</div>
</form>

<script>
    function referSelectedLeads() {
        if($('#refer_new_counsellor_form').valid()) {
            var new_counsellor = $('#refer_lead_owner').val();
            var all_selected_leads = '<?php echo $selected_leads ?>';
            var comments = $('#referal_comment').val();

            $.ajax
                ({
                  type: "POST",
                  url: "ajax_leads/ajax_refer_selected.php",
                  data: { "new_counsellor": new_counsellor, "all_selected_leads": all_selected_leads, "comments": comments },
                  success: function (data) {
                    console.log(data);
                    if(data.match("true")) {
                        toastr.success('Selected leads referred successfully');
                        location.reload();
                    }
                    else {
                        toastr.error('Unable to refer leads');
                    }
                  }
                });
            }
        }
</script>

<script>
    $(document).ready(function() {
        $('#refer_new_counsellor_form').validate({
            rules: {
                refer_lead_owner: {
                    required: true
                }
            },
            messages: {
                refer_lead_owner: {
                    required: 'Please select a counsellor'
                }
            },
            highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
            }
        });
    })    
</script>

<?php
exit;
?>