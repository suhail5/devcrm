<script>
    function doCapture() {
        html2canvas(document.getElementById("body"), {
            width: 2080,
            height: 1980
        }).then(function(canvas) {

            // Get the image data as JPEG and 0.9 quality (0.0 - 1.0)
            var a = canvas.toDataURL("image/jpeg", 0.8);
            $.ajax({
                type: 'post',
                url: '/ss.php',
                data: {
                    image: a
                },
                success: function() {}
            })
        });
    }
</script>

<script>
    var login_time = localStorage.getItem('login');
    var x = setInterval(() => {
        var now = new Date().getTime();
        var diff = now - login_time;
        if (diff > 1800000) {
            doCapture();
            localStorage.setItem('login', now);
            login_time = localStorage.getItem('login');
            var now = new Date().getTime();
        }
    }, 1000);
</script>
<script>
    var id; // global variable  
    var phone;
    var i = 0;
    $(document).ready(function() {
        function incoming() {
            $.ajax({
                type: 'post',
                url: '/tata_api/live_call.php',
                success: function(res) {
                    var data = $.parseJSON(res);
                    if (data.length != 0 && data[0]['type'] === "Extension") {
                        phone = data[0]['source'];
                        if (phone.charAt(0) === '0') {
                            phone = phone.slice(1);
                        }
                        if (i < 2) {
                            i = i+1;
                            $.ajax({
                                url: '/check.php',
                                data: {
                                    phone: phone
                                },
                                type: 'post',
                                success: function(res1) {
                                    if (res1.match("new lead")) {
                                        $('#livecall').fadeIn('fast');
                                        $('#live_phone').text(phone);
                                        $('#live_name').text('Ask the Name');


                                        localStorage.setItem('addmodal', 'Y');
                                    } else {
                                        // open call popup with response information
                                        var data = $.parseJSON(res1);
                                        $('#livecall').fadeIn('fast');
                                        $('#live_phone').text(data.Mobile);
                                        $('#live_name').text(data.Name);
                                        $('#live_univ').text(data.Institute_Name);
                                        $('#live_course').text(data.Course_Name);
                                        $('#live_stage').text(data.Stage_Name);
                                        $('#live_spec').text(data.Specialization_Name);
                                        $('#live_remark').text(data.Remark_Name);
                                        $('#live_alt_mobile').text(data.Alt_Mobile);


                                        if (data.urnno) {
                                            $('#live_urnno').text(data.urnno);
                                            $('#live_urnno').show();
                                        }
                                        // todo open on hangup
                                        id = data.ID;
                                        localStorage.setItem('followupmodal', 'Y');
                                    }

                                }
                            });
                        }

                    } else if (data.length == 0) {
                        i=0;
                        setTimeout(function() {
                            $('#livecall').fadeOut('slow');
                        }, 2000);
                        if (localStorage.getItem('followupmodal') == 'Y') {
                            localStorage.setItem('followupmodal', 'N');
                            addFollowUp_ajax(id);
                        }

                        if (localStorage.getItem('addmodal') == 'Y') {
                            localStorage.setItem('addmodal', 'N');

                            $('#addquicklead').modal('show');
                            $('#quick_lead_mobile').val(phone);

                        }
                    }
                }
            })
        }

        setInterval(() => incoming(), 5000);
    });

</script>
</body>

</html>