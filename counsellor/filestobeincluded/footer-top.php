<!-- Footer Start -->

<?php
$counsellor_id = $_SESSION['useremployeeid'];
?>

<style type="text/css">

.float{
    position:fixed;
    width:60px;
    height:60px;
    bottom:40px;
    left:40px;
    background-color:#ff5c75;
    color:#FFF;
    border-radius:50px;
    text-align:center;
    box-shadow: 2px 2px 3px #999;
    z-index: 1000;
}

.float:hover {
    color: #FFF;
}

.my-float{
    margin-top: 19px;
    color: white;
}

.chat-popup{
    display: none;
    position: fixed;
    bottom: 80px;
    left: 102px;
    z-index: 1000;
    height: 429px;
    width: 300px;
    background-color: white;
    /* display: flex; */
    flex-direction: column;
    justify-content: space-between;
    padding: 0.75rem;
    box-shadow: 2px 2px 3px #999;
    border-radius: 10px;
}

.show{
    display: flex;
}

.chat-area{
    height: 80%;
    overflow-y: auto;
    overflow-x: hidden;
}

@media (max-width:500px){

    .chat-popup{
        bottom: 120px;
        right:10%;
        width: 80vw;
    }
}


</style>

<a href="#" class="float" id="float">
    <i id="call-icon" data-feather="phone-off" class="icon-dual my-float"></i>
</a>

<div class="chat-popup">
    <div class="modal-content" style="border: none;">
        <div class="modal-header">
            <div class="row">
                <h3 id="counsellor_name" class="modal-title" style="margin-left: 6px;">Loading...</h3>&nbsp;&nbsp;
                <span id="counsellor_status" class="badge badge-soft-danger" style="height: 24px; margin-top: 6px; padding: 5px 10px 0px 10px; font-size: 100%;">Disconnected</span>  
            </div>
        </div>
        <div class="modal-body">
            <p><b>Total Login Duration: </b><span id="total_login_time">Loading...</span></p>
            <p><b>Total Break Duration: </b><span id="total_break_time">Loading...</span></p>
            <p><b>Total Idle Duration: </b><span id="total_idle_time">Loading...</span></p>
            <p><b>Total Calls: </b><span id="total_calls">Loading...</span></p>
            <p><b>Total Talk Time: </b><span id="total_talk_time">Loading...</span></p>
            <p><b>Total Wrapup Time: </b><span id="total_wrapup_time">Loading...</span></p>
        </div>
        <div class="modal-footer" style="padding-left: 0px; padding-right: 27px;">
            <p style="margin: 0px;"><b>Updated On: </b><span id="update_time">Never</span></p>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                2020 &copy; Black Board. All Rights Reserved.
            </div>
        </div>
    </div>
</footer>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

    <script type="text/javascript">

        // var checkDialer = setInterval(getDialerUpdates, 3000);

        function getDialerUpdates() {

            var counsellor_id = '<?php echo $counsellor_id; ?>';

            $.ajax
            ({
                type: "POST",
                url: "/DialerAPI/getMISData.php",
                data: {"counsellor_id": counsellor_id},
                dataType: "json",
                beforeSend: function(){$('#divLoader').css("display", "none");},
                success: function(data) {
                    var userData = data.data[0];
                    var today  = new Date();
                    var finalTime = today.toLocaleString();

                    document.getElementById("counsellor_name").innerHTML = userData.Agent_ID;

                    document.getElementById("total_login_time").innerHTML = userData.Login_Time;
                    document.getElementById("total_break_time").innerHTML = userData.Total_Break_Duration;
                    document.getElementById("total_idle_time").innerHTML = userData.Total_idle_Time;
                    document.getElementById("total_calls").innerHTML = userData.Total_Calls;
                    document.getElementById("total_talk_time").innerHTML = userData.Talk_Time;
                    document.getElementById("total_wrapup_time").innerHTML = userData.Wrap_Time;

                    document.getElementById("update_time").innerHTML = finalTime;

                    $.ajax
                    ({
                        type: "POST",
                        url: "/DialerAPI/getAgentStatus.php",
                        beforeSend: function(){$('#divLoader').css("display", "none");},
                        success: function(response) {
                            if(response.match("STATUS NOT FOUND")) {
                                document.getElementById("counsellor_status").innerHTML = "Disconnected";
                                document.getElementById("counsellor_status").className = "badge badge-soft-danger";

                                document.getElementById("float").style.backgroundColor = "#ff5c75";
                                $('#call-icon').html('<i id="call-icon" data-feather="phone-off" class="icon-dual my-float"></i>');
                                feather.replace();
                            }
                            else if(response.split("~")[0].match("IDLE")) {
                                document.getElementById("counsellor_status").innerHTML = "Idle";
                                document.getElementById("counsellor_status").className = "badge badge-soft-warning";

                                document.getElementById("float").style.backgroundColor = "#ffbe0b";
                                $('#call-icon').html('<i id="call-icon" data-feather="phone" class="icon-dual my-float"></i>');
                                feather.replace();
                            }
                            else if(response.split("~")[0].match("RINGING")) {
                                document.getElementById("counsellor_status").innerHTML = "Ringing";
                                document.getElementById("counsellor_status").className = "badge badge-soft-primary";

                                document.getElementById("float").style.backgroundColor = "#5A45FF";
                                $('#call-icon').html('<i id="call-icon" data-feather="phone-call" class="icon-dual my-float"></i>');
                                feather.replace();
                            }
                            else if(response.split("~")[0].match("WRAPUP")) {
                                document.getElementById("counsellor_status").innerHTML = "Wrapup";
                                document.getElementById("counsellor_status").className = "badge badge-soft-info";

                                document.getElementById("float").style.backgroundColor = "#5369F8";
                                $('#call-icon').html('<i id="call-icon" data-feather="phone-call" class="icon-dual my-float"></i>');
                                feather.replace();
                            }
                            else if(response.split("~")[0].match("INCALL")) {
                                document.getElementById("counsellor_status").innerHTML = "In-Call";
                                document.getElementById("counsellor_status").className = "badge badge-soft-success";

                                document.getElementById("float").style.backgroundColor = "#32CD32";
                                $('#call-icon').html('<i id="call-icon" data-feather="phone-call" class="icon-dual my-float"></i>');
                                feather.replace();
                            }
                        }
                    });
                }
            });
        }
    </script>

    <script type="text/javascript">
        const popup = document.querySelector('.chat-popup');
        const chatBtn = document.querySelector('.float');

        chatBtn.addEventListener('click', ()=>{
            popup.classList.toggle('show');
        });
    </script>

    <!-- Vendor js -->
    <script src="assets/js/vendor.min.js"></script>

    </div>
    <!-- END wrapper -->
	<!-- Plugins Js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.9.0/jquery.validate.min.js" integrity="sha512-FyKT5fVLnePWZFq8zELdcGwSjpMrRZuYmF+7YdKxVREKomnwN0KTUG8/udaVDdYFv7fTMEc+opLqHQRqBGs8+w==" crossorigin="anonymous"></script>
        <script src="assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
        <script src="assets/libs/select2/select2.min.js"></script>
        <script src="assets/libs/multiselect/jquery.multi-select.js"></script>
        <script src="assets/libs/flatpickr/flatpickr.min.js"></script>
        <script src="assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
        <script src="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
        
        <!-- Init js-->
        <script src="assets/js/pages/form-advanced.init.js"></script>


    <!-- datatable js -->
    <script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
        <script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
        
        <script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
        <script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
        <script src="assets/libs/datatables/buttons.html5.min.js"></script>
        <script src="assets/libs/datatables/buttons.flash.min.js"></script>
        <script src="assets/libs/datatables/buttons.print.min.js"></script>

        <script src="assets/libs/datatables/dataTables.keyTable.min.js"></script>
        <script src="assets/libs/datatables/dataTables.select.min.js"></script>

        <!-- Datatables init -->
        <script src="assets/js/pages/datatables.init.js"></script>

       
	<script src="assets/libs/moment/moment.min.js"></script>
        <script src="assets/libs/apexcharts/apexcharts.min.js"></script>
        <script src="assets/libs/flatpickr/flatpickr.min.js"></script>
        
        <script src="assets/libs/smartwizard/jquery.smartWizard.min.js"></script>

        <script src="assets/js/pages/form-wizard.init.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <!-- page js -->
        

    <!-- App js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/toastr.min.js"></script>


    <script>
        toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
    </script>

<?php
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if($actual_link=='https://crm.collegevidya.com/teamlead/myfollowup'){
        $table_id = '#followup_table';
    }else if($actual_link=='https://crm.collegevidya.com/counsellor/myfollowup'){
        $table_id = '#followup_table';
    }else if($actual_link=='https://crm.collegevidya.com/myfollowup'){
        $table_id = '#followup_table';
    }else{
        $table_id = '#datatable-buttons';
    }  
    
    
?>
<script>
    $(document).ready(function() {
        var dataTable = $('<?php echo $table_id; ?>').dataTable(
            
        );
        $("#searchbox").keyup(function() {
            dataTable.fnFilter(this.value);
        });
            
    });
</script>

<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.summernote').summernote({
                height: 330,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false                 // set focus to editable area after initializing summernote
            });
        });
    </script>

