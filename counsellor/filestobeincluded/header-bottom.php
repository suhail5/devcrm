<?php require "db_config.php" ?>

<?php

if(session_status() === PHP_SESSION_NONE) session_start();

if ($_SESSION['User_Type'] == 'b2b') {
    $stages_query_res = $conn->query("SELECT * FROM Stages WHERE User_Type = 'b2b' or ID='1' ORDER BY Name ASC ");
} else {
    $stages_query_res = $conn->query("SELECT * FROM Stages WHERE User_Type is NULL ORDER BY Name ASC ");
}

$all_sources = array();

$sources_query_res = $conn->query("SELECT * FROM Sources");
while ($row = $sources_query_res->fetch_assoc()) {
    $all_sources[] = $row;
}

$all_stages = array();


while ($row = $stages_query_res->fetch_assoc()) {
    $all_stages[] = $row;
}

$all_users = array();

$users_query_res = $conn->query("SELECT * FROM users  WHERE ID = '" . $_SESSION['useremployeeid'] . "'");
while ($row = $users_query_res->fetch_assoc()) {
    $all_users[] = $row;
}

$all_institutes = array();

$institutes_query_res = $conn->query("SELECT * FROM Institutes WHERE ID = '" . $_SESSION['INSTITUTE_ID'] . "'");
while ($row = $institutes_query_res->fetch_assoc()) {
    $all_institutes[] = $row;
}

$all_notifications = array();

date_default_timezone_set('Asia/Kolkata');
$current_timestamp = date('Y-m-d H:i:s');

$notifications_query = $conn->query("SELECT * FROM Refer_Notifications WHERE User_ID = '" . $_SESSION['useremployeeid'] . "' ORDER BY Date DESC LIMIT 20");
while ($row = $notifications_query->fetch_assoc()) {
    $all_notifications[] = $row;
}

$all_upcoming_followups = array();

$followup_query = $conn->query("SELECT * FROM Follow_Ups WHERE Counsellor_ID = '" . $_SESSION['useremployeeid'] . "' AND '" . $current_timestamp . "' < Followup_Timestamp AND Followup_Type <> '0' ORDER BY Followup_Timestamp ASC");
while ($row = $followup_query->fetch_assoc()) {
    $get_lead_dets = $conn->query("SELECT * FROM Leads WHERE ID = '" . $row['Lead_ID'] . "'");
    $gld = mysqli_fetch_assoc($get_lead_dets);
    if ($gld != NULL) {
        if (strcasecmp($gld['Stage_ID'], '3') == 0 || strcasecmp($gld['Stage_ID'], '6') == 0) {
        } else {
            $all_upcoming_followups[] = $row;
        }
    }
}

if (isset($_GET['followup_phn'])) {
?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#searchbox").val("<?php echo $_GET['followup_phn']; ?>");
            $('#searchbox').keyup();

        });
    </script>
<?php
}

?>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
</head>

<body id="body">


    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        <div class="navbar navbar-expand flex-column flex-md-row navbar-custom">
            <div class="container-fluid">
                <!-- LOGO -->
                <a href="/" class="navbar-brand mr-0 mr-md-2 logo">
                    <span class="logo-lg">
                        <center><img src="/assets/images/logo.png" alt="" height="80" /></center>
                        <!--<span class="d-inline h4 ml-1 text-logo">CRM</span>-->
                    </span>
                    <span class="logo-sm">
                        <img src="/assets/images/logo.png" alt="" height="35">
                    </span>
                </a>

                <ul class="navbar-nav bd-navbar-nav flex-row list-unstyled menu-left mb-0">
                    <li class="">
                        <button class="button-menu-mobile open-left disable-btn">
                            <i data-feather="menu" class="menu-icon"></i>
                            <i data-feather="x" class="close-icon"></i>
                        </button>
                    </li>
                </ul>

                <ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu float-right mb-0">
                    <div id="divShowHide2" style="display: none;">
                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Send SMS to selected lead">
                            <a href="javascript:void(0);" class="nav-link selected-sms">
                                <i data-feather="message-square"></i>
                            </a>
                        </li>
                        <script type='text/javascript'>
                            $(document).ready(function() {

                                $('.selected-sms').click(function() {

                                    var data_id = $('#checkbox-form').find('input[name="id[]"]').serialize();
                                    console.log(data_id);

                                    // AJAX request
                                    $.ajax({
                                        url: 'ajax_leads/sms_selected.php',
                                        type: 'post',
                                        data: {
                                            "data_id": data_id
                                        },
                                        success: function(response) {
                                            // Add response in Modal body
                                            $('#modal-body-sms-selected').html(response);

                                            // Display Modal
                                            $('#selected_sms').modal('show');
                                        }
                                    });
                                });
                            });
                        </script>
                    </div>
                    <div id="divShowHide3" style="display: none;">
                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Send Mail to selected lead">
                            <a href="javascript:void(0);" class="nav-link selected-mail">
                                <i data-feather="mail"></i>
                            </a>
                        </li>
                        <script type='text/javascript'>
                            $(document).ready(function() {

                                $('.selected-mail').click(function() {

                                    var data_id = $('#checkbox-form').find('input[name="id[]"]').serialize();
                                    console.log(data_id);

                                    // AJAX request
                                    $.ajax({
                                        url: 'ajax_leads/mail_selected.php',
                                        type: 'post',
                                        data: {
                                            "data_id": data_id
                                        },
                                        success: function(response) {
                                            // Add response in Modal body
                                            $('#modal-body-mail-selected').html(response);

                                            // Display Modal
                                            $('#selected_mail').modal('show');
                                        }
                                    });
                                });
                            });
                        </script>
                    </div>
                    <style>
                        .jumbotron {
                            background: #6b7381;
                            color: #da4b16;
                        }

                        .jumbotron h1 {
                            color: #fff;
                        }

                        .example {
                            margin: 4rem auto;
                        }

                        .example>.row {
                            margin-top: 2rem;
                            height: 5rem;
                            vertical-align: middle;
                            text-align: center;
                            border: 1px solid rgba(189, 193, 200, 0.5);
                        }

                        .example>.row:first-of-type {
                            border: none;
                            height: auto;
                            text-align: left;
                        }

                        .example h3 {
                            font-weight: 400;
                        }

                        .example h3>small {
                            font-weight: 200;
                            font-size: .75em;
                            color: #939aa5;
                        }

                        .example h6 {
                            font-weight: 700;
                            font-size: .65rem;
                            letter-spacing: 3.32px;
                            text-transform: uppercase;
                            color: #bdc1c8;
                            margin: 0;
                            line-height: 5rem;
                        }

                        .example .btn-toggle {
                            top: 50%;
                            transform: translateY(-50%);
                        }

                        .btn-toggle {
                            margin: 0 4rem;
                            padding: 0;
                            position: relative;
                            border: none;
                            height: 1.5rem;
                            width: 3rem;
                            border-radius: 1.5rem;
                            color: #6b7381;
                            background: #bdc1c8;
                        }

                        .btn-toggle:focus,
                        .btn-toggle.focus,
                        .btn-toggle:focus.active,
                        .btn-toggle.focus.active {
                            outline: none;
                        }

                        .btn-toggle:before,
                        .btn-toggle:after {
                            line-height: 1.5rem;
                            width: 4rem;
                            text-align: center;
                            font-weight: 600;
                            font-size: 0.75rem;
                            text-transform: uppercase;
                            letter-spacing: 2px;
                            position: absolute;
                            bottom: 0;
                            transition: opacity .25s;
                        }

                        .btn-toggle:before {
                            content: 'Busy';
                            left: -4rem;
                            color: #333;
                            ;
                        }

                        .btn-toggle:after {
                            content: 'Available';
                            right: -4rem;
                            opacity: .5;
                            color: #333;
                        }

                        .btn-toggle>.handle {
                            position: absolute;
                            top: 0.1875rem;
                            left: 0.1875rem;
                            width: 1.125rem;
                            height: 1.125rem;
                            border-radius: 1.125rem;
                            background: #fff;
                            transition: left .25s;
                        }

                        .btn-toggle.active {
                            transition: background-color 0.25s;
                        }

                        .btn-toggle.active>.handle {
                            left: 1.6875rem;
                            transition: left .25s;
                        }

                        .btn-toggle.active:before {
                            opacity: .5;
                        }

                        .btn-toggle.active:after {
                            opacity: 1;
                        }

                        .btn-toggle.btn-sm:before,
                        .btn-toggle.btn-sm:after {
                            line-height: -0.5rem;
                            color: #fff;
                            letter-spacing: .75px;
                            left: 0.41250000000000003rem;
                            width: 2.325rem;
                        }

                        .btn-toggle.btn-sm:before {
                            text-align: right;
                        }

                        .btn-toggle.btn-sm:after {
                            text-align: left;
                            opacity: 0;
                        }

                        .btn-toggle.btn-sm.active:before {
                            opacity: 0;
                        }

                        .btn-toggle.btn-sm.active:after {
                            opacity: 1;
                        }

                        .btn-toggle.btn-xs:before,
                        .btn-toggle.btn-xs:after {
                            display: none;
                        }

                        .btn-toggle:before,
                        .btn-toggle:after {
                            color: #6b7381;
                        }

                        .btn-toggle.active {
                            background-color: #29b5a8;
                        }

                        .btn-toggle.btn-lg {
                            margin: 0 5rem;
                            padding: 0;
                            position: relative;
                            border: none;
                            height: 2.5rem;
                            width: 5rem;
                            border-radius: 2.5rem;
                        }

                        .btn-toggle.btn-lg:focus,
                        .btn-toggle.btn-lg.focus,
                        .btn-toggle.btn-lg:focus.active,
                        .btn-toggle.btn-lg.focus.active {
                            outline: none;
                        }

                        .btn-toggle.btn-lg:before,
                        .btn-toggle.btn-lg:after {
                            line-height: 2.5rem;
                            width: 5rem;
                            text-align: center;
                            font-weight: 600;
                            font-size: 1rem;
                            text-transform: uppercase;
                            letter-spacing: 2px;
                            position: absolute;
                            bottom: 0;
                            transition: opacity .25s;
                        }

                        .btn-toggle.btn-lg:before {
                            content: 'Busy';
                            left: -5rem;
                        }

                        .btn-toggle.btn-lg:after {
                            content: 'Available';
                            right: -5rem;
                            opacity: .5;
                        }

                        .btn-toggle.btn-lg>.handle {
                            position: absolute;
                            top: 0.3125rem;
                            left: 0.3125rem;
                            width: 1.875rem;
                            height: 1.875rem;
                            border-radius: 1.875rem;
                            background: #fff;
                            transition: left .25s;
                        }

                        .btn-toggle.btn-lg.active {
                            transition: background-color 0.25s;
                        }

                        .btn-toggle.btn-lg.active>.handle {
                            left: 2.8125rem;
                            transition: left .25s;
                        }

                        .btn-toggle.btn-lg.active:before {
                            opacity: .5;
                        }

                        .btn-toggle.btn-lg.active:after {
                            opacity: 1;
                        }

                        .btn-toggle.btn-lg.btn-sm:before,
                        .btn-toggle.btn-lg.btn-sm:after {
                            line-height: 0.5rem;
                            color: #fff;
                            letter-spacing: .75px;
                            left: 0.6875rem;
                            width: 3.875rem;
                        }

                        .btn-toggle.btn-lg.btn-sm:before {
                            text-align: right;
                        }

                        .btn-toggle.btn-lg.btn-sm:after {
                            text-align: left;
                            opacity: 0;
                        }

                        .btn-toggle.btn-lg.btn-sm.active:before {
                            opacity: 0;
                        }

                        .btn-toggle.btn-lg.btn-sm.active:after {
                            opacity: 1;
                        }

                        .btn-toggle.btn-lg.btn-xs:before,
                        .btn-toggle.btn-lg.btn-xs:after {
                            display: none;
                        }



                        .btn-toggle.btn-sm {
                            margin-top: 9px;
                            position: relative;
                            border: none;
                            height: 12px;
                            width: 45px;
                            border-radius: 1.5rem;
                            background: #fff;
                            right: -3px;
                        }

                        .btn-toggle.btn-sm:focus,
                        .btn-toggle.btn-sm.focus,
                        .btn-toggle.btn-sm:focus.active,
                        .btn-toggle.btn-sm.focus.active {
                            outline: none;
                        }

                        .btn-toggle.btn-sm:before,
                        .btn-toggle.btn-sm:after {
                            line-height: 1.5rem;
                            width: 0.5rem;
                            text-align: center;
                            font-weight: 600;
                            font-size: 0.55rem;
                            text-transform: uppercase;
                            letter-spacing: 2px;
                            position: absolute;
                            bottom: 0;
                            transition: opacity .25s;
                        }

                        .btn-toggle.btn-sm:before {
                            content: 'Busy';
                            left: -0.5rem;
                        }

                        .btn-toggle.btn-sm:after {
                            content: 'Available';
                            right: -0.5rem;
                            opacity: .5;
                        }

                        .btn-toggle.btn-sm>.handle {
                            position: absolute;
                            top: -3px;
                            left: 1.6875rem;
                            width: 1.125rem;
                            height: 1.125rem;
                            border-radius: 1.125rem;
                            background: #ff5c75;
                            transition: left .25s;
                        }

                        .btn-toggle.btn-sm.active {
                            transition: background-color 0.25s;
                        }

                        .btn-toggle.btn-sm.active>.handle {
                            left: 0rem;
                            transition: left .25s;
                            background: green;
                        }

                        .btn-toggle.btn-sm.active:before {
                            opacity: .5;
                        }

                        .btn-toggle.btn-sm.active:after {
                            opacity: 1;
                        }

                        .btn-toggle.btn-sm.btn-sm:before,
                        .btn-toggle.btn-sm.btn-sm:after {
                            line-height: -0.5rem;
                            color: #333;
                            letter-spacing: .75px;
                            left: 0.41250000000000003rem;
                            width: 2.325rem;
                        }

                        .btn-toggle.btn-sm.btn-sm:before {
                            text-align: left;
                            position: absolute;
                            top: -4px;
                            left: -48px;
                        }


                        .btn-toggle.btn-sm.btn-sm:after {
                            text-align: left;
                            opacity: 0;
                            position: absolute;
                            left: -60px;
                            top: -5px;
                        }

                        .btn-toggle.btn-sm.btn-sm.active:before {
                            opacity: 0;
                        }

                        .btn-toggle.btn-sm.btn-sm.active:after {
                            opacity: 1;
                        }

                        .btn-toggle.btn-sm.btn-xs:before,
                        .btn-toggle.btn-sm.btn-xs:after {
                            display: none;
                        }

                        .btn-toggle.btn-xs {
                            margin: 0 0;
                            padding: 0;
                            position: relative;
                            border: none;
                            height: 1rem;
                            width: 2rem;
                            border-radius: 1rem;
                        }

                        .btn-toggle.btn-xs:focus,
                        .btn-toggle.btn-xs.focus,
                        .btn-toggle.btn-xs:focus.active,
                        .btn-toggle.btn-xs.focus.active {
                            outline: none;
                        }

                        .btn-toggle.btn-xs:before,
                        .btn-toggle.btn-xs:after {
                            line-height: 1rem;
                            width: 0;
                            text-align: center;
                            font-weight: 600;
                            font-size: 0.75rem;
                            text-transform: uppercase;
                            letter-spacing: 2px;
                            position: absolute;
                            bottom: 0;
                            transition: opacity .25s;
                        }

                        .btn-toggle.btn-xs:before {
                            content: 'Busy';
                            left: 0;
                        }

                        .btn-toggle.btn-xs:after {
                            content: 'Available';
                            right: 0;
                            opacity: .5;
                        }

                        .btn-toggle.btn-xs>.handle {
                            position: absolute;
                            top: 0.125rem;
                            left: 0.125rem;
                            width: 0.75rem;
                            height: 0.75rem;
                            border-radius: 0.75rem;
                            background: #fff;
                            transition: left .25s;
                        }

                        .btn-toggle.btn-xs.active {
                            transition: background-color 0.25s;
                        }

                        .btn-toggle.btn-xs.active>.handle {
                            left: 1.125rem;
                            transition: left .25s;
                        }

                        .btn-toggle.btn-xs.active:before {
                            opacity: .5;
                        }

                        .btn-toggle.btn-xs.active:after {
                            opacity: 1;
                        }

                        .btn-toggle.btn-xs.btn-sm:before,
                        .btn-toggle.btn-xs.btn-sm:after {
                            line-height: -1rem;
                            color: #fff;
                            letter-spacing: .75px;
                            left: 0.275rem;
                            width: 1.55rem;
                        }

                        .btn-toggle.btn-xs.btn-sm:before {
                            text-align: right;
                        }

                        .btn-toggle.btn-xs.btn-sm:after {
                            text-align: left;
                            opacity: 0;
                        }

                        .btn-toggle.btn-xs.btn-sm.active:before {
                            opacity: 0;
                        }

                        .btn-toggle.btn-xs.btn-sm.active:after {
                            opacity: 1;
                        }

                        .btn-toggle.btn-xs.btn-xs:before,
                        .btn-toggle.btn-xs.btn-xs:after {
                            display: none;
                        }

                        .btn-toggle.btn-secondary:before,
                        .btn-toggle.btn-secondary:after {
                            color: #6b7381;
                        }

                        .btn-toggle.btn-secondary.active {
                            background-color: #eee !important;
                            box-shadow: none !important;
                        }

                        .notification-list .notify-item .notify-details {
                            white-space: break-spaces;
                        }

                        svg#header-call-details {
                            height: 35px;
                            width: 35px;
                        }
                    </style>
                    <style type="text/css">
                        .notification-mainbox {
                            position: fixed;
                            width: 460px;
                            top: 35%;
                            right: -0%;
                            background: #fff;
                            box-shadow: 0px 3px 9px #00000038;
                            transition: .4s;
                            z-index: 99;
                            border-radius: 5px;
                            /*overflow: hidden;*/
                        }

                        button.open-noti-btn {
                            border: none;
                            background: #4caf50;
                            color: #fff;
                            height: 50px;
                            width: 100%;
                            right: 0;
                            border-radius: 5px 5px 0px 0px;
                        }

                        .noti-bodybox {
                            padding: 10px 20px;
                        }

                        span.close-notibtn {
                            position: absolute;
                            top: 0px;
                            left: 0px;
                            width: 40px;
                            height: 40px;
                            /* background: #fd5d51f7; */
                            text-align: center;
                            font-size: 20px;
                            line-height: 45px;
                            color: #fff;
                            cursor: pointer;
                            border-radius: 0px;
                            transition: .3s;
                        }

                        /*span.close-notibtn:hover { color: #f44336;}*/

                        button.open-noti-btn:focus {
                            box-shadow: none;
                            outline: none;
                        }

                        .noti-bodybox ul {
                            list-style: none;
                            padding: 0;
                        }

                        .noti-bodybox ul li {
                            display: flex;
                            height: 35px;
                            margin-bottom: 10px;
                            border-bottom: 1px solid #eee;
                        }

                        .noti-bodybox ul {
                            list-style: none;
                            padding: 0;
                            column-count: 2;
                        }

                        .noti-bodybox ul li span {
                            padding-left: 5px;
                            font-size: 14px;
                            line-height: 14px;
                            padding-top: 4px;
                        }

                        .notification-mainbox-open {
                            right: -460px;
                        }

                        .notification-mainbox-open span.close-notibtn.animated.wobble {
                            margin-left: -45px !important;
                            background: #5369f8;
                            line-height: 40px;
                            border-radius: 10px;
                        }

                        .animated {
                            animation-duration: 2.5s;
                            animation-fill-mode: both;
                            animation-iteration-count: infinite;
                        }

                        @keyframes wobble {
                            0% {
                                transform: translateX(0%);
                            }

                            15% {
                                transform: translateX(-25%) rotate(-5deg);
                            }

                            30% {
                                transform: translateX(20%) rotate(3deg);
                            }

                            45% {
                                transform: translateX(-15%) rotate(-3deg);
                            }

                            60% {
                                transform: translateX(10%) rotate(2deg);
                            }

                            75% {
                                transform: translateX(-5%) rotate(-1deg);
                            }

                            100% {
                                transform: translateX(0%);
                            }
                        }

                        .wobble {
                            animation-name: wobble;
                        }

                        .callcut {
                            position: absolute;
                            top: 5px;
                            right: 10px;
                            margin: 0;
                            background: #f54b65;
                            transform: rotate(175deg);
                            width: 40px;
                            height: 39px;
                            padding: 8px;
                            border-radius: 22px;
                        }
                    </style>




                    <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Add Quick Lead">
                        <a href="javascript:void(0);" class="nav-link" data-toggle="modal" data-target="#addquicklead">
                            <i data-feather="plus-circle"></i>
                        </a>
                    </li>
                    <!-- <li class="dropdown notification-list" style="width: 140px;">
                        <div class="mt-3" style="background: #faa524; height: 35px; border-radius: 50px;">
                            <button id="tata" type="button" class="btn btn-sm btn-secondary btn-toggle" data-toggle="button" aria-pressed="<?php print_r(mysqli_fetch_row($type)[1]) ?>" autocomplete="Busy">
                                <div class="handle"></div>
                            </button>
                        </div>
                    </li> -->
                    <li class="dropdown notification-list" data-toggle="tooltip" data-placement="right" title="Dialer Logs">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i data-feather="phone-call"></i>
                            <!-- <span class="noti-icon-badge"></span> -->
                            <!-- <svg id="header-call-details" class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                                <path class="icon-shape" d="M21.825,21.8q3.24-3.24,2.36-4.12l-.16-.16a5.8,5.8,0,0,1-.82-.96,1.535,1.535,0,0,1-.08-1.08,4.4,4.4,0,0,1,.98-1.76,7.062,7.062,0,0,1,.74-.78,1.157,1.157,0,0,1,.7-.32q.36-.02.6-.02a1.033,1.033,0,0,1,.58.26l.48.36a5.073,5.073,0,0,1,.52.5l.42.44q.96.96-.12,3.88a16.733,16.733,0,0,1-4.08,5.9,17.345,17.345,0,0,1-5.9,4.08q-2.9,1.1-3.86.14l-.46-.44q-.42-.4-.5-.5l-.36-.48a1,1,0,0,1-.26-.62l.04-.6a1.2,1.2,0,0,1,.3-.7,5.818,5.818,0,0,1,.76-.74,7.292,7.292,0,0,1,1.4-.94,1.284,1.284,0,0,1,1.08-.04,3.541,3.541,0,0,1,.7.36q.18.141.78.74Q18.544,25.077,21.825,21.8Zm-2.333-4.271a.784.784,0,0,0-.784-.785H13.089a.784.784,0,0,0-.784.785v1.042a.784.784,0,0,0,.784.784h5.619a.784.784,0,0,0,.784-.784Zm0-4.2a.785.785,0,0,0-.784-.785H13.089a.785.785,0,0,0-.784.785v1.042a.784.784,0,0,0,.784.784h5.619a.784.784,0,0,0,.784-.784Zm0-4.2a.785.785,0,0,0-.784-.785H13.089a.785.785,0,0,0-.784.785v1.042a.785.785,0,0,0,.784.785h5.619a.785.785,0,0,0,.784-.785Zm-8.712,8.4A.784.784,0,0,0,10,16.74H8.769a.784.784,0,0,0-.784.785v1.042a.784.784,0,0,0,.784.784H10a.784.784,0,0,0,.784-.784Zm0-4.2A.785.785,0,0,0,10,12.538H8.769a.785.785,0,0,0-.784.785v1.042a.784.784,0,0,0,.784.784H10a.784.784,0,0,0,.784-.784Zm0-4.2A.785.785,0,0,0,10,8.336H8.769a.785.785,0,0,0-.784.785v1.042a.785.785,0,0,0,.784.785H10a.785.785,0,0,0,.784-.785Z" fill="#65696e"></path>
                            </svg> -->
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                            <!-- item-->
                            <div class="dropdown-item noti-title border-bottom">
                                <h5 class="m-0 font-size-16">Logs</h5>
                            </div>

                            <div class="slimscroll noti-scroll">
                                <?php
                                $ch = curl_init();

                                curl_setopt($ch, CURLOPT_URL, 'https://api-cloudphone.tatateleservices.com/v1/logs');

                                $headers = array();
                                $headers[] = 'Accept: application/json';
                                $headers[] = 'Authorization: ' . $_SESSION['token'] . '';
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $result = curl_exec($ch);
                                if (curl_errno($ch)) {
                                    echo 'Error:' . curl_error($ch);
                                } else {
                                    $data = json_decode($result);
                                    foreach ($data as $key => $val) {
                                        if ($key === 'data') {
                                            foreach ($val as $v) {
                                ?>

                                                <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom active">
                                                    <div class="notify-icon bg-success"><i class="uil uil-comment-message"></i> </div>
                                                    <p class="notify-details"><?php echo $v[5]  ?><small class="text-muted"><?php echo $v[4]  ?></small></p>
                                                </a>
                                <?php
                                            }
                                        }
                                    }
                                }
                                curl_close($ch);

                                ?>
                            </div>
                        </div>





                    <li class="d-none d-sm-block">
                        <div class="app-search">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="searchbox" class="form-control" onkeypress="handle(event);" placeholder="Search..." />

                                </div>
                                <button type="button" onclick="getList()" class="btn btn-primary" style="height:36px;">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </li>

                    <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="<?php echo (int)count($all_upcoming_followups) + (int)count($all_notifications); ?> notifications">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i data-feather="bell"></i>
                            <span class="noti-icon-badge"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                            <!-- item-->
                            <div class="dropdown-item noti-title border-bottom">
                                <h5 class="m-0 font-size-16">
                                    <span class="float-right">
                                        <a href="" class="text-dark">
                                            <small>Clear All</small>
                                        </a>
                                    </span>Notification
                                </h5>
                            </div>

                            <div class="slimscroll noti-scroll">

                                <?php

                                foreach ($all_upcoming_followups as $followup) {
                                    $get_lead_dets = $conn->query("SELECT * FROM Leads WHERE ID = '" . $followup['Lead_ID'] . "'");
                                    $lead_dets = mysqli_fetch_assoc($get_lead_dets);
                                ?>
                                    <a href="/counsellor/myfollowup?followup_phn=<?php echo $lead_dets['Mobile']; ?>" class="dropdown-item notify-item border-bottom">
                                        <div class="notify-icon bg-secondary"><span class="iconify" data-icon="ri:chat-follow-up-line" data-inline="false" style="color: #ffffff;"></span></div>
                                        <p class="notify-details">Upcoming - <strong><?php echo $followup['Followup_Type']; ?></strong></p>
                                        <p class="notify-details">Applicant - <strong><?php echo $lead_dets['Name']; ?></strong></p>
                                        <p class="notify-details"><small class="text-muted"><?php echo date("F j, Y g:i a", strtotime($followup["Followup_Timestamp"]));  ?></small></p>
                                    </a>

                                <?
                                }

                                foreach ($all_notifications as $notification) {
                                ?>
                                    <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom">
                                        <div class="notify-icon bg-primary"><i class="uil uil-user-plus"></i></div>
                                        <p class="notify-details"><b>A lead has been referred to you.</b><small class="text-muted"><?php echo date("F j, Y g:i a", strtotime($notification["Date"]));  ?></small>
                                        </p>
                                    </a>

                                <?
                                }
                                ?>
                            </div>

                            <!-- All-->
                            <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all border-top">
                                View all
                                <i class="fi-arrow-right"></i>
                            </a>

                        </div>
                    </li>

                </ul>
            </div>
            <script>
                function handle(e) {
                    if (e.keyCode === 13) {
                        e.preventDefault();
                        getList();
                    }
                }
            </script>
        </div>
        <!-- end Topbar -->

        <div class="notification-mainbox" id="livecall" style="display:none">
            <button class="open-noti-btn">Call From <span id="live_name"></span><br>
                <i class="fa fa-phone"></i> &nbsp; <span class="contact-info" id="live_phone"></span>
                <!-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="callcut feather feather-phone-off icon-dual my-float" id="call-icon">
                    <path d="M10.68 13.31a16 16 0 0 0 3.41 2.6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7 2 2 0 0 1 1.72 2v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.42 19.42 0 0 1-3.33-2.67m-2.67-3.34a19.79 19.79 0 0 1-3.07-8.63A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91"></path>
                    <line x1="23" y1="1" x2="1" y2="23"></line>
                </svg> -->
            </button>
            <span class="close-notibtn animated wobble"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
            <div class="noti-bodybox">
                <ul>
                    <!-- <li><b>Email:</b><span id="live_email"></span></li>
                    <li><b>Mobile:</b><span id="live_mobile"></span></li>
                    <li><b>Alternate Mobile:</b><span id="live_alt_mobile"></span></li> -->

                    <li><b>Alternate Mobile:</b><span id="live_alt_mobile"></span></li>
                    <li><b>Lead Remark:</b><span id="live_remark"></span></li>
                    <li><b> University:</b><span id="live_univ"> </span></li>
                    <li><b>Course:</b><span id="live_course"></span></li>
                    <li><b>Specialization:</b><span id="live_spec"> </span></li>
                    <li style="display:none"><b>SVU ID:</b><span id="live_urnno"> </span></li>
                    <li><b> Stage:</b><span id="live_stage"></span></li>
                </ul>
            </div>
        </div>
        <!------------Quick Lead Modal----------->
        <div id="addquicklead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addquicklead">Add Quick Leads</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="quick_lead_form">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Full Name</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" id="quick_lead_name" name="quick_lead_name" placeholder="Full Name">
                                </div>
                                <label class="col-lg-2 col-form-label">Email ID</label>
                                <div class="col-lg-4">
                                    <input type="email" class="form-control" id="quick_lead_email" name="quick_lead_email" placeholder="leadmail@email.com">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Mobile Number</label>
                                <div class="col-lg-4">
                                    <input type="number" maxlength="10" minlength="10" name="quick_lead_mobile" class="form-control" id="quick_lead_mobile" placeholder="9450XXXXXX">
                                </div>
                                <label class="col-lg-2 col-form-label">Alternate Number</label>
                                <div class="col-lg-4">
                                    <input type="number" maxlength="10" minlength="10" name="quick_lead_alt_mobile" class="form-control" id="quick_lead_alt_mobile" placeholder="9450XXXXXX">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="dob1">DOB</label>
                                <div class="col-lg-4">
                                    <input type="date" id="dob1" name="dob1" class="form-control" placeholder="Date of birth">
                                </div>
                                <label class="col-lg-2 col-form-label" for="quick_lead_remarks">Remarks</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" id="quick_lead_remarks" placeholder="Remarks">
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Source</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" name="quick_lead_source" id="quick_lead_source">
                                        <option selected disabled>Choose</option>
                                        <?php
                                        foreach ($all_sources as $source) {
                                        ?>
                                            <option value="<?php echo $source['ID']; ?>"><?php echo $source['Name']; ?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label class="col-lg-2 col-form-label">Sub-Source</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" name="quick_lead_sub_source" id="quick_lead_sub_source">
                                        <option selected disabled>Choose</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Stage</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" name="quick_lead_stage" id="quick_lead_stage">
                                        <option selected disabled>Choose</option>
                                        <?php
                                        foreach ($all_stages as $stage) {
                                        ?>
                                            <option value="<?php echo $stage['ID']; ?>"><?php echo $stage['Name']; ?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label class="col-lg-2 col-form-label">Reason</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" name="quick_lead_reason" id="quick_lead_reason">
                                        <option selected disabled>Choose</option>
                                    </select>
                                </div>
                            </div>
                            <?php if($_SESSION['User_Type'] == 'b2b'){ ?>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" style="display: none;">Counsellor</label>
                                <div class="col-lg-4" style="display: none;">
                                    <select data-plugin="customselect" class="small" name="quick_lead_lead_owner" id="quick_lead_lead_owner">
                                        <?php
                                        foreach ($all_users as $user) {
                                        ?>
                                            <option selected value="<?php echo $user['ID']; ?>"><?php echo $user['Name']; ?></option>
                                        <?
                                        }

                                        ?>
                                    </select>
                                </div>
                                <label class="col-lg-2 col-form-label">University</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" name="quick_lead_university" id="quick_lead_university" >
                                        <option disabled>Choose</option>
                                        <?php
                                        foreach ($all_institutes as $institute) {
                                        ?>
                                            <option value="51" >Chandigarh University</option>
                                            <option value="<?php echo $institute['ID']; ?>"><?php echo $institute['Name']; ?></option>
                                            <option value="65" >Both</option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                           <?php }else{ ?>
                            <div class="form-group row" style="display: none;">
                                <label class="col-lg-2 col-form-label">Counsellor</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" name="quick_lead_lead_owner" id="quick_lead_lead_owner">
                                        <?php
                                        foreach ($all_users as $user) {
                                        ?>
                                            <option selected value="<?php echo $user['ID']; ?>"><?php echo $user['Name']; ?></option>
                                        <?
                                        }

                                        ?>
                                    </select>
                                </div>
                                <label class="col-lg-2 col-form-label">University</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" name="quick_lead_university" id="quick_lead_university" >
                                        <option disabled>Choose</option>
                                        <?php
                                        foreach ($all_institutes as $institute) {
                                        ?>
                                            <option selected value="<?php echo $institute['ID']; ?>"><?php echo $institute['Name']; ?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                           <?php } ?>
                           <?php if($_SESSION['User_Type'] != 'b2b'){ ?>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Course</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" name="quick_lead_course" id="quick_lead_course">
                                        <option selected disabled>Choose</option>
                                    </select>
                                </div>
                                <label class="col-lg-2 col-form-label">Specialization</label>
                                <div class="col-lg-4">
                                    <select data-plugin="customselect" class="small" id="quick_lead_specialization">
                                        <option selected disabled>Choose</option>
                                    </select>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="row">
                                <label class="col-lg-2 col-form-label">State</label>
                                <div class="form-group col-lg-4">

                                    <select data-plugin="customselect" class="form-control" id="new_state_selectf" name="state_select">
                                        <option selected value="<?php echo $row['State_ID'] ?>"><?
                                                                                                $get_state_name = $conn->query("SELECT * FROM States WHERE ID = '" . $row['State_ID'] . "'");
                                                                                                $state_name = mysqli_fetch_assoc($get_state_name);
                                                                                                if ($get_state_name->num_rows > 0) {
                                                                                                    echo $state_name['Name'];
                                                                                                } else {
                                                                                                    $state_name['Name'] = 'Select State';
                                                                                                    echo $state_name['Name'];
                                                                                                }
                                                                                                ?></option>
                                        <?php
                                        $result_state = $conn->query("SELECT * FROM States WHERE Country_ID = 101");
                                        while ($states = $result_state->fetch_assoc()) {
                                        ?>
                                            <option value="<?php echo $states['ID']; ?>"><?php echo $states['Name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="col-lg-2 col-form-label" for="new_city_selectf">City</label>
                                <div class="form-group col-lg-4">

                                    <select data-plugin="customselect" class="form-control" id="new_city_selectf">
                                        <option selected value="<?php echo $row['City_ID'] ?>"><?
                                                                                                $get_city_name = $conn->query("SELECT * FROM Cities WHERE ID = '" . $row['City_ID'] . "'");
                                                                                                $city_name = mysqli_fetch_assoc($get_city_name);
                                                                                                if ($get_city_name->num_rows > 0) {
                                                                                                    echo $city_name['Name'];
                                                                                                } else {
                                                                                                    $city_name['Name'] = 'Choose City';
                                                                                                    echo $city_name['Name'];
                                                                                                }

                                                                                                ?></option>
                                        <?php
                                        $result_city = $conn->query("SELECT * FROM Cities WHERE State_ID = '" . $row['State_ID'] . "'");
                                        while ($cities = $result_city->fetch_assoc()) {
                                        ?>
                                            <option value="<?php echo $cities['ID']; ?>"><?php echo $cities['Name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="addQuickLead();">Save</button>
                    </div>
                    </form>


                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-----------End Modal------------------->

        <script>
            var empID = '<?php echo $_SESSION['useremployeeid'] ?>';
            var checkFollowUp = setInterval(checkUpComingFollowups, 5000);

            function checkUpComingFollowups() {
                $.ajax({
                    type: "POST",
                    url: "filestobeincluded/upcoming_followups.php",
                    global: false,
                    data: {
                        "counsellor_id": empID
                    },
                    success: function(data) {
                        if (data.trim() != '') {
                            Swal.fire({
                                icon: 'info',
                                position: 'top-end',
                                html: 'You have upcoming follow-up(s) with <b>' + data + '</b> in 10 minutes',
                                confirmButtonText: `OK`
                            });
                        }
                    }
                });
            }
        </script>

        <script>
            function addQuickLead() {
                if ($('#quick_lead_form').valid()) {

                    var fullName = $('#quick_lead_name').val();
                    var emailID = $('#quick_lead_email').val();
                    var mobileNumber = $('#quick_lead_mobile').val();
                    var altMobileNumber = $('#quick_lead_alt_mobile').val();
                    var sourceID = $('#quick_lead_source').val();
                    var subsourceID = $('#quick_lead_sub_source').val();
                    var stageID = $('#quick_lead_stage').val();
                    var reasonID = $('#quick_lead_reason').val();
                    var leadOwner = $('#quick_lead_lead_owner').val();
                    var instituteID = $('#quick_lead_university').val();
                    var courseID = $('#quick_lead_course').val();
                    var specializationID = $('#quick_lead_specialization').val();
                    var remarks = $('#quick_lead_remarks').val();
                    var dob = $('#dob1').val();
                    var state_ID = $('#new_state_selectf').val();
                    var city_ID = $('#new_city_selectf').val();

                    $.ajax({
                        type: "POST",
                        url: "filestobeincluded/add_quick_lead.php",
                        data: {
                            "fullName": fullName,
                            "emailID": emailID,
                            "mobileNumber": mobileNumber,
                            "altMobileNumber": altMobileNumber,
                            "sourceID": sourceID,
                            "subsourceID": subsourceID,
                            "stageID": stageID,
                            "reasonID": reasonID,
                            "leadOwner": leadOwner,
                            "instituteID": instituteID,
                            "courseID": courseID,
                            "specializationID": specializationID,
                            "remarks": remarks,
                            "dob": dob,
                            "state_ID": state_ID,
                            "city_ID": city_ID
                        },
                        success: function(data) {
                            console.log(data);
                            $('#addquicklead').modal('hide');
                            if (data.match("true")) {
                                toastr.success('Lead added successfully');
                                window.location.reload();
                            } else if (data.match("renquired")) {
                                toastr.warning('Lead already exists');
                                window.location.reload();
                            } else {
                                toastr.error('Unable to add lead');
                            }
                        }
                    });
                    return false;
                }
            }
        </script>

        <script>
            $(document).ready(function() {
                $('#quick_lead_source').change(function() {

                    $('#quick_lead_sub_source').html("<option disabled selected>Select Sub-Source</option>");

                    var selected_source = $('#quick_lead_source').val();
                    $.ajax({
                        type: "POST",
                        url: "../onselect/onSelect.php",
                        data: {
                            "source_select": "",
                            "selected_source": selected_source
                        },
                        success: function(data) {
                            if (data != "") {
                                $('#quick_lead_sub_source').html(data);
                            } else {
                                $('#quick_lead_sub_source').html("<option disabled selected>Select Sub-Source</option>");
                            }
                        }
                    });
                });
            });
        </script>

        <script>
            $(document).ready(function() {
                $('#quick_lead_stage').change(function() {

                    $('#quick_lead_reason').html("<option disabled selected>Select Reason</option>");

                    var selected_stage = $('#quick_lead_stage').val();
                    $.ajax({
                        type: "POST",
                        url: "../onselect/onSelect.php",
                        data: {
                            "stage_select": "",
                            "selected_stage": selected_stage
                        },
                        success: function(data) {
                            if (data != "") {
                                $('#quick_lead_reason').html(data);
                            } else {
                                $('#quick_lead_reason').html("<option disabled selected>Select Reason</option>");
                            }
                        }
                    });
                });
            });
        </script>

        <script>
            $(document).ready(function() {
                $('#quick_lead_university').change(function() {

                    $('#quick_lead_course').html("<option disabled selected>Select Course</option>");
                    $('#quick_lead_specialization').html("<option disabled selected>Select Specialization</option>");

                    var selected_institute = $('#quick_lead_university').val();
                    $.ajax({
                        type: "POST",
                        url: "../onselect/onSelect.php",
                        data: {
                            "select_institute": "",
                            "selected_institute": selected_institute
                        },
                        success: function(data) {
                            if (data != "") {
                                $('#quick_lead_course').html(data);
                                jQuery('#quick_lead_course').trigger('change');
                            } else {
                                $('#quick_lead_course').html("<option disabled selected>Select Course</option>");
                                $('#quick_lead_specialization').html("<option disabled selected>Select Specialization</option>");
                            }
                        }
                    });
                });
            });
        </script>

        <script>
            $(document).ready(function() {
                $('#quick_lead_course').change(function() {

                    $('#quick_lead_specialization').html("<option disabled selected>Select Specialization</option>");

                    var selected_course = $('#quick_lead_course').val();
                    var selected_institute = $('#quick_lead_university').val();

                    $.ajax({
                        type: "POST",
                        url: "../onselect/onSelect.php",
                        data: {
                            "select_course": "",
                            "selected_course": selected_course,
                            "selected_institute": selected_institute
                        },
                        success: function(data) {
                            if (data != "") {
                                $('#quick_lead_specialization').html(data);
                            } else {
                                $('#quick_lead_specialization').html("<option disabled selected>Select Specialization</option>");
                            }
                        }
                    });
                });
            });
        </script>


        <script>
            $(document).ready(function() {

                $('#quick_lead_form').validate({
                    rules: {
                        quick_lead_name: {
                            required: true
                        },
                        quick_lead_email: {
                            // email: "Please enter a valid email address."
                            remote:{
                                url:'/duplicate.php',
                                type:'post',
                                data:{
                                    email:function(){
                                        return $('#quick_lead_email').val();
                                    }
                                }
                                
                            }
                        },
                        quick_lead_mobile: {
                            required: true,
                            minlength: 10,
                            maxlength: 15,
                            remote:{
                                url:'/duplicate.php',
                                type:'post',
                                data:{
                                    phone:function(){
                                        return $('#quick_lead_mobile').val();
                                    }
                                }
                                
                            }
                        },
                        quick_lead_alt_mobile: {
                            minlength: 10,
                            maxlength: 15
                        },
                        quick_lead_source: {
                            required: true
                        },
                        quick_lead_sub_source: {
                            required: true
                        },
                        quick_lead_stage: {
                            required: true
                        },
                        quick_lead_reason: {
                            required: true
                        },
                        quick_lead_lead_owner: {
                            required: true
                        },
                        quick_lead_university: {
                            required: true
                        },
                        quick_lead_course: {
                            required: true
                        }
                    },
                    messages: {
                        quick_lead_name: {
                            required: 'Full Name is required'
                        },
                        quick_lead_email: {
                            email:'Please enter a valid email address.',
                            remote:'Already Exists'
                        },
                        quick_lead_mobile: {
                            required: 'Mobile number is required',
                            minlength: 'Invalid mobile number',
                            maxlength: 'Invalid mobile number',
                            remote: 'Already taken'
                        },
                        quick_lead_alt_mobile: {
                            minlength: 'Invalid mobile number',
                            maxlength: 'Invalid mobile number'
                        },
                        quick_lead_source: {
                            required: 'Please select a source'
                        },
                        quick_lead_sub_source: {
                            required: 'Please select a sub-source'
                        },
                        quick_lead_stage: {
                            required: 'Please select a stage'
                        },
                        quick_lead_reason: {
                            required: 'Please select a reason'
                        },
                        quick_lead_lead_owner: {
                            required: 'Please select a lead owner'
                        },
                        quick_lead_university: {
                            required: 'Please select a university'
                        },
                        quick_lead_course: {
                            required: 'Please select a course'
                        }
                    },
                    highlight: function(element) {
                        $(element).addClass('error');
                        $(element).closest('.form-control').addClass('has-error');
                        $(element).closest('.small').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).removeClass('error');
                        $(element).closest('.form-control').removeClass('has-error');
                        $(element).closest('.small').removeClass('has-error');
                    }
                });

            });
        </script>

        <script>
            $(document).ready(function() {
                jQuery('#quick_lead_university').trigger('change');
            });
            $(document).ready(function() {
                jQuery('#quick_lead_course').trigger('change');
            });
        </script>
        <script>
            $(document).ready(function() {
                $('#new_state_selectf').change(function() {

                    $('#new_city_selectf').html("<option disabled selected>Select City</option>");

                    var new_selected_state = $('#new_state_selectf').val();
                    $.ajax({
                        type: "POST",
                        url: "../onselect/onSelect.php",
                        data: {
                            "state_select": "",
                            "selected_state": new_selected_state
                        },
                        success: function(data) {
                            if (data != "") {
                                $('#new_city_selectf').html(data);
                            } else {
                                $('#new_city_selectf').html("<option disabled selected>Select City</option>");
                            }
                        }
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('#new_state_selectf').change(function() {

                    $('#new_city_selectf').html("<option disabled selected>Select City</option>");

                    var new_selected_state = $('#new_state_selectf').val();
                    $.ajax({
                        type: "POST",
                        url: "../onselect/onSelect.php",
                        data: {
                            "state_select": "",
                            "selected_state": new_selected_state
                        },
                        success: function(data) {
                            if (data != "") {
                                $('#new_city_selectf').html(data);
                            } else {
                                $('#new_city_selectf').html("<option disabled selected>Select City</option>");
                            }
                        }
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function($) {
                        $("#quick_lead_mobile").keyup(function() {
                            var phone = $('#quick_lead_mobile').val();
                            if(phone.length > 9){
                                $.ajax({
                                    url: '/duplicate.php',
                                    type: 'post',
                                    data: {
                                        phone: phone
                                    },
                                    success: function(res) {
                                        console.log(res);
                                        if(res.match("duplicate")){

                                        }
                                    }
                                })
                            }
                        });

                    });
        </script>