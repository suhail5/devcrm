<?php

    $re_enquired_count = array();
    $grlq = $conn->query("SELECT * FROM Re_Enquired WHERE Counsellor_ID = '".$_SESSION['useremployeeid']."'");
    while($row = $grlq->fetch_assoc()) {
        $re_enquired_count[] = $row;
    }
?>
        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">
            <div class="media user-profile mt-2 mb-2">
                <img src="assets/images/users/avatar-7.jpg" class="avatar-sm rounded-circle mr-2" alt="Shreyu" />
                <img src="assets/images/users/avatar-7.jpg" class="avatar-xs rounded-circle mr-2" alt="Shreyu" />
                <?php
                    $employeeidlog = $_SESSION['useremployeeid'];
                    $result = $conn->query("SELECT * FROM users WHERE ID = '$employeeidlog'");                     
                    while($row = $result->fetch_assoc()) {
                        $ROLE = $row['Role'];
                        
                ?>
                
                <div class="media-body">
                    <h6 class="pro-user-name mt-0 mb-0"><?php echo ucfirst($row['Name']); ?></h6>
                    <span class="pro-user-desc"><?php echo strtoupper($row['Role']); ?></span>
                </div>
                <?php
                      }  
                ?>
                <div class="dropdown align-self-center profile-dropdown-menu">
                    <a class="dropdown-toggle mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                        aria-expanded="false">
                        <span data-feather="chevron-down"></span>
                    </a>
                    <div class="dropdown-menu profile-dropdown">
                        <a href="profile" class="dropdown-item notify-item">
                            <i data-feather="user" class="icon-dual icon-xs mr-2"></i>
                            <span>My Account</span>
                        </a>

                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i data-feather="help-circle" class="icon-dual icon-xs mr-2"></i>
                            <span>Support</span>
                        </a>

                        <div class="dropdown-divider"></div>

                        <a href="javascript:void(0);" class="dropdown-item notify-item" onclick="logout();">
                            <i data-feather="log-out" class="icon-dual icon-xs mr-2"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="sidebar-content">
                <!--- Sidemenu -->
                <div id="sidebar-menu" class="slimscroll-menu">
                    <ul class="metismenu" id="menu-bar">
                        <li class="menu-title">Navigation</li>

                        <li>
                            <a href="dashboard">
                                <i data-feather="home"></i>
                                <!-- <span class="badge badge-success float-right">1</span> -->
                                <span> Dashboard </span>
                            </a>
                        </li>

                        <li>
                            <a href="callhistory">
                                <i data-feather="phone"></i>
                                <span> CDR(Call Detail Records) </span>
                            </a>
                        </li>

                        <li class="menu-title">Leads</li>
                        <li>
                            <a href="leads">
                                <i data-feather="users"></i>
                                <span> All Leads </span>
                            </a>
                        </li>

                        <!-- <li>
                            <a href="failed-leads">
                                <i data-feather="user-x"></i>
                                <span> Failed Leads </span>
                            </a>
                        </li> -->

                        <li>
                            <a href="myfollowup">
                                <i data-feather="calendar"></i>
                                <span> My Follow-ups </span>
                            </a>
                        </li>

                        
                        
                        
                        <!-- <li class="menu-title">Marketing</li>
                        
                        <li>
                            <a href="campaign">
                                <i data-feather="thumbs-up"></i>
                                <span> Campaigns </span>
                            </a>
                        </li>

                        <li>
                            <a href="drip-marketing">
                                <i data-feather="triangle"></i>
                                <span> Drip Marketing </span>
                            </a>
                        </li> -->



                        <!-- <li class="menu-title">Reports</li>

                        <li>
                            <a href="reports">
                                <i data-feather="file-text"></i>
                                <span> Reports </span>
                            </a>
                        </li> -->

                        <!-- <li>
                            <a href="analysis">
                                <i data-feather="pie-chart"></i>
                                <span> Analysis </span>
                            </a>
                        </li> -->

                    </ul>
                </div>
                <!-- End Sidebar -->

                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->

        <script>

            function logout(){
                
             $.ajax({
                url:'/tata_api/logout.php',
                type:'post',
                success: function(res){

                    var res = $.parseJSON(res);
                    if(res.message === "Successfully logged out"){
                        
                        setTimeout(() => {
                            window.location.href = "/logout";
                        }, 2500);
                        toastr.success("Successfully Logout..");
                        toastr.warning('Edutra Dialer Logout..');
                    }else if(res.message === "TOKEN HAS EXPIRED"){
                        toastr.warning('TOKEN HAS EXPIRED');
                    }else{
                        window.location.href = "/logout";
                        toastr.success("Successfully Logout..");
                    }
                }

                })
            }
                                   
    </script>
