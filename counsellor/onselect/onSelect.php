<?php require "../filestobeincluded/db_config.php" ?>

<?php

if(isset(($_POST['stage_select']))) {

	$all_reasons = "";

	$selected_stage = $_POST['selected_stage'];

	$get_reasons = $conn->query("SELECT * FROM Reasons WHERE Stage_ID = '".$selected_stage."' AND Status = 'Y' ORDER BY Name ASC");
	while($row = $get_reasons->fetch_assoc()) {
		$all_reasons = $all_reasons."<option selected value='".$row['ID']."'>".$row['Name']."</option>";
	}
	echo $all_reasons;
}

if(isset(($_POST['state_select']))) {

	$all_cities = "";

	$selected_state = $_POST['selected_state'];

	$get_cities = $conn->query("SELECT * FROM Cities WHERE State_ID = '".$selected_state."'");
	while($row = $get_cities->fetch_assoc()) {
		$all_cities = $all_cities."<option selected value='".$row['ID']."'>".$row['Name']."</option>";
	}
	echo $all_cities;
}

if(isset(($_POST['source_select']))) {

	$all_subsources = "";

	$selected_source = $_POST['selected_source'];

	$get_subsources = $conn->query("SELECT * FROM Sub_Sources WHERE Source_ID = '".$selected_source."' AND Status = 'Y'");
	while($row = $get_subsources->fetch_assoc()) {
		$all_subsources = $all_subsources."<option selected value='".$row['ID']."'>".$row['Name']."</option>";
	}
	echo $all_subsources;
}

if(isset(($_POST['select_institute']))) {

	$all_courses = "";

	$selected_institute = $_POST['selected_institute'];

	$get_courses = $conn->query("SELECT * FROM Courses WHERE Institute_ID = '".$selected_institute."' AND Status = 'Y'");
	while($row = $get_courses->fetch_assoc()) {
		$all_courses = $all_courses."<option selected value='".$row['ID']."'>".$row['Name']."</option>";
	}
	echo $all_courses;
}

if(isset(($_POST['select_course']))) {

	$all_specializations = "";

	$selected_course = $_POST['selected_course'];
	$selected_institute = $_POST['selected_institute'];

	$get_specializations = $conn->query("SELECT * FROM Specializations WHERE Course_ID = '".$selected_course."' AND Institute_ID = '".$selected_institute."' AND Status = 'Y'");
	while($row = $get_specializations->fetch_assoc()) {
		$all_specializations = $all_specializations."<option selected value='".$row['ID']."'>".$row['Name']."</option>";
	}
	echo $all_specializations;
}


?>