<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_states = array();
$all_countries = array();
$all_cities = array();

$states_query_res = $conn->query("SELECT * FROM States");
while($row = $states_query_res->fetch_assoc()) {
    $all_states[] = $row;
}

$countries_query_res = $conn->query("SELECT * FROM Countries");
while ($row = $countries_query_res->fetch_assoc()) {
    $all_countries[] = $row;
}
/*,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41*/
$cities_query_res = $conn->query("SELECT * FROM Cities Where State_ID IN (1)");
while ($row = $cities_query_res->fetch_assoc()) {
    $all_cities[] = $row;
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
        <div class="card-header" id="headingTen"><h5 class="m-0 font-size-16">Cities <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addcitymodal"> <i class="uil uil-plus-circle"></i> Add City</button>
            <!----Add Specialization Modal-------->
            <div class="modal fade" id="addcitymodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New City</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">City</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="city_name" placeholder="City">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">States</label>
                                    <div class="col-lg-10">
                                        <select id="cities_state_id" class="form-control custom-select">
                                            <?
                                            foreach ($all_states as $state) {
                                                ?>
                                                <option value="<?php echo($state['ID']); ?>"><?php echo $state['Name'];  ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Country</label>
                                    <div class="col-lg-10">
                                        <select id="cities_country_id_test" class="form-control custom-select">
                                            <?
                                            foreach ($all_countries as $country) {
                                                ?>
                                                <option value="<?php echo($country['ID']); ?>"><?php echo $country['Name'];  ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="saveCity()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_cities" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table id="basic-datatable" class="table table-hover mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">City</th>
                        <th scope="col">State</th>
                        <th scope="col">Country</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_cities as $city) {
                            $counter++;
                            /*if(strcasecmp($city['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }*/

                            $get_state_dets = $conn->query("SELECT * FROM States WHERE ID = '".$city['State_ID']."'");
                            $state_dets = mysqli_fetch_assoc($get_state_dets);

                            /*$get_country_dets = $conn->query("SELECT * FROM Countries WHERE ID = '".$city['Country_ID']."'");
                            $country_dets = mysqli_fetch_assoc($get_country_dets);*/
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $city['Name']; ?></td>
                                <td><?php echo $state_dets['Name']; ?></td>
                                <td><?php echo $country_dets['Name']; ?></td>
                                <!--<td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>-->
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editcitymodal<?php echo $counter; ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editcitymodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Course</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <input type="hidden" id="city_id<?php echo $counter; ?>" value="<?php echo $city['ID']; ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label"
                                                                for="simpleinput">City</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_city_name<?php echo $counter; ?>" placeholder="City" value="<?php echo $city['Name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">States</label>
                                                            <div class="col-lg-10">
                                                                <select id="cities_new_state_id<?php echo $counter; ?>" class="form-control custom-select">
                                                                    <?php
                                                                    foreach ($all_states as $state) {
                                                                        ?>
                                                                        <option value="<?php echo($state['ID']); ?>"><?php echo $state['Name'];  ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Country</label>
                                                            <div class="col-lg-10">
                                                                <select id="cities_new_country_id_test<?php echo $counter; ?>" class="form-control custom-select">
                                                                    <?php
                                                                    foreach ($all_countries as $country) {
                                                                        ?>
                                                                        <option value="<?php echo($country['ID']); ?>"><?php echo $country['Name'];  ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateCity(<?php echo $counter; ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletecitymodal<?php echo $counter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletecitymodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_city_id<?php echo $counter; ?>" value="<?php echo $city['ID']; ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteCity(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
  function saveCity() {
    var city_name = $('#city_name').val();
    var state_id = $('#cities_state_id').val();
    var country_id = $('#cities_country_id_test').val();
    $.ajax
    ({
      type: "POST",
      url: "settings_pages/ajax_city/add_city.php",
      data: { "city_name": city_name, "state_id": state_id, "country_id":  country_id},
      success: function (data) {
        $('#addcitymodal').modal('hide');
        if(data.match("true")) {
            toastr.success('City added successfully');
            $("#accordionExample").load(location.href + " #accordionExample" , function () {
                $("#headingTen").click();
            });
        }
        else {
            toastr.error('Unable to add city');
        }
      }
    });
    return false;
  }
</script>

<script>
    function updateCity(id) {
        var city_id = $('#city_id'.concat(id)).val();
        var new_state_id = $('#cities_new_state_id'.concat(id)).val();
        var new_country_id = $('#cities_new_country_id_test'.concat(id)).val();
        var new_city_name = $('#new_city_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_city/update_city.php",
          data: { "city_id": city_id, "new_state_id": new_state_id, "new_country_id": new_country_id, "new_city_name":  new_city_name},
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('City updated successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingTen").click();
                });
            }
            else {
                toastr.error('Unable to update city');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteCity(id) {
        var city_id = $('#delete_city_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_city/delete_city.php",
          data: { "city_id": city_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('City deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingTen").click();
                });
            }
            else {
                toastr.error('Unable to delete city');
            }
          }
        });
        return false;
    }
</script>