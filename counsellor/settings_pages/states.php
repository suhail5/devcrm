<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_countries = array();
$all_states = array();

$countries_query_res = $conn->query("SELECT * FROM Countries WHERE ID = 101");
while($row = $countries_query_res->fetch_assoc()) {
    $all_countries[] = $row;
}

$states_query_res = $conn->query("SELECT * FROM States WHERE Country_ID = 101");
while ($row = $states_query_res->fetch_assoc()) {
    $all_states[] = $row;
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
        <div class="card-header" id="headingNine"><h5 class="m-0 font-size-16">States <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addstatesmodal"> <i class="uil uil-plus-circle"></i> Add States</button>
            <!----Add Course Modal-------->
            <div class="modal fade" id="addstatesmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New States</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">States</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="state_name" placeholder="States">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Country</label>
                                    <div class="col-lg-10">
                                        <select id="states_country_id" class="form-control custom-select">
                                            <?
                                            foreach ($all_countries as $country) {
                                                ?>
                                                <option value="<?php echo($country['ID']); ?>"><?php echo $country['Name'];  ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="saveState()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_states" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table id="basic-datatable" class="table table-hover mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">State</th>
                        <th scope="col">Country</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_states as $state) {
                            $counter++;
                            

                            $get_country_dets = $conn->query("SELECT * FROM Countries WHERE ID = 101");
                            $country_dets = mysqli_fetch_assoc($get_country_dets);
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $state['Name']; ?></td>
                                <td><?php echo $country_dets['Name']; ?></td>
                                <!--<td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>-->
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editstatemodal<?php echo($counter); ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editstatemodal<?php echo($counter); ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit State</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <input type="hidden" id="state_id<?php echo($counter); ?>" value="<?php echo $state['ID']; ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label"
                                                                for="simpleinput">State</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_state_name<?php echo($counter); ?>" placeholder="State Name" value="<?php echo $state['Name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Country</label>
                                                            <div class="col-lg-10">
                                                                <select id="states_new_country_id<?php echo($counter); ?>" class="form-control custom-select">
                                                                    <?php
                                                                    foreach ($all_countries as $country) {
                                                                        ?>
                                                                        <option value="<?php echo($country['ID']); ?>"><?php echo $country['Name'];  ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateState(<?php echo($counter); ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletestatemodal<?php echo($counter); ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletestatemodal<?php echo($counter); ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_state_id<?php echo($counter); ?>" value="<?php echo $state['ID']; ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteState(<?php echo($counter); ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
	function saveState() {
		var state_name = $('#state_name').val();
      var country_id = $('#states_country_id').val();

      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_state/add_state.php",
          data: { "state_name": state_name, "country_id": country_id },
          success: function (data) {
            $('#addstatesmodal').modal('hide');
            if(data.match("true")) {
                toastr.success('State added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingNine").click();
                });
            }
            else {
                toastr.error('Unable to add state');
            }
          }
        });
        return false;
	}
</script>

<script>
    function updateState(id) {
        var state_id = $('#state_id'.concat(id)).val();
        var new_country_id = $('#states_new_country_id'.concat(id)).val();
        var new_state_name = $('#new_state_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_state/update_state.php",
          data: { "state_id": state_id, "new_country_id": new_country_id, "new_state_name": new_state_name },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('State updated successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingNine").click();
                });
            }
            else {
                toastr.error('Unable to update state');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteState(id) {
        var state_id = $('#delete_state_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_state/delete_state.php",
          data: { "state_id": state_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('State deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingNine").click();
                });
            }
            else {
                toastr.error('Unable to delete state');
            }
          }
        });
        return false;
    }
</script>