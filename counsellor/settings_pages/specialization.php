<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_courses = array();
$all_institutes = array();
$all_specializations = array();

$courses_query_res = $conn->query("SELECT * FROM Courses");
while($row = $courses_query_res->fetch_assoc()) {
    $all_courses[] = $row;
}

$institutes_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> '0'");
while ($row = $institutes_query_res->fetch_assoc()) {
    $all_institutes[] = $row;
}

$specializations_query_res = $conn->query("SELECT * FROM Specializations WHERE Institute_ID  = '".$_SESSION['INSTITUTE_ID']."'");
while ($row = $specializations_query_res->fetch_assoc()) {
    $all_specializations[] = $row;
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
        <div class="card-header" id="headingSeven"><h5 class="m-0 font-size-16">Specializations <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addspecializationmodal"> <i class="uil uil-plus-circle"></i> Add Specialization</button>
            <!----Add Specialization Modal-------->
            <div class="modal fade" id="addspecializationmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Specialization</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">Specialization</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="specialization_name" placeholder="Specialization" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Course</label>
                                    <div class="col-lg-10">
                                        <select id="specialization_course_id" class="form-control custom-select">
                                            <?
                                            foreach ($all_courses as $course) {
                                                ?>
                                                <option value="<?php echo($course['ID']); ?>"><?php echo $course['Name'];  ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Institute</label>
                                    <div class="col-lg-10">
                                        <select id="specialization_institute_id_test" class="form-control custom-select">
                                            <?
                                            foreach ($all_institutes as $institute) {
                                                ?>
                                                <option value="<?php echo($institute['ID']); ?>"><?php echo $institute['Name'];  ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="saveSpecialization()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_specializations" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Specialization</th>
                        <th scope="col">Course</th>
                        <th scope="col">Institute</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_specializations as $specialization) {
                            $counter++;
                            if(strcasecmp($specialization['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }

                            $get_course_dets = $conn->query("SELECT * FROM Courses WHERE ID = '".$specialization['Course_ID']."'");
                            $course_dets = mysqli_fetch_assoc($get_course_dets);

                            $get_institute_dets = $conn->query("SELECT * FROM Institutes WHERE ID = '".$specialization['Institute_ID']."'");
                            $institute_dets = mysqli_fetch_assoc($get_institute_dets);
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $specialization['Name']; ?></td>
                                <td><?php echo $course_dets['Name']; ?></td>
                                <td><?php echo $institute_dets['Name']; ?></td>
                                <td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editspecializationmodal<?php echo $counter; ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editspecializationmodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Course</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <div class="form-group row">
                                                            <input type="hidden" id="specialization_id<?php echo $counter; ?>" value="<?php echo $specialization['ID']; ?>">
                                                            <label class="col-lg-2 col-form-label"
                                                                for="simpleinput">Specialization</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_specialization_name<?php echo $counter; ?>" placeholder="Specialization" value="<?php echo $specialization['Name'] ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Course</label>
                                                            <div class="col-lg-10">
                                                                <select id="specialization_new_course_id<?php echo $counter; ?>" class="form-control custom-select">
                                                                    <?php
                                                                    foreach ($all_courses as $course) {
                                                                        ?>
                                                                        <option value="<?php echo($course['ID']); ?>"><?php echo $course['Name'];  ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Institute</label>
                                                            <div class="col-lg-10">
                                                                <select id="specialization_new_institute_id_test<?php echo $counter; ?>" class="form-control custom-select">
                                                                    <?php
                                                                    foreach ($all_institutes as $institute) {
                                                                        ?>
                                                                        <option value="<?php echo($institute['ID']); ?>"><?php echo $institute['Name'];  ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateSpecialization(<?php echo $counter; ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletespecializationmodal<?php echo $counter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletespecializationmodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post">
                                                        <input type="hidden" id="delete_specialization_id<?php echo $counter; ?>" value="<?php echo $specialization['ID']; ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteSpecialization(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?php
                        }

                        ?>
                    </tbody>
                </table>
            </div>      
        </div>
    </div>
</div>

<script>
    function saveSpecialization() {
        var specialization_name = $('#specialization_name').val();
      var course_id = $('#specialization_course_id').val();
      var institute_id = $('#specialization_institute_id_test').val();

      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_specialization/add_specialization.php",
          data: { "specialization_name": specialization_name, "course_id": course_id, "institute_id":  institute_id},
          success: function (data) {
            $('#addspecializationmodal').modal('hide');
            if(data.match("true")) {
                toastr.success('Specialization added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingSeven").click();
                });
            }
            else {
                toastr.error('Unable to add specialization');
            }
          }
        });
        return false;
    }
</script>

<script>
    function updateSpecialization(id) {
        var specialization_id = $('#specialization_id'.concat(id)).val();
        var new_course_id = $('#specialization_new_course_id'.concat(id)).val();
        var new_institute_id = $('#specialization_new_institute_id_test'.concat(id)).val();
        var new_specialization_name = $('#new_specialization_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_specialization/update_specialization.php",
          data: { "specialization_id": specialization_id, "new_course_id": new_course_id, "new_institute_id": new_institute_id, "new_specialization_name":  new_specialization_name},
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Specialization updated successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingSeven").click();
                });
            }
            else {
                toastr.error('Unable to update specialization');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteSpecialization(id) {
        var specialization_id = $('#delete_specialization_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_specialization/delete_specialization.php",
          data: { "specialization_id": specialization_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Specialization deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingSeven").click();
                });
            }
            else {
                toastr.error('Unable to delete specialization');
            }
          }
        });
        return false;
    }
</script>