<?php require "filestobeincluded/db_config.php" ?>
<?php
    $all_users = array();
    $users_query_res = $conn->query("SELECT *, CAST(AES_DECRYPT(password, '60ZpqkOnqn0UQQ2MYTlJ') AS CHAR(50)) pass FROM users");
    while ($row = $users_query_res->fetch_assoc()) {
        $all_users[] = $row;
    }
?>

<?php

$all_institutes = array();

$institutes_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> '0'");
while($row = $institutes_query_res->fetch_assoc()) {
    $all_institutes[] = $row;
}
$theins = "";
foreach ($all_institutes as $ins) {
	$theins = $theins."<option selected value='".$ins['ID']."'>".$ins['Name']."</option>";
}

?>

    <br><br>
    <div id="all_users">
    <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
    <table id="datatable-multiple" class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Role</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $user_counter=0;
                foreach($all_users as $au){
                    $user_counter++;
                    $users_employee_id = $au['ID'];
                    $full_name_users = $au['Name'];
                    $users_email = strtolower($au['Email']);
                    $users_email_pass = $au['Email_Password'];
                    $users_mobile = $au['Mobile'];
                    $users_role = $au['Role'];
                    $users_pass = $au['pass'];
                    $users_designation = $au['Designation'];
                    $users_id = $au['ID'];
                    $users_status = $au['Status'];
                    if(strcasecmp($users_status, 'Y')==0){
                        $status_user = 'checked';
                    }else{
                        $status_user= 'not';
                    }

                    $get_reporting_user_dets = $conn->query("SELECT * FROM users WHERE ID = '".$au['Reporting_To_User_ID']."'");
                    $reporting_user_dets = mysqli_fetch_assoc($get_reporting_user_dets);
                
            ?>
            <tr>
            <td><?php echo $user_counter; ?></td>
            <td><?php echo $full_name_users; ?></td>
            <td><?php echo $users_email; ?></td>
            <td><?php echo $users_mobile; ?></td>
            <td><?php echo $users_role; ?></td>
            </tr>
                <?php } ?>
        </tbody>
    </table>
    </div>