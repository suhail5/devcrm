<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_institutes = array();
$all_courses = array();

$institutes_query_res = $conn->query("SELECT * FROM Institutes  WHERE ID <> '0'");
while($row = $institutes_query_res->fetch_assoc()) {
    $all_institutes[] = $row;
}

$courses_query_res = $conn->query("SELECT * FROM Courses");
while ($row = $courses_query_res->fetch_assoc()) {
    $all_courses[] = $row;
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
        <div class="card-header" id="headingSix"><h5 class="m-0 font-size-16">Courses <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addcoursemodal"> <i class="uil uil-plus-circle"></i> Add Course</button>
            <!----Add Course Modal-------->
            <div class="modal fade" id="addcoursemodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Course</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">Course</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="course_name" placeholder="Course Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Institute</label>
                                    <div class="col-lg-10">
                                        <select id="courses_institute_id" class="form-control custom-select">
                                            <?
                                            foreach ($all_institutes as $institute) {
                                                ?>
                                                <option value="<?php echo($institute['ID']); ?>"><?php echo $institute['Name'];  ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="addCourse()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_courses" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Course</th>
                        <th scope="col">Institute</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_courses as $course) {
                            $counter++;
                            if(strcasecmp($course['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }

                            $get_ins_dets = $conn->query("SELECT * FROM Institutes WHERE ID = '".$course['Institute_ID']."'");
                            $ins_dets = mysqli_fetch_assoc($get_ins_dets);
                            ?>
                            <tr>
                                <td scope="row"><?php echo $counter; ?></td>
                                <td><?php echo $course['Name']; ?></td>
                                <td><?php echo $ins_dets['Name']; ?></td>
                                <td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editcoursemodal<?php echo($counter); ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editcoursemodal<?php echo($counter); ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Course</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <input type="hidden" id="course_id<?php echo($counter); ?>" value="<?php echo $course['ID']; ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label" for="simpleinput">Course</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_course_name<?php echo($counter); ?>" placeholder="Course Name" value="<?php echo $course['Name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Institute</label>
                                                            <div class="col-lg-10">
                                                                <select id="courses_new_institute_id<?php echo($counter); ?>" class="form-control custom-select">
                                                                    <?php
                                                                    foreach ($all_institutes as $institute) {
                                                                        ?>
                                                                        <option value="<?php echo($institute['ID']); ?>"><?php echo $institute['Name'];  ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateCourse(<?php echo($counter); ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletecoursemodal<?php echo($counter); ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletecoursemodal<?php echo($counter); ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_course_id<?php echo($counter); ?>" value="<?php echo $course['ID']; ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteCourse(<?php echo($counter); ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function addCourse() {
        var course_name = $('#course_name').val();
      var institute_id = $('#courses_institute_id').val();
      console.log("ok");
      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_course/add_course.php",
          data: { "course_name": course_name, "institute_id": institute_id },
          success: function (data) {
            $('#addcoursemodal').modal('hide');
            if(data.match("true")) {
                toastr.success('Course added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingSix").click();
                });
            }
            else {
                toastr.error('Unable to add course');
            }
          }
        });
        return false;
    }
</script>

<script>
    function updateCourse(id) {
        var course_id = $('#course_id'.concat(id)).val();
        var new_institute_id = $('#courses_new_institute_id'.concat(id)).val();
        var new_course_name = $('#new_course_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_course/update_course.php",
          data: { "course_id": course_id, "new_institute_id": new_institute_id, "new_course_name": new_course_name },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Course updated successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingSix").click();
                });
            }
            else {
                toastr.error('Unable to update course');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteCourse(id) {
        var course_id = $('#delete_course_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_course/delete_course.php",
          data: { "course_id": course_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Course deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingSix").click();
                });
            }
            else {
                toastr.error('Unable to delete course');
            }
          }
        });
        return false;
    }
</script>