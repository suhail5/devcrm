<?php include 'filestobeincluded/header-top.php' ?>
<?php include 'filestobeincluded/header-bottom.php' ?>
<?php include 'filestobeincluded/navigation.php' ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12 d-flex justify-content-between align-items-center flex-row-reverse">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                            <button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Refresh data" onclick="window.location.reload();"><i data-feather="rotate-ccw" class="icon-sm"></i></button>
                            </nav>
                            <h4 class="mb-0 mt-0"><img class="call-img" src="assets/images/dashboard.png"> Dashboard</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->

                <div class="row">
                    <div  class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                            <center><p> Last Updated on</p>
                                <font style="font-size: 18px;line-height:38px;"><b><?php echo date("j M, h:i A"); ?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total Leads</p>
                                <font style="font-size: 26px;"><b><?php $get_all_leads = $conn->query("SELECT COUNT(ID) FROM Leads WHERE Counsellor_ID = '".$_SESSION['useremployeeid']."'"); echo mysqli_fetch_row($get_all_leads)[0]?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total Emails Sent</p>
                                <font style="font-size: 26px;"><b><?php $get_all_leads = $conn->query("SELECT COUNT(ID) FROM Email_Logs WHERE Employee_ID = '".$_SESSION['useremployeeid']."'"); echo mysqli_fetch_row($get_all_leads)[0]?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total SMS Sent</p>
                                <font style="font-size: 26px;"><b><?php $get_all_leads = $conn->query("SELECT COUNT(ID) FROM SMS_Logs WHERE Employee_ID = '".$_SESSION['useremployeeid']."'"); echo mysqli_fetch_row($get_all_leads)[0]?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Total Follow-ups</p>
                                <font style="font-size: 26px;"><b><?php $get_all_leads = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp FROM Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID WHERE Follow_Ups.Counsellor_ID = '".$_SESSION['useremployeeid']."' AND (Leads.Stage_ID  != 3 AND Leads.Stage_ID != 6) AND (Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."') GROUP BY Lead_ID ORDER BY Follow_Ups.ID DESC"); echo mysqli_num_rows($get_all_leads)?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Done Follow-ups</p>
                                <font style="font-size: 26px;"><b><?php $get_all_leads = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp, Follow_Ups.Follow_Up_Status as F_Status FROM Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID WHERE Follow_Ups.Counsellor_ID  = '".$_SESSION['useremployeeid']."' AND Follow_Ups.Follow_Up_Status='YES' AND (Leads.Stage_ID  != 3 AND Leads.Stage_ID != 6) AND (Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."') GROUP BY Lead_ID ORDER BY Follow_Ups.ID DESC"); echo mysqli_num_rows($get_all_leads)?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Missed Follow-ups</p>
                                <font style="font-size: 26px;"><b><?php
                                    date_default_timezone_set('Asia/Kolkata');
                                    $current_timestamp = date('Y-m-d h:i:s');
                                $get_all_leads = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp, Follow_Ups.Follow_Up_Status as F_Status FROM Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID WHERE Follow_Ups.Counsellor_ID  = '".$_SESSION['useremployeeid']."' AND Follow_Ups.Follow_Up_Status='NO' AND Followup_Timestamp < '$current_timestamp' AND (Leads.Stage_ID  != 3 AND Leads.Stage_ID != 6) AND (Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."') GROUP BY Lead_ID ORDER BY Follow_Ups.ID DESC"); echo mysqli_num_rows($get_all_leads)?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                            <center><p>Planned Follow-ups</p>
                                <font style="font-size: 26px;"><b><?php $get_all_leads = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp FROM Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID WHERE Follow_Ups.Counsellor_ID = '".$_SESSION['useremployeeid']."' AND Follow_Ups.Followup_Timestamp > NOW() AND (Leads.Stage_ID  != 3 AND Leads.Stage_ID != 6) AND (Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."') GROUP BY Lead_ID ORDER BY Follow_Ups.ID DESC"); echo mysqli_num_rows($get_all_leads)?></b></font></center>
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-4">
                        <div class="card">
                            <div class="card-body mb-0">
                            <center><p>Admission Done</p></center>
                            <div class="row mb-0">
                                <div class="col-md-6 text-left mb-0">
                                    <p>Previous Month</p>
                                    <p style="font-size: 22px;"><b>
                                    <?php 
                                        $start_date = date("Y-n-j", strtotime("first day of previous month"));
                                        $end_date = date("Y-n-j", strtotime("last day of previous month"));
                                        $get_preadmission_count = $conn->query("SELECT COUNT(ID) as lead_count FROM Leads WHERE Stage_ID = '5' AND (TimeStamp BETWEEN '$start_date. 00:00:00' AND '$end_date. 23:59:59') AND Counsellor_ID = '".$_SESSION['useremployeeid']."'");
                                        $gpac = mysqli_fetch_assoc($get_preadmission_count);
                                        echo $gpac['lead_count'];
                                    ?>
                                    </b></p>
                                </div>
                                <div class="col-md-6 text-right mb-0">
                                    <p>This Month</p>
                                    <p style="font-size: 22px;"><b><?php 
                                        $start_dates = date('Y-m-01'); // hard-coded '01' for first day
                                        $end_dates = date('Y-m-t');
                                        $get_admission_count = $conn->query("SELECT COUNT(ID) as lead_count FROM Leads WHERE Stage_ID = '5' AND (TimeStamp BETWEEN'$start_dates. 00:00:00' AND '$end_dates. 23:59:59') AND Counsellor_ID = '".$_SESSION['useremployeeid']."'");
                                        $gac = mysqli_fetch_assoc($get_admission_count);
                                        echo $gac['lead_count'];
                                    ?></b></p>
                                </div>
                                
                            </div>
                                
                            </div>
                        </div>
                    </div>
                    <div  class="col-lg-4">
                        <div class="card">
                            <div class="card-body mb-0">
                            <center><p>Today's Record</p></center>
                            <div class="row mb-0">
                                <div class="col-md-4 text-left mb-0">
                                    <p>Attempts Done</p>
                                    <p style="font-size: 22px;"><b>
                                    <?php 
                                        $today_date = date('Y-m-d');
                                        $get_attempt_count = $conn->query("SELECT COUNT(ID) as lead_count FROM History WHERE Created_At BETWEEN '$today_date 00:00:00' AND '$today_date 23:59:59' AND Counsellor_ID = '".$_SESSION['useremployeeid']."'");
                                        $gac = mysqli_fetch_assoc($get_attempt_count);
                                        echo $gac['lead_count'];
                                    ?>
                                    </b></p>
                                </div>
                                <div class="col-md-4 text-center">
                                    <p>New Leads</p>
                                    <p style="font-size: 22px;"><b><?php 
                                        $today_date = date('Y-m-d'); // hard-coded '01' for first day
                                        $get_today_count = $conn->query("SELECT COUNT(ID) as lead_count FROM Leads WHERE Created_At BETWEEN '$today_date 00:00:00' AND '$today_date 23:59:59' AND Counsellor_ID = '".$_SESSION['useremployeeid']."' AND Stage_ID = 1");
                                        $gtc = mysqli_fetch_assoc($get_today_count);
                                        echo $gtc['lead_count'];
                                    ?></b></p>
                                </div>
                                <div class="col-md-4 text-right mb-0">
                                    <p>Total Lead</p>
                                    <p style="font-size: 22px;"><b><?php 
                                        $today_date = date('Y-m-d'); // hard-coded '01' for first day
                                        $get_today_count = $conn->query("SELECT COUNT(ID) as lead_count FROM Leads WHERE Created_At BETWEEN '$today_date 00:00:00' AND '$today_date 23:59:59' AND Counsellor_ID = '".$_SESSION['useremployeeid']."'");
                                        $gtc = mysqli_fetch_assoc($get_today_count);
                                        echo $gtc['lead_count'];
                                    ?></b></p>
                                </div>
                            </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label>Creation Date</label>
                                    <input type="text" class="form-control datepicker_range" id="lead_date" onchange="date_select();" placeholder="Select Date">
                                    <input type="hidden" id="stage">
                                    <input type="hidden" id="course">
                                    <input type="hidden" id="counsellor">
                                    <input type="hidden" id="university">
                                    <input type="hidden" id="manager">
                                    <input type="hidden" id="source">
                                </div>

                                <div class="form-group mb-3">
                                    <label>Update Date</label>
                                    <input type="text" class="form-control datepicker_range" id="update_date" onchange="update_date_select();" placeholder="Select Date">
                                </div>
                                    
                            </div>
                        </div>
                    </div>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mt-0 mb-0">Stage wise Leads</h6>
                                    <div id="stage_chart" style="height: 300px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body pb-0">
                                <h6 class="card-title mb-0">Course wise Leads</h6>
                                    <div id="piechart" style="height: 320px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body pb-0" style="display:none;">
                                <h6 class="card-title mt-0 mb-0">Counsellor wise Leads</h6>
                                <div id="counsellor_chart" style="height: 400px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
                    

            <div class="col-lg-3">
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body pb-0">
                                <h6 class="card-title mb-0">University Wise Leads</h6>
                                    <div id="university_chart" style="height: 320px; padding-top:30px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12" style="display:none;">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mt-0 mb-0">Manager wise Report</h6><br />
                                    <div id="manager_chart" style="padding-top:10px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12" style="display:none;">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mt-0 mb-0">Source wise Report</h6><br />
                                    <div id="source_chart" style="padding-top:10px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
                </div>

            </div> <!-- content -->


<!----------------Stage Chart---------------------->
<script type="text/javascript">
var manager = $('#manager').val();
var counsellor = $('#counsellor').val();
var course = $('#course').val();
var stage = $('#stage').val();
var source = $('#source').val();
var university = $('#university').val();
var date = $('#lead_date').val();
var update_date = $('#update_date').val();




    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart_stage);

    function drawChart_stage() {
        var manager = $('#manager').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var source = $('#source').val();
        var university = $('#university').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        
        var Data = $.ajax({
            type:'POST',
            url:'charts/stage_chart.php',
            dataType: "json",
            data:{"manager":manager, "university":university, "counsellor":counsellor, "course":course, "source":source, "date":date, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        var data = new google.visualization.arrayToDataTable(Data);
    var options = {
        legend: {
            position: 'none'
        },
        bars: 'horizontal',
        
    };

    var chart = new google.charts.Bar(document.getElementById('stage_chart'));
    chart.draw(data, google.charts.Bar.convertOptions(options));
    $(window).resize(function() {
      chart.draw(data, google.charts.Bar.convertOptions(options));
    });
    google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selection = chart.getSelection();
                var message = '';
                for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                var category = data
                .getValue(chart.getSelection()[0].row, 0)
                message += '' + category + '';
                }
                }
                if (message == '') {
                message = '';
                }
                $('#stage').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }
            }
    }
</script>
<!----------------Stage Chart END---------------------->



<!---------------------Course Chart--------------------->
<script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChartcourse);
      function drawChartcourse() {
        var manager = $('#manager').val();
        var counsellor = $('#counsellor').val();
        var stage = $('#stage').val();
        var source = $('#source').val();
        var university = $('#university').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var CourseData = $.ajax({
            type:'POST',
            url:'charts/course_chart.php',
            dataType: "json",
            data:{"manager":manager, "university":university, "counsellor":counsellor, "stage":stage, "source":source, "date":date, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(CourseData);
        var data = new google.visualization.arrayToDataTable(CourseData);

        var options = {
          legend: 'none',
          pieSliceText: 'label',
          slices: {  4: {offset: 0.2},
                    12: {offset: 0.3},
                    14: {offset: 0.4},
                    15: {offset: 0.5},
          },
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
        $(window).resize(function() {
            chart.draw(data, options);
        });
        google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selectedItem = chart.getSelection()[0];

                if (selectedItem) 
                {
                    var topping = data.getValue(selectedItem.row, 0);
                    $('#course').val(topping);
                }else{
                    $('#course').val('');
                }
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }

            }
      }
</script>
<!---------------------Course Chart END--------------------->

<!---------------------Counsellor Chart--------------------->
<script type="text/javascript">

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(counsellor_chart_draw);
      function counsellor_chart_draw() {  
        var manager = $('#manager').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var source = $('#source').val();
        var university = $('#university').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();        
            var Data = $.ajax({
            type:'POST',
            url:'charts/counsellor_chart.php',
            dataType: "json",
            data:{"manager":manager, "university":university, "course":course, "stage":stage, "source":source, "date":date, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        var data = new google.visualization.arrayToDataTable(Data);
        

        var options = {
            legend: {
                position: 'none'
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('counsellor_chart'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
        $(window).resize(function() {
            chart.draw(data, google.charts.Bar.convertOptions(options));
        });
        google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selection = chart.getSelection();
                var message = '';
                for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                var category = data
                .getValue(chart.getSelection()[0].row, 0)
                message += '' + category + '';
                }
                }
                if (message == '') {
                    message = '';
                    var manager = $('#manager').val('');
                    var counsellor = $('#counsellor').val('');
                    var course = $('#course').val('');
                    var stage = $('#stage').val('');
                    var source = $('#source').val('');
                    var university = $('#university').val('');
                    counsellor_chart_draw();
                }
                $('#counsellor').val(message);
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }
                if(($('#manager').val()).length==0){
                    drawTable_manager();
                }
            }
        }

</script>
<!---------------------Counsellor Chart END--------------------->




<!---------------------University Chart--------------------->
<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart_univ);

      function drawChart_univ() {
        var manager = $('#manager').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var source = $('#source').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/university_chart.php',
            dataType: "json",
            data:{"manager":manager, "counsellor":counsellor, "course":course, "stage":stage, "source":source, "date":date, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        var data = new google.visualization.arrayToDataTable(Data);

        var options = {
            legend: {
                position: 'none'
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('university_chart'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
        $(window).resize(function() {
            chart.draw(data, google.charts.Bar.convertOptions(options));
        });
        google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selection = chart.getSelection();
                var message = '';
                for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                var category = data
                .getValue(chart.getSelection()[0].row, 0)
                message += '' + category + '';
                }
                }
                if (message == '') {
                message = '';
                }
                $('#university').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#manager').val()).length==0){
                    drawTable_manager();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }
            }
      }
</script>
<!---------------------University Chart END--------------------->




<!---------------------Manager Chart--------------------->
<script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable_manager);

      function drawTable_manager() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Manager');
        data.addColumn('number', 'Lead Count');
        var university = $('#university').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var source = $('#source').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/manager_chart.php',
            dataType: "json",
            data:{"university":university, "counsellor":counsellor, "course":course, "stage":stage, "source":source, "date":date, "update_date":update_date},
            async: false
            }).responseJSON;
        
        console.log(Data);
        //var data = new google.visualization.arrayToDataTable(Data);


        data.addRows(Data);
        //data.sort({column: 1, desc: true});


        var manager_table = new google.visualization.Table(document.getElementById('manager_chart'));
        manager_table.draw(data, {width: '100%', height: '100%'});
        google.visualization.events.addListener(manager_table, 'select', selectHandler);

            // The selection handler.
            // Loop through all items in the selection and concatenate
            // a single message from all of them.
            function selectHandler() {
            var selection = manager_table.getSelection();
            var message = '';
            for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                message += '' + str + '';
                } else if (item.row != null) {
                var str = data.getFormattedValue(item.row, 0);
                message += '' + str + '';
                } else if (item.column != null) {
                var str = data.getFormattedValue(0, item.column);
                message += '' + str + '';
                }
            }
            if (message == '') {
                message = '';
            }
                $('#manager').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#source').val()).length==0){
                    drawTable_source(); 
                }

            }
        }
      
</script>
<!---------------------Manager Chart END--------------------->





<!---------------------Source Chart--------------------->
<script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable_source);

      function drawTable_source() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Sources');
        data.addColumn('number', 'Lead Count');
        var university = $('#university').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var manager = $('#manager').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/source_chart.php',
            dataType: "json",
            data:{"university":university, "counsellor":counsellor, "course":course, "stage":stage, "manager":manager, "date":date, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        data.addRows(Data);
        //data.sort({column: 1, desc: true});


        var table = new google.visualization.Table(document.getElementById('source_chart'));
        table.draw(data, {width: '100%', height: '100%'});
        google.visualization.events.addListener(table, 'select', selectHandler);

            // The selection handler.
            // Loop through all items in the selection and concatenate
            // a single message from all of them.
            function selectHandler() {
            var selection = table.getSelection();
            var message = '';
            for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                message += '' + str + '';
                } else if (item.row != null) {
                var str = data.getFormattedValue(item.row, 0);
                message += '' + str + '';
                } else if (item.column != null) {
                var str = data.getFormattedValue(0, item.column);
                message += '' + str + '';
                }
            }
            if (message == '') {
                message = '';
            }
                $('#source').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                
        }
      }
</script>
<!---------------------Source Chart END--------------------->

<Script>
function date_select(){
    var manager = $('#manager').val('');
    var counsellor = $('#counsellor').val('');
    var course = $('#course').val('');
    var stage = $('#stage').val('');
    var source = $('#source').val('');
    var university = $('#university').val('');
    var update_date = $('#update_date').val('');

    counsellor_chart_draw();
    drawChartcourse();
    drawChart_stage();
    drawChart_univ();
    drawTable_manager();
    drawTable_source();
}

function update_date_select(){
    var manager = $('#manager').val('');
    var counsellor = $('#counsellor').val('');
    var course = $('#course').val('');
    var stage = $('#stage').val('');
    var source = $('#source').val('');
    var university = $('#university').val('');
    var date = $('#lead_date').val('');

    counsellor_chart_draw();
    drawChartcourse();
    drawChart_stage();
    drawChart_univ();
    drawTable_manager();
    drawTable_source();
}
</script>

<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>
