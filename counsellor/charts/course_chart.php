<?php 
    include '../filestobeincluded/db_config.php';
    if(session_status() === PHP_SESSION_NONE) session_start();
    
    if(!empty($_POST['manager'])){
        $manager = $_POST['manager'];
        $exp = explode('(', $manager);
        $manager_id = rtrim($exp[1], ')');
        $manager_query = "AND (U1.ID = '".$_SESSION['useremployeeid']."' OR Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."')";
    }else{
        $manager_query = "AND (U1.ID = '".$_SESSION['useremployeeid']."' OR Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."')";
    }

    if(!empty($_POST['counsellor'])){
        $couns = $_POST['counsellor'];
        $exp = explode('(', $couns);
        $couns_id = rtrim($exp[1], ')');
        $couns_query = "AND Leads.Counsellor_ID = '$couns_id'";
    }else{
        $couns_query = ' ';
    }

    if(!empty($_POST['stage'])){
        $stage = $_POST['stage'];
        $get_stage_id = $conn->query("SELECT ID FROM Stages WHERE Name like '$stage'");
        $gsi = mysqli_fetch_assoc($get_stage_id);
        $stage_query = "AND Leads.Stage_ID = '".$gsi['ID']."'";
    }else{
        $stage_query = ' ';
    }

    if(!empty($_POST['university'])){
        $univ = $_POST['university'];
        $get_univ_id = $conn->query("SELECT ID FROM Institutes WHERE Name like '$univ'");
        $gui = mysqli_fetch_assoc($get_univ_id);
        $univ_query = "AND Leads.Institute_ID = '".$gui['ID']."'";
    }else{
        $univ_query = ' ';
    }

    if(!empty($_POST['source'])){
        $source = $_POST['source'];
        $get_source_id = $conn->query("SELECT ID FROM Sources WHERE Name like '$source'");
        $gsourcei = mysqli_fetch_assoc($get_source_id);
        $source_query = "AND Leads.Source_ID = '".$gsourcei['ID']."'";
    }else{
        $source_query = ' ';
    }

    if(!empty($_POST['date'])){
        if(strpos($_POST['date'],'to')>0)
        {
          $date = explode('to', $_POST['date']);
          $from_date = date($date[0]);
          $to_date = date($date[1]);
          $date_query = "AND (Leads.Created_at BETWEEN '$from_date' AND '$to_date')";
        }
        else{
          $from_date = date($_POST['date']);
          $date_query = "AND (Leads.Created_at BETWEEN '".$from_date." 00:00:00' AND '".$from_date." 23:59:59')";
        }
    }else{
        $date_query = ' ';
    }
    if(!empty($_POST['update_date'])){
        if(strpos($_POST['update_date'],'to')>0)
        {
          $date = explode('to', $_POST['update_date']);
          $from_date = date($date[0]);
          $to_date = date($date[1]);
          $update_date_query = "AND (Leads.TimeStamp BETWEEN '$from_date' AND '$to_date')";
        }
        else{
          $from_date = date($_POST['date']);
          $update_date_query = "AND (Leads.TimeStamp BETWEEN '".$from_date." 00:00:00' AND '".$from_date." 23:59:59')";
        }
    }else{
        $update_date_query = ' ';
    }

    $data3[] = Array('Courses', 'Lead Count');   
    
    $get_reason_count = $conn->query("SELECT COUNT(Leads.ID) as lead_count, Courses.Name AS Name, Institutes.Name AS univ FROM Leads LEFT JOIN users ON Leads.Counsellor_ID = users.ID LEFT JOIN users as U1 ON users.Reporting_To_User_ID = U1.ID LEFT JOIN Institutes ON Leads.Institute_ID = Institutes.ID LEFT JOIN Courses ON Leads.Course_ID = Courses.ID LEFT JOIN Stages ON Leads.Stage_ID = Stages.ID LEFT JOIN Sources ON Leads.Source_ID = Sources.ID WHERE users.Role != 'Administrator' ".$manager_query.$univ_query.$stage_query.$source_query.$couns_query.$date_query.$update_date_query." GROUP BY Leads.Course_ID ORDER BY lead_count DESC");
        while($grc = $get_reason_count->fetch_assoc()){
            if(is_null($grc['Name'])){
                $grc['Name'] = 'Unknown';
            }
            $data3[] = Array($grc['Name'].'-'.$grc['univ'], (int)$grc['lead_count']);
        }
        
        echo json_encode($data3);
        


?>