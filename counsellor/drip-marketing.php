<?php include 'filestobeincluded/header-top.php' ?>
<?php include 'filestobeincluded/header-bottom.php' ?>
<?php include 'filestobeincluded/navigation.php' ?>
        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#addmarketingrule">Add Rule</button>
                                <!---Rule Modal------>
                                    <div class="modal fade" id="addmarketingrule" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Add Rule</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p style="font-size: 12px; text-align:left;"><i>A new rule can be added using this dialog, you need to select Rules and actions to be performed based on the Rules</i></p>
                                                    
                                                        <div class="form-group row">
                                                            <div class="col-lg-6" style="padding-top: 20px;">
                                                                <input class="form-control" type="text" id="marketing_rule_name" placeholder="Name of the Rule">
                                                            </div>
                                                            <div class="col-lg-3" style="padding-top: 20px;">
                                                                <input type="text" id="basic-datepicker" class="form-control" placeholder="Start Date">
                                                            </div>
                                                            <div class="col-lg-3" style="padding-top: 20px;">
                                                                <input type="text" id="basic-timepicker" class="form-control" placeholder="Start Time">
                                                            </div>
                                                        </div>
                                                        <ul class="nav nav-tabs">
                                                            <li class="nav-item">
                                                                <a href="#ifcondition" data-toggle="tab" aria-expanded="false"
                                                                    class="nav-link active">
                                                                    <span class="d-block d-sm-none">IF</span>
                                                                    <span class="d-none d-sm-block">IF</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#thencondition" data-toggle="tab" aria-expanded="true"
                                                                    class="nav-link">
                                                                    <span class="d-block d-sm-none">THEN</span>
                                                                    <span class="d-none d-sm-block">THEN</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content p-3 text-muted">
                                                            <div class="tab-pane show active" id="ifcondition">
                                                                <div class="row" style="padding-top: 20px; padding-bottom:20px;">
                                                                    <div class="col-lg-3">
                                                                        <ul class="nav nav-pills navtab-bg nav-justified">
                                                                            <li class="nav-item">
                                                                                <a href="#and" data-toggle="tab" id="and" aria-expanded="false"
                                                                                    class="nav-link">
                                                                                    
                                                                                    <span class="d-none d-sm-block" >AND</span>
                                                                                </a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a href="#or" data-toggle="tab" id="or" aria-expanded="true"
                                                                                    class="nav-link active">
                                                                                    
                                                                                    <span class="d-none d-sm-block" >OR</span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <script>
                                                                    $('#and').click(function(){
                                                                        $('.operator').val('AND');
                                                                    });
                                                                    $('#or').click(function(){
                                                                        $('.operator').val('OR');
                                                                    });
                                                                </script>
                                                                
                                                                <div class="wrapper" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                                    <div class="element" style="display: block;" id="ele">
                                                                        <div class="row" style="padding-top: 15px;">
                                                                            <div class="col-xl-3 col-sm-12">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select" name="trigger_select[]">
                                                                                        <option disabled selected>Select Trigger</option>
                                                                                        <option value="city">City</option>
                                                                                        <option value="course">Course</option>
                                                                                        <option value="university">University</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-3 col-sm-12" id="balance_select">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select" name="balance_select[]">
                                                                                        <option disabled selected>Select Operator</option>
                                                                                        <option value="equalto">EqualsTo</option>
                                                                                        <option value="notequalto">NotEqualsTo</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-sm-12" id="data_select">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select" name="data_select[]">
                                                                                        <option disabled selected>Select Value</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-2 col-sm-12" id="more_buttons">
                                                                                <button class="btn btn-link remove" style="float: right;" onclick="removeFunction();"><i data-feather="x-square"></i></button>
                                                                                <button class="btn btn-link clone" style="float: right;" onclick="addFunction();"><i data-feather="plus-square"></i></button>
                                                                            </div>
                                                                            
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="results" id="result">
                                                                    </div>
                                                                    
                                                                    
                                                                </div>

                                                                
                                                                <script>
                                                                    function addFunction() {
                                                                        let final_div = document.getElementById("result");
                                                                        let div = document.createElement('div');
                                                                        div.id = 'operator_value';
                                                                        div.class = 'col-xl-12 col-sm-12';
                                                                        div.innerHTML = '<input type="text" disabled="" class="btn btn-primary btn-sm operator" id="operator_value" value="OR">';
                                                                        final_div.appendChild(div);
                                                                    }

                                                                    function removeFunction() {
                                                                        $('#result').children().last().remove();
                                                                        $('#result').children().last().remove();
                                                                    }

                                                                    $('.wrapper').on('click', '.clone', function() {
                                                                        $(".result").html($("#products-area").find(".mysets-area").clone().show());
                                                                        $('.clone').closest('.wrapper').find('.element').first().clone().appendTo('.results');
                                                                    });
                                                                    $('select').change(function() { 
                                                                        $.uniform.update('#' + $(this).attr('id')); 
                                                                    });
                                                                </script>
                                                            </div> 
                                                            <div class="tab-pane" id="thencondition">
                                                                <p>asdasdasda</p>
                                                            </div>

                                                            
                                                        </div>
                                                    
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                <!-----End Rule Modal-------->
                            </nav>
                            <h4 class="mb-1 mt-0">Drip Marketing</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->







            </div> <!-- content -->


<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>