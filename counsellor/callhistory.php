<?php include 'filestobeincluded/header-top.php' ?>
<script src="https://kit.fontawesome.com/3212b33ef4.js" crossorigin="anonymous"></script>
<?php include 'filestobeincluded/header-bottom.php' ?>

<style>
	th,
	td,
	p,
	input {
		font: 14px Verdana;
	}

	table,
	th,
	td {
		border: solid 1px #DDD;
		border-collapse: collapse;
		padding: 2px 3px;
		text-align: center;
	}

	th {
		font-weight: bold;
		text-transform: capitalize;
	}





	.col-sm-2.call-dtlmain {
		padding: 0px 3px;
	}

	.call-dtlbox {
		height: 100%;
		box-shadow: 0 2px 15px 0 rgb(0 0 0 / 3%);
		padding: 10px;
		border: 1px solid #eee;
	}

	.call-dtlbox input.form-control {
		height: 30px;
		border: none;
		color: #000000;
		padding-left: 0;
	}

	.call-dtlbox input.form-control:focus {
		box-shadow: none;
		outline: none;
	}

	.nav-pills {
		padding: 0 !important
	}

	.call-dtlbox p {
		font-size: 14px;
		text-transform: uppercase;
		font-weight: 600;
		color: #404040;
		margin: 0;
	}

	.call-dtlbox span {
		display: inline-block;
		/* margin-left: 12px; */
	}

	div#pills-tabContent button.btn.btn-primary {
		width: 160px;
		height: 47px;
	}

	ul#pills-tab li.nav-item {
		background: #eee;
		border: 1px solid #eee;
	}


	ul#pills-tab {
		margin: 0 !important;
	}

	div#pills-tabContent {
		border: 1px solid #eee;
		padding: 20px 10px;
	}

	ul#pills-tab li.nav-item a {
		padding: 16px 35px;
		font-weight: 500;
		border-radius: 0;
	}

	ul#pills-tab li.nav-item a:hover {
		background: #5369f8;
		color: #fff;
	}

	.daterangepicker.opensright:after {
		right: 45px !important;
		left: auto !important;
	}

	.daterangepicker.opensright:before {
		left: auto !important;
		right: 44px !important;
	}

	input#tofrom {
		background: #eee;
		border: 1px solid #828282;
	}

	.nav-pills .nav-link.active,
	.nav-pills .show>.nav-link {
		background: #5369f8;
		color: #fff !important;
	}
</style>

<!-- Pre-loader -->
<!-- <div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div> -->
<!-- End Preloader-->
<?php include 'filestobeincluded/navigation.php' ?>


<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
	<div class="content">

		<!-- Start Content-->
		<div class="container-fluid">
			<div class="row page-title">
				<div class="col-md-12">
					<nav aria-label="breadcrumb" class="float-right mt-1 navbuttons">
					</nav>
					<h4 class="mb-1 mt-0"><img class="call-img" src="assets/images/phone.png"> Call History</h4>
				</div>
			</div>
		</div> <!-- container-fluid -->


		<!-- <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
							<form id="checkbox-form" method="POST">

								<p id="showData"></p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div> -->



		<div class="row">
			<div class="col-lg-12 card card-body">
				<!-- <?php print_r($_SESSION); ?> -->
				<nav class="nav nav-pills">
					<ul class="nav nav-pills" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="pills-incoming-tab" data-toggle="pill" href="#pills-incoming" role="tab" aria-controls="pills-incoming" aria-selected="true">Incoming</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-Clickcall-tab" data-toggle="pill" href="#pills-Clickcall" role="tab" aria-controls="pills-Clickcall" aria-selected="false">Outgoing</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-Missed-tab" data-toggle="pill" href="#pills-Missed" role="tab" aria-controls="pills-Missed" aria-selected="false">Incoming Missed</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-Notanswered-tab" data-toggle="pill" href="#pills-Notanswered" role="tab" aria-controls="pills-Notanswered" aria-selected="false">Outgoing Not Answered</a>
						</li>
						<!-- <li class="nav-item">
						<a class="nav-link" id="pills-Broadcast-tab" data-toggle="pill" href="#pills-Broadcast" role="tab" aria-controls="pills-Broadcast" aria-selected="false">Broadcast</a>
					</li> -->
					</ul>
					<ul class="nav nav-pills ml-auto">
						<li class="nav-link">
							<input class="form-control" id="tofrom" type="button" name="datetimes">
						</li>
					</ul>
				</nav>



				<div class="tab-content" id="pills-tabContent">
					<div class="tab-pane fade show active" id="pills-incoming" role="tabpanel" aria-labelledby="pills-incoming-tab">
						<div class="container-fluid">
							<div class="row">
								<!-- <div class="col-sm-3 call-dtlbox">
									<div class="form-group mt-3 mt-sm-0">
										<label for="destination_number1">FROM</label>
										<input class="form-control" type="text" name="destination_number1" id="destination_number1" placeholder="Enter Client Number...">
									</div>
								</div> -->
								<div class="col-sm-3 ">
									<div class="call-dtlbox mb-1">

										<?php $q = $conn->query("SELECT Extension FROM users WHERE ID='" . $_SESSION['useremployeeid'] . "'");
										$ext = mysqli_fetch_row($q);
										?>
										<div class="form-group mt-3 mt-sm-0">
											<label>Agent</label>
											<input readonly type="text" class="form-control" value="<?php echo $ext[0]; ?>">
										</div>
									</div>
								</div>
								<!-- <div class="col-sm-3 call-dtlbox">
									<div class="form-group mt-3 mt-sm-0">
										<label>IVR</label>
										<select style="border: none;" class="form-control" id="ivr1">
											<option selected disabled>Choose</option>
											<option value="all">All</option>
											<option value="5326">Testing</option>
											<option value="5524">Thank You</option>
										</select>
									</div>
								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">
									<div class="form-group mt-3 mt-sm-0">
										<label>Departments</label>

										<select style="border: none;" class="form-control" id="department" name="department">
											<option selected disabled>Choose</option>
											<option value="all">All</option>
											<option value='11200'>Shri Venkateshwara University</option>
											<option value='11218'>Narsee Monjee</option>
											<option value='11404'>Chandigarh University</option>
											<option value='12394'>B Tech Evening</option>
											<option value='13814'>College Vidya</option>
											<option value='14006'>B2B Partners</option>
											<option value='16652'>Department 917969013429</option>
										</select>
									</div>
								</div> -->
								<div class="col-sm-3">
									<div class="call-dtlbox mb-1">
										<div class="form-group mt-3 mt-sm-0">
											<label>RESULT</label>
											<select style="border: none;" class="form-control" id="result1">
												<option selected disabled>Choose</option>
												<option value="c">Answered By Customer</option>
												<option value="m">Missed</option>
											</select>
										</div>
									</div>
								</div>
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>OPERATION</label>
										<select style="border: none;" class="form-control" id="operation1">
											<option selected disabled>Choose</option>
											<option value=">">Greater Than</option>
											<option value="<">Less Than</option>
											<option value=">=">Greater Than or Equal To</option>
											<option value="<=">Less Than or Equal To</option>
											<option value="!=">Is not Equal To</option>
											<option value="=">Equal</option>
										</select>
									</div>

								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>CALL DURATION</label>
										<input type="number" class="form-control" id="duration1" placeholder="Enter duration in seconds...">
									</div>

								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label for="note">NOTE</label>
										<select style="border: none;" class="form-control" id="note1">
											<option selected disabled>Choose</option>
											<option>Yes</option>
											<option>No</option>
										</select>
									</div>

								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>
											<span>KEY PHRASE</span></label>
										<input class="form-control" type="text" id="keyphrase" name="keyphrase" placeholder="Enter key phrase" title="Enter Key Phrase">
									</div>

								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>CIRCLE</label>
										<select style="border:none;" class="form-control" id="circle1">
											<option selected disabled>Choose</option>
											<option value='Andhra Pradesh &amp; Telangana'>Andhra Pradesh &amp; Telangana</option>
											<option value='Assam'>Assam</option>
											<option value='Bihar &amp; Jharkhand'>Bihar &amp; Jharkhand</option>
											<option value='Chennai'>Chennai</option>
											<option value='Delhi'>Delhi</option>
											<option value='Gujarat'>Gujarat</option>
											<option value='Himachal Pradesh'>Himachal Pradesh</option>
											<option value='Haryana'>Haryana</option>
											<option value='Jammu and Kashmir'>Jammu and Kashmir</option>
											<option value='Kerala &amp; Lakshadweep'>Kerala &amp; Lakshadweep</option>
											<option value='Karnataka'>Karnataka</option>
											<option value='Kolkata'>Kolkata</option>
											<option value='Maharashtra &amp; Goa'>Maharashtra &amp; Goa</option>
											<option value='Madhya Pradesh &amp; Chhattisgarh'>Madhya Pradesh &amp; Chhattisgarh</option>
											<option value='Mumbai'>Mumbai</option>
											<option value='North East'>North East</option>
											<option value='Odisha'>Odisha</option>
											<option value='Punjab'>Punjab</option>
											<option value='Rajasthan'>Rajasthan</option>
											<option value='Tamil Nadu'>Tamil Nadu</option>
											<option value='UP (East)'>UP (East)</option>
											<option value='UP (West)'>UP (West)</option>
											<option value='West Bengal'>West Bengal</option>
										</select>
									</div>

								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>OPERATOR</label>
										<select style="border: none;" class="form-control" id="operator1" data-placeholder="Choose an option please">
											<option selected disabled>Choose</option>
											<option value='Airtel India'>Airtel India</option>
											<option value='Idea'>Idea</option>
											<option value='MTNL'>MTNL</option>
											<option value='Jio'>Jio</option>
											<option value='Reliance Mobile GSM'>Reliance Mobile GSM</option>
											<option value='Tata Docomo'>Tata Docomo</option>
											<option value='Vodafone'>Vodafone</option>
											<option value='Telenor'>Telenor</option>
											<option value='Videcon'>Videcon</option>
											<option value='Aircel'>Aircel</option>
											<option value='S Tel'>S Tel</option>
											<option value='MTS India'>MTS India</option>
											<option value='BSNL'>BSNL</option>
											<option value='Etisalat India'>Etisalat India</option>
											<option value='Reliance Mobile CDMA'>Reliance Mobile CDMA</option>
											<option value='Loop Mobile'>Loop Mobile</option>
											<option value='BSNL CDMA'>BSNL CDMA</option>
											<option value='Subrin Rintel'>Subrin Rintel</option>
										</select>
									</div>

								</div> -->


							</div>
							<div class="row mt-4">
								<div class="col-sm-6 text-left">
									<h2>Detailed Records</h2>
								</div>
								<div class="col-sm-6 text-right">
								<button id="reset1" type="button" onclick="reset1()" class="btn btn-primary">Reset</button>
									<button type="button" class="btn btn-primary" onclick="searchapi1();">Search</button>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
									<div class="card">

										<div class="card-body">
											<div class="table-responsive">
												<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
												<form id="checkbox-form" method="POST">


													<p id="showData1"></p>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="pills-Clickcall" role="tabpanel" aria-labelledby="pills-Clickcall-tab">
						<div class="container-fluid">

							<div class="row">
								<!-- <div class="col-sm-3 call-dtlbox">
									<div class="form-group mt-3 mt-sm-0">
										<label for="destination_number">TO</label>
										<input class="form-control" type="text" name="destination_number2" id="destination_number2" placeholder="Enter Client Number...">
									</div>
								</div> -->
								<div class="col-sm-3 ">
									<div class="call-dtlbox mb-1">
										<?php $q = $conn->query("SELECT Extension FROM users WHERE ID='" . $_SESSION['useremployeeid'] . "'");
										$ext = mysqli_fetch_row($q);
										?>
										<div class="form-group mt-3 mt-sm-0">
											<label>FROM</label>
											<input readonly type="text" class="form-control" value="<?php echo $ext[0]; ?>">
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="call-dtlbox mb-1">
										<div class="form-group mt-3 mt-sm-0">
											<label>RESULT</label>
											<select style="border: none;" class="form-control" id="result2">
												<option selected disabled>Choose</option>
												<option value="c">Answered By Customer</option>
												<option value="m">Missed</option>
											</select>
										</div>
									</div>

								</div>
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>OPERATION</label>
										<select style="border: none;" class="form-control" id="operation2">
											<option selected disabled>Choose</option>
											<option value=">">Greater Than</option>
											<option value="<">Less Than</option>
											<option value=">=">Greater Than or Equal To</option>
											<option value="<=">Less Than or Equal To</option>
											<option value="!=">Is not Equal To</option>
											<option value="=">Equal</option>
										</select>
									</div>

								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>CALL DURATION</label>
										<input type="text" class="form-control" id="duration2" placeholder="Enter duration in seconds...">
									</div>

								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label for="note">NOTE</label>
										<select style="border: none;" class="form-control" id="note2">
											<option selected disabled>Choose</option>
											<option>Yes</option>
											<option>No</option>
										</select>
									</div>

								</div> -->
								<!-- <div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>CIRCLE</label>
										<select style="border:none;" class="form-control" id="circle2">
											<option selected disabled>Choose</option>
											<option value='Andhra Pradesh &amp; Telangana'>Andhra Pradesh &amp; Telangana</option>
											<option value='Assam'>Assam</option>
											<option value='Bihar &amp; Jharkhand'>Bihar &amp; Jharkhand</option>
											<option value='Chennai'>Chennai</option>
											<option value='Delhi'>Delhi</option>
											<option value='Gujarat'>Gujarat</option>
											<option value='Himachal Pradesh'>Himachal Pradesh</option>
											<option value='Haryana'>Haryana</option>
											<option value='Jammu and Kashmir'>Jammu and Kashmir</option>
											<option value='Kerala &amp; Lakshadweep'>Kerala &amp; Lakshadweep</option>
											<option value='Karnataka'>Karnataka</option>
											<option value='Kolkata'>Kolkata</option>
											<option value='Maharashtra &amp; Goa'>Maharashtra &amp; Goa</option>
											<option value='Madhya Pradesh &amp; Chhattisgarh'>Madhya Pradesh &amp; Chhattisgarh</option>
											<option value='Mumbai'>Mumbai</option>
											<option value='North East'>North East</option>
											<option value='Odisha'>Odisha</option>
											<option value='Punjab'>Punjab</option>
											<option value='Rajasthan'>Rajasthan</option>
											<option value='Tamil Nadu'>Tamil Nadu</option>
											<option value='UP (East)'>UP (East)</option>
											<option value='UP (West)'>UP (West)</option>
											<option value='West Bengal'>West Bengal</option>
										</select>
									</div>

								</div> -->
								<div class="col-sm-3">
									<div class="call-dtlbox mb-1">
										<div class="form-group mt-3 mt-sm-0">
											<label>OPERATOR</label>
											<select style="border: none;" class="form-control" id="operator2" data-placeholder="Choose an option please">
												<option selected disabled>Choose</option>
												<option value='Airtel India'>Airtel India</option>
												<option value='Idea'>Idea</option>
												<option value='MTNL'>MTNL</option>
												<option value='Jio'>Jio</option>
												<option value='Reliance Mobile GSM'>Reliance Mobile GSM</option>
												<option value='Tata Docomo'>Tata Docomo</option>
												<option value='Vodafone'>Vodafone</option>
												<option value='Telenor'>Telenor</option>
												<option value='Videcon'>Videcon</option>
												<option value='Aircel'>Aircel</option>
												<option value='S Tel'>S Tel</option>
												<option value='MTS India'>MTS India</option>
												<option value='BSNL'>BSNL</option>
												<option value='Etisalat India'>Etisalat India</option>
												<option value='Reliance Mobile CDMA'>Reliance Mobile CDMA</option>
												<option value='Loop Mobile'>Loop Mobile</option>
												<option value='BSNL CDMA'>BSNL CDMA</option>
												<option value='Subrin Rintel'>Subrin Rintel</option>
											</select>
										</div>
									</div>
								</div>


							</div>
							<div class="row mt-4">
								<div class="col-sm-6 text-left">
									<h2>Our Datasource</h2>
								</div>
								<div class="col-sm-6 text-right">
								<button id="reset2" type="button" onclick="reset2()" class="btn btn-primary">Reset</button>
								<button type="button" class="btn btn-primary" onclick="searchapi();">Search</button>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
									<div class="card">

										<div class="card-body">
											<div class="table-responsive">
												<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
												<form id="checkbox-form" method="POST">
													<p id="showData2"></p>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- <div class="tab-pane fade" id="pills-Broadcast" role="tabpanel" aria-labelledby="pills-Broadcast-tab">
						<div class="container-fluid">

							<div class="row">
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>
								<div class="col-sm-3 call-dtlbox">

									<div class="form-group mt-3 mt-sm-0">
										<label>FROM</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>

								</div>


							</div>
							<div class="row mt-4">
								<div class="col-sm-6 text-left">
									<h2>Our Datasource</h2>
								</div>
								<div class="col-sm-6 text-right">
									<button type="button" class="btn btn-primary">Reset</button>
									<button type="button" class="btn btn-primary">Search</button>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 mt-3">
									<div class="table table-responsive">
										<table class="table table-bordered">
											<thead class="thead-light">
												<tr>
													<th>TO</th>
													<th>DATE</th>
													<th>TIME</th>
													<th>RESULTS</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td> 919997386921 </td>
													<td> 11-03-2021 </td>
													<td>17:20:48</td>
													<td>Call answered by customer</td>
												</tr>
												<tr>
													<td> 919997386921 </td>
													<td> 11-03-2021 </td>
													<td>17:20:48</td>
													<td>Call answered by customer</td>
												</tr>
												<tr>
													<td> 919997386921 </td>
													<td> 11-03-2021 </td>
													<td>17:20:48</td>
													<td>Call answered by customer</td>
												</tr>
												<tr>
													<td> 919997386921 </td>
													<td> 11-03-2021 </td>
													<td>17:20:48</td>
													<td>Call answered by customer</td>
												</tr>

												<tr>
													<td> 919997386921 </td>
													<td> 11-03-2021 </td>
													<td>17:20:48</td>
													<td>Call answered by customer</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-sm-6">
									<span>Showing 1 to 5 of 5 entries</span>
								</div>
								<div class="col-sm-6">
									<nav aria-label="Page navigation example">
										<ul class="pagination justify-content-end">
											<li class="page-item"><a class="page-link" href="#">Previous</a></li>
											<li class="page-item"><a class="page-link" href="#">1</a></li>
											<li class="page-item"><a class="page-link" href="#">2</a></li>
											<li class="page-item"><a class="page-link" href="#">3</a></li>
											<li class="page-item"><a class="page-link" href="#">Next</a></li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div> -->
					<div class="tab-pane fade" id="pills-Missed" role="tabpanel" aria-labelledby="pills-Missed-tab">
						<div class="container-fluid">
							<div class="row">
								<div class="col-lg-12">
									<div class="card">

										<div class="card-body">
											<div class="table-responsive">
												<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
												<form id="checkbox-form" method="POST">
													<p id="missed"></p>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="pills-Notanswered" role="tabpanel" aria-labelledby="pills-Notanswered-tab">
						<div class="container-fluid">
							<div class="row">
								<div class="col-lg-12">
									<div class="card">

										<div class="card-body">
											<div class="table-responsive">
												<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
												<form id="checkbox-form" method="POST">
													<p id="notanswered"></p>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>


		<script>
			$(document).ready(function() {
				searchapi();
				searchapi1();
				missed();
				notanswered();
			});

			function reset1() {
				console.log("reset");
				$("#result1")[0].selectedIndex = 0;
			}

			function reset2() {
				$("#result2")[0].selectedIndex = 0;
				$("#operator2")[0].selectedIndex = 0;

			}


			function searchapi() {
				var destination = $("#destination_number2").val();
				var call_type = $("#result2").val();
				var operator = $("#operation2").val();
				var direction = "outbound";
				var duration = $("#duration2").val();
				var date = $('#tofrom').val();


				$.ajax({
					url: '/tata_api/CDR.php',
					data: {
						destination: destination,
						call_type: call_type,
						operator: operator,
						duration: duration,
						direction: direction,
						date: date,
					},
					type: 'POST',
					success: function(res) {
						var data = $.parseJSON(res);
						if ((data.message) == "Token has expired") {
							window.location.reload();
						}
						var col = ['client_number', 'date', 'time', 'call_duration', 'status'];
						var datas = data.results;
						var table = document.createElement("table");
						table.setAttribute('class', 'table table-striped table-hower');
						table.setAttribute('id', 'basic-datatable1');
						var tr = table.insertRow(-1);
						for (var i = 0; i < col.length; i++) {
							var th = document.createElement("th"); // TABLE HEADER.
							if (i == 3) {
								th.innerHTML = col[i] + ' (In Seconds)';
							} else {
								th.innerHTML = col[i];
							}
							tr.appendChild(th);
						}

						for (var i = 0; i < datas.length; i++) {
							if (datas[i]['direction'] === 'outbound') {
								tr = table.insertRow(-1);
								for (var j = 0; j < col.length; j++) {
									var tabCell = tr.insertCell(-1);
									if (j == 3) {
										var a = new Date(datas[i][col[j]] * 1000).toISOString().substr(11, 8);
										tabCell.innerHTML = a;
									} else {

										tabCell.innerHTML = datas[i][col[j]];
									}
								}
							}
						}
						var divContainer = document.getElementById("showData2");
						divContainer.innerHTML = "";
						divContainer.appendChild(table);

					}

				})
			}

			function searchapi1() {
				var destination = $("#destination_number1").val();
				var call_type = $("#result1").val();
				var operator = $("#operation1").val();
				var direction = "inbound";
				var duration = $("#duration1").val();
				var ivr = $("#ivr1").val();
				var department = $("#department").val();
				var keyphrase = $("#keyphrase").val();
				var date = $('#tofrom').val();



				$.ajax({
					url: '/tata_api/CDR.php',
					data: {
						destination: destination,
						call_type: call_type,
						operator: operator,
						duration: duration,
						direction: direction,
						ivr: ivr,
						keyphrase: keyphrase,
						department: department,
						date: date,
					},
					type: 'POST',
					success: function(res) {
						var data = $.parseJSON(res);
						var col = ['client_number', 'date', 'time', 'call_duration', 'status'];
						if ((data.message) == "Token has expired") {
							window.location.reload();
						}
						var datas = data.results;
						var table = document.createElement("table");
						table.setAttribute('id', 'basic-datatable2');
						table.setAttribute('class', 'table table-striped table-hower');
						var tr = table.insertRow(-1);
						for (var i = 0; i < col.length; i++) {
							var th = document.createElement("th"); // TABLE HEADER.
							if (i == 3) {
								th.innerHTML = col[i] + ' (In Seconds)';
							} else {
								th.innerHTML = col[i];
							}
							tr.appendChild(th);
						}

						for (var i = 0; i < datas.length; i++) {
							if (datas[i]['direction'] === 'inbound') {

								tr = table.insertRow(-1);
								for (var j = 0; j < col.length; j++) {
									var tabCell = tr.insertCell(-1);
									if (j == 3) {
										var a = new Date(datas[i][col[j]] * 1000).toISOString().substr(11, 8);
										tabCell.innerHTML = a;
									} else {

										tabCell.innerHTML = datas[i][col[j]];
									}
								}
							}
						}
						var divContainer = document.getElementById("showData1");
						divContainer.innerHTML = "";
						divContainer.appendChild(table);

					}

				})
			}

			function missed() {
				var call_type = 'm';
				var direction = "inbound";
				$.ajax({
					url: '/tata_api/CDR.php',
					data: {
						call_type: call_type,
						direction: direction,

					},
					type: 'POST',
					success: function(res) {
						var data = $.parseJSON(res);
						var col = ['client_number', 'date', 'time', 'call_duration', 'status'];
						if ((data.message) == "Token has expired") {
							window.location.reload();
						}
						var datas = data.results;
						var table = document.createElement("table");
						table.setAttribute('id', 'basic-datatable2');
						table.setAttribute('class', 'table table-striped table-hower');
						var tr = table.insertRow(-1);
						for (var i = 0; i < col.length; i++) {
							var th = document.createElement("th"); // TABLE HEADER.
							if (i == 3) {
								th.innerHTML = col[i] + ' (In Seconds)';
							} else {
								th.innerHTML = col[i];
							}
							tr.appendChild(th);
						}

						for (var i = 0; i < datas.length; i++) {
							if (datas[i]['direction'] === 'inbound' && datas[i]['status'] === 'missed') {
								tr = table.insertRow(-1);
								for (var j = 0; j < col.length; j++) {
									var tabCell = tr.insertCell(-1);
									if (j == 3) {
										var a = new Date(datas[i][col[j]] * 1000).toISOString().substr(11, 8);
										tabCell.innerHTML = a;
									} else {

										tabCell.innerHTML = datas[i][col[j]];
									}
								}
							}
						}
						var divContainer = document.getElementById("missed");
						divContainer.innerHTML = "";
						divContainer.appendChild(table);

					}

				})
			}

			function notanswered() {
				var call_type = 'm';
				var direction = "outbound";
				$.ajax({
					url: '/tata_api/CDR.php',
					data: {
						call_type: call_type,
						direction: direction,

					},
					type: 'POST',
					success: function(res) {
						var data = $.parseJSON(res);
						var col = ['client_number', 'date', 'time', 'call_duration', 'status'];
						if ((data.message) == "Token has expired") {
							window.location.reload();
						}
						var datas = data.results;
						var table = document.createElement("table");
						table.setAttribute('id', 'basic-datatable2');
						table.setAttribute('class', 'table table-striped table-hower');
						var tr = table.insertRow(-1);
						for (var i = 0; i < col.length; i++) {
							var th = document.createElement("th"); // TABLE HEADER.
							if (i == 3) {
								th.innerHTML = col[i] + ' (In Seconds)';
							} else {
								th.innerHTML = col[i];
							}
							tr.appendChild(th);
						}

						for (var i = 0; i < datas.length; i++) {
							if (datas[i]['direction'] === 'outbound') {
								tr = table.insertRow(-1);
								for (var j = 0; j < col.length; j++) {
									var tabCell = tr.insertCell(-1);
									if (datas[i][col[j]] == 'missed') {
										datas[i][col[j]] = 'not answered';
									}
									if (j == 3) {
										var a = new Date(datas[i][col[j]] * 1000).toISOString().substr(11, 8);
										tabCell.innerHTML = a;
									} else {

										tabCell.innerHTML = datas[i][col[j]];
									}
								}
							}
						}
						var divContainer = document.getElementById("notanswered");
						divContainer.innerHTML = "";
						divContainer.appendChild(table);

					}

				})
			}
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#basic-datatable1").DataTable({
					"pageLength": 20,

					"aoColumns": [{
							"bSearchable": true
						},
						{
							"bSearchable": false
						},
						{
							"bSearchable": false
						},
						{
							"bSearchable": false
						},

					],
					language: {
						paginate: {
							previous: "<i class='uil uil-angle-left'>",
							next: "<i class='uil uil-angle-right'>"
						}
					},
					drawCallback: function() {
						$(".dataTables_paginate > .pagination").addClass("pagination-rounded")
					}
				});
			});
		</script>
		<script>
			$(document).ready(function() {
				$('input[name="datetimes"]').daterangepicker({
					timePicker: true,
					startDate: moment().startOf('hour'),
					endDate: moment().startOf('hour').add(32, 'hour'),
					locale: {
						format: 'M/DD hh:mm A'
					}
				});
			});
		</script>
		<?php include 'filestobeincluded/footer-top.php' ?>
		<?php include 'filestobeincluded/footer-bottom.php' ?>