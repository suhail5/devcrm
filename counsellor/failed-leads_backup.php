<?php include 'filestobeincluded/header-top.php' ?>
<?php include 'filestobeincluded/header-bottom.php' ?>
<!-- Pre-loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div>
<!-- End Preloader-->
<?php include 'filestobeincluded/navigation.php' ?>


<?php

$all_leads = array();
$all_users = array();

if(!isset($_GET['leads'])){
	$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID in (3,7) AND Counsellor_ID = '". $_SESSION['USERS_ID'] ."' ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
}
?>


        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1 navbuttons">
                            </nav>
                            <h4 class="mb-1 mt-0">Failed Leads</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->
													
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
								
								<div class="table-responsive">
								<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
								<form id="checkbox-form" method="POST">
                                <table id="datatable-buttons" class="table table-striped table-hover nowrap" data-export-title="Blackboard_Leads_<?php echo date("Y-m-d h:i a"); ?>">
                                    <thead>
                                        <tr>
											<th><div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input checkbox-selectall" id="customCheck2">
                                                <label class="custom-control-label" for="customCheck2"></label>
                                            	</div></th>
                                            <th>Name</th>
                                            <th>Email</th>
											<th>Phone</th>
											<th>Counsellor</th>
											<th>Stage</th>
											<th>Reason</th>
                                            <th>Course</th>
                                            <th>Specialization</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php

                                    	$counter = 1;
                                    	foreach ($all_leads as $lead) {
                                    		
                                    		?>
                                    		<tr>
												<td>
												<div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input checkbox-function" name="id[]" id="customCheck2<?php echo $lead['ID']; ?>" value="<?php echo $lead['ID']; ?>">
                                                <label class="custom-control-label" for="customCheck2<?php echo $lead['ID']; ?>"></label>
                                            	</div>
												</td>
                                    			<td><?php echo $lead['Name']; ?></td>
		                                        <td><?php echo $lead['Email']; ?></td>
												<td><?php echo $lead['Mobile']; ?></td>
												<td style="width: 10px;">
													<?php
		                                        		$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$lead['Counsellor_ID']."'");
		                                        		$counsellor = mysqli_fetch_assoc($counsellor_query);
		                                        		if($counsellor_query->num_rows > 0){
															echo $counsellor['Name'];
														}else{
															$counsellor['Name'] = ' ';
															echo $counsellor['Name'];
														}
		                                        	?>
												</td>
		                                        <td>
		                                        	<?php
		                                        		$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
		                                        		$stage = mysqli_fetch_assoc($stage_query);
		                                        		if($stage_query->num_rows > 0){
															echo '<b>'.$stage['Name'].'</b>';
														}else{
															$stage['Name'] = ' ';
															echo $stage['Name'];
														}
		                                        	?>
												</td>
												<td>
		                                        	<?php
		                                        		$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
		                                        		$reason = mysqli_fetch_assoc($reason_query);
		                                        		if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
		                                        			$badge = "success";
		                                        		}
		                                        		else if(strcasecmp($stage['Name'], "COLD")==0) {
		                                        			$badge = "warning";
		                                        		}
		                                        		else {
		                                        			$badge = "danger";
		                                        		}
		                                        	?>
		                                        	<span class="badge badge-soft-<?php echo($badge); ?> py-1">
														<?if($reason_query->num_rows > 0){
															echo $reason['Name'];
														}else{
															$reason['Name'] = ' ';
															echo $reason['Name'];
														}?>
		                                        	</span>
												</td>
		                                        <td>
		                                        	<?php
		                                        		$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
		                                        		$course = mysqli_fetch_assoc($course_query);
		                                        		if($course_query->num_rows > 0){
															echo $course['Name'];
														}else{
															$course['Name'] = ' ';
															echo $course['Name'];
														}
		                                        	?>
		                                        </td>
		                                        <td>
		                                        	<?php
		                                        		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
		                                        		$specialization = mysqli_fetch_assoc($specialization_query);
		                                        		if($specialization_query->num_rows > 0){
															echo $specialization['Name'];
														}else{
															$specialization['Name'] = ' ';
															echo $specialization['Name'];
														}
		                                        	?>
		                                        </td>
												
		                                        <td style="font-size: 18px;">
		                                            
													<div class="btn-group">
														<span data-toggle="tooltip" data-placement="top" data-original-title="Refer Lead" title=""><i class="fa fa-share-alt referlead" style="cursor: pointer;" data-id="<?php echo $lead['ID']; ?>" aria-hidden="true"></i></span>&nbsp;&nbsp;
														<span data-toggle="tooltip" data-placement="top" data-original-title="Delete Lead" title=""><i class="fa fa-trash deletelead" style="cursor: pointer;" data-id="<?php echo $lead['ID']; ?>" aria-hidden="true"></i></span>&nbsp;&nbsp;
													</div>	
												</td>

                                    		</tr>
                                    		<?php
                                    	}
                                    	?>
                                    </tbody>
								</table>
								</form>								
							</div>
							
<script type='text/javascript'>
$(document).ready(function () {
    $(".checkbox-selectall").click(function () {
		$(".checkbox-function").prop('checked', $(this).prop('checked'));
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide4").css({display: "none"});
		}
    });
    
    $(".checkbox-function").change(function(){
        if (!$(this).prop("checked")){
            $(".checkbox-selectall").prop("checked",false);
        }
	});
	
	$(".checkbox-function").click(function () {
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide4").css({display: "none"});
		}
    });

});           	
</script>

<!-------Refer Selected modal-------->
<div class="modal fade" id="selected_refer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Refer Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-refer-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Refer Selected modal-------->
<!-------Delete Selected modal-------->
<div class="modal fade" id="selected_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-xs">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Delete Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-delete-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Delete Selected modal-------->

<script type='text/javascript'>
	$(document).ready(function(){

		$('.deletelead').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/del_lead.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-delete').html(response); 

					// Display Modal
					$('#deletelead').modal('show'); 
				}
			});
		});
	});
</script>	
<!-------Delete Lead----->
<div class="modal fade" id="deletelead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-delete">
				
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--------End Delete Lead---------->

<script type='text/javascript'>
	$(document).ready(function(){

		$('.referlead').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/refer_lead.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-refer').html(response); 

					// Display Modal
					$('#referlead').modal('show'); 
				}
			});
		});
	});
</script>								
<!--------refer lead---->
<div id="referlead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="referallleadsLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="referallleadsLabel">Refer Lead </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-refer">
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!------End refer lead------>





					</div>
				</div>
			</div>
	</div>
</div> <!-- content -->





<script>
    function deleteLeads(id) {
        var delete_lead_id = $('#delete_lead_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "ajax_leads/delete_lead.php",
          data: { "delete_lead_id": delete_lead_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
				toastr.success('Lead deleted successfully');
				window.location.reload();
            }
            else {
                toastr.error('Unable to delete lead');
            }
          }
        });
        return false;
    }
</script>

<script>  
      $(document).ready(function(){  
           $('#upload_csv').on("submit", function(e){  
                e.preventDefault(); //form will not submitted  
                $.ajax({  
					url: "ajax_leads/upload_lead.php",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){  
                     	console.log(data);
						$('.modal').modal('hide');
						if(data.match("true")) {
							toastr.success('Lead uploaded successfully');
							//window.location.reload();
						}
						else {
							toastr.error('Unable to upload lead');
						}  
                     }  
                })  
           });  
      });  
</script>

<script type='text/javascript'>
	function updateOwner(){
            var lead_id = $('#lead_id').val();
            var lead_owner_id = $('#lead_owner').val();
            var refer_lead_owner_id = $('#refer_lead_owner').val();
            var refer_comment = $('#referal_comment').val();
        $.ajax
        ({
          type: "POST",
          url: "ajax_leads/refer_lead_sql.php",
          data: { "lead_owner_id": lead_owner_id, "refer_lead_owner_id": refer_lead_owner_id, "refer_comment": refer_comment, "lead_id": lead_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
				toastr.success('Lead refered successfully');
				window.location.reload();
            }
            else {
                toastr.error('Unable to refer lead');
            }
          }
        });
        return false;
    }
</script>



<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>