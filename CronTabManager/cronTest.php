<?php

require_once 'vendor/autoload.php';

use TiBeN\CrontabManager\CrontabJob;
use TiBeN\CrontabManager\CrontabRepository;
use TiBeN\CrontabManager\CrontabAdapter;

$crontabRepository = new CrontabRepository(new CrontabAdapter());

$crontabJob = CrontabJob::createFromCrontabLine('*/2 * * * * /opt/lampp/bin/php /opt/lampp/htdocs/CronTabManager/cronSave.php > /home/ubuntu/log.txt');

$crontabRepository->addJob($crontabJob);
$crontabRepository->persist();

print_r($crontabRepository);

?>