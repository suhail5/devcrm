<?php 

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST,GET');
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");



$data = json_decode(file_get_contents('php://input'));


if($data->key == 'Ee5909vv' ){ 
    
    require 'filestobeincluded/db_config.php';
    

    // $name = mysqli_real_escape_string($conn,$_GET['Name']);
    // $mobileNumber = mysqli_real_escape_string($conn,$_GET['Mobile']);
    // $email = mysqli_real_escape_string($conn,$_GET['Email']);
    // $dob = mysqli_real_escape_string($conn,$_GET['dob']);
    // $course_name = mysqli_real_escape_string($conn,$_GET['Course_Name']);

    $name = mysqli_real_escape_string($conn,$data->Name);
    $mobileNumber = mysqli_real_escape_string($conn,$data->Mobile);
    $email = mysqli_real_escape_string($conn,$data->Email);
    $dob = mysqli_real_escape_string($conn,$data->dob);
    $course_name = mysqli_real_escape_string($conn,$data->Course_Name);
    

    if(!$name){
        echo 'name field is empty!';
        exit();
    }
    if(!$mobileNumber){
        echo "Mobile number cannot be null!";
        exit();
    }


    
    $q = $conn->query("SELECT ID,Institute_ID FROM Courses WHERE Name like '".$course_name."' and Institute_ID in ('51','57','64')  ");
    $a = array();
    while($univ = mysqli_fetch_assoc($q))
        {
            array_push($a,$univ['Institute_ID']);
        }

    $a = implode("', '", $a);    
    $shuffle_univ = $conn->query("SELECT * FROM Institute_Shuffle WHERE Institute_ID in ('$a') ORDER BY Last_Updated ASC");
    $unividupdate = $conn->query("SELECT Institute_ID FROM Institute_Shuffle WHERE Institute_ID in ('$a') ORDER BY Last_Updated ASC");
    $unividupdate = mysqli_fetch_row($unividupdate)[0]; 
   
    $udateUniv = $conn->query("UPDATE Institute_Shuffle SET Last_Updated = now() WHERE Institute_ID = '$unividupdate' ");
    
        
   
    
    if($mobileNumber != ''){
        $check = $conn->query("SELECT ID from Leads WHERE (Mobile like '$mobileNumber' OR Alt_Mobile like '$mobileNumber')");
        $check = $conn->query("SELECT ID from Siksha_Leads WHERE (Mobile like '$mobileNumber')");
    }elseif($email!=''){
		$check = $conn->query("SELECT ID FROM `Leads` WHERE (Email like '$email')");
        $check = $conn->query("SELECT ID FROM Siksha_Leads WHERE (Email like '$email')");    
	}

    date_default_timezone_set("Asia/Bangkok");
    $d = date("Y-m-d");
    $date = date("Y-m-d", strtotime($d));

    if($check->num_rows> 0)
	{	
		echo("Lead Already Exists");
        exit();
	}else{        
        while($univ = mysqli_fetch_assoc($shuffle_univ))
        {
            if($univ['Institute_ID']==51){
				$userID = $conn->query("SELECT Users_ID FROM Equal_Distribution WHERE Institute_ID = 51 ORDER BY `Timestamp_siksha` ASC");
				$user = mysqli_fetch_row($userID);
				$counsellorID = $user[0];
				$updatetime = $conn->query("UPDATE Equal_Distribution SET `Timestamp_siksha` = now() WHERE Users_ID = '".$counsellorID."' ");
                 
                $course_ID = $conn->query("SELECT ID FROM Courses WHERE Name like '".$course_name."' and Institute_ID = '".$univ['Institute_ID']."' ");
                $course_ID = mysqli_fetch_row($course_ID)[0];
				$addlead = $conn->query("INSERT INTO Siksha_Leads(Name, Email, Mobile, Stage_ID, Reason_ID, Institute_ID,Course_ID,Counsellor_ID,Source_ID,Subsource_ID,Creation_Date) VALUES ('$name','$email','$mobileNumber','1','25','51','".$course_ID."','".$counsellorID."','50','111','$date')");
                $date = date("Y-m-d", strtotime($date. ' + 1 day'));
                
			}elseif($univ['Institute_ID']==57){
				$userID = $conn->query("SELECT Users_ID FROM Equal_Distribution WHERE Institute_ID = 57 ORDER BY `Timestamp_siksha` ASC");
				$user = mysqli_fetch_row($userID);
				$counsellorID = $user[0];
				$updatetime = $conn->query("UPDATE Equal_Distribution SET `Timestamp_siksha` = now() WHERE Users_ID = '".$counsellorID."' ");
                
                $course_ID = $conn->query("SELECT ID FROM Courses WHERE Name like '".$course_name."' and Institute_ID = '".$univ['Institute_ID']."' ");
                $course_ID = mysqli_fetch_row($course_ID)[0];
				$addlead = $conn->query("INSERT INTO Siksha_Leads(Name, Email, Mobile, Stage_ID, Reason_ID,Institute_ID,Course_ID,Counsellor_ID,Source_ID,Subsource_ID,Creation_Date) VALUES ('$name','$email','$mobileNumber','1','25','57','".$course_ID."', '".$counsellorID."','50','111','$date')");
                $date = date("Y-m-d", strtotime($date. ' + 1 day'));
            }elseif($univ['Institute_ID']==64){
				$userID = $conn->query("SELECT Users_ID FROM Equal_Distribution WHERE Institute_ID = 64 ORDER BY `Timestamp_siksha` ASC");
				$user = mysqli_fetch_row($userID);
				$counsellorID = $user[0];
				$updatetime = $conn->query("UPDATE Equal_Distribution SET `Timestamp_siksha` = now() WHERE Users_ID = '".$counsellorID."' ");
                
                $course_ID = $conn->query("SELECT ID FROM Courses WHERE Name like '".$course_name."' and Institute_ID = '".$univ['Institute_ID']."' ");
                $course_ID = mysqli_fetch_row($course_ID)[0];
				$addlead = $conn->query("INSERT INTO Siksha_Leads(Name, Email, Mobile, Stage_ID, Reason_ID,Institute_ID,Course_ID,Counsellor_ID,Source_ID,Subsource_ID,Creation_Date) VALUES ('$name','$email','$mobileNumber','1','25','64','".$course_ID."', '".$counsellorID."','50','111','$date')");
                $date = date("Y-m-d", strtotime($date. ' + 1 day'));
            }
            

        }
        if($addlead){
            echo("Lead Added Successfully");
        }
    }
    
    include 'siksha_add.php';
}else{
    echo('Not Allowed');
}

?>