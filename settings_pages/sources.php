<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_sources = array();

$sources_query_res = $conn->query("SELECT * FROM Sources");
while($row = $sources_query_res->fetch_assoc()) {
    $all_sources[] = $row;
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
        <div class="card-header" id="headingOne"><h5 class="m-0 font-size-16">Sources <i class="uil uil-angle-down float-right accordion-arrow"></i> </h5></div>
    </a>
    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body text-muted">

        <button class="btn btn-primary" data-toggle="modal" data-target="#uploadsourcemodal"><i data-feather="upload" class="icon-xs"></i> Upload Reason Data</button>
<!--------------Upload Leads Modal------------->
<div id="uploadsourcemodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="uploadleads" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadleads">Upload Sources</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="upload_source" method="POST" enctype="multipart/form-data">
                
                <div class="form-group row">
                    <label class="col-lg-9 col-form-label"
                        for="example-fileinput">Upload File with Prospect Sources Data</label>
                        <div class="col-lg-3 col-sm-12">
                        <a href="/assets/files/Blackboard Source Data Sample.csv" download="Blackboard Source Data Sample.csv"><p><i data-feather="file" class="icon-dual icon-xs"></i> Download Sample</p></a>
                    </div><br>
                    <div class="col-lg-12">
                        <input type="file" name="lead_file" class="form-control" style="border: 0ch;"
                            accept=".csv">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                <button type="submit" name="upload" id="upload" class="btn btn-primary">&nbsp;&nbsp;Upload&nbsp;&nbsp;</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>  
      $(document).ready(function(){  
           $('#upload_source').on("submit", function(e){  
                e.preventDefault(); //form will not submitted  
                $.ajax({  
					url: "settings_pages/ajax_source/upload_source.php",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){  
                     	console.log(data);
						$('.modal').modal('hide');
						if(data.match("true")) {
							toastr.success('Sources uploaded successfully');
							$("#settingstab").load(location.href + " #settingstab" , function () {
                                $("#headingOne").click();
                            });
						}
						else {
							toastr.error('Unable to upload sources');
						}  
                     }  
                })  
           });  
      });  
 </script>
<!----------------End---------------->



            <button class="btn btn-primary" data-toggle="modal" data-target="#addsourcemodal"> <i class="uil uil-plus-circle"></i> Add Sources</button>
            <button class="btn btn-primary" id="ExportSorcetoExcel"> <i class="fa fa-download"></i> Download</button>
<script>
                                        $("#ExportSorcetoExcel").on("click", function() {
                                             
                                                $.ajax
                                                ({
                                                type: "POST",
                                                url: "/ajax_leads/export_source.php",
                                                data: {  },
                                                success: function (data) {
                                                     /*
                                                    * Make CSV downloadable
                                                    */
                                                    var downloadLink = document.createElement("a");
                                                    var fileData = ['\ufeff'+data];

                                                    var blobObject = new Blob(fileData,{
                                                        type: "text/csv;charset=utf-8;"
                                                    });

                                                    

                                                    var url = URL.createObjectURL(blobObject);
                                                    downloadLink.href = url;
                                                    downloadLink.download = "Blackboard_Source_<?php echo date("d-m-Y h:i a"); ?>.csv";

                                                    /*
                                                    * Actually download CSV
                                                    */
                                                    document.body.appendChild(downloadLink);
                                                    downloadLink.click();
                                                    document.body.removeChild(downloadLink);
                                                                                                    

                                                }
                                                });
                                                return false;
                                        });</script>
            <!----Add Source Modal-------->
            <div class="modal fade" id="addsourcemodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Source</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">Source</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="source_name" placeholder="Source Name">
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="saveSource()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_sources" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table id="basic-datatable" class="table table-hover mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Sources</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_sources as $source) {
                            $counter++;
                            if(strcasecmp($source['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $source['Name']; ?></td>
                                <td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editsourcemodal<?php echo $counter; ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editsourcemodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Source</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <input type="hidden" id="source_id<?php echo $counter; ?>" value="<?php echo($source['ID']); ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label" for="simpleinput">Source</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_source_name<?php echo $counter; ?>" placeholder="Source" value="<?php echo($source['Name']); ?>">
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateSource(<?php echo $counter; ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletesourcemodal<?php echo $counter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletesourcemodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_source_id<?php echo $counter; ?>" value="<?php echo($source['ID']); ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteSource(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function saveSource() {
        var source_name = $('#source_name').val();
      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_source/add_source.php",
          data: { "source_name": source_name },
          success: function (data) {
            $('#addsourcemodal').modal('hide');
            if(data.match("true")) {
                toastr.success('Source added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingOne").click();
                });
            }
            else {
                toastr.error('Unable to add source');
            }
          }
        });
        return false;
    }
</script>

<script>
    function updateSource(id) {
        var source_id = $('#source_id'.concat(id)).val();
        var new_source_name = $('#new_source_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_source/update_source.php",
          data: { "source_id": source_id, "new_source_name": new_source_name },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Source updated successfully');
                $("#settingstab").load(location.href + " #settingstab" , function () {
                    $("#headingOne").click();
                });
            }
            else {
                toastr.error('Unable to update source');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteSource(id) {
        var source_id = $('#delete_source_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_source/delete_source.php",
          data: { "source_id": source_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Source deleted successfully');
                $("#settingstab").load(location.href + " #settingstab" , function () {
                    $("#headingOne").click();
                });
            }
            else {
                toastr.error('Unable to delete source');
            }
          }
        });
        return false;
    }
</script>