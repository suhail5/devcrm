<?php
//include database configuration file
ini_set('max_input_time','600');
if(session_status() === PHP_SESSION_NONE) if(session_status() === PHP_SESSION_NONE) session_start();
require "../../filestobeincluded/db_config.php";

$employeeid = mysqli_real_escape_string($conn,$_POST['id']);

$time_login = $conn->query("SELECT * FROM Time_Details WHERE Type like 'login' AND User_ID='" . $employeeid . "' ");
$time_logout = $conn->query("SELECT * FROM Time_Details WHERE Type like 'logout' AND User_ID='" . $employeeid . "' ");
$row = array();
$i = 0;
while ($r = mysqli_fetch_assoc($time_login)) {
    $row[$i][0] = $r['Time'];
    $i++;
}
$i = 0;
while ($r = mysqli_fetch_assoc($time_logout)) {
    $row[$i][1] = $r['Time'];
    $i++;
}


if($time_login->num_rows > 0 || $time_logout->num_rows > 0){
    $delimiter = ",";
    $filename = "Time_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Login Time','Logout Time');

    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    foreach($row as $r){
        

        $lineData = array($r[0], $r[1]);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;
