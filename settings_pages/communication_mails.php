<?php require "filestobeincluded/db_config.php" ?>

<?php


?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapfsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
        <div class="card-header" id="headingTen"><h5 class="m-0 font-size-16">Communication Mail<i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addSendermodal"> <i class="uil uil-plus-circle"></i> Add Sender</button>
            <!----Add Specialization Modal-------->
            <div class="modal fade" id="addSendermodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add Sender</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="comm_mail">Email ID</label>
                                    <div class="col-lg-10">
                                        <input type="email" class="form-control" id="comm_email" placeholder="Communication Mail" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="email_name">Sender Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="email_name" placeholder="Sender Name" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Institute</label>
                                    <div class="col-lg-10">
                                        <select id="email_institute" class="form-control custom-select">
                                            <option selected disabled>Choose Institute</option>
                                            <?php
                                                $get_ins = $conn->query("SELECT * FROM Institutes WHERE ID <> 0");
                                                while($gi = $get_ins->fetch_assoc()){ ?>
                                                <option value="<?php echo $gi['ID']; ?>"><?php echo $gi['Name']; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-light" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" onclick="save_sender();">Add</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_cities" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-1">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Sender Email</th>
                            <th scope="col">Sender Name</th>
                            <th scope="col">Institute</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>''
                        <?php 
                            $counter = 0;
                            $get_senders = $conn->query("SELECT Communication_Mail.ID, Communication_Mail.Email, Communication_Mail.Name, Institutes.Name AS Institute FROM `Communication_Mail` LEFT JOIN Institutes ON Communication_Mail.Institute_ID = Institutes.ID");
                            while($gs = $get_senders->fetch_assoc()){ $counter++;
                        ?>
                            <tr>
                                <td><?php echo $counter; ?></td>
                                <td><?php echo $gs['Email'] ?></td>
                                <td><?php echo $gs['Name'] ?></td>
                                <td><?php echo $gs['Institute'] ?></td>
                                <td>
                                    <i class="fa fa-edit mx-1" style="cursor: pointer;" data-toggle="modal" data-target="#editcommunicationmodal<?php echo $counter; ?>"  aria-hidden="true" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editcommunicationmodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Communication Mail</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <div class="form-group row">
                                                            <input type="hidden" id="sender_id<?php echo $counter; ?>" value="<?php echo $gs['ID']; ?>">
                                                            <label class="col-lg-2 col-form-label" for="">Sender Email</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_sender_email<?php echo $counter; ?>" placeholder="Sender Email" value="<?php echo $gs['Email'] ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Sender Name</label>
                                                            <div class="col-lg-10">
                                                            <input type="text" class="form-control" id="new_sender_name<?php echo $counter; ?>" placeholder="Sender Name" value="<?php echo $gs['Name'] ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Institute</label>
                                                            <div class="col-lg-10">
                                                            <select id="new_institute_name<?php echo $counter; ?>" class="form-control custom-select">
                                                                <option selected disabled>Choose Institute</option>
                                                                <?php
                                                                    $get_ins = $conn->query("SELECT * FROM Institutes WHERE ID <> 0");
                                                                    while($gi = $get_ins->fetch_assoc()){ ?>
                                                                    <option value="<?php echo $gi['ID']; ?>"><?php echo $gi['Name']; ?></option>
                                                                <?php }
                                                                ?>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateCommunicationdetail(<?php echo $gs['ID'] ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- <i class="fa fa-trash mx-1" style="cursor:pointer;" onclick="delete_sender(<?php echo $gs['ID'] ?>);"></i> -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletecommmodal<?php echo $counter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletecommmodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post">
                                                        <input type="hidden" id="delete_comm_id<?php echo $counter; ?>" value="<?php echo $gs['ID']; ?>">
                                                        
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteComm(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
function save_sender(){
    var email = $('#comm_email').val();
    var name = $('#email_name').val();
    var institute = $('#email_institute').val();

    $.ajax({
        url:'settings_pages/ajax_communication/add.php',
        data:{"email":email, "name":name, "institute":institute},
        type:'POST',
        success: function(data){
            if(data.match('true')){
                $('.modal').modal('hide');
                toastr.success("Sender added successfully!");
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingTen").click();
                });
            }else{
                toastr.error(data);
            }
        }
    })
}
</script>

<script>

    function updateCommunicationdetail(id){
        var ID = $('#sender_id'.concat(id)).val();
        var new_email = $('#new_sender_email'.concat(id)).val();
        var new_sender = $('#new_sender_name'.concat(id)).val();
        var new_institute = $('#new_institute_name'.concat(id)).val();
        console.log(ID);
        console.log(new_email);
        console.log(new_sender);
        console.log(new_institute);

        $.ajax({
            url:'settings_pages/ajax_communication/update.php',
            type:'POST',
            data:{id:ID,new_email:new_email,new_sender:new_sender,new_institute:new_institute},
            success: function(data){
                if(data.match('true')){
                    $('.modal').modal('hide');
                    toastr.success("Sender updated successfully!");
                    window.location.reload();
                }else{
                    toastr.error(data);
                }
            }

        })


    }

</script>
<script>
    function deleteComm(id) {
        var comm_id = $('#delete_comm_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_specialization/delete_specialization.php",
          data: { "specialization_id": specialization_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Specialization deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingSeven").click();
                });
            }
            else {
                toastr.error('Unable to delete specialization');
            }
          }
        });
        return false;
    }
</script>