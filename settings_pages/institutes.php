<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_institutes = array();

$institutes_query_res = $conn->query("SELECT * FROM Institutes  WHERE ID <> '0'");
while($row = $institutes_query_res->fetch_assoc()) {
	$all_institutes[] = $row;
}

?>

<?php

if(isset($_POST['updatestatus'])) {
	$institute_id = $_POST['institute_id'];
	$institute_status = $_POST['institute_status'];

	$update_institute = $conn->query("UPDATE Institutes SET Status = '".$institute_status."' WHERE ID = '".$institute_id."'");
	if($update_institute) {
		?>
		<script>
			alert('Institute updated');   
			window.location.replace("settings");                       
        </script>
		<?
	}
	else {
		?>
		<script>
			alert('Unable to update institute');
			window.location.replace("settings");                     
        </script>
		<?
	}
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
        <div class="card-header" id="headingFive"><h5 class="m-0 font-size-16">Universities <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addinstitutemodal"> <i class="uil uil-plus-circle"></i> Add University</button>
        <button class="btn btn-primary" id="ExportUnitoExcel"> <i class="fa fa-download"></i> Download</button>
<script>
                                        $("#ExportUnitoExcel").on("click", function() {
                                             
                                                $.ajax
                                                ({
                                                type: "POST",
                                                url: "/ajax_leads/export_university.php",
                                                data: {  },
                                                success: function (data) {
                                                     /*
                                                    * Make CSV downloadable
                                                    */
                                                    var downloadLink = document.createElement("a");
                                                    var fileData = ['\ufeff'+data];

                                                    var blobObject = new Blob(fileData,{
                                                        type: "text/csv;charset=utf-8;"
                                                    });

                                                    

                                                    var url = URL.createObjectURL(blobObject);
                                                    downloadLink.href = url;
                                                    downloadLink.download = "Blackboard_university_<?php echo date("d-m-Y h:i a"); ?>.csv";

                                                    /*
                                                    * Actually download CSV
                                                    */
                                                    document.body.appendChild(downloadLink);
                                                    downloadLink.click();
                                                    document.body.removeChild(downloadLink);
                                                                                                    

                                                }
                                                });
                                                return false;
                                        });</script>

            <!----Add Institute Modal-------->
            <div class="modal fade" id="addinstitutemodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Source</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">University</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="institute_name" name="institute_name" placeholder="University Name" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">SMS Sender ID</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="institute_sms_sender_id" name="institute_sms_sender_id" placeholder="SMS Sender ID" required>
                                    </div>
                                </div>
                                <button onclick="addInstitute()" class="btn btn-primary float-right" type="button">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_institutes" class="table-responsive">
			<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-0 basic-datatable">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">University</th>
                        <th scope="col">SMS Sender ID</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                    	<?php

                    	$counter = 0;
                    	foreach ($all_institutes as $institute) {
                    		$counter++;
                    		if(strcasecmp($institute['Status'], 'Y')==0) {
                    			$switch_status = 'checked';
                    			$update_status = 'N';
                    		}
                    		else {
                    			$switch_status = 'unchecked';
                    			$update_status = 'Y';
                    		}
                    		?>
                    		<tr>
		                        <th scope="row"><?php echo $counter; ?></th>
		                        <td><?php echo $institute['Name']; ?></td>
		                        <td><?php echo $institute['Sender_ID']; ?></td>
		                        <td>
		                            <div class="custom-control custom-switch mb-2">
		                                <form method="POST">
		                                	<input type="hidden" name="institute_id" value="<?php echo($institute['ID']); ?>">
		                                	<input type="hidden" name="institute_status" value="<?php echo($update_status); ?>">
		                            		<input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="<?php echo($institute['ID']); ?>">
		                            		<label class="custom-control-label" for="customSwitch1"></label>
		                            	</form>
		                            </div>
		                        </td>
		                        <td>
		                            <i class="fa fa-edit" data-toggle="modal" data-target="#editinstitutemodal<?php echo($institute['ID']); ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
		                            <!----Edit Source Modal-------->
		                            <div class="modal fade" id="editinstitutemodal<?php echo($institute['ID']); ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
		                                <div class="modal-dialog modal-dialog-centered modal-lg">
		                                    <div class="modal-content">
		                                        <div class="modal-header">
		                                            <h5 class="modal-title" id="myCenterModalLabel">Edit University</h5>
		                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                                                <span aria-hidden="true">&times;</span>
		                                            </button>
		                                        </div>
		                                        <div class="modal-body">
		                                            <form method="POST">
		                                            	<input type="hidden" id="institutes_institute_id<?php echo $counter; ?>" value="<?php echo($institute['ID']); ?>">
		                                                <div class="form-group row">
		                                                    <label class="col-lg-2 col-form-label"
		                                                        for="simpleinput">University</label>
		                                                    <div class="col-lg-10">
		                                                        <input type="text" class="form-control" id="institutes_new_institute_name<?php echo $counter; ?>" value="<?php echo($institute['Name']); ?>" required>
		                                                    </div>
		                                                </div>
		                                                 <div class="form-group row">
		                                                    <label class="col-lg-2 col-form-label"
		                                                        for="simpleinput">SMS Sender ID</label>
		                                                    <div class="col-lg-10">
		                                                        <input type="text" class="form-control" id="new_institute_sms_sender_id<?php echo $counter; ?>" value="<?php echo($institute['Sender_ID']); ?>" required>
		                                                    </div>
		                                                </div>
		                                                <button class="btn btn-primary float-right" type="button" onclick="updateInstitute(<?php echo $counter; ?>)">Update</button>
		                                            </form>
		                                        </div>
		                                    </div><!-- /.modal-content -->
		                                </div><!-- /.modal-dialog -->
		                            </div>
		                            <!-- /.modal -->
		                            <i class="fa fa-trash" class="icon-dual icon-xs" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deleteinstitutemodal<?php echo($institute['ID']); ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
		                            <!----delete Source Modal-------->
		                            <div class="modal fade" id="deleteinstitutemodal<?php echo($institute['ID']); ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
		                                <div class="modal-dialog modal-dialog-centered modal-md">
		                                    <div class="modal-content">
		                                        <div class="modal-header">
		                                            <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
		                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                                                <span aria-hidden="true">&times;</span>
		                                            </button>
		                                        </div>
		                                        <div class="modal-body">
		                                        	<form method="POST">
		                                        		<input type="hidden" id="delete_institutes_institute_id<?php echo $counter; ?>" value="<?php echo($institute['ID']); ?>">
		                                        		<center><button class="btn btn-danger textS-center" type="button" onclick="deleteInstitute(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
		                                        	</form>
		                                        </div>
		                                    </div><!-- /.modal-content -->
		                                </div><!-- /.modal-dialog -->
		                            </div>
		                            <!-- /.modal -->
		                        </td>
		                        
		                        </tr>
                    		<?php
                    	}

                    	?>          
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
	function addInstitute() {
		console.log("ok");
		var institute_name = $('#institute_name').val();
		var institute_sms_sender_id = $('#institute_sms_sender_id').val();
		$.ajax
		({
			type: "POST",
			url: "settings_pages/ajax_institute/add_institute.php",
	          data: { "institute_name": institute_name, "institute_sms_sender_id": institute_sms_sender_id },
	          success: function (data) {
	          	$('#addinstitutemodal').modal('hide');
	          	if(data.match("true")) {
	          		toastr.success('Institute added successfully');
	          		$("#accordionExample").load(location.href + " #accordionExample" , function () {
	                $("#headingFive").click();
	              });
	          	}
	          	else {
	          		toastr.error('Unable to add institute');
	          	}
	          }
	      });
		return false;
	}
</script>

<script>
	function updateInstitute(id) {
		var institute_id = $('#institutes_institute_id'.concat(id)).val();
		var new_institute_sms_sender_id = $('#new_institute_sms_sender_id'.concat(id)).val();
		var new_institute_name = $('#institutes_new_institute_name'.concat(id)).val();

		$.ajax
		({
		  type: "POST",
          url: "settings_pages/ajax_institute/update_institute.php",
          data: { "institute_id": institute_id, "new_institute_name": new_institute_name, "new_institute_sms_sender_id": new_institute_sms_sender_id },
          success: function (data) {
          	$('.modal').modal('hide');
          	if(data.match("true")) {
          		toastr.success('Institute updated successfully');
          		$("#accordionExample").load(location.href + " #accordionExample" , function () {
          			$("#headingFive").click();
          		});
          	}
          	else {
          		toastr.error('Unable to update institute');
          	}
          }
		});
		return false;
	}
</script>

<script>
	function deleteInstitute(id) {
		var institute_id = $('#delete_institutes_institute_id'.concat(id)).val();

		$.ajax
		({
		  type: "POST",
          url: "settings_pages/ajax_institute/delete_institute.php",
          data: { "institute_id": institute_id },
          success: function (data) {
          	$('.modal').modal('hide');
          	if(data.match("true")) {
          		toastr.success('Institute deleted successfully');
          		$("#accordionExample").load(location.href + " #accordionExample" , function () {
                $("#headingFive").click();
              });
          	}
          	else {
          		toastr.error('Unable to delete institute');
          	}
          }
		});
		return false;
	}
</script>