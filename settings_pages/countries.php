<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_countries = array();

$countries_query_res = $conn->query("SELECT * FROM Countries Where ID = 101");
while($row = $countries_query_res->fetch_assoc()) {
    $all_countries[] = $row;
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
        <div class="card-header" id="headingEight"><h5 class="m-0 font-size-16">Countries <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addcountrymodal"> <i class="uil uil-plus-circle"></i> Add Country</button>
            <!----Add Source Modal-------->
            <div class="modal fade" id="addcountrymodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Country</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">Country</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="country_name" placeholder="Country">
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="saveCountry()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_countries" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Country</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_countries as $country) {
                            $counter++;
                            /*if(strcasecmp($country['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }*/
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $country['Name']; ?></td>
                                <!--<td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>-->
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editcountrymodal<?php echo $counter; ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editcountrymodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Country</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <input type="hidden" id="country_id<?php echo $counter; ?>" value="<?php echo($country['ID']); ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label"
                                                                for="simpleinput">Country</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_country_name<?php echo $counter; ?>" placeholder="Country Name" value="<?php echo($country['Name']); ?>">
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateCountry(<?php echo $counter; ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletecountrymodal<?php echo $counter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletecountrymodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_country_id<?php echo $counter; ?>" value="<?php echo($country['ID']); ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteCountry(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
  function saveCountry() {
    var country_name = $('#country_name').val();
      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_countries/add_country.php",
          data: { "country_name": country_name },
          success: function (data) {
            $('#addcountrymodal').modal('hide');
            if(data.match("true")) {
                toastr.success('Country added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingEight").click();
                });
            }
            else {
                toastr.error('Unable to add country');
            }
          }
        });
        return false;
  }
</script>

<script>
    function updateCountry(id) {
        var country_id = $('#country_id'.concat(id)).val();
        var new_country_name = $('#new_country_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_countries/update_country.php",
          data: { "country_id": country_id, "new_country_name": new_country_name },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Country updated successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingEight").click();
                });
            }
            else {
                toastr.error('Unable to update country');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteCountry(id) {
        var country_id = $('#delete_country_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_countries/delete_country.php",
          data: { "country_id": country_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Country deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingEight").click();
                });
            }
            else {
                toastr.error('Unable to delete country');
            }
          }
        });
        return false;
    }
</script>