<?php include 'filestobeincluded/header-top.php' ?>
<?php include 'filestobeincluded/header-bottom.php' ?>
<?php include 'filestobeincluded/navigation.php' ?>

<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<?php

$all_campaigns = array();
$rr_query = $conn->query("SELECT * FROM Campaigns ORDER BY Timestamp DESC");
while($row = $rr_query->fetch_assoc()) {
  $all_campaigns[] = $row;
}

?>
        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                                <ol class="breadcrumb">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#addcampaign"><i class="fa fa-plus" style="font-size:12px;"></i> Add Campaign</button>
                                </ol>
                            </nav>
                            <h4 class="mb-1 mt-0">Campaign</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->
                <div class="modal fade" id="addcampaign" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myCenterModalLabel">Add Campaign</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            
                            <div class="modal-body">
                                <form method="POST" id="campaign_form">
                                  <div class="form-group row">
                                        <label class="col-lg-2 col-form-label"
                                            for="campaign_name">Campaign Name</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="campaign_name" name="campaign_name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">University</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select" id="form_university" name="form_university">
                                                <option selected disabled>Choose University</option>
                                                <?php
                                                    $get_university=$conn->query("SELECT * FROM Institutes WHERE Status = 'Y' AND ID > 0");
                                                    while($row=$get_university->fetch_assoc()){?>
                                                        <option value="<?php echo $row['ID']?>"><?php echo $row['Name']; ?></option>
                                                    <?php }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label"
                                            for="form_ID">Form ID</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="form_ID" name="form_ID">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label"
                                            for="form_name">Form Name</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="form_name" name="form_name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Source</label>
                                        <div class="col-lg-4">
                                            <select data-plugin="customselect" class="small" name="form_source" id="form_source">
                                                <option selected disabled>Choose</option>
                                                <?php
                                                foreach ($all_sources as $source) {
                                                    ?>
                                                    <option value="<?php echo $source['ID']; ?>"><?php echo $source['Name']; ?></option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <label class="col-lg-2 col-form-label">Sub-Source</label>
                                        <div class="col-lg-4">
                                            <select data-plugin="customselect" class="small" name="form_sub_source" id="form_sub_source">
                                                <option selected disabled>Choose</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-light" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary" type="button" onclick="addCampaign();">Add</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!---------------------Add Page Content Here-------------------------->


                <!---------------------END Page-------------------------->
            </div> <!-- content -->

            <div class="row" style="margin-left: 10px; margin-right: 10px;">
                  <div class="col-lg-12 card card-body">
                    <div class="table-responsive">
                    <table id="basic-datatable" class="table table-hover mb-0">
                      <thead>
                          <tr>
                          <th scope="col">#</th>
                          <th scope="col">Campaign Name</th>
                          <th scope="col">Form ID</th>
                          <th scope="col">Form Name</th>
                          <th scope="col">Source</th>
                          <th scope="col">Sub-Source</th>
                          <th scope="col">University</th>
                          <th scope="col">Actions</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                         $counter = 0;
                         foreach ($all_campaigns as $campaign) {
                          $counter++;
                          ?>
                          <tr>
                          <th scope="row"><?php echo $counter; ?></th>
                          <td><?php echo $campaign['Campaign_Name']; ?></td>
                          <td><?php echo $campaign['Form_ID']; ?></td>
                          <td><?php echo $campaign['Form_Name']; ?></td>
                          <td>
                            <?php
                            $source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$campaign['Source_ID']."'");
                            $source_res = mysqli_fetch_assoc($source_query);
                            echo $source_res['Name'];
                            ?>
                          </td>
                          <td>
                            <?php
                            $subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$campaign['Subsource_ID']."'");
                            $subsource_res = mysqli_fetch_assoc($subsource_query);
                            echo $subsource_res['Name'];
                            ?>
                          </td>
                          <td>
                            <?php
                            $stage_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$campaign['University_ID']."'");
                            $stage_res = mysqli_fetch_assoc($stage_query);
                            echo $stage_res['Name'];
                            ?>
                          </td>
                          <td>
                          	<!-- <i class="fa fa-edit" style="cursor: pointer;" aria-hidden="true" onclick="editCampaign('<?php echo $campaign['ID'];?>');"></i>&nbsp;&nbsp;&nbsp;&nbsp; -->
                          	<i class="fa fa-trash" onclick="delete_campaign('<?php echo $campaign['ID'];?>');"  style="cursor: pointer;"></i>
                          </td>
                        </tr>
                          <?php
                         }
                        ?>
                      </tbody>
                    </table>
                  </div>
                  </div>
                </div>

<script type='text/javascript'>

		function delete_campaign(id){
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_campaign/delete_campaign.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-delete').html(response); 

					// Display Modal
					$('#deletelead').modal('show'); 
				}
			});
		}
</script>	
<!-------Delete Lead----->
<div class="modal fade" id="deletelead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-delete">
				
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--------End Delete Lead---------->

<script>
    $(document).ready(function() {
        $('#form_source').change(function() {

            $('#form_sub_source').html("<option disabled selected>Select Sub-Source</option>");

            var selected_source = $('#form_source').val();
            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "source_select": "", "selected_source": selected_source },
                success: function(data) {
                    if(data != "") {
                        $('#form_sub_source').html(data);
                    }
                    else {
                        $('#form_sub_source').html("<option disabled selected>Select Sub-Source</option>");
                    }
                }
            });
        });
    });
</script>

<script>
  $(document).ready(function() {
    $('#campaign_form').validate({
      rules: {
        form_university: {
          required: true
        },
        campaign_name: {
          required: true
        },
        form_ID: {
          required: true
        },
        form_name: {
          required: true
        },
        form_source: {
          required: true
        },
        form_sub_source: {
          required: true
        }
      },
      messages: {
        form_university: {
          required: "Please choose a University"
        },
        campaign_name: {
          required: "Campaign Name is required"
        },
        form_ID: {
          required: "Form ID is required"
        },
        form_name: {
          required: "Form Name is required"
        },
        form_source: {
          required: "Source is required"
        },
        form_sub_source: {
          required: "Sub-Source is required"
        }
      },
      highlight: function (element) {
        $(element).addClass('error');
        $(element).closest('.form-control').addClass('has-error');
        $(element).closest('.small').addClass('has-error');
      },
      unhighlight: function (element) {
        $(element).removeClass('error');
        $(element).closest('.form-control').removeClass('has-error');
        $(element).closest('.small').removeClass('has-error');
      }
    });
  });
</script>

<script>
	function addCampaign() {

    if($("#campaign_form").valid()) {
      var campaign_name = $("#campaign_name").val();
      var form_id = $("#form_ID").val();
      var form_name = $("#form_name").val();
      var univ_id = $("#form_university").val();
      var form_source_id = $("#form_source").val();
      var form_subsource_id = $("#form_sub_source").val();

      $.ajax({
        type: "POST",
        url: "/ajax_campaign/add_campaign.php",
        data: { "campaign_name": campaign_name, "form_id": form_id, "form_name": form_name, "univ_id": univ_id, "source_id": form_source_id, "subsource_id": form_subsource_id },
        success: function(data) {
          console.log(data);
          if(data.match("true")) {
            toastr.success("Campaign added successfully");
            $(".modal").modal("hide");
            $('#campaign_form')[0].reset();
            $("#basic-datatable").load(location.href + " #basic-datatable");
          }
          else {
            toastr.error("Unable to add campaign");
          }
        }
      });
      return false;
    }
	}
</script>

<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>