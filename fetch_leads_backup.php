<?php require 'filestobeincluded/db_config.php';?>

<?php

if(isset($_POST["startLimit"]) && isset($_POST["endLimit"]))
{

$startLimit = (int)$_POST['startLimit'];
$endLimit = (int)$_POST['endLimit'];

 $result = $conn->query("SELECT * FROM Leads ORDER BY Timestamp DESC LIMIT ".$startLimit.", ".$endLimit."");
 while($row = $result->fetch_assoc())
 {
  ?>
  <tr>
	<td>
		<div class="row" style="padding-top: 10px;">
			<div class="col-lg-1">
				<div class="custom-control custom-checkbox">
				<input type="checkbox" class="custom-control-input checkbox-function" name="id[]" id="customCheck2<?php echo $row['ID']; ?>" value="<?php echo $row['ID']; ?>">
				<label class="custom-control-label" for="customCheck2<?php echo $row['ID']; ?>"></label>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Name:</b> <?php echo $row['Name']; ?></p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Email:</b> <?php echo substr($row['Email'],0,15).'...'; ?></p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Mobile:</b> <a href="tel:<?php echo $row['Mobile']; ?>"><?php echo $row['Mobile']; ?></a></p>
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
					<ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu mb-0">
							<li class="nav-link" role="button" aria-haspopup="false" aria-expanded="false">
							<?php $fsql = "SELECT COUNT(Lead_ID) as Leadid FROM Follow_Ups WHERE Lead_ID = '".$row['ID']."' GROUP BY Lead_ID"; $fresult = $conn->query($fsql); if ($fresult->num_rows > 0) { while($frow = $fresult->fetch_assoc()) { $gfc = $frow["Leadid"]; }} else { $gfc = "0";} ?>
								<font style="font-size: 24px; cursor:pointer;" onclick="followupmodal(<?php echo $row['ID']; ?>);"><i class="fas fa-user-tie"></i></font>
								<span><mark class="mark1">&nbsp;<?php echo $gfc; ?>&nbsp;</mark></span>
							</a></li>
							<li class="nav-link" role="button" aria-haspopup="false"
								aria-expanded="false">
								<?php $elsql = "SELECT COUNT(Lead_ID) as Leadid FROM Email_Logs WHERE Lead_ID = '".$row['ID']."' GROUP BY Lead_ID"; $elresult = $conn->query($elsql); if ($elresult->num_rows > 0) { while($elrow = $elresult->fetch_assoc()) { $gelc = $elrow["Leadid"]; }} else { $gelc = "0";} 
									$slsql = "SELECT COUNT(Lead_ID) as Leadid FROM SMS_Logs WHERE Lead_ID = '".$row['ID']."' GROUP BY Lead_ID"; $slresult = $conn->query($slsql); if ($slresult->num_rows > 0) { while($slrow = $slresult->fetch_assoc()) { $gslc = $slrow["Leadid"]; }} else { $gslc = "0";} 
									$add_both = $gelc + $gslc;?>
								<font style="font-size: 24px; cursor:pointer;" onclick="responsesmodal(<?php echo $row['ID']; ?>);"><i class="fas fa-user-graduate"></i></font>
								<span><mark class="mark2">&nbsp;<?php echo $add_both ?>&nbsp;</mark></span>
							</a></li>
							<li class="nav-link" role="button" aria-haspopup="false"
								aria-expanded="false">
								<?php $rsql = "SELECT COUNT(ID) as Leadid FROM Re_Enquired WHERE Name = '".$row['Name']."' AND Email = '".$row['Email']."' AND Mobile = '".$row['Mobile']."' AND Institute_ID = '".$row['Institute_ID']."'"; $rresult = $conn->query($rsql); if ($rresult->num_rows > 0) { while($rrow = $rresult->fetch_assoc()) { $grc = $rrow["Leadid"]; }} else { $grc = "0";} ?>
								<font style="font-size: 24px; cursor:pointer;" onclick="re_enquiredmodal(<?php echo $row['ID']; ?>);"><i class="fas fa-user-tie"></i></font>
								<span><mark class="mark3">&nbsp;<?php echo $grc; ?>&nbsp;</mark></span>
							</a></li>
						</ul>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">
						<?php
							$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$row['Counsellor_ID']."'");
							$counsellor = mysqli_fetch_assoc($counsellor_query);
							if($counsellor_query->num_rows > 0){
								$couns = explode(' ',$counsellor['Name']);
							}else{
								$counsellor['Name'] = ' ';
								$couns = $counsellor['Name'];
							}
						?>
						<p><b>Counsellor:</b> <?php echo $couns[0]; ?></p>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>University:</b> <?php
								$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$row['Institute_ID']."'");
								$univ = mysqli_fetch_assoc($univ_query);
								if($univ_query->num_rows > 0){
									echo $univ['Name'];
								}else{
									$course['Name'] = ' ';
									echo $univ['Name'];
								}
							?>
						</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Course:</b> <?php
								$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$row['Course_ID']."'");
								$course = mysqli_fetch_assoc($course_query);
								if($course_query->num_rows > 0){
									echo $course['Name'];
								}else{
									$course['Name'] = ' ';
									echo $course['Name'];
								}
							?>
						</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Specialization:</b> <?php
								$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$row['Specialization_ID']."'");
								$specialization = mysqli_fetch_assoc($specialization_query);
								if($specialization_query->num_rows > 0){
									echo substr($specialization['Name'],0,27);
								}else{
									$specialization['Name'] = ' ';
									echo $specialization['Name'];
								}
							?>
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<?php
							$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$row['Stage_ID']."'");
							$stage = mysqli_fetch_assoc($stage_query);
							if($stage_query->num_rows > 0){
								$lead_stage = $stage['Name'];
							}else{
								$stage['Name'] = ' ';
								$lead_stage = $stage['Name'];
							}
						?>
						<p><b>Stage:</b> <?php echo $lead_stage; ?></p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<?php
							$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$row['Reason_ID']."'");
							$reason = mysqli_fetch_assoc($reason_query);
							if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
								$badge = "success";
							}
							else if(strcasecmp($stage['Name'], "COLD")==0) {
								$badge = "warning";
							}
							else {
								$badge = "danger";
							}
						?>
						<p><b>Reason:</b> <span class="badge badge-soft-<?php echo($badge); ?> py-1">
								<?if($reason_query->num_rows > 0){
									echo $reason['Name'];
								}else{
									$reason['Name'] = ' ';
									echo $reason['Name'];
								}?>
							</span>
						</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<?php
							$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$row['Subsource_ID']."'");
							$subsource = mysqli_fetch_assoc($subsource_query);
							if($subsource_query->num_rows > 0){
								$lead_sub = $subsource['Name'];
							}else{
								$subsource['Name'] = ' ';
								$lead_sub = $subsource['Name'];
							}
						?>
						<p><b>Sub-Source:</b> <?php echo $lead_sub; ?></p>
					</div>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<div class="btn-group">
							<span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" style="cursor: pointer;" onclick="whatsApp(<?php echo $row['ID']; ?>);" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;
							
							<a href="tel:<?php echo $row['Mobile']; ?>"><span data-toggle="tooltip" data-placement="top" data-original-title="Call" title=""><p style="font-size: 20px;"><i class="fa fa-phone" data-toggle="modal" style="cursor: pointer;" data-target="#leadcallmodal" aria-hidden="true"></i></p></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
							<span data-toggle="dropdown"><p style="font-size: 20px;"><i class="fa fa-ellipsis-v" style="cursor: pointer;" aria-hidden="true"></i></p></span>
							<div class="dropdown-menu dropdown-menu-right">
								<span class="dropdown-item"><i class="fas fa-notes-medical" style="font-size: 16px; color: #6C757D;"></i> <font class="addfollowupmodal" onclick="addFollowUp_ajax(<?php echo $row['ID']; ?>);" style="cursor: pointer;">Add Followup</font></span>
								<span class="dropdown-item"><i class="fas fa-user-edit" style="font-size: 16px; color: #6C757D;"></i> <font class="editlead" onclick="editLead(<?php echo $row['ID']; ?>);" style="cursor: pointer;">Edit Lead</font></span>
								<span class="dropdown-item"><i class="fas fa-history" style="font-size: 16px; color: #6C757D;"></i> <font class="leadhistory" onclick="viewLeadHistory(<?php echo $row['ID']; ?>);" style="cursor: pointer;">View History</font></span>
								<span class="dropdown-item"><i class="fas fa-share-alt" style="font-size: 16px; color: #6C757D;"></i> <font class="referlead" onclick="referLead(<?php echo $row['ID']; ?>);" style="cursor: pointer;">Refer Lead</font></span>
								<span class="dropdown-item"><i class="fas fa-trash" style="font-size: 16px; color: #6C757D;"></i> <font class="deletelead" onclick="deleteLead(<?php echo $row['ID']; ?>);" style="cursor: pointer;">Delete</font></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</td>
</tr>
  <?
 }
}

?>