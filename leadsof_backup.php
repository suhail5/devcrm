<?php include 'filestobeincluded/header-top.php' ?>
<script src="https://kit.fontawesome.com/3212b33ef4.js" crossorigin="anonymous"></script>
<?php include 'filestobeincluded/header-bottom.php' ?>
<!-- Pre-loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div>
<!-- End Preloader-->
<?php include 'filestobeincluded/navigation.php' ?>


<?php
if(isset($_GET['counsellor'])){
	$counsellor_id = $_GET['counsellor'];
	$counsellor_ids = "'".$counsellor_id."'";
	$leadof = 'counsellor=';
	$getleadof = 'Counsellor_ID';
}else if(isset($_GET['manager'])){
	$counsellor_id = $_GET['manager'];
	$all_array = array();
    $coun_array = array();
    $couns_array = array();
    $get_tree = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '". $counsellor_id ."'");
    while($row = $get_tree->fetch_assoc()) {
        $all_array[] = $row;
    }
    foreach($all_array as $univ){
        $university = $univ['Institute_ID'];
        $get_counsellors = $conn->query("SELECT * FROM users WHERE Institute_ID = $university AND Role = 'Counsellor'");
        while($rows = $get_counsellors->fetch_assoc()) {
            $coun_array[] = $rows;
        }
      }
        foreach($coun_array as $cuniv){
            $couns_array[]=$cuniv['ID'];                                                     
        }
        $imp = "'" . implode( "','", ($couns_array) ) . "'";
        $counsellor_ids = $imp.",'".$counsellor_id."'";
	$leadof = 'manager=';
	$getleadof = 'Counsellor_ID';
}elseif(isset($_GET['university'])){
	$counsellor_id = $_GET['university'];
	$counsellor_ids = "'".$counsellor_id."'";
	$leadof = 'university=';
	$getleadof = 'Institute_ID';
}


$all_leads = array();
$all_users = array();

if(!isset($_GET['leads'])){
	$leads_query_res = $conn->query("SELECT * FROM Leads WHERE $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
}

if(isset($_GET['leads'])){
	$lead_query = $_GET['leads'];
	if($lead_query=='all'){
		$leads_query_res = $conn->query("SELECT * FROM Leads  WHERE $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}else if($lead_query=='1'){
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}
	else if($lead_query=='2'){
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}
	else if($lead_query=='3'){
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}else if($lead_query=='4'){
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}else if($lead_query=='5'){
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}else if($lead_query=='6'){
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}else if($lead_query=='7'){
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND $getleadof in ($counsellor_ids) ORDER BY TimeStamp DESC");
		while($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}
}

?>


        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1 navbuttons">
                                    <ol class="breadcrumb">
										
                                    </ol>
                            </nav>
                            <h4 class="mb-1 mt-0">Leads</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->
													
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
								<div style="padding-top: 20px; padding-bottom:20px;">
								<?php 
								$allleads_query = $conn->query("SELECT * FROM Leads WHERE $getleadof in ($counsellor_ids)");
								$row_count_allleads = mysqli_num_rows($allleads_query);
								?>
								<a href="?leads=all&<?php echo $leadof ?><?php echo $counsellor_id ?>" class="btn btn-light">All leads (<?php echo $row_count_allleads ?>)</a>
								<?php 
								$newleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 1 AND $getleadof in ($counsellor_ids)");
								$row_count_newleads = mysqli_num_rows($newleads_query);
								?>
								<a href="?leads=1&<?php echo $leadof ?><?php echo $counsellor_id ?>" class="btn btn-light">New leads (<?php echo $row_count_newleads ?>)</a>
								<?php 
								$connectedleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 2 AND $getleadof in ($counsellor_ids)");
								$row_count_connectedleads = mysqli_num_rows($connectedleads_query);
								?>
								<a href="?leads=2&<?php echo $leadof ?><?php echo $counsellor_id ?>" class="btn btn-light">Follow-up (<?php echo $row_count_connectedleads ?>)</a>
								<?php 
								$notintrestedleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 3 AND $getleadof in ($counsellor_ids)");
								$row_count_notintrestedleads = mysqli_num_rows($notintrestedleads_query);
								?>
								<a href="?leads=3&<?php echo $leadof ?><?php echo $counsellor_id ?>" class="btn btn-light">Not Intrested (<?php echo $row_count_notintrestedleads ?>)</a>
								<?php 
								$registrationleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 4 AND $getleadof in ($counsellor_ids)");
								$row_count_registrationleads = mysqli_num_rows($registrationleads_query);
								?>
								<a href="?leads=4&<?php echo $leadof ?><?php echo $counsellor_id ?>" class="btn btn-light">Registration Done (<?php echo $row_count_registrationleads ?>)</a>
								<?php 
								$admissionleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 5 AND $getleadof in ($counsellor_ids)");
								$row_count_admissionleads = mysqli_num_rows($admissionleads_query);
								?>
								<a href="?leads=5&<?php echo $leadof ?><?php echo $counsellor_id ?>" class="btn btn-light">Admission Done (<?php echo $row_count_admissionleads ?>)</a>
								<?php 
								$b2bleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 6 AND $getleadof in ($counsellor_ids)");
								$row_count_b2bleads = mysqli_num_rows($b2bleads_query);
								?>
								<a href="?leads=6&<?php echo $leadof ?><?php echo $counsellor_id ?>" class="btn btn-light">Intrested for B2B (<?php echo $row_count_b2bleads ?>)</a>
								<?php 
								$notconnectedleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 7 AND $getleadof in ($counsellor_ids)");
								$row_count_notconnectedleads = mysqli_num_rows($notconnectedleads_query);
								?>
								<a href="?leads=7&<?php echo $leadof ?><?php echo $counsellor_id ?>" class="btn btn-light">Not Connected (<?php echo $row_count_notconnectedleads ?>)</a>

								<button class="btn btn-light" id="reenquiredleadsbutton">Re-enquired</button>

								</div>
								<div class="table-responsive">
								<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
								<form id="checkbox-form" method="POST">
                                <table id="datatable-buttons" class="table table-striped table-borderless table-hover" data-export-title="Blackboard_Leads_<?php echo date("Y-m-d h:i a"); ?>">
									<tbody>
									<?php foreach ($all_leads as $lead) { ?>
										<tr>
											<td>
												<div class="row" style="padding-top: 10px;">
													<div class="col-lg-1">
														<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input checkbox-function" name="id[]" id="customCheck2<?php echo $lead['ID']; ?>" value="<?php echo $lead['ID']; ?>">
														<label class="custom-control-label" for="customCheck2<?php echo $lead['ID']; ?>"></label>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Name:</b> <?php echo $lead['Name']; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Email:</b> <?php echo substr($lead['Email'],0,15).'...'; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Mobile:</b> <a href="tel:<?php echo $lead['Mobile']; ?>"><?php echo $lead['Mobile']; ?></a></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																	<?php 
																	$get_followup_remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' ORDER BY ID DESC LIMIT 1");
																	if($get_followup_remark->num_rows > 0){
																		$re_mark = mysqli_fetch_assoc($get_followup_remark);
																		if(strlen($re_mark['Remark'])>40){
																			echo '<p><b>Remark:</b>&nbsp;' . substr($re_mark['Remark'],0,40).'...<button type="button" onclick="pop();" class="btn btn-link btn-sm" data-container="body" title=""
																			data-toggle="popover" data-placement="left"
																			data-content= "'.$re_mark['Remark'].'"
																			data-original-title="Remark">
																			Read More
																		</button>';
																		}else{
																			echo '<p><b>Remark:</b>&nbsp;' . $re_mark['Remark'];
																		}
																		
																	}else{
																		echo '';
																	}

																?>
																
																
																
																</p>
															</div>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
															<ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu mb-0">
																	<li class="nav-link" role="button" aria-haspopup="false" aria-expanded="false">
																	<?php $fsql = "SELECT COUNT(Lead_ID) as Leadid FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' GROUP BY Lead_ID"; $fresult = $conn->query($fsql); if ($fresult->num_rows > 0) { while($frow = $fresult->fetch_assoc()) { $gfc = $frow["Leadid"]; }} else { $gfc = "0";} ?>
																		<font style="font-size: 24px; cursor:pointer;" onclick="followupmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-tie"></i></font>
																		<span><mark class="mark1">&nbsp;<?php echo $gfc; ?>&nbsp;</mark></span>
																	</a></li>
																	<li class="nav-link" role="button" aria-haspopup="false"
																		aria-expanded="false">
																		<?php $elsql = "SELECT COUNT(Lead_ID) as Leadid FROM Email_Logs WHERE Lead_ID = '".$lead['ID']."' GROUP BY Lead_ID"; $elresult = $conn->query($elsql); if ($elresult->num_rows > 0) { while($elrow = $elresult->fetch_assoc()) { $gelc = $elrow["Leadid"]; }} else { $gelc = "0";} 
																			$slsql = "SELECT COUNT(Lead_ID) as Leadid FROM SMS_Logs WHERE Lead_ID = '".$lead['ID']."' GROUP BY Lead_ID"; $slresult = $conn->query($slsql); if ($slresult->num_rows > 0) { while($slrow = $slresult->fetch_assoc()) { $gslc = $slrow["Leadid"]; }} else { $gslc = "0";} 
																			$add_both = $gelc + $gslc;?>
																		<font style="font-size: 24px; cursor:pointer;" onclick="responsesmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-graduate"></i></font>
																		<span><mark class="mark2">&nbsp;<?php echo $add_both ?>&nbsp;</mark></span>
																	</a></li>
																	<li class="nav-link" role="button" aria-haspopup="false"
																		aria-expanded="false">
																		<?php $rsql = "SELECT COUNT(ID) as Leadid FROM Re_Enquired WHERE Name = '".$lead['Name']."' AND Email = '".$lead['Email']."' AND Mobile = '".$lead['Mobile']."' AND Institute_ID = '".$lead['Institute_ID']."'"; $rresult = $conn->query($rsql); if ($rresult->num_rows > 0) { while($rrow = $rresult->fetch_assoc()) { $grc = $rrow["Leadid"]; }} else { $grc = "0";} ?>
																		<font style="font-size: 24px; cursor:pointer;" onclick="re_enquiredmodal(<?php echo $lead['ID']; ?>);"><i class="fas fa-user-tie"></i></font>
																		<span><mark class="mark3">&nbsp;<?php echo $grc; ?>&nbsp;</mark></span>
																	</a></li>
																</ul>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">
																<?php
																	$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$lead['Counsellor_ID']."'");
																	$counsellor = mysqli_fetch_assoc($counsellor_query);
																	if($counsellor_query->num_rows > 0){
																		$couns = explode(' ',$counsellor['Name']);
																	}else{
																		$counsellor['Name'] = ' ';
																		$couns = $counsellor['Name'];
																	}
																?>
																<p><b>Counsellor:</b> <?php echo $couns[0]; ?></p>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>University:</b> <?php
																		$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$lead['Institute_ID']."'");
																		$univ = mysqli_fetch_assoc($univ_query);
																		if($univ_query->num_rows > 0){
																			echo $univ['Name'];
																		}else{
																			$course['Name'] = ' ';
																			echo $univ['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Course:</b> <?php
																		$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
																		$course = mysqli_fetch_assoc($course_query);
																		if($course_query->num_rows > 0){
																			echo $course['Name'];
																		}else{
																			$course['Name'] = ' ';
																			echo $course['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Specialization:</b> <?php
																		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
																		$specialization = mysqli_fetch_assoc($specialization_query);
																		if($specialization_query->num_rows > 0){
																			echo substr($specialization['Name'],0,27);
																		}else{
																			$specialization['Name'] = ' ';
																			echo $specialization['Name'];
																		}
																	?>
																</p>
															</div>

															<div class="col-lg-12 col-md-3 col-sm-12">
																
																<?php 
																	$get_followup_date = $conn->query("SELECT Followup_Timestamp FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' ORDER BY ID DESC LIMIT 1");
																	if($get_followup_date->num_rows > 0){
																		$current_timestamp = date('Y-m-d h:i:s');
																		$date = mysqli_fetch_assoc($get_followup_date);
																		if($current_timestamp < $date['Followup_Timestamp']){
																			echo '<p><b>Next Follow-up Date:</b>&nbsp;'. date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
																		}else{
																			echo '<p><b>Previous Follow-up Date:</b>&nbsp;'. date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
																		}
																		
																	}else{
																		echo '';
																	}

																?>
																</p>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
																	$stage = mysqli_fetch_assoc($stage_query);
																	if($stage_query->num_rows > 0){
																		$lead_stage = $stage['Name'];
																	}else{
																		$stage['Name'] = ' ';
																		$lead_stage = $stage['Name'];
																	}
																?>
																<p><b>Stage:</b> <?php echo $lead_stage; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
																	$reason = mysqli_fetch_assoc($reason_query);
																	if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
																		$badge = "success";
																	}
																	else if(strcasecmp($stage['Name'], "COLD")==0) {
																		$badge = "warning";
																	}
																	else {
																		$badge = "danger";
																	}
																?>
																<p><b>Reason:</b> <span class="badge badge-soft-<?php echo($badge); ?> py-1">
																		<?if($reason_query->num_rows > 0){
																			echo $reason['Name'];
																		}else{
																			$reason['Name'] = ' ';
																			echo $reason['Name'];
																		}?>
																	</span>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$lead['Subsource_ID']."'");
																	$subsource = mysqli_fetch_assoc($subsource_query);
																	if($subsource_query->num_rows > 0){
																		$lead_sub = $subsource['Name'];
																	}else{
																		$subsource['Name'] = ' ';
																		$lead_sub = $subsource['Name'];
																	}
																?>
																<p><b>Sub-Source:</b> <?php echo $lead_sub; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Creation Date: </b>
																<?php 
																	$get_creation_date = $conn->query("SELECT TimeStamp FROM History WHERE Lead_ID = '".$lead['ID']."' ORDER BY ID ASC LIMIT 1");
																	if($get_creation_date->num_rows > 0){
																		$date = mysqli_fetch_assoc($get_creation_date);
																		echo date("F j, Y g:i a", strtotime($date["TimeStamp"]));
																	}else{
																		echo date("F j, Y g:i a", strtotime($lead["TimeStamp"]));
																	}

																?>
																</p>
															</div>
														</div>
													</div>
													<div class="col-lg-1">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<div class="btn-group">
																	<span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" style="cursor: pointer;" data-id="<?php echo $lead['ID']; ?>" aria-hidden="true"></i></p></span>&nbsp;&nbsp;
																	
																	<a href="tel:<?php echo $lead['Mobile']; ?>"><span data-toggle="tooltip" data-placement="top" data-original-title="Call" title=""><p style="font-size: 20px;"><i class="fa fa-phone" data-toggle="modal" style="cursor: pointer;" data-target="#leadcallmodal" aria-hidden="true"></i></p></span></a>&nbsp;&nbsp;
																	<span data-toggle="dropdown"><p style="font-size: 20px;"><i class="fa fa-ellipsis-v" style="cursor: pointer;" aria-hidden="true"></i></p></span>
																	<div class="dropdown-menu dropdown-menu-right">
																		<span class="dropdown-item"><i class="fas fa-notes-medical" style="font-size: 16px; color: #6C757D;"></i> <font class="addfollowupmodal" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Add Followup</font></span>
																		<span class="dropdown-item"><i class="fas fa-user-edit" style="font-size: 16px; color: #6C757D;"></i> <font class="editlead" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Edit Lead</font></span>
																		<span class="dropdown-item"><i class="fas fa-history" style="font-size: 16px; color: #6C757D;"></i> <font class="leadhistory" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">View History</font></span>
																		<span class="dropdown-item"><i class="fas fa-share-alt" style="font-size: 16px; color: #6C757D;""></i> <font class="referlead" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Refer Lead</font></span>
																		<span class="dropdown-item"><i class="fas fa-trash" style="font-size: 16px; color: #6C757D;"></i> <font class="deletelead" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Delete</font></span>
																	</div>
																</div>
															</div>
														</div>
														
														
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
								</form>								
							</div>


<!--------------------ICON MODAL-------------->

<script type='text/javascript'>

		function followupmodal(id) {
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"userid": userid},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}
</script>

<script type='text/javascript'>

		function responsesmodal(id) {
			
			var responseid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"responseid": responseid},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}
</script>

<script type='text/javascript'>

		function re_enquiredmodal(id) {
			
			var renquiredid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"renquiredid": renquiredid},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}
</script>

<div class="modal fade" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-slideout modal-md">
    <div class="modal-content" id="right-modal-body">
      
    </div>
  </div>
</div>
<!--------------------ICON MODAL-------------->
							
<script type='text/javascript'>
$(document).ready(function () {
    $(".checkbox-selectall").click(function () {
		$(".checkbox-function").prop('checked', $(this).prop('checked'));
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide1").css({display: "block"});
			$("#divShowHide2").css({display: "block"});
			$("#divShowHide3").css({display: "block"});
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide1").css({display: "none"});
			$("#divShowHide2").css({display: "none"});
			$("#divShowHide3").css({display: "none"});
			$("#divShowHide4").css({display: "none"});
		}
    });
    
    $(".checkbox-function").change(function(){
        if (!$(this).prop("checked")){
            $(".checkbox-selectall").prop("checked",false);
        }
	});
	
	$(".checkbox-function").click(function () {
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide1").css({display: "block"});
			$("#divShowHide2").css({display: "block"});
			$("#divShowHide3").css({display: "block"});
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide1").css({display: "none"});
			$("#divShowHide2").css({display: "none"});
			$("#divShowHide3").css({display: "none"});
			$("#divShowHide4").css({display: "none"});
		}
    });

});           	
</script>

<!-------Refer Selected modal-------->
<div class="modal fade" id="selected_refer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Refer Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-refer-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Refer Selected modal-------->
<!-------SMS Selected modal-------->
<div class="modal fade" id="selected_sms" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Send SMS to Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-sms-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End SMS Selected modal-------->
<!-------SMS Selected modal-------->
<div class="modal fade" id="selected_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Send Mail to Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-mail-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End SMS Selected modal-------->
<!-------Delete Selected modal-------->
<div class="modal fade" id="selected_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-xs">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Delete Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-delete-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Delete Selected modal-------->

<script type='text/javascript'>
	$(document).ready(function(){

		$('.whatsapp').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/whatsapp.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-whatsapp').html(response); 

					// Display Modal
					$('#whatsappmessage').modal('show'); 
				}
			});
		});
	});
</script>
<!-------whatsapp modal-------->
<div class="modal fade" id="whatsappmessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
	<div class="modal-dialog modal-xs" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Send WhatsApp Message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-whatsapp">
			</div>	
		</div>
	</div>
</div>
<!-------whatsapp modal-------->

<script type='text/javascript'>
	$(document).ready(function(){

		$('.addfollowupmodal').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/add_followup.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-addfollowup').html(response); 

					// Display Modal
					$('#addfollowup').modal('show'); 
				}
			});
		});
	});
</script>												
<!-------Add followup modal-------->
<div class="modal fade" id="addfollowup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Followup</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-addfollowup">
				</div>
			</div>
		</div>
	</div>
<!-------Add Followup modal-------->


<script type='text/javascript'>
	$(document).ready(function(){

		$('.editlead').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/edit_lead_form.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-edit').html(response); 

					// Display Modal
					$('#editlead').modal('show'); 
				}
			});
		});
	});
</script>
<!--------Edit Lead----------->
<div class="modal fade" id="editlead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myCenterModalLabel">Edit Lead Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-edit">
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- /.modal end-->
<!--------Edit Lead End----------->


<script type='text/javascript'>
	$(document).ready(function(){

		$('.leadhistory').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/lead_history.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-history').html(response); 

					// Display Modal
					$('#viewhistory').modal('show'); 
				}
			});
		});
	});
</script>
<!-------History modal-------->
<div class="modal fade" id="viewhistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">History</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-history">
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-------End History modal-------->

<script type='text/javascript'>
	$(document).ready(function(){

		$('.referlead').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/refer_lead.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-refer').html(response); 

					// Display Modal
					$('#referlead').modal('show'); 
				}
			});
		});
	});
</script>								
<!--------refer lead---->
<div id="referlead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="referallleadsLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="referallleadsLabel">Refer Lead </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-refer">
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!------End refer lead------>


<script type='text/javascript'>
	$(document).ready(function(){

		$('.deletelead').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/del_lead.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-delete').html(response); 

					// Display Modal
					$('#deletelead').modal('show'); 
				}
			});
		});
	});
</script>	
<!-------Delete Lead----->
<div class="modal fade" id="deletelead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-delete">
				
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--------End Delete Lead---------->

<div id="referselectedlead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="referselectedleadLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="referselectedleadLabel">Refer  Leads to</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group row">
					<label class="col-lg-2 col-form-label">Select Counsellor</label>
					<div class="col-lg-10">
						<select data-plugin="customselect" class="form-control" id="refer_lead_owner">
							<option disabled selected>Choose</option>
							<?php
								$result_refer_lead = $conn->query("SELECT * FROM users");
								while($refer_leads = $result_refer_lead->fetch_assoc()) {
							?>
								<option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-2 col-form-label">Add Comment</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" id="referal_comment"
							placeholder="">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" >&nbsp;&nbsp;Confirm&nbsp;&nbsp;</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->













					</div>
				</div>
			</div>
	</div>
</div> <!-- content -->

<script>
	function setEmailStatus() {
		if($('#swemail').val() == 'on') {
			$('#swemail').val('off');
		}
		else {
			$('#swemail').val('on');
		}
	}
</script>

<script>
	function setSMSStatus() {
		if($('#swsms').val() == 'on') {
			$('#swsms').val('off');
		}
		else {
			$('#swsms').val('on');
		}
	}
</script>

<script>
	function addLead() {
		if($('#step_1_form').valid() && $('#step_2_form').valid() && $('#step_3_form').valid() && $('#step_4_form').valid() && $('#step_5_form').valid()) {
			var stageID = $('#stage_select').val();
			var reasonID = $('#reason_select').val();
			var fullName = $('#full_name').val();
			var emailID = $('#email_id').val();
			var mobileNumber = $('#mobile_number').val();
			var remarks = $('#remarks').val();

			var address = $('#address').val();
			var stateID = $('#state_select').val();
			var cityID = $('#city_select').val();
			var pincode = $('#pin_code').val();

			var sourceID = $('#source_select').val();
			var subsourceID = $('#subsource_select').val();
			var campaignName = $('#campaign_name').val();
			var leadOwner = $('#lead_owner').val();

			var swEmail = $('#swemail').val();
			var swSMS = $('#swsms').val();

			var schoolName = $('#school_name').val();
			var percentage = $('#percentage').val();
			var qualification = $('#qualification').val();
			var refer = $('#refer').val();

			var instituteID = $('#select_institute').val();
			var courseID = $('#select_course').val();
			var specializationID = $('#select_specialization').val();

			if(swEmail == 'on') {
				$.ajax
		        ({
		          type: "POST",
		          url: "/Mailer/send_mail.php",
		          data: { "leadName": fullName, "emailID": emailID },
		          success: function (data) {
		          	console.log(data);
		            if(data.match("true")) {
		                toastr.success('Welcome Email sent!');
		            }
		            else {
		                toastr.error('Unable to send welcome Email');
		            }
		          }
		        });
			}

			if(swSMS == 'on') {
				$.ajax
		        ({
		          type: "POST",
		          url: "/Twilio/index.php",
		          data: { "leadName": fullName, "phoneNumber": mobileNumber },
		          success: function (data) {
		          	console.log(data);
		            if(data.match("true")) {
		                toastr.success('Welcome SMS sent!');
		            }
		            else {
		                toastr.error('Unable to send welcome SMS');
		            }
		          }
		        });
			}

			$.ajax
	        ({
	          type: "POST",
	          url: "/ajax_leads/add_lead.php",
	          data: { "stageID": stageID, "reasonID":  reasonID, "fullName": fullName, "emailID": emailID, "mobileNumber": mobileNumber, "remarks": remarks, "address": address, "stateID": stateID, "cityID": cityID, "pincode": pincode, "sourceID": sourceID, "subsourceID": subsourceID, "campaignName": campaignName, "leadOwner": leadOwner, "schoolName": schoolName, "percentage": percentage, "qualification": qualification, "refer": refer, "instituteID": instituteID, "courseID": courseID, "specializationID": specializationID },
	          success: function (data) {
	          	console.log(data);
	            $('#leadadd').modal('hide');
	            if(data.match("true")) {
	                toastr.success('Lead added successfully');
	                window.location.reload();
	            }
	            else {
	                toastr.error('Unable to add lead');
	            }
	          }
	        });
	        return false;
		}
		else {
			if('step_1_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 0);
			}
			if('step_2_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 1);
			}
			if('step_3_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 2);
			}
			if('step_4_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 3);
			}
			if('step_5_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 4);
			}
		}
	}
</script>


<script>
	function mywizard(){
		$('#smartwizard-arrows').smartWizard({
			theme: 'arrows'
		});

		$('#step_1_form').validate({
			rules: {
				stage_select: {
					required: true
				},
				full_name: {
					required: true
				},
				email_id: {
					email: "Please enter a valid email address."
				},
				mobile_number: {
					required: true,
					minlength: 10,
					maxlength: 10
				},
				remarks: {
					required: true
				}
			},
			messages: {
				stage_select: {
					required: 'Please select a stage'
				},
				full_name: {
					required: 'Name is required'
				},
				mobile_number: {
					required: 'Mobile number is required',
					minlength: 'Invalid mobile number',
					maxlength: 'Invalid mobile number'
				},
				remarks: {
					required: 'Remarks are required'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});

		$('#step_2_form').validate({
			rules: {
				state_select: {
					required: true
				},
				pin_code: {
					minlength: 6,
					maxlength: 6
				}
			},
			messages: {
				state_select: {
					required: 'Please select a state'
				},
				pin_code: {
					minlength: 'Invalid Pincode',
					minlength: 'Invalid Pincode'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});

		$('#step_3_form').validate({
			rules: {
				source_select: {
					required: true
				}
			},
			messages: {
				source_select: {
					required: 'Please select a source'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});

		$('#step_5_form').validate({
			rules: {
				select_institute: {
					required: true
				}
			},
			messages: {
				select_institute: {
					required: 'Please select an university'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});

		$("#smartwizard-arrows").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
			if(currentStepIndex == 0) {
				if($('#step_1_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
			if(currentStepIndex == 1) {
				if($('#step_2_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
			if(currentStepIndex == 2) {
				if($('#step_3_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
			if(currentStepIndex == 3) {
				if($('#step_4_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
			if(currentStepIndex == 4) {
				if($('#step_5_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
		});
};
</script>

<script>
	$(document).ready(function() {
		$('#stage_select').change(function() {

			$('#reason_select').html("<option disabled selected>Select Reason</option>");

			var selected_stage = $('#stage_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "stage_select": "", "selected_stage": selected_stage },
				success: function(data) {
					if(data != "") {
						$('#reason_select').html(data);
					}
					else {
						$('#reason_select').html("<option disabled selected>Select Reason</option>");
					}
				}
			});
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#state_select').change(function() {

			$('#city_select').html("<option disabled selected>Select City</option>");

			var selected_state = $('#state_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "state_select": "", "selected_state": selected_state },
				success: function(data) {
					if(data != "") {
						$('#city_select').html(data);
					}
					else {
						$('#city_select').html("<option disabled selected>Select City</option>");
					}
				}
			});
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#source_select').change(function() {

			$('#subsource_select').html("<option disabled selected>Select Sub-Source</option>");

			var selected_source = $('#source_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "source_select": "", "selected_source": selected_source },
				success: function(data) {
					if(data != "") {
						$('#subsource_select').html(data);
					}
					else {
						$('#subsource_select').html("<option disabled selected>Select Sub-Source</option>");
					}
				}
			});
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#select_institute').change(function() {

			$('#select_course').html("<option disabled selected>Select Course</option>");

			var selected_institute = $('#select_institute').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "select_institute": "", "selected_institute": selected_institute },
				success: function(data) {
					if(data != "") {
						$('#select_course').html(data);
						jQuery('#select_course').trigger('change');
					}
					else {
						$('#select_course').html("<option disabled selected>Select Course</option>");
					}
				}
			});
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#select_course').change(function() {
			console.log("yes");

			$('#select_specialization').html("<option disabled selected>Select Specialization</option>");

			var selected_course = $('#select_course').val();
			var selected_institute = $('#select_institute').val();

			console.log(selected_institute);
			
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "select_course": "", "selected_course": selected_course, "selected_institute": selected_institute },
				success: function(data) {
					console.log(data);
					if(data != "") {
						$('#select_specialization').html(data);
					}
					else {
						$('#select_specialization').html("<option disabled selected>Select Specialization</option>");
					}
				}
			});
		});
	});
</script>


<!-----Edit Lead Script--------->
<script>
	function updateLead(id) {
			var new_leadID = $('#new_leadID').val();
			var new_stageID = $('#new_stage_select').val();
			var new_reasonID = $('#new_reason_select').val();
			var new_fullName = $('#new_full_name').val();
			var new_emailID = $('#new_email_id').val();
			var new_mobileNumber = $('#new_mobile_number').val();
			var new_remarks = $('#new_remarks').val();

			var new_address = $('#new_address').val();
			var new_stateID = $('#new_state_select').val();
			var new_cityID = $('#new_city_select').val();
			var new_pincode = $('#new_pin_code').val();

			var new_sourceID = $('#new_source_select').val();
			var new_subsourceID = $('#new_subsource_select').val();
			var new_campaignName = $('#new_campaign_name').val();
			var new_leadOwner = $('#new_select_counsellor').val();

			var new_schoolName = $('#new_school_name').val();
			var new_percentage = $('#new_percentage').val();
			var new_qualification = $('#new_qualification').val();
			var new_refer = $('#new_refer').val();

			var new_instituteID = $('#new_select_institute').val();
			var new_courseID = $('#new_select_course').val();
			var new_specializationID = $('#new_select_specialization').val();


			$.ajax
	        ({
	          type: "POST",
	          url: "/ajax_leads/edit_lead.php",
	          data: {"new_leadID": new_leadID, "new_stageID": new_stageID, "new_reasonID":  new_reasonID, "new_fullName": new_fullName, "new_emailID": new_emailID, "new_mobileNumber": new_mobileNumber, "new_remarks": new_remarks, "new_address": new_address, "new_stateID": new_stateID, "new_cityID": new_cityID, "new_pincode": new_pincode, "new_sourceID": new_sourceID, "new_subsourceID": new_subsourceID, "new_campaignName": new_campaignName, "new_leadOwner": new_leadOwner, "new_schoolName": new_schoolName, "new_percentage": new_percentage, "new_qualification": new_qualification, "new_refer": new_refer, "new_instituteID": new_instituteID, "new_courseID": new_courseID, "new_specializationID": new_specializationID },
	          success: function (data) {
	          	console.log(data);
				  $('.modal').modal('hide');
	            if(data.match("true")) {
	                toastr.success('Lead updated successfully');
	                window.location.reload();
	            }
	            else {
	                toastr.error('Unable to update lead');
	            }
	          }
	        });
	        return false;
		
	}
</script>

<script>
	$(document).ready(function() {
		$('#new_stage_select').change(function() {

			$('#new_reason_select').html("<option disabled selected>Select Reason</option>");

			var new_selected_stage = $('#new_stage_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "stage_select": "", "selected_stage": new_selected_stage },
				success: function(data) {
					if(data != "") {
						$('#new_reason_select').html(data);
					}
					else {
						$('#new_reason_select').html("<option disabled selected>Select Reason</option>");
					}
				}
			});
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#new_select_institute').change(function() {

			$('#new_select_course').html("<option disabled selected>Select Course</option>");

			var new_selected_institute = $('#new_select_institute').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "select_institute": "", "selected_institute": new_selected_institute },
				success: function(data) {
					if(data != "") {
						$('#new_select_course').html(data);
						getSpecializations();
					}
					else {
						$('#new_select_course').html("<option disabled selected>Select Course</option>");
					}
				}
			});
		});
	});
</script>

<script>
	function getSpecializations() {

		$('#new_select_specialization').html("<option disabled selected>Select Specialization</option>");

		var new_selected_course = $('#new_select_course').val();
		
		$.ajax
		({
			type: "POST",
			url: "onselect/onSelect.php",
			data: { "select_course": "", "selected_course": new_selected_course },
			success: function(data) {
				if(data != "") {
					$('#new_select_specialization').html(data);
				}
				else {
					$('#new_select_specialization').html("<option disabled selected>Select Specialization</option>");
				}
			}
		});
	}
</script>

<script>
    function deleteLeads(id) {
        var delete_lead_id = $('#delete_lead_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "ajax_leads/delete_lead.php",
          data: { "delete_lead_id": delete_lead_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
				toastr.success('Lead deleted successfully');
				window.location.reload();
            }
            else {
                toastr.error('Unable to delete lead');
            }
          }
        });
        return false;
    }
</script>

<script>  
      $(document).ready(function(){  
           $('#upload_csv').on("submit", function(e){  
                e.preventDefault(); //form will not submitted  
                $.ajax({  
					url: "ajax_leads/upload_lead.php",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){  
                     	console.log(data);
						$('.modal').modal('hide');
						if(data.match("true")) {
							toastr.success('Lead uploaded successfully');
							//window.location.reload();
						}
						else {
							toastr.error('Unable to upload lead');
						}  
                     }  
                })  
           });  
      });  
 </script>

<script type='text/javascript'>
	function updateOwner(){
            var lead_id = $('#lead_id').val();
			var new_stage = $('#follow_stage_select').val();
			var new_reason = $('#follow_reason').val();
            var lead_owner_id = $('#lead_owner').val();
            var refer_lead_owner_id = $('#refer_lead_owner').val();
            var refer_comment = $('#referal_comment').val();
        $.ajax
        ({
          type: "POST",
          url: "ajax_leads/refer_lead_sql.php",
          data: { "lead_owner_id": lead_owner_id, "refer_lead_owner_id": refer_lead_owner_id, "refer_comment": refer_comment, "lead_id": lead_id, "new_stage":new_stage, "new_reason":new_reason },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
				toastr.success('Lead refered successfully');
				window.location.reload();
            }
            else {
                toastr.error('Unable to refer lead');
            }
          }
        });
        return false;
    }
</script>


<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>