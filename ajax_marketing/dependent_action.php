<?php
require '../filestobeincluded/db_config.php';

$value = $_POST['val'];
$sql = $conn->query("SELECT template_name FROM Email_Templates");
$result=mysqli_fetch_assoc($sql);
echo json_encode($result);
?>
   <!-- Dependent Action Modal -->
            						<!---Rule Modal------>
                                    <div class="modal fade" id="addDependent" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Add Dependent Action</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="row">
                                                    <div class="col-lg-2">
                                                      <h1 class="dependent-if-then-label">IF</h1>  
                                                    </div>

                                                    <div class="col-lg-9 if-block">
                                                        <div class="wrapper row" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                            <div class="col-lg-4">
                                                                <p class="should-be-text dependent-template-name" title="Ipb Fresh1">SMS : Ipb Fresh1</p>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <p class="should-be-text" style="font-size: 20px;">is</p>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <select class="form-control">
                                                                    <option>Select Status</option>
                                                                    <option>Test1</option>
                                                                    <option>Test2</option>
                                                                </select>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>


                                                   <div class="row" style="margin-top: 15px;">
                                                    <div class="col-lg-2">
                                                        <h1 class="dependent-if-then-label">THEN</h1>
                                                    </div>

                                                    <div class="col-lg-9">
                                                        <div class="row">
                                                            <div class="col-lg-12 if-block">
                                                                <div class="wrapper row" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                                    <div class="col-lg-4">
                                                                    	<select class="form-control">
                                                                    <option>Select Status</option>
                                                                    <option>Test1</option>
                                                                    <option>Test2</option>
                                                                </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12" style="margin-top: 10px;">
                                                                    <input type="text" disabled="" class="btn btn-primary btn-sm operator" id="operator_value" value="AND">
                                                                    
                                                                </div>
                                                                <div class="col-lg-12 if-block" style="margin-top: 10px;">
                                                                    <div class="wrapper row" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                                       
                                                                        <div class="communication-modes">
                                                                            <select class="form-control">
                                                                    <option>Select Status</option>
                                                                    <option>Test1</option>
                                                                    <option>Test2</option>
                                                                </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      
                                                  </div>

                                                  
                                                    
                                                        
                                                    
                                                        
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                </div>
                                <!-- End Dependent Modal -->

                                <!---Rule Modal------>
                                    <div class="modal fade" id="edit_addDependent" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="edit_myCenterModalLabel">Add Dependent Action</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="row">
                                                    <div class="col-lg-2">
                                                      <h1 class="dependent-if-then-label">IF</h1>  
                                                    </div>

                                                    <div class="col-lg-9 if-block">
                                                        <div class="wrapper row" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                            <div class="col-lg-4">
                                                                <p class="should-be-text dependent-template-name" title="Ipb Fresh1">SMS : Ipb Fresh1</p>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <p class="should-be-text" style="font-size: 20px;">is</p>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <select class="form-control">
                                                                    <option>Select Status</option>
                                                                    <option>Test1</option>
                                                                    <option>Test2</option>
                                                                </select>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>


                                                   <div class="row" style="margin-top: 15px;">
                                                    <div class="col-lg-2">
                                                        <h1 class="dependent-if-then-label">THEN</h1>
                                                    </div>

                                                    <div class="col-lg-9">
                                                        <div class="row">
                                                            <div class="col-lg-12 if-block">
                                                                <div class="wrapper row" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                                    <div class="col-lg-4">
                                                                        <select class="form-control">
                                                                    <option>Select Status</option>
                                                                    <option>Test1</option>
                                                                    <option>Test2</option>
                                                                </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12" style="margin-top: 10px;">
                                                                    <input type="text" disabled="" class="btn btn-primary btn-sm operator" id="edit_operator_value" value="AND">
                                                                    
                                                                </div>
                                                                <div class="col-lg-12 if-block" style="margin-top: 10px;">
                                                                    <div class="wrapper row" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                                       
                                                                        <div class="communication-modes">
                                                                            <select class="form-control">
                                                                    <option>Select Status</option>
                                                                    <option>Test1</option>
                                                                    <option>Test2</option>
                                                                </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      
                                                  </div>

                                                  
                                                    
                                                        
                                                    
                                                        
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                </div>
                                <!-- End Dependent Modal -->