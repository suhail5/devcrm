<?php require 'filestobeincluded/db_config.php'; ?>
<?php


$itemPerPage = 25;
$page = 1;
if (!empty($_GET['itemPerPage']))
	$itemPerPage = $_GET['itemPerPage'];
if (!empty($_GET['page']))
	$page = $_GET['page'];

$offset = (($page - 1) * $itemPerPage);
$flag = 1;

$str = '';


if ($_GET['searchstring'] != '') {
	$str = mysqli_real_escape_string($conn, $_GET['searchstring']);
}

$tabValue = '';
if ($_GET['tabValue'] != '') {
	$tabValue = $_GET['tabValue'];
}
$subdivision = '';
if (isset($_GET['subdivision']) != '') {
	$subdivision = $_GET['subdivision'];
}
$from_date = '';
$to_date = '';
if (isset($_GET['lead_date'])) {
	if (strpos($_GET['lead_date'], 'to') > 0) {
		$date = explode('to', $_GET['lead_date']);
		$from_date = date($date[0]);
		$to_date = date($date[1]);
	} else {
		$from_date = date($_GET['lead_date']);
	}
}





//echo $request->bookmark; die;
// if lead Date
if ($from_date != '' && $to_date != '') {
	if ($str != '') {
		if ($tabValue != '') {
			if ($tabValue == 0 && $subdivision != '') {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM `Leads` LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 0) {


				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp ,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue > 0  && $tabValue <= 10  && $subdivision != '') {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Stage_ID = '" . $tabValue . "' AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Stage_ID = '" . $tabValue . "' AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue > 0  && $tabValue <= 10) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID = $tabValue AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID = $tabValue AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 10) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID = $tabValue AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID = $tabValue AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
		}
	} else {
		if ($tabValue != '') {
			if ($tabValue > 0 && $subdivision == null) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID= '" . $tabValue . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT " . $itemPerPage . " OFFSET " . $offset . "");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID= '" . $tabValue . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue > 0 && $tabValue <= 10 && $subdivision != null) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID = '" . $tabValue . "' AND Leads.Reason_ID = '" . $subdivision . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT " . $itemPerPage . " OFFSET " . $offset . "");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID = '" . $tabValue . "' AND Leads.Reason_ID = '" . $subdivision . "' GROUP BY Leads.ID  ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 0) {

				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}

			if ($tabValue > 0  && $tabValue <= 10 && $subdivision == null) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID = '" . $tabValue . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) >= '" . $from_date . "' AND date(Leads.Created_at) <= '" . $to_date . "') AND Leads.Stage_ID = $tabValue GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
		}
	}
}
// end if lead Date 
elseif ($from_date != '') {
	if ($str != '') {
		if ($tabValue != '') {
			if ($tabValue == 0 && $subdivision != '') {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 0) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp ,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) = '" . $from_date . "'') AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue > 0  && $tabValue <= 10  && $subdivision != '') {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Stage_ID = '" . $tabValue . "' AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Stage_ID = '" . $tabValue . "' AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue > 0  && $tabValue <= 10) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Stage_ID = $tabValue AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Stage_ID = $tabValue AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
		}
	} else {
		if ($tabValue != '') {
			if ($tabValue > 0 && $subdivision == null) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Stage_ID= '" . $tabValue . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT " . $itemPerPage . " OFFSET " . $offset . "");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Stage_ID= '" . $tabValue . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue > 0 && $tabValue <= 10 && $subdivision != null) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Stage_ID = '" . $tabValue . "' AND Leads.Reason_ID = '" . $subdivision . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT " . $itemPerPage . " OFFSET " . $offset . "");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Stage_ID = '" . $tabValue . "' AND Leads.Reason_ID = '" . $subdivision . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 0) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}

			if ($tabValue > 0  && $tabValue <= 10 && $subdivision == null) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Stage_ID = '" . $tabValue . "' GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN History ON Leads.ID=History.Lead_ID WHERE (date(Leads.Created_at) = '" . $from_date . "') AND Leads.Stage_ID = $tabValue GROUP BY Leads.ID ORDER BY Leads.TimeStamp DESC")->num_rows;
			}
		}
	}
}
// else lead date
else {
	if ($str != '') {
		if ($tabValue != '') {
			if ($tabValue == 0 && $subdivision != '') {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') ORDER BY TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 0) {

				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') ORDER BY TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 1  || $tabValue == 2 || $tabValue == 3 || $tabValue == 4 || $tabValue == 5 || $tabValue == 6 || $tabValue == 7 || $tabValue == 8 || $tabValue == 10 && $subdivision != '') {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Stage_ID = $tabValue AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Stage_ID = $tabValue AND Leads.Reason_ID='" . $subdivision . "' AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') ORDER BY TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 1  || $tabValue == 2 || $tabValue == 3 || $tabValue == 4 || $tabValue == 5 || $tabValue == 6 || $tabValue == 7 || $tabValue == 8 || $tabValue == 10) {
				$result = $conn->query("SELECT Leads.ID as ID,Leads.State_ID,Leads.Name As Name,Leads.dob As dob,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Leads.TimeStamp,Leads.Created_at FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Leads.Stage_ID = $tabValue AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT Leads.ID as ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Leads.Stage_ID = $tabValue AND (Leads.Name LIKE '%$str%' OR Leads.Email LIKE '%$str%' OR Leads.Mobile LIKE '%$str%' OR users.Name LIKE '%$str%' OR Institutes.Name LIKE '%$str%' OR Leads.Alt_Mobile LIKE '%$str%' OR Leads.urnno LIKE '%$str%') ORDER BY TimeStamp DESC")->num_rows;
			}
		}
	} else {
		if ($tabValue != '') {
			if ($tabValue > 0 && $subdivision == null) {
				$result = $conn->query("SELECT * FROM Leads WHERE Stage_ID= '" . $tabValue . "' ORDER BY TimeStamp DESC LIMIT " . $itemPerPage . " OFFSET " . $offset . "");
				$totRecords = $conn->query("SELECT ID FROM Leads WHERE Stage_ID= '" . $tabValue . "' ORDER BY TimeStamp DESC")->num_rows;
			}
			if ($tabValue > 0 && $tabValue < 10 && $subdivision != null) {
				$result = $conn->query("SELECT * FROM Leads WHERE Stage_ID = '" . $tabValue . "' AND Reason_ID = '" . $subdivision . "' ORDER BY TimeStamp DESC LIMIT " . $itemPerPage . " OFFSET " . $offset . "");
				$totRecords = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = '" . $tabValue . "' AND Reason_ID = '" . $subdivision . "' ORDER BY TimeStamp DESC")->num_rows;
			}
			if ($tabValue == 0) {


				$result = $conn->query("SELECT * FROM Leads ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT ID FROM Leads ORDER BY TimeStamp DESC")->num_rows;
			}
			if ($tabValue > 0  && $tabValue < 10 && $subdivision == null) {
				$result = $conn->query("SELECT * FROM Leads WHERE Stage_ID = '" . $tabValue . "' ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
				$totRecords = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = $tabValue ORDER BY TimeStamp DESC")->num_rows;
			}
		}
	}
}
// end else lead date







//$totRecords=$jobs1->get()->count();

//$jobs=$jobs1->offset($offset)->limit($itemPerPage)->orderby('jobs.id', 'desc')->get();
$totalPages = ceil($totRecords / $itemPerPage);
?>
<div id="totalrecords" style="float:left;">
	<p>
		<b>Total Leads:</b> <?php echo $totRecords; ?>
	</p>
</div>
<?php



if ($totalPages > 1) {
	echo '<div id="pagination" class="candidates-list" style="float:right;margin-bottom:10px;">
                      <span id="pagination_info" data-totalpages ="' . $totalPages . '"  data-currentpage ="' . $page . '" >
                          Page ' . $page . ' out of ' . $totalPages . '  Pages
                      </span>
                      <a href="javascript:void(0)" data-page="' . $page . '" id="pre" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" onClick="getList(`pre`)";>
                          Previous
                      </a>
                      <a href="javascript:void(0)" id="next" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" data-page ="' . $page . '" onClick="getList(`next`)";>
                          Next
                      </a>
                </div><br><br>';
}
while ($row = $result->fetch_assoc()) {

	echo '<tr>
	<td>
		<div class="row" style="padding-top: 10px;" id="row' . $row['ID'] . '">
			<div class="col-lg-1">
				<div class="custom-control custom-checkbox">
				<input type="checkbox" onclick="checkbox()" class="custom-control-input checkbox-function" name="id[]" id="customCheck2' . $row['ID'] . '" value="' . $row['ID'] . '">
				<label class="custom-control-label" for="customCheck2' . $row['ID'] . '"></label>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p id="person_name' . $row['ID'] . '" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy name"><b>Name:</b> ' . $row['Name'] . '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p id="person_email' . $row['ID'] . '" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy email"><b>Email:</b> ' . $row['Email'] . ' </p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p id="person_mobile' . $row['ID'] . '" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy mobile"><b>Mobile:</b> <a href="tel:' . $row['Mobile'] . '">' . $row['Mobile'] . '</a></p>
					</div>';
	if ($row['Alt_Mobile'] != '') {
		echo '<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Alt Mobile:</b> <a href="tel:' . $row['Alt_Mobile'] . '">' . $row['Alt_Mobile'] . '</a></p>
						</div>';
	}
	echo '
					<div class="col-lg-12 col-md-3 col-sm-12">'; ?>
	<?php
	$get_followup_remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '" . $row['ID'] . "' ORDER BY ID DESC LIMIT 1");
	if ($get_followup_remark->num_rows > 0) {
		$re_mark = mysqli_fetch_assoc($get_followup_remark);
		if (strlen($re_mark['Remark']) > 40) {
			echo '<p><b>Remark:</b>&nbsp;' . substr($re_mark['Remark'], 0, 40) . '...<button type="button" onclick="pop();" class="btn btn-link btn-sm" data-container="body" title=""
									data-toggle="popover" data-placement="left"
									data-content= "' . $re_mark['Remark'] . '"
									data-original-title="Remark">
									Read More
								</button>';
		} else {
			echo '<p><b>Remark:</b>&nbsp;' . $re_mark['Remark'];
		}
	} else {
		echo '';
	}

	?>



	<?php echo '</p>
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu mb-0">
							<li class="nav-link" role="button" aria-haspopup="false" aria-expanded="false">';
	$fsql = "SELECT COUNT(Lead_ID) as Leadid FROM Follow_Ups WHERE Lead_ID = '" . $row['ID'] . "' GROUP BY Lead_ID";
	$fresult = $conn->query($fsql);
	if ($fresult->num_rows > 0) {
		while ($frow = $fresult->fetch_assoc()) {
			$gfc = $frow["Leadid"];
		}
	} else {
		$gfc = "0";
	}
	echo '<font style="font-size: 24px; cursor:pointer;" onclick="followupmodal(' . $row['ID'] . ');"><i class="fas fa-user-tie"></i></font>
								<span><mark class="mark1">&nbsp;' . $gfc . '&nbsp;</mark></span>
							</a></li>
							<li class="nav-link" role="button" aria-haspopup="false"
								aria-expanded="false">';
	$elsql = "SELECT COUNT(Lead_ID) as Leadid FROM Email_Logs WHERE Lead_ID = '" . $row['ID'] . "' GROUP BY Lead_ID";
	$elresult = $conn->query($elsql);
	if ($elresult->num_rows > 0) {
		while ($elrow = $elresult->fetch_assoc()) {
			$gelc = $elrow["Leadid"];
		}
	} else {
		$gelc = "0";
	}
	$slsql = "SELECT COUNT(Lead_ID) as Leadid FROM SMS_Logs WHERE Lead_ID = '" . $row['ID'] . "' GROUP BY Lead_ID";
	$slresult = $conn->query($slsql);
	if ($slresult->num_rows > 0) {
		while ($slrow = $slresult->fetch_assoc()) {
			$gslc = $slrow["Leadid"];
		}
	} else {
		$gslc = "0";
	}
	$clsql = "SELECT COUNT(Lead_ID) as Leadid FROM Call_Logs WHERE Lead_ID = '" . $row['ID'] . "' GROUP BY Lead_ID";
	$clresult = $conn->query($clsql);
	if ($clresult->num_rows > 0) {
		while ($clrow = $clresult->fetch_assoc()) {
			$gclc = $clrow["Leadid"];
		}
	} else {
		$gclc = "0";
	}
	$add_both = $gelc + $gslc + $gclc;
	echo '<font style="font-size: 24px; cursor:pointer;" onclick="responsesmodal(' . $row['ID'] . ');"><i class="fas fa-user-graduate"></i></font>
								<span><mark class="mark2">&nbsp;' . $add_both . '&nbsp;</mark></span>
							</a></li>
							<li class="nav-link" role="button" aria-haspopup="false"
								aria-expanded="false">';
	$lead_name = mysqli_real_escape_string($conn, $row['Name']);
	$rsql = "SELECT COUNT(ID) as Leadid FROM Re_Enquired WHERE Name = '" . $lead_name . "' AND Email = '" . $row['Email'] . "' AND Mobile = '" . $row['Mobile'] . "' AND Institute_ID = '" . $row['Institute_ID'] . "'";
	$rresult = $conn->query($rsql);
	if ($rresult->num_rows > 0) {
		while ($rrow = $rresult->fetch_assoc()) {
			$grc = $rrow["Leadid"];
		}
	} else {
		$grc = "0";
	}
	echo '<font style="font-size: 24px; cursor:pointer;" onclick="re_enquiredmodal(' . $row['ID'] . ');"><i class="fas fa-user-tie"></i></font>
								<span><mark class="mark3">&nbsp;' . $grc . '&nbsp;</mark></span>
							</a></li>
						</ul>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">';

	$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '" . $row['Counsellor_ID'] . "'");
	$counsellor = mysqli_fetch_assoc($counsellor_query);
	if ($counsellor_query->num_rows > 0) {
		$couns = $counsellor['Name'];
	} else {
		$counsellor['Name'] = ' ';
		$couns = $counsellor['Name'];
	}

	echo '<p><b>Counsellor:</b> ' . $couns . '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">';
	$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '" . $row['Institute_ID'] . "'");
	$univ = mysqli_fetch_assoc($univ_query);
	$suv_id_query = $conn->query("SELECT urnno from Leads where ID ='" . $row['ID'] . "'");
	$suv = mysqli_fetch_assoc($suv_id_query);
	$newdate = date("d-M-Y", strtotime($row['dob']));
	if (($univ['ID'] == 51 || $univ['ID'] == 60) && $suv['urnno']) {
		echo '<p><b>IDOL Password:</b> ';
		echo strtoupper(str_replace('-', '', $newdate)) . '</p>';
	}
	echo '
					</div>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>University:</b>';

	if ($univ_query->num_rows > 0) {
		if (strcasecmp($univ['Name'], 'Admin') == 0) {
			$univ['Name'] = '';
			echo $univ['Name'];
		} else {
			echo $univ['Name'];
		}
	} else {
		$univ['Name'] = ' ';
		echo $univ['Name'];
	}

	echo '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Course:</b>';
	$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '" . $row['Course_ID'] . "'");
	$course = mysqli_fetch_assoc($course_query);
	if ($course_query->num_rows > 0) {
		echo $course['Name'];
	} else {
		$course['Name'] = ' ';
		echo $course['Name'];
	}

	echo '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Specialization:</b>';
	$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '" . $row['Specialization_ID'] . "'");
	$specialization = mysqli_fetch_assoc($specialization_query);
	if ($specialization_query->num_rows > 0) {
		echo substr($specialization['Name'], 0, 27);
	} else {
		$specialization['Name'] = ' ';
		echo $specialization['Name'];
	}
	echo '</p>
                </div>';
	if ($univ['ID'] == 64) {
		echo '<div class="col-lg-12 col-md-3 col-sm-12" id="svu' . $row['ID'] . '">
					<p><b>SVU ID:</b>';

		if ($suv['urnno']) {
			echo $suv['urnno'];
		} else {

			echo '<button type="button" onclick="callSVUApi(&#39;' . $row['ID'] . '&#39;,&#39;' . $row['dob'] . '&#39;,&#39;' . $row['Specialization_ID'] . '&#39;,&#39;' . $row['Email'] . '&#39;)" class="btn btn-sm btn-outline-primary mx-1">Generate SVU ID</button>';
		}
		echo '</p>
					</div>';
	} else if ($univ['ID'] == 51 || $univ['ID'] == 60) {
		echo '<div class="col-lg-12 col-md-3 col-sm-12" id="idolid' . $row['ID'] . '">
					<p><b>IDOL ID:</b>';
		$suv_id_query = $conn->query("SELECT urnno from Leads where ID ='" . $row['ID'] . "'");
		$suv = mysqli_fetch_assoc($suv_id_query);
		if ($suv['urnno']) {
			echo $suv['urnno'];
		} else {
			echo '<button type="button" onclick="callCuApi(&#39;' . $row['ID'] . '&#39;);" class="btn btn-sm btn-outline-primary mx-1">Generate IDOL ID</button>';
		}
		echo '</p>
					</div>';
	}

	echo '
                <div class="col-lg-12 col-md-3 col-sm-12">
                  <p><b>Source:</b>';

	$source_query = $conn->query("SELECT ID,Name FROM Sources WHERE ID = '" . $row['Source_ID'] . "'");
	$source = mysqli_fetch_assoc($source_query);

	if ($source['Name']) {
		echo $source['Name'];
	}



	echo '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						'; ?>
	<?php
	$get_followup_date = $conn->query("SELECT Followup_Timestamp FROM Follow_Ups WHERE Lead_ID = '" . $row['ID'] . "' ORDER BY ID DESC LIMIT 1");
	if ($get_followup_date->num_rows > 0) {
		$current_timestamp = date('Y-m-d h:i:s');
		$date = mysqli_fetch_assoc($get_followup_date);
		if ($current_timestamp < $date['Followup_Timestamp']) {
			echo '<p><b>Next Follow-up Date:</b>&nbsp;' . date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
		} else {
			echo '<p><b>Previous Follow-up Date:</b>&nbsp;' . date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
		}
	} else {
		echo '';
	}

	?>

	<?php echo '</p>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">';

	$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '" . $row['Stage_ID'] . "'");
	$stage = mysqli_fetch_assoc($stage_query);
	if ($stage_query->num_rows > 0) {
		$lead_stage = $stage['Name'];
	} else {
		$stage['Name'] = ' ';
		$lead_stage = $stage['Name'];
	}

	echo '<p><b>Stage:</b> ' . $lead_stage . '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">';

	$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '" . $row['Reason_ID'] . "'");
	$reason = mysqli_fetch_assoc($reason_query);
	if (strcasecmp($stage['Name'], "NEW") == 0 || strcasecmp($stage['Name'], "FRESH") == 0) {
		$badge = "success";
	} else if (strcasecmp($stage['Name'], "COLD") == 0) {
		$badge = "warning";
	} else {
		$badge = "danger";
	}

	echo '<p><b>Reason:</b> <span class="badge badge-soft-' . $badge . ' py-1">';
	if ($reason_query->num_rows > 0) {
		echo $reason['Name'];
	} else {
		$reason['Name'] = ' ';
		echo $reason['Name'];
	}
	echo '</span>
						</p>
					</div>

					<div class="col-lg-12 col-md-3 col-sm-12">';
	$state_query = $conn->query("SELECT * FROM States WHERE ID = '" . $row['State_ID'] . "'");
	$state = mysqli_fetch_assoc($state_query);
	if ($state_query->num_rows > 0) {
		$lead_state = $state['Name'];
	} else {
		$state['Name'] = ' ';
		$lead_state = $state['Name'];
	}



	echo '<p><b>State:</b>' . $lead_state . '</p>
							  </div>';
	if ($row['Stage_ID'] == 8) {
		echo '<div class="col-lg-12 col-md-3 col-sm-12">';

		$source_query = $conn->query("SELECT ID,Name FROM Sources WHERE ID = '" . $row['Source_ID'] . "'");
		$source = mysqli_fetch_assoc($source_query);
		if ($source_query->num_rows > 0) {
			$lead_source = $source['Name'];
		} else {
			$source['Name'] = ' ';
			$lead_source = $source['Name'];
		}

		echo '<p><b>Source:</b> ' . $lead_source . '</p>
						</div>';
	}


	echo '<div class="col-lg-12 col-md-3 col-sm-12">';

	$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '" . $row['Subsource_ID'] . "'");
	$subsource = mysqli_fetch_assoc($subsource_query);
	if ($subsource_query->num_rows > 0) {
		$lead_sub = $subsource['Name'];
	} else {
		$subsource['Name'] = ' ';
		$lead_sub = $subsource['Name'];
	}


	echo '<p><b>Sub-Source:</b> ' . $lead_sub . '</p>
					</div>';
	if ($row['Stage_ID'] == 8) {
		$renquired_date = $conn->query("SELECT Sources.Name,Re_Enquired.TimeStamp FROM Re_Enquired LEFT JOIN Sources ON Re_Enquired.Source_ID = Sources.ID  WHERE Re_Enquired.Lead_ID = '" . $row['ID'] . "' ORDER BY Re_Enquired.TimeStamp DESC");
		$renquired = mysqli_fetch_assoc($renquired_date);
		echo '<div class="col-lg-12 col-md-3 col-sm-12">
							<p><b>Renquired Date:</b>'; ?>
		<?php


		//echo $renquired["TimeStamp"];
		if (isset($renquired["TimeStamp"]) != '') {
			echo date("F j, Y g:i a", strtotime($renquired["TimeStamp"]));
		} else {
			echo '';
		}

		?>

		<?php echo '</p>
						</div><div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Renquired Source:</b>'; ?>
		<?php


		//echo $renquired["TimeStamp"];
		if (isset($renquired["Name"]) != '') {
			echo $renquired["Name"];
		} else {
			echo '';
		}

		?>

	<?php echo '</p>
					</div>';
	}

	echo '<div class="col-lg-12 col-md-3 col-sm-12">
							<p><b>Creation Date:</b>'; ?>
	<?php


	echo date("F j, Y g:i a", strtotime($row["Created_at"]));


	?>

<?php echo '</p>
						</div>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<div class="btn-group">
							<span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" style="cursor: pointer;" onclick="whatsApp(' . $row['ID'] . ');" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;


							
							<span data-container="body" title="" data-toggle="popover" data-placement="bottom" onclick="callLeads(' . $row['ID'] . ');" data-content="" data-original-title=""><p style="font-size: 20px;"><i class="fa fa-phone" style="cursor: pointer;" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;
							<span data-toggle="dropdown"><p style="font-size: 20px;"><i class="fa fa-ellipsis-v re_hide" style="cursor: pointer;" aria-hidden="true"></i></p></span>
							<div class="dropdown-menu dropdown-menu-right" >
								<span class="dropdown-item"><i class="fas fa-notes-medical" style="font-size: 16px; color: #6C757D;"></i> <font class="addfollowupmodal" onclick="addFollowUp_ajax(' . $row['ID'] . ');" style="cursor: pointer;">Add Followup</font></span>';
	if ($_SESSION['useremployeeid'] != 'CV_2021_SVU01') {
		echo '<span class="dropdown-item"><i class="fas fa-user-edit" style="font-size: 16px; color: #6C757D;"></i> <font class="editlead" onclick="editLead(' . $row['ID'] . ');" style="cursor: pointer;">Edit Lead</font></span>';
	}

	echo '<span class="dropdown-item"><i class="fas fa-history" style="font-size: 16px; color: #6C757D;"></i> <font class="leadhistory" onclick="viewLeadHistory(' . $row['ID'] . ');" style="cursor: pointer;">View History</font></span>';
	if ($_SESSION['useremployeeid'] != 'CV_2021_SVU01') {
		echo '<span class="dropdown-item"><i class="fas fa-share-alt" style="font-size: 16px; color: #6C757D;"></i> <font class="referlead" onclick="referLead(' . $row['ID'] . ');" style="cursor: pointer;">Refer Lead</font></span>';
		echo '<span class="dropdown-item"><i class="fas fa-trash" style="font-size: 16px; color: #6C757D;"></i> <font class="deletelead" onclick="deleteLead(' . $row['ID'] . ');" style="cursor: pointer;">Delete</font></span>';
	}
	echo '</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</td>
</tr>';
}
if ($totalPages > 1) {
	echo '<div id="pagination" class="candidates-list" style="float:right">
                                <span id="pagination_info" data-totalpages ="' . $totalPages . '"  data-currentpage ="' . $page . '">
                                    Page ' . $page . ' out of ' . $totalPages . '  Pages
                                </span>
                                <a href="javascript:void(0)" data-page="' . $page . '" id="pre" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" onClick="getList(`pre`)";>
                                    Previous
                                </a>
                                <a href="javascript:void(0)" id="next" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" data-page ="' . $page . '" onClick="getList(`next`)";>
                                    Next
                                </a>';
}
?>