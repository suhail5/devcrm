<?php 
require '../filestobeincluded/db_config.php';
date_default_timezone_set('Asia/Kolkata');
$userid = $_POST['userid'];
 
$sql = "select * from Leads where ID=".$userid;
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);
?>

<style type="text/css">
    .tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 2px 10px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}
</style>
<div class="row">
    <div class="tab">
  <button class="tablinks" value="1" onclick="viewLeadHistory('<?php echo $userid; ?>')">History</button>
  <button class="tablinks" value="2" onclick="Refer('<?php echo $userid; ?>')">Refer</button>
  
</div>
    <div class="col-lg-12" id="historyLead">
        <div class="row">
            <div class="col-lg-12">
                <?php $timestamp = $row['TimeStamp']; ?>
                <p><b>Created on:</b> <font style="font-weight: 600;"><b><?php echo date("F j, Y g:i a", strtotime($row["TimeStamp"])) ?></b></font></p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-6">
                <p><b>Full Name:</b> <font style="font-weight: 600;"><?php echo $row['Name']; ?></font></p>
            </div>
            <div class="col-lg-6">
                <p><b>Campaign Name:</b> <font style="font-weight: 600;"><?php echo $row['CampaignName']; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <p><b>Email:</b> <font style="font-weight: 600;"><?php echo $row['Email']; ?></font></p>
            </div>
            <div class="col-lg-6">
                <p><b>Mobile:</b> <font style="font-weight: 600;"><?php echo $row['Mobile']; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <p><b>Alternate Mobile:</b> <font style="font-weight: 600;"><?php echo $row['Alt_Mobile']; ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$row['Counsellor_ID']."'");
                $counsellor = mysqli_fetch_assoc($counsellor_query);
                $couns_name = $counsellor['Name'];?>
                <p><b>Counsellor:</b> <font style="font-weight: 600;"><?php echo $couns_name; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$row['Stage_ID']."'");
                $stage = mysqli_fetch_assoc($stage_query);?>
                <p><b>Stage:</b> <font style="font-weight: 600;"><?php echo $stage['Name']; ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$row['Reason_ID']."'");
                $reason = mysqli_fetch_assoc($reason_query);
                if($reason_query->num_rows>0){
                    $reason_name = $reason['Name'];
                }else{
                    $reason_name = ' ';
                }
                ?>
                <p><b>Reason:</b> <font style="font-weight: 600;"><?php echo $reason_name; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$row['Source_ID']."'");
                $source = mysqli_fetch_assoc($source_query);
                if($source_query->num_rows>0){
                    $source_name = $source['Name'];
                }else{
                    $source_name = ' ';
                }?>
                <p><b>Source:</b> <font style="font-weight: 600;"><?php echo $source_name; ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$row['Subsource_ID']."'");
                $subsource = mysqli_fetch_assoc($subsource_query);
                if($subsource_query->num_rows>0){
                    $subsource_name = $subsource['Name'];
                }else{
                    $subsource_name = ' ';
                }
                ?>
                <p><b>Sub-source:</b> <font style="font-weight: 600;"><?php echo $subsource_name ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$row['Institute_ID']."'");
                $university = mysqli_fetch_assoc($univ_query);
                if($univ_query->num_rows>0){
                    $university_name = $university['Name'];
                }else{
                    $university_name = ' ';
                }
                ?>
                <p><b>University:</b> <font style="font-weight: 600;"><?php echo $university_name; ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$row['Course_ID']."'");
                $course = mysqli_fetch_assoc($course_query);
                if($course_query->num_rows>0){
                    $course_name = $course['Name'];
                }else{
                    $course_name = ' ';
                }
                ?>
                <p><b>Course:</b> <font style="font-weight: 600;"><?php echo $course_name; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $spec_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$row['Specialization_ID']."'");
                $specialization = mysqli_fetch_assoc($spec_query);
                if($spec_query->num_rows>0){
                    $specialization_name = $specialization['Name'];
                }else{
                    $specialization_name = ' ';
                }
                ?>
                <p><b>Specialization:</b> <font style="font-weight: 600;"><?php echo $specialization_name; ?></font></p>
            </div>
            <div class="col-lg-12">
                <p><b>Remarks:</b> <font style="font-weight: 600;"><?php echo $row['Remarks']; ?></font></p>
            </div>
            <br>
            <hr>
            <div class="col-lg-12">
                <p><b><u>Follow Up Details</u> - </b></p>
            </div>
            <?php
                $all_follow_ups = array();

                $followups_query_res = $conn->query("SELECT * FROM Follow_Ups WHERE Lead_ID = $userid ORDER BY ID DESC");
                while($row = $followups_query_res->fetch_assoc()) {
                    $all_follow_ups[] = $row;
                }
                foreach($all_follow_ups as $fup){ ?>
            <div class="col-lg-6">
                <p><b>Follow-up Date:</b> <font style="font-weight: 500;"><?php echo date("F j, Y g:i a", strtotime($fup["Followup_Timestamp"])) ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$fup['Counsellor_ID']."'");
                $counsellor = mysqli_fetch_assoc($counsellor_query);
                if(isset($counsellor['Name'])!='')
                {
                    $couns_name = $counsellor['Name'];
                }
                else {
                    $couns_name = '';
                }
                ?>
                <p><b>Follow-up Created by:</b> <font style="font-weight: 600;"><?php echo $couns_name; ?></font></p>
            </div>
            <div class="col-lg-12" style="padding-bottom:20px;">
                <p><b>Follow Up Comment:</b> <font style="font-weight: 600;"><?php echo $fup['Remark']; ?></font></p>
            </div>
            
            <?php
                }
            ?>
            

        </div>
    </div>
</div>
<br><hr>
<?php
$lead_history = array();
$sql_history = "select * from History where Lead_ID='$userid' ORDER BY ID DESC";
$result_history = $conn->query($sql_history);
while($history_row = mysqli_fetch_assoc($result_history)){
    $lead_history[]=$history_row;
}
foreach($lead_history as $history_rows){
?>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <?php $timestamp = $history_rows['TimeStamp']; ?>
                <p><b>Created on:</b> <font style="font-weight: 600;"><b><?php echo date("F j, Y g:i a", strtotime($history_rows["TimeStamp"])) ?></b></font></p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-6">
                <p><b>Full Name:</b> <font style="font-weight: 600;"><?php echo $history_rows['Name']; ?></font></p>
            </div>
            <div class="col-lg-6">
                <p><b>Campaign Name:</b> <font style="font-weight: 600;"><?php echo $history_rows['CampaignName']; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <p><b>Email:</b> <font style="font-weight: 600;"><?php echo $history_rows['Email']; ?></font></p>
            </div>
            <div class="col-lg-6">
                <p><b>Mobile:</b> <font style="font-weight: 600;"><?php echo $history_rows['Mobile']; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <p><b>Alternate Mobile:</b> <font style="font-weight: 600;"><?php echo $history_rows['Alt_Mobile']; ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$history_rows['Counsellor_ID']."'");
                $counsellor = mysqli_fetch_assoc($counsellor_query);
                $couns_name = $counsellor['Name'];?>
                <p><b>Counsellor:</b> <font style="font-weight: 600;"><?php echo $couns_name; ?></font></p>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$history_rows['Stage_ID']."'");
                $stage = mysqli_fetch_assoc($stage_query);?>
                <p><b>Stage:</b> <font style="font-weight: 600;"><?php echo $stage['Name']; ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$history_rows['Reason_ID']."'");
                $reason = mysqli_fetch_assoc($reason_query);
                if($reason_query->num_rows>0){
                    $reason_name = $reason['Name'];
                }else{
                    $reason_name = ' ';
                }
                ?>
                <p><b>Reason:</b> <font style="font-weight: 600;"><?php echo $reason_name; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$history_rows['Source_ID']."'");
                $source = mysqli_fetch_assoc($source_query);
                if($source_query->num_rows>0){
                    $source_name = $source['Name'];
                }else{
                    $source_name = ' ';
                }?>
                <p><b>Source:</b> <font style="font-weight: 600;"><?php echo $source_name; ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$history_rows['Subsource_ID']."'");
                $subsource = mysqli_fetch_assoc($subsource_query);
                if($subsource_query->num_rows>0){
                    $subsource_name = $subsource['Name'];
                }else{
                    $subsource_name = ' ';
                }
                ?>
                <p><b>Sub-source:</b> <font style="font-weight: 600;"><?php echo $subsource_name ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$history_rows['Institute_ID']."'");
                $university = mysqli_fetch_assoc($univ_query);
                if($univ_query->num_rows>0){
                    $university_name = $university['Name'];
                }else{
                    $university_name = ' ';
                }
                ?>
                <p><b>University:</b> <font style="font-weight: 600;"><?php echo $university_name; ?></font></p>
            </div>
            <div class="col-lg-6">
                <?php
                $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$history_rows['Course_ID']."'");
                $course = mysqli_fetch_assoc($course_query);
                if($course_query->num_rows>0){
                    $course_name = $course['Name'];
                }else{
                    $course_name = ' ';
                }
                ?>
                <p><b>Course:</b> <font style="font-weight: 600;"><?php echo $course_name; ?></font></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $spec_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$history_rows['Specialization_ID']."'");
                $specialization = mysqli_fetch_assoc($spec_query);
                if($spec_query->num_rows>0){
                    $specialization_name = $specialization['Name'];
                }else{
                    $specialization_name = ' ';
                }
                ?>
                <p><b>Specialization:</b> <font style="font-weight: 600;"><?php echo $specialization_name; ?></font></p>
            </div>
            <div class="col-lg-12">
                <p><b>Remarks:</b> <font style="font-weight: 600;"><?php echo $history_rows['Remarks']; ?></font></p>
            </div>
        </div>
    </div>
</div>
<br><hr>
<?php
}
?>


<script type="text/javascript">
    function Refer(id)
    {

        $.ajax({
            url: "ajax_leads/refer_history.php",
            type: "POST",
            data: {"user_id":id},
            success:function(response)
            {
                console.log(response);
                $('#historyLead').empty();
                $('#historyLead').html(response);
            }
        })
    }
    
</script>
