<?php
//include database configuration file
if(session_status() === PHP_SESSION_NONE) session_start();
include '../filestobeincluded/db_config.php';


    $query = $conn->query("SELECT * FROM Sub_Sources ORDER BY ID ASC");




if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "SubSources_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Sub Source', 'Source');

    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $get_source = $conn->query("SELECT * FROM Sources WHERE ID='".$row['Source_ID']."'");
        $source = mysqli_fetch_assoc($get_source);
      
        $lineData = array($row['Name'], $source['Name']);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>