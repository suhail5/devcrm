<?php
//include database configuration file
if(session_status() === PHP_SESSION_NONE) session_start();
include '../filestobeincluded/db_config.php';


    $query = $conn->query("SELECT * FROM Specializations ORDER BY ID ASC");




if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "speclization_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Name','Institute','Course');

    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        
      $get_university = $conn->query("SELECT * FROM Institutes WHERE ID = '".$row['Institute_ID']."'");
      $university = mysqli_fetch_assoc($get_university);
      $get_course = $conn->query("SELECT * FROM Courses WHERE ID = '".$row['Course_ID']."'");
      $course = mysqli_fetch_assoc($get_course);
        $lineData = array($row['Name'],$university['Name'],$course['Name']);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>