<?php
    require '../filestobeincluded/db_config.php';
    $all_institutes = array();
    $institute_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> 0");
    while ($row = $institute_query_res->fetch_assoc()) {
        $all_institutes[] = $row;
    }
?>

<form method="POST" action="">
    <div class="form-group row">
        <label class="col-lg-2 col-form-label"
            for="temp_name_email">Template Name</label>
        <div class="col-lg-4">
            <input type="text" class="form-control" id="temp_name_email"
                placeholder="Template Name" required>
        </div>
        <div class="col-lg-1"></div>
        <label class="col-lg-1 col-form-label"
            for="temp_subject_email">Subject</label>
        <div class="col-lg-4">
            <input type="text" class="form-control" id="temp_subject_email"
                placeholder="Subject" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label">Institute</label>
        <div class="col-lg-10">
            <select class="form-control custom-select" id="email_institute_id">
                <option disabled selected>Select Institute</option>
                <?
                foreach ($all_institutes as $institute) {
                    ?>
                    <option value="<?php echo $institute['ID'] ?>"><?php echo $institute['Name']; ?></option>
                    <?
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label">Choose Variable</label>
        <div class="col-lg-10">
            <select class="form-control custom-select" id="choose_var_email">
                <option value="">Choose</option>
                <option value="$lead_name">Lead Name</option>
                <option value="$counsellor_name">Counsellor Name</option>
                <option value="$counsellor_contact_no">Counsellor Contact No</option>
                <option value="$counsellor_email">Counsellor Email</option>
            </select>
        </div>
    </div>
    <br>
    <div id="emailEditor" style="border: 1px solid #e6e6e6; border-radius:5px;">
        <div class="controls">
            <textarea class="summernote input-block-level" id="email_content" required>

            </textarea>
        </div>
    </div>
    <br><br>

    <div class="modal-footer">
    <button class="btn btn-info" type="button" onclick="testemail();">Test</button>
    <button class="btn btn-primary" type="button" id="saveemailtemplate">Publish</button>
    </div>
    
    
</form>
<script>
    $(document).ready(function() {
        $("#choose_var_email").change(function(){
            var choose_variab = $('#choose_var_email option:selected').val();
            $('#email_content').summernote('editor.saveRange');
            $('#email_content').summernote('editor.focus');
            $('#email_content').summernote('editor.insertText', choose_variab);
        })
    });
</script>

<script type="text/javascript">
	function uploadImage(image) {
	    var data = new FormData();
	    data.append("image", image);
	    $.ajax({
		        url: 'ajax_leads/upload_email_image.php',
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data,
		        type: "post",
		        success: function(url) {
                    console.log(url);
		            var image = $('<img>').attr('src', url);
		            $('#email_content').summernote("insertNode", image[0]);
		        },
		        error: function(data) {
		            console.log(data);
		        }
		 });
}
</script>


<script>
  $(document).ready(function () {
    $('#saveemailtemplate').click(function (e) {
      e.preventDefault();
      var temp_name_email = $('#temp_name_email').val();
      var temp_subject_email = $('#temp_subject_email').val();
      var email_institute_id = $('#email_institute_id').val();
      var email_content = $('#email_content').val();
      console.log(email_content);

      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_email/add_email.php",
          data: { "temp_name_email": temp_name_email, "temp_subject_email": temp_subject_email, "email_content":  email_content, "email_institute_id": email_institute_id},
          success: function (data) {
            $('#addemailtempmodal').modal('hide');
           
            if(data.match("true")) {
                $("#emailtable").load(location.href + " #emailtable");
                $('#addemailtempmodal form')[0].reset();
                $('#email_content').summernote('reset');
                
                toastr.success('Email Template added successfully');
                
            }
            else {
                toastr.error('Unable to add email template');
            }
          }
        });
    });
  });
</script>



<div class="modal fade" id="testemailtemplate" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myCenterModalLabel">Test Email</h5>
                <button type="button" class="close" onclick="closeTest()" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label"
                            for="receiver">Receiver Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="receiver"
                                placeholder="Name" required>
                        </div>
                        <label class="col-lg-2 col-form-label"
                            for="receiver_email">Receiver Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="receiver_email"
                                placeholder="Email ID" required>
                        </div>
                    </div>
                    <div id="emailEditor" style="border: 1px solid #e6e6e6; border-radius:5px;">
                        <div class="controls">
                            <textarea class="summernote input-block-level test-temp" id="email_content_test" required>

                            </textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" onclick="sendMailTest();">Send</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    function testemail(){
            var email_content = $.trim($("#email_content").val());
            if(email_content == ""){
                toastr.error('Email body empty!');
            }else{
                $(".test-temp").summernote("code", email_content);
                $('#testemailtemplate').modal('show'); 
            }
        };
</script>

<script>
    function closeTest(){
        $('#testemailtemplate').modal('hide')
    }
</script>

<script>
function sendMailTest() {
    
    var receiver_mail_id = $('#receiver_email').val();
    var receiver_name = $('#receiver').val();
    var test_mail_temp = $('#email_content_test').val();

    $.ajax
        ({
            type: "POST",
            url: "Mailer/send_test_mail.php",
            data: { "receiver_mail_id" :receiver_mail_id, "receiver_name" :receiver_name, "test_mail_temp":test_mail_temp },
            success: function (data) {
            console.log(data);
            if(data.match("true")) {
                $('#testemailtemplate').modal('hide');
                toastr.success('Test Mail send successfully');

            }
            else {
                toastr.error('Unable to send mail');
            }
            }
        });
    
}
</script>

<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
<script src="assets/libs/summernote/summernote-image-attributes.js"></script>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            popover: {
                    image: [
                        ['custom', ['imageAttributes']],
                        ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                        ['remove', ['removeMedia']]
                    ],
                },
            lang: 'en-US',
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',
                removeEmpty:false,
                disableUpload: false
            },
            height: 330,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            callbacks: {
		        onImageUpload: function(image) {
		            uploadImage(image[0]);
		        }
		    }
        });
    });
</script>