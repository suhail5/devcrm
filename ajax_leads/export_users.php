<?php
//include database configuration file
if(session_status() === PHP_SESSION_NONE) session_start();
include '../filestobeincluded/db_config.php';


    $query = $conn->query("SELECT users.*, CAST(AES_DECRYPT(users.Password, '60ZpqkOnqn0UQQ2MYTlJ') AS CHAR(50)) pass,Institutes.Name as Institute_Name FROM users LEFT JOIN Institutes ON users.Institute_ID = Institutes.ID ORDER BY ID ASC");




if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "Users_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Emp ID','Name','Designation',	'Email', 'Mobile', 'Role', 'Password', 'Extension', 'Reporting to','University','Status');

    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $rep_sql = $conn->query("SELECT * FROM users WHERE ID = '".$row['Reporting_To_User_ID']."'");
        $get_rep = mysqli_fetch_assoc($rep_sql);
      
        $lineData = array($row['ID'],$row['Name'],$row['Designation'], $row['Email'], $row['Mobile'], $row['Role'], $row['pass'], $row['Extension'], $get_rep['Name'],$row['Institute_Name'],$row['Status']);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>