<?php 
require '../filestobeincluded/db_config.php';
$selected_leads = $_POST['data_id'];
$selected_leads = str_replace("%5B", "", $selected_leads);
$selected_leads = str_replace("%5D", "", $selected_leads);
$selected_leads = str_replace("id=", "", $selected_leads);
$selected_leads = str_replace("&", ",", $selected_leads);

$all_users = array();
$users_query_res = $conn->query("SELECT * FROM users  WHERE ID <> '0' AND Reporting_To_User_ID = '1' ");
while($row = $users_query_res->fetch_assoc()) {
    $all_users[] = $row;
}

$all_comm = array();
$comm = $conn->query("SELECT * FROM Communication_Mail WHERE ID <> '0'");
while($row = $comm->fetch_assoc()) {
    $all_comm[] = $row;
}

?>
<form method="POST">
<div class="form-group row">
    <div class="col-lg-4">
        <select data-plugin="customselect" class="form-control" id="email_templates">
            <option disabled selected>Choose Template</option>
            <?php
                $result_email_template = $conn->query("SELECT * FROM Email_Templates");
                while($email_temp = $result_email_template->fetch_assoc()) {
            ?>
                <option value="<?php echo $email_temp['id']; ?>"><?php echo $email_temp['template_name']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-4">
    <select data-plugin="customselect" class="form-control" name="mail_owner" id="mail_owner">
        <option selected disabled>Choose Counsellor</option>
        <?php
        foreach ($all_users as $user) {
            ?>
            <option value="<?php echo $user['ID']; ?>"><?php echo $user['Name']; ?></option>
            <?
        }
        ?>
    </select>
    </div>
    <div class="col-lg-4">
        <select name="email_owner" id="email_owner" class="form-control">
            <option selected disabled>Choose Email</option>
            <?php
                foreach ($all_comm as $comm) {
                    ?>
                    <option value="<?php echo $comm['ID']; ?>"><?php echo $comm['Name']; ?></option>
                    <?
                }
            ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12">
        <div id="mailshow" style="border: 1px solid #e6e6e6; border-radius:5px;">
            <div class="controls">
                <textarea class="summernote input-block-level" id="email_textareas" required>
                        
                </textarea>
            </div>
        </div>
    </div>
</div>
    

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" onclick="sendMailSelected();" class="btn btn-primary">&nbsp;&nbsp;Send&nbsp;&nbsp;</button>
</div>
</form>

<script>


function sendMailSelected() {
    
    var mail_id = $('#email_templates').val();
    var leadid = '<?php echo $selected_leads ?>';
    var counsellor = $('#mail_owner').val();
    var email_name = $('#email_owner').val();
    var url = window.location.pathname;  
    
    $.ajax
        ({
            type: "POST",
            url: "Mailer/ajax_send_mail_selected.php",
            data: { mail_id :mail_id, leadid :leadid,email_name:email_name,counsellor:counsellor },
            success: function (data) {
            if(data.match("true")) {
                $('.modal').modal('hide');
                $("#divShowHide1").css({display: "none"});
                $("#divShowHide2").css({display: "none"});
                $("#divShowHide3").css({display: "none"});
                $("#divShowHide4").css({display: "none"});
                toastr.success('Mails send successfully');
                $('#checkbox-form').find('input[name="id[]"]:checked').each(function () {
                    var id = $(this).val();
                    if(url=='/myfollowup'){
                        $.ajax({
                            url:'/ajax_leads/fetch_followup_lead_data.php',
                            data:{id:id},
                            type:'POST',
                            success:function(data){
                                $('#row'+id).html(data);
                            }
                        })
                    }else{
                        $.ajax({
                            url:'/ajax_leads/fetch_lead_data.php',
                            data:{id:id},
                            type:'POST',
                            success:function(data){
                                $('#row'+id).html(data);
                            }
                        })        
                    } 
                });
                $(".checkbox-function").prop('checked', false);
            }
            else {
                toastr.error('Unable to send Mails');
            }
            }
        });
        
        
        

}
</script>


<script>
    $(document).ready(function() {
        $('#email_templates').change(function() {

            $('#email_textareas').summernote('pasteHTML', "<b>Select Template</b>");
            var template_id = $('#email_templates').val();

            $.ajax
            ({
                type: "POST",
                url: "ajax_leads/ajax_email.php",
                data: { "email_template_id": template_id },
                success: function(data) {
                    if(data != "") {
                        $('#email_textareas').summernote('code', data);
                    }
                    else {
                        $('#email_textareas').summernote('pasteHTML', "<b>Select Template</b>");
                    }
                }
            });
        });
    });
</script>
<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 330,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
    });
</script>
<?php
exit;
?>