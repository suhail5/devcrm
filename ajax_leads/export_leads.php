<?php
//include database configuration file
include '../filestobeincluded/db_config.php';

//get records from database
$stage = $_POST['stage_id'];
$header = trim($_POST['header'], '[]');
$a = str_replace('"', '', $header);
$selected = explode(',',$a);
foreach($selected as $sl){
    $selected_header[] = $sl;
}
if($stage == '0'){
    $query = $conn->query("SELECT * FROM Leads ORDER BY ID DESC");
}else if($stage == '8'){
    $query = $conn->query("SELECT Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID, TimeStamp FROM Re_Enquired ORDER BY ID DESC");
}else{
    $query = $conn->query("SELECT ID, Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID, TimeStamp FROM Leads WHERE Stage_ID = $stage ORDER BY ID DESC");
}

if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "Leads_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array("Name", "Email", "Mobile", "Alt Contact", "Stage", "Reason", "Remarks", "University", "Course", "Specialization", "State", "City", "Source", "Subsource", "Counsellor ID", "Creation Date");
    $final_header = array_intersect($fields, $selected_header);
    fputcsv($f, $final_header, $delimiter);
    $lineData = array();
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = [];

        if (strpos($header, 'Name') !== false) {
            array_push($lineData, $row['Name']);
        }
        if (strpos($header, 'Email') !== false) {
            array_push($lineData, $row['Email']);
        }
        if (strpos($header, 'Mobile') !== false) {
            array_push($lineData, $row['Mobile']);
        }
        if (strpos($header, 'Alt Contact') !== false) {
            array_push($lineData, $row['Alt_Mobile']);
        }


        if (strpos($header, 'Stage') !== false) {
            $stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$row['Stage_ID']."'");
            if($stage_query->num_rows > 0) {
                $stage_fet = mysqli_fetch_assoc($stage_query);
                $stage_Id = $stage_fet['Name'];
            }else{
                $stage_Id = ' ';
            }
            array_push($lineData, $stage_Id);
        }
        if (strpos($header, 'Reason') !== false) {
            $reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$row['Reason_ID']."'");
            if($reason_query->num_rows > 0) {
                $reason_fet = mysqli_fetch_assoc($reason_query);
                $reason_Id = $reason_fet['Name'];
            }else{
                $reason_Id = ' ';
            }
            array_push($lineData, $reason_Id);
        }

        if (strpos($header, 'Remarks') !== false) {
            array_push($lineData, $row['Remarks']);
        }

        if (strpos($header, 'University') !== false) {
            $institute_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$row['Institute_ID']."'");
            if($institute_query->num_rows > 0) {
                $institute_fet = mysqli_fetch_assoc($institute_query);
                $institute_Id = $institute_fet['Name'];
                if($institute_Id == 'Admin'){
                    $institute_Id = '';
                }
                if(strcasecmp($institute_Id, 'Admin')==0){
                    $institute_Id= '';
                    echo $institute_Id;
                }
            }else{
                $institute_Id = ' ';
            }
            array_push($lineData, $institute_Id);
        }
        if (strpos($header, 'Course') !== false) {
            $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$row['Course_ID']."' AND Institute_ID = '".$row['Institute_ID']."'");
            if($course_query->num_rows > 0) {
                $course_fet = mysqli_fetch_assoc($course_query);
                $course_Id = $course_fet['Name'];
            }else{
                $course_Id = ' ';
            }
            array_push($lineData, $course_Id);
        }
        if (strpos($header, 'Specialization') !== false) {
            $specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$row['Specialization_ID']."' AND Institute_ID = '".$row['Institute_ID']."' AND Course_ID = '".$row['Course_ID']."'");
            if($specialization_query->num_rows > 0) {
                $specialization_fet = mysqli_fetch_assoc($specialization_query);
                $specialization_Id = $specialization_fet['Name'];
            }else{
                $specialization_Id = ' ';
            }
            array_push($lineData, $specialization_Id);
        }
        if (strpos($header, 'State') !== false) {
            $state_query = $conn->query("SELECT * FROM States WHERE ID = '".$row['State_ID']."'");
            if($state_query->num_rows > 0) {
                $state_fet = mysqli_fetch_assoc($state_query);
                $state_Id = $state_fet['Name'];
            }else{
                $state_Id = ' ';
            }
            array_push($lineData, $state_Id);
        }
        if (strpos($header, 'City') !== false) {
            $city_query = $conn->query("SELECT * FROM Cities WHERE ID = '".$row['City_ID']."'");
            if($city_query->num_rows > 0) {
                $city_fet = mysqli_fetch_assoc($city_query);
                $city_Id = $city_fet['Name'];
            }else{
                $city_Id = ' ';
            }
            array_push($lineData, $city_Id);
        }
        if (strpos($header, 'Source') !== false) {
            $source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$row['Source_ID']."'");
            if($source_query->num_rows > 0) {
                $source_fet = mysqli_fetch_assoc($source_query);
                $source_Id = $source_fet['Name'];
            }else{
                $source_Id = ' ';
            }
            array_push($lineData, $source_Id);
        }
        if (strpos($header, 'Subsource') !== false) {
            $subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$row['Subsource_ID']."'");
            if($subsource_query->num_rows > 0) {
                $subsource_fet = mysqli_fetch_assoc($subsource_query);
                $subsource_Id = $subsource_fet['Name'];
            }else{
                $subsource_Id = ' ';
            }
            array_push($lineData, $subsource_Id);
        }
        if (strpos($header, 'Counsellor ID') !== false) {
            $employee_id_query = $conn->query("SELECT * FROM users WHERE ID = '".$row['Counsellor_ID']."'");
            if($employee_id_query->num_rows > 0) {
                $employee_fet = mysqli_fetch_assoc($employee_id_query);
                $employee_Id = $employee_fet['ID'];
            }else{
                $employee_Id = ' ';
            }
            array_push($lineData, $employee_Id);
        }
        if (strpos($header, 'Creation Date') !== false) {
            $get_creation_date = $conn->query("SELECT TimeStamp FROM History WHERE Lead_ID = '".$row['ID']."' ORDER BY ID ASC LIMIT 1");
            if($get_creation_date->num_rows > 0){
                $date = mysqli_fetch_assoc($get_creation_date);
                $created_date= $date["TimeStamp"];
            }else{
                $created_date= $row["TimeStamp"];
            }
            array_push($lineData, $created_date);
        }

        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>