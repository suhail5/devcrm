<?php include 'filestobeincluded/header-top.php' ?>
<?php include 'filestobeincluded/header-bottom.php' ?>
<!-- Pre-loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div>
<!-- End Preloader-->
<?php include 'filestobeincluded/navigation.php' ?>



<?php

$all_follow_ups = array();

$followups_query_res = $conn->query("SELECT * FROM Follow_Ups WHERE Counsellor_ID in ($tree_ids) GROUP BY Lead_ID ORDER BY ID DESC");
while($row = $followups_query_res->fetch_assoc()) {
	$all_follow_ups[] = $row;
}
?>

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                                <!--<ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Shreyu</a></li>
                                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Starter</li>
                                    </ol>-->
                            </nav>
                            <h4 class="mb-1 mt-0">My Follow-ups</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-lg-9"></div>
                                    <label class="col-lg-1 col-form-label"
                                        for="followup-date">Select Date</label>
                                    <div class="col-lg-2">
                                        <input class="form-control" id="followup_date" type="date"
                                            name="date">
									</div>
                                </div>
								<br><br>
								<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                                <form id="checkbox-form" method="POST">
                                <table id="followup_table" class="table table-striped nowrap">
                                    <tbody>
                                    	<?php

										
                                    	foreach ($all_follow_ups as $follow_up) {
                                    		$lead_query = $conn->query("SELECT * FROM Leads WHERE ID = '".$follow_up['Lead_ID']."'");
											$lead = mysqli_fetch_assoc($lead_query);
                                    		?>
                                    		<tr>
											<td>
												<div class="row" style="padding-top: 10px;">
													<div class="col-lg-1">
														<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input checkbox-function" name="id[]" id="customCheck2<?php echo $lead['ID']; ?>" value="<?php echo $lead['ID']; ?>">
														<label class="custom-control-label" for="customCheck2<?php echo $lead['ID']; ?>"></label>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Name:</b> <?php echo $lead['Name']; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Email:</b> <?php echo substr($lead['Email'],0,15).'...'; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Mobile:</b> <a href="tel:<?php echo $lead['Mobile']; ?>"><?php echo $lead['Mobile']; ?></a></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	if(strlen($follow_up['Remark'])>50){
																		$comment = substr($follow_up['Remark'],0,50).'...</p>'.'<button type="button" class="btn btn-light btn-sm" data-container="body" title=""
																		data-toggle="popover" data-placement="left"
																		data-content="'.$follow_up['Remark'].'"
																		data-original-title="Remark">
																		See More
																	</button>';
																	}else{
																		$comment = $follow_up['Remark'].'</p>';
																	}

																?>
																<p><b>Followup Comment:</b>&nbsp;&nbsp;<?php echo $comment ?>
															</div>
														</div>
													</div>
													<div class="col-lg-2">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<i data-feather="user" class="icon-dual"></i>&nbsp;&nbsp;<i data-feather="user" class="icon-dual"></i>&nbsp;&nbsp;<i data-feather="user" class="icon-dual"></i>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">
																<?php
																	$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$lead['Counsellor_ID']."'");
																	$counsellor = mysqli_fetch_assoc($counsellor_query);
																	if($counsellor_query->num_rows > 0){
																		$couns = explode(' ',$counsellor['Name']);
																	}else{
																		$counsellor['Name'] = ' ';
																		$couns = $counsellor['Name'];
																	}
																?>
																<p><b>Counsellor:</b> <?php echo $couns[0]; ?></p>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>University:</b> <?php
																		$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$lead['Institute_ID']."'");
																		$univ = mysqli_fetch_assoc($univ_query);
																		if($univ_query->num_rows > 0){
																			echo $univ['Name'];
																		}else{
																			$course['Name'] = ' ';
																			echo $univ['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Course:</b> <?php
																		$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
																		$course = mysqli_fetch_assoc($course_query);
																		if($course_query->num_rows > 0){
																			echo $course['Name'];
																		}else{
																			$course['Name'] = ' ';
																			echo $course['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Specialization:</b> <?php
																		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
																		$specialization = mysqli_fetch_assoc($specialization_query);
																		if($specialization_query->num_rows > 0){
																			echo substr($specialization['Name'],0,27);
																		}else{
																			$specialization['Name'] = ' ';
																			echo $specialization['Name'];
																		}
																	?>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<p><b>Follow-up Date:</b> <?php echo explode(" ", $follow_up['Followup_Timestamp'])[0]; ?></p>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
																	$stage = mysqli_fetch_assoc($stage_query);
																	if($stage_query->num_rows > 0){
																		$lead_stage = $stage['Name'];
																	}else{
																		$stage['Name'] = ' ';
																		$lead_stage = $stage['Name'];
																	}
																?>
																<p><b>Stage:</b> <?php echo $lead_stage; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
																	$reason = mysqli_fetch_assoc($reason_query);
																	if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
																		$badge = "success";
																	}
																	else if(strcasecmp($stage['Name'], "COLD")==0) {
																		$badge = "warning";
																	}
																	else {
																		$badge = "danger";
																	}
																?>
																<p><b>Reason:</b> <span class="badge badge-soft-<?php echo($badge); ?> py-1">
																		<?if($reason_query->num_rows > 0){
																			echo $reason['Name'];
																		}else{
																			$reason['Name'] = ' ';
																			echo $reason['Name'];
																		}?>
																	</span>
																</p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$lead['Source_ID']."'");
																	$source = mysqli_fetch_assoc($source_query);
																	if($source_query->num_rows > 0){
																		$lead_source = $source['Name'];
																	}else{
																		$subsource['Name'] = ' ';
																		$lead_source = $source['Name'];
																	}
																?>
																<p><b>Source:</b> <?php echo $lead_source; ?></p>
															</div>
															<div class="col-lg-12 col-md-3 col-sm-12">
																<?php
																	$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$lead['Subsource_ID']."'");
																	$subsource = mysqli_fetch_assoc($subsource_query);
																	if($subsource_query->num_rows > 0){
																		$lead_sub = $subsource['Name'];
																	}else{
																		$subsource['Name'] = ' ';
																		$lead_sub = $subsource['Name'];
																	}
																?>
																<p><b>Sub-Source:</b> <?php echo $lead_sub; ?></p>
															</div>
														</div>
													</div>
													<div class="col-lg-1">
														<div class="row">
															<div class="col-lg-12 col-md-3 col-sm-12">
																<div class="btn-group">
																	<span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" style="cursor: pointer;" data-id="<?php echo $lead['ID']; ?>" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;
																	
																	<a href="tel:<?php echo $lead['Mobile']; ?>"><span data-toggle="tooltip" data-placement="top" data-original-title="Call" title=""><p style="font-size: 20px;"><i class="fa fa-phone" data-toggle="modal" style="cursor: pointer;" data-target="#leadcallmodal" aria-hidden="true"></i></p></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
																	<span data-toggle="dropdown"><p style="font-size: 20px;"><i class="fa fa-ellipsis-v" style="cursor: pointer;" aria-hidden="true"></i></p></span>
																	<div class="dropdown-menu dropdown-menu-right">
																		<span class="dropdown-item"><i class="fa fa-user-plus"></i> <font class="addfollowupmodal" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">Schedule Next Followup</font></span>																		
																		<span class="dropdown-item"><i class="fa fa-history"></i> <font class="leadhistory" data-id="<?php echo $lead['ID']; ?>" style="cursor: pointer;">View History</font></span>
																	</div>
																</div>
															</div>
														</div>
														
														
													</div>
												</div>
											</td>

                                    		</tr>
                                    		<?php
                                    	}
                                    	?>
                                    </tbody>
								</table>
								</form>

                            </div>
                        </div>
                    </div>
                </div>

			</div> <!-- content -->

<script type='text/javascript'>
$(document).ready(function () {
    
	$(".checkbox-function").click(function () {
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide1").css({display: "block"});
			$("#divShowHide2").css({display: "block"});
			$("#divShowHide3").css({display: "block"});
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide1").css({display: "none"});
			$("#divShowHide2").css({display: "none"});
			$("#divShowHide3").css({display: "none"});
			$("#divShowHide4").css({display: "none"});
		}
    });

});           	
</script>
			
<script type='text/javascript'>
	$(document).ready(function(){

		$('.addfollowupmodal').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/add_followup.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-addfollowup').html(response); 

					// Display Modal
					$('#addfollowup').modal('show'); 
				}
			});
		});
	});
</script>												
<!-------Add followup modal-------->
<div class="modal fade" id="addfollowup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Schedule Next Followup</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-addfollowup">
				</div>
			</div>
		</div>
	</div>
<!-------Add Followup modal-------->

<script type='text/javascript'>
	$(document).ready(function(){

		$('.leadhistory').click(function(){
			
			var userid = $(this).data('id');

			// AJAX request
			$.ajax({
				url: 'ajax_leads/followup_history.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-history').html(response); 

					// Display Modal
					$('#viewhistory').modal('show'); 
				}
			});
		});
	});
</script>
<!-------History modal-------->
<div class="modal fade" id="viewhistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">History</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-history">
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-------End History modal-------->


<script>
	$(document).ready(function(){
    var buttonCommon = {
        init: function (dt, node, config) {
          var table = dt.table().context[0].nTable;
          if (table) config.title = $(table).data('export-title')
        },
        title: 'default title'
      };
    
    $("#basic-datatable").DataTable({language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}});
    
    var a=$("#followup_table").DataTable({lengthChange:!1,dom: 'Brfrtip',buttons:[$.extend( true, {}, buttonCommon, {
    extend: 'csv',
    exportOptions: {
      columns: 'th:not(:last-child)'
    }
} ),
$.extend( true, {}, buttonCommon, {
    extend: 'copy',
    orientation: 'landscape',
    exportOptions: {
      columns: 'th:not(:last-child)'
    }
} ),
$.extend( true, {}, buttonCommon, {
    extend: 'pdf',
    exportOptions: {
      columns: 'th:not(:last-child)'
    },
    orientation: 'landscape'
} )],initComplete: function() {
    var $buttons = $('.dt-buttons').hide();
    $('#exportLink').on('change', function() {
      var btnClass = $(this).find(":selected")[0].id 
        ? '.buttons-' + $(this).find(":selected")[0].id 
        : null;
      if (btnClass) $buttons.find(btnClass).click(); 
    })
  },"aaSorting": [],
    "bSortable":false,
	"orderable": false,
	"ordering": false,
  'columnDefs': [{
    'targets': 0,
    'width': '1%',
 }],"pageLength": 50,language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}});$("#selection-datatable").DataTable({select:{style:"multi"},language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}}),$("#key-datatable").DataTable({keys:!0,language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}}),a.buttons().container().appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)")});


</script>

<script>
	$(document).ready(function() {
		var table = $('#followup_table').DataTable();
 
		$('#followup_date').on('change', function () {
		    table.search(this.value).draw();
		} );
	});
</script>

<!-------Refer Selected modal-------->
<div class="modal fade" id="selected_refer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Refer Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-refer-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Refer Selected modal-------->
<!-------SMS Selected modal-------->
<div class="modal fade" id="selected_sms" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Send SMS to Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-sms-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End SMS Selected modal-------->
<!-------SMS Selected modal-------->
<div class="modal fade" id="selected_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Send Mail to Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-mail-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End SMS Selected modal-------->
<!-------Delete Selected modal-------->
<div class="modal fade" id="selected_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-xs">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Delete Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-delete-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Delete Selected modal-------->


<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>