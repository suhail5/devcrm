<?php require "db_config.php" ?>

<?php
$add_lead ='';
$add_renquired = '';

if(session_status() === PHP_SESSION_NONE) session_start();
require_once '../../DialerAPI/vendor/autoload.php';
//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require '../Mailer/vendor/autoload.php';
function send_re_enquired_mail($conn, $lead_ID, $sourceID, $courseID){

    $get_all_leads = $conn->query("SELECT Timestamp from Leads WHERE ID = '$lead_ID'");
    while($row = $get_all_leads->fetch_assoc()) {
        $all_array[] = $row;
    }
    foreach($all_array as $lead){
        $get_creation_date = $conn->query("SELECT TimeStamp FROM History WHERE Lead_ID = '$lead_ID' ORDER BY ID ASC LIMIT 1");
        if($get_creation_date->num_rows > 0){
            $date = mysqli_fetch_assoc($get_creation_date);
            $creation_date = date("F j, Y g:i a", strtotime($date["TimeStamp"]));
        }else{
            $creation_date = date("F j, Y g:i a", strtotime($lead["Timestamp"])).'<br>' ;
        }
    }

    $aald_id_query_res = $conn->query("SELECT * FROM Leads WHERE ID = '$lead_ID'");
    $aald_id_res = mysqli_fetch_assoc($aald_id_query_res);
    $fullName = $aald_id_res['Name'];
    $emailID = $aald_id_res['Email'];
    $mobileNumber = $aald_id_res['Mobile'];
    $alt_mobileNumber = $aald_id_res['Alt_Mobile'];
    $aald_c_id = $aald_id_res['Counsellor_ID'];

    $get_cc_dets_query = $conn->query("SELECT * FROM users WHERE ID = '".$aald_c_id."'");
    $cc_dets_res = mysqli_fetch_assoc($get_cc_dets_query);

    $counsellor_email = $cc_dets_res['Email'];
    $counsellor_name = $cc_dets_res['Name'];

    $counsellor_manager = $cc_dets_res['Reporting_To_User_ID'];
    $get_cm_dets_query = $conn->query("SELECT * FROM users WHERE ID = '".$counsellor_manager."'");
    $cm_dets_res = mysqli_fetch_assoc($get_cm_dets_query);

    $manager_email = $cm_dets_res['Email'];
    $manager_name = $cm_dets_res['Name'];

    //Create a new PHPMailer instance
    $mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
    $mail->isSMTP();

    //Enable SMTP debugging
    // SMTP::DEBUG_OFF = off (for production use)
    // SMTP::DEBUG_CLIENT = client messages
    // SMTP::DEBUG_SERVER = client and server messages
    $mail->SMTPDebug = 1;

    //Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587;

    //Set the encryption mechanism to use - STARTTLS or SMTPS
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '$courseID'");
    $course = mysqli_fetch_assoc($course_query);
    $cou_rse = $course['Name'];
    
    $get_source = $conn->query("SELECT Name FROM Sources WHERE ID = '$sourceID'");
    $source = mysqli_fetch_assoc($get_source);
    $sou_rce = $source['Name'];

    $get_last_remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '$lead_ID' ORDER BY ID DESC LIMIT 1");
    $remark = mysqli_fetch_assoc($get_last_remark);
    $re_mark = $remark['Remark'];
                                
//Student Name, Email Id, Phone nUmber, Course, Lead Source, alternative number, lead creation date, last remark
    $body = 'Counsellor Name:&nbsp;&nbsp;'.$counsellor_name.'<br>Student Name:&nbsp;&nbsp;'.$fullName.'<br>Email ID:&nbsp;&nbsp;'.$emailID.'<br>Mobile Number:&nbsp;&nbsp;'.$mobileNumber.'<br>Alternate Number:&nbsp;&nbsp;'.$alt_mobileNumber.'<br>Course:&nbsp;&nbsp;'.$cou_rse.'<br>Lead Source:&nbsp;&nbsp;'.$sou_rce.'<br>Creation Date:&nbsp;&nbsp;'.$creation_date.'<br>Last Remark:&nbsp;&nbsp;'.$re_mark;
    
    /**
     * This example shows settings to use when sending via Google's Gmail servers.
     * This uses traditional id & password authentication - look at the gmail_xoauth.phps
     * example to see how to use XOAUTH2.
     * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
     */
    $admin_query = $conn->query("SELECT * FROM users WHERE Role = 'Administrator'");
    $admin_res = mysqli_fetch_assoc($admin_query);
    
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = $admin_res['Email'];

    //Password to use for SMTP authentication
    $mail->Password = $admin_res['Email_Password'];
    

    //Set who the message is to be sent from
    $mail->setFrom($admin_res['Email'], $admin_res['Name']);

    //Set an alternative reply-to address
    //$mail->addReplyTo($counsellor_email, $counsellor_name);

    //Set who the message is to be sent to
    $mail->addAddress($counsellor_email, $counsellor_name);
    $mail->addCC($manager_email, $manager_name);

    //Set the subject line
    $mail->Subject = 'Re-Enquired for '. $cou_rse;

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //$mail->msgHTML(file_get_contents('contents.html'), __DIR__);

    //Replace the plain text body with one created manually
    $mail->Body = $body;
    $mail->IsHTML(true);
    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.png');
    //send the message, check for errors
        



    if (!$mail->send()) {
        //echo 'Mailer Error: '. $mail->ErrorInfo;
    } else {
        echo 'send success';
        //Section 2: IMAP
        //Uncomment these to save your message in the 'Sent Mail' folder.
        #if (save_mail($mail)) {
        #    echo "Message saved!";
        #}
    }
}

$fullName = mysqli_real_escape_string($conn,$_POST['fullName']);
$emailID = $_POST['emailID'];
$mobileNumber = $_POST['mobileNumber'];
$altMobileNumber = $_POST['altMobileNumber'];
$sourceID = $_POST['sourceID'];
$subsourceID = $_POST['subsourceID'];
$stageID = $_POST['stageID'];
$reasonID = $_POST['reasonID'];
$leadOwner = $_POST['leadOwner'];
$instituteID = $_POST['instituteID'];
$courseID = $_POST['courseID'];
$get_course_name = $conn->query("SELECT Name FROM Courses WHERE ID = '$courseID'");
$gcn = mysqli_fetch_assoc($get_course_name);
$course_name = $gcn['Name'];
$specializationID = $_POST['specializationID'];
$remarks = $_POST['remarks'];
$state_Id = $_POST['state_ID'];
$city_Id = $_POST['city_ID'];
$dob = $_POST['dob'];

if($altMobileNumber!=''){
$check = $conn->query("SELECT * FROM `Leads` WHERE ((Alt_Mobile like '$altMobileNumber' OR Mobile like '$altMobileNumber')) AND Institute_ID = '$instituteID'");    
}elseif($mobileNumber!=''){
    $check = $conn->query("SELECT * FROM `Leads` WHERE ((Mobile like '$mobileNumber' OR Alt_Mobile like '$mobileNumber')) AND Institute_ID = '$instituteID'");    
}elseif($emailID!=''){
    $check = $conn->query("SELECT * FROM `Leads` WHERE Email like '$emailID' AND Institute_ID = '$instituteID'");    
}

if ($check->num_rows== 0) {
    $add_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Alt_Mobile, Remarks, Source_ID, Subsource_ID, Counsellor_ID, Institute_ID, Course_ID, Specialization_ID,dob,State_ID,City_ID) VALUES ('$stageID', '$reasonID', '$fullName', '$emailID', '$mobileNumber', '$altMobileNumber', '$remarks', '$sourceID', '$subsourceID', '$leadOwner', '$instituteID', '$courseID' , '$specializationID','$dob','$state_Id','$city_Id')");	
    // $last_insert_id = $conn->insert_id;
    // $client = new \GuzzleHttp\Client();
    // $promise = $client->request('GET', 'https://crm.collegevidya.com/initiateDripMarketing.php?initiateDripMarketing=ok&Stage_ID='.$stageID.'&Institute_ID='.$instituteID.'&Course_Name='.$course_name.'&Reason_ID='.$reasonID.'&State_ID='.$state_Id.'&Lead_Name='.$fullName.'&Lead_Email='.$emailID.'&Lead_Primary_Number='.$mobileNumber.'&Lead_Alt_Number='.$altMobileNumber.'&Counsellor_ID='.$leadOwner.'&LEAD_ID='.$last_insert_id.'');
}else{

    $get_lead_dets = mysqli_fetch_assoc($check);
    $lead_stage_id = $get_lead_dets['Stage_ID'];
    $lead_ID = $get_lead_dets['ID'];
    
    if(strcasecmp($lead_stage_id, "4")==0 || strcasecmp($lead_stage_id, "5")==0 || strcasecmp($lead_stage_id, "6")==0) {
        $insert_new_lead = true;
    }
    else {
        $move_lead = $conn->query("UPDATE Leads SET Stage_ID = '8', Reason_ID = '' WHERE ID = '".$lead_ID."'");
    }

    $add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`, `Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID`) SELECT * FROM Leads WHERE ID = '".$lead_ID."'");
    
    $add_renquired = $conn->query("INSERT INTO Re_Enquired(Lead_ID, Stage_ID, Reason_ID, Name, Email, Mobile, Alt_Mobile, Remarks, Source_ID, Subsource_ID, Counsellor_ID, Institute_ID, Course_ID, Specialization_ID) VALUES ('$lead_ID', '$stageID', '$reasonID', '$fullName', '$emailID', '$mobileNumber', '$altMobileNumber', '$remarks', '$sourceID', '$subsourceID', '$leadOwner', '$instituteID', '$courseID' , '$specializationID')");
    
    send_re_enquired_mail($conn ,$lead_ID, $sourceID, $courseID);
}




if($add_lead == 1) {
    echo "true";
}
elseif($add_renquired == 1)
{
    echo "renquired";
}
else {
	echo mysqli_error($conn);
}

?>