<?php
function session_error_function() {
  echo '<script language="javascript">';
  echo 'location.href="/";';
  echo '</script>';
}

set_error_handler('session_error_function');
if(session_status() === PHP_SESSION_NONE) session_start();
if($_SESSION['role']=='Counsellor'){
  header("location: /counsellor/dashboard");
}else if($_SESSION['role']=='Administrator'){
  header("location: /dashboard");
}

restore_error_handler();

header('Content-Type: text/html; charset=utf-8');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

?>
<?php include 'filestobeincluded/db_config.php' ?>

<?php 
    $all_array = array();
    $coun_array = array();
    $couns_array = array();
    $get_tree = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '". $_SESSION['useremployeeid'] ."'");
    while($row = $get_tree->fetch_assoc()) {
        $all_array[] = $row;
    }
    foreach($all_array as $univ){
   
        $get_counsellors = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '". $_SESSION['useremployeeid'] ."' AND Role = 'Counsellor' AND Status = 'Y' ");
        while($rows = $get_counsellors->fetch_assoc()) {
            $coun_array[] = $rows;
        }
      }
        foreach($coun_array as $cuniv){
            $couns_array[]=$cuniv['ID'];                                                     
        }
	if($get_counsellors->num_rows>0){
		$imp = "'" . implode( "','", ($couns_array) ) . "'";
        $tree_ids = $imp.",'".$_SESSION['USERS_ID']."'";
	}else{
	    $tree_ids = "'".$_SESSION['USERS_ID']."'";
	}
        
		
    
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>CRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="Black Board & Virtual Analytics" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/blackboard.ico">

    <!-- plugin css -->
    <link href="assets/libs/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" /> 

    <link href="assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/libs/smartwizard/smart_wizard.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/libs/smartwizard/smart_wizard_theme_arrows.min.css" type="text/css" />
    <!-- Plugins css -->
    <link href="assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/libs/select2/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/multiselect/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
    


    <!-- Summernote css -->
    <link href="assets/libs/summernote/summernote-bs4.css" rel="stylesheet" />

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/toastr.min.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
function check_session()
{
	const status = fetch("https://crm.collegevidya.com/teamlead/filestobeincluded/check_session.php");

	status.then(response => response.text())
	.then(result => {
		if(result==="0") {
			window.location.href="logout";
		}
	})
	.catch(error => {

	})
}
check_session(); // This will run on page load
setInterval(function(){
    check_session() // this will run after every 5 seconds
}, 2000);
</script>
    <style>.dataTables_filter {display: none;}
        .error {
          color: red;
          margin-bottom: 5px;
        }
        .has-error {
            border: 1px solid red;
        }
        div.dt-buttons {
            float: right;
          padding-bottom:15px;
        }
        .modal-dialog-slideout {min-height: 100%; margin: 0 0 0 auto;background: #fff;}
        .modal.fade .modal-dialog.modal-dialog-slideout {-webkit-transform: translate(100%,0)scale(1);transform: translate(100%,0)scale(1);}
        .modal.fade.show .modal-dialog.modal-dialog-slideout {-webkit-transform: translate(0,0);transform: translate(0,0);display: flex;align-items: stretch;-webkit-box-align: stretch;height: 100%;}
        .modal.fade.show .modal-dialog.modal-dialog-slideout .modal-body{overflow-y: auto;overflow-x: hidden;}
        .modal-dialog-slideout .modal-content{border: 0;}
        .modal-dialog-slideout .modal-header, .modal-dialog-slideout .modal-footer {height: 69px; display: block;} 
        .modal-dialog-slideout .modal-header h5 {float:left;}


        .mark1 {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 50px;   
                border: 2px solid #FFF;
                width: 30px;
                height: 30px;  
                background-color: #5369F8;
                position: relative;
                top: -5px;
                left: -10px;
                font-size: 10px;
                line-height: 10px;
                font-family: 'Roboto', sans-serif;
                font-weight: 400;
                color: #FFF;
                font-weight: 500;
                }
                .mark2 {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 50px;   
                border: 2px solid #FFF;
                width: 30px;
                height: 30px;  
                background-color: #F64744;
                position: relative;
                top: -5px;
                left: -10px;
                font-size: 10px;
                line-height: 10px;
                font-family: 'Roboto', sans-serif;
                font-weight: 400;
                color: #FFF;
                font-weight: 500;
                }
                .mark3 {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 50px;   
                border: 2px solid #FFF;
                width: 30px;
                height: 30px;  
                background-color: #32CD32;
                position: relative;
                top: -5px;
                left: -10px;
                font-size: 10px;
                line-height: 10px;
                font-family: 'Roboto', sans-serif;
                font-weight: 400;
                color: #FFF;
                font-weight: 500;
                }
    </style>
 