<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_sources = array();
$all_subsources = array();

$sources_query_res = $conn->query("SELECT * FROM Sources");
while($row = $sources_query_res->fetch_assoc()) {
    $all_sources[] = $row;
}

$subsources_query_res = $conn->query("SELECT * FROM Sub_Sources");
while ($row = $subsources_query_res->fetch_assoc()) {
    $all_subsources[] = $row;
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        <div class="card-header" id="headingTwo"><h5 class="m-0 font-size-16">Sub-Sources <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addsubsourcemodal"> <i class="uil uil-plus-circle"></i> Add Sub-Sources</button>
            <!----Add Sub-Source Modal-------->
            <div class="modal fade" id="addsubsourcemodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Sub-Source</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="simpleinput">Sub-Source</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="subsource_name" placeholder="Source">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Source</label>
                                    <div class="col-lg-10">
                                        <select id="subsources_source_id" class="form-control custom-select">
                                            <?
                                            foreach ($all_sources as $source) {
                                                ?>
                                                <option value="<?php echo($source['ID']); ?>"><?php echo $source['Name'];  ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="saveSubSource()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_subsources" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Sub-Source</th>
                        <th scope="col">Source</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_subsources as $subsource) {
                            $counter++;
                            if(strcasecmp($subsource['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }

                            $get_source_dets = $conn->query("SELECT * FROM Sources WHERE ID = '".$subsource['Source_ID']."'");
                            $source_dets = mysqli_fetch_assoc($get_source_dets);
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $subsource['Name']; ?></td>
                                <td><?php echo $source_dets['Name']; ?></td>
                                <td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editsubsourcemodal<?php echo($counter); ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editsubsourcemodal<?php echo($counter); ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Sub-Source</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="subsource_id<?php echo($counter); ?>" value="<?php echo $subsource['ID']; ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label"
                                                                for="simpleinput">Sub-Source</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_subsource_name<?php echo($counter); ?>" placeholder="Sub-Source Name" value="<?php echo $subsource['Name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Source</label>
                                                            <div class="col-lg-10">
                                                                <select id="subsources_new_source_id<?php echo($counter); ?>" class="form-control custom-select">
                                                                    <?php
                                                                    foreach ($all_sources as $source) {
                                                                        ?>
                                                                        <option value="<?php echo($source['ID']); ?>"><?php echo $source['Name'];  ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateSubsource(<?php echo($counter); ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletesubsourcemodal<?php echo($counter); ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletesubsourcemodal<?php echo($counter); ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_subsource_id<?php echo($counter); ?>" value="<?php echo $subsource['ID']; ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteSubsource(<?php echo($counter); ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function saveSubSource() {
        var subsource_name = $('#subsource_name').val();
      var source_id = $('#subsources_source_id').val();

      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_subsource/add_subsource.php",
          data: { "subsource_name": subsource_name, "source_id": source_id },
          success: function (data) {
            $('#addsubsourcemodal').modal('hide');
            if(data.match("true")) {
                toastr.success('Sub-source added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingTwo").click();
                });
            }
            else {
                toastr.error('Unable to add sub-source');
            }
          }
        });
        return false;
    }
</script>

<script>
    function updateSubsource(id) {
        var subsource_id = $('#subsource_id'.concat(id)).val();
        var new_source_id = $('#subsources_new_source_id'.concat(id)).val();
        var new_subsource_name = $('#new_subsource_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_subsource/update_subsource.php",
          data: { "subsource_id": subsource_id, "new_source_id": new_source_id, "new_subsource_name": new_subsource_name },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Sub-source updated successfully');
                $("#settingstab").load(location.href + " #settingstab" , function () {
                    $("#headingTwo").click();
                });
            }
            else {
                toastr.error('Unable to update sub-source');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteSubsource(id) {
        var subsource_id = $('#delete_subsource_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_subsource/delete_subsource.php",
          data: { "subsource_id": subsource_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Sub-source deleted successfully');
                $("#settingstab").load(location.href + " #settingstab" , function () {
                    $("#headingTwo").click();
                });
            }
            else {
                toastr.error('Unable to delete sub-source');
            }
          }
        });
        return false;
    }
</script>