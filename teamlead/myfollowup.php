<?php include 'filestobeincluded/header-top.php' ?>
<script src="https://kit.fontawesome.com/3212b33ef4.js" crossorigin="anonymous"></script>
<?php include 'filestobeincluded/header-bottom.php' ?>
<!-- Pre-loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div>
<!-- End Preloader-->
<?php include 'filestobeincluded/navigation.php' ?>
<script>
	$(document).ready(function(){
	  $('[data-toggle="popover"]').popover();
	});
</script>
<?php $users_query = $conn->query("SELECT * FROM users WHERE ID = '".$_SESSION['useremployeeid']."'");
	$users_data = mysqli_fetch_assoc($users_query);
?>

<?php

$all_follow_ups = array();

$followups_query_res = $conn->query("SELECT * FROM Follow_Ups WHERE Counsellor_ID in ($tree_ids) GROUP BY Lead_ID ORDER BY ID DESC");
while($row = $followups_query_res->fetch_assoc()) {
	$all_follow_ups[] = $row;
}
?>

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                                <ol class="breadcrumb">
                              	<button class="btn btn-primary" data-toggle="modal" data-target="#filterModal"><span data-toggle="tooltip" data-placement="top" title="Filter Follow Ups"><i data-feather="filter" class="icon-sm"></i></span></button>&nbsp;&nbsp;
                              </ol> 
                            </nav>
                            <h4 class="mb-1 mt-0">My Follow-ups</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->
                <div class="row" >
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                            	<div id="FollowUpFil">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                    	<div class="row">
											
											<?php
												$get_followup_missed = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp, Follow_Ups.Follow_Up_Status as F_Status FROM Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID WHERE Follow_Ups.ID IN (SELECT MAX(ID) FROM Follow_Ups GROUP BY Lead_ID) AND Follow_Ups.Counsellor_ID in ($tree_ids) AND Followup_Timestamp < Follow_Ups.Last_Updated_Timestamp GROUP BY Lead_ID ORDER BY Follow_Ups.ID DESC");
												$row_count_missed = mysqli_num_rows($get_followup_missed);
											?>
											<?php 
												$planed_ups_query = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp FROM Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID WHERE Follow_Ups.ID IN (SELECT MAX(ID) FROM Follow_Ups GROUP BY Lead_ID) AND Follow_Ups.Counsellor_ID in ($tree_ids) AND Follow_Ups.Followup_Timestamp > NOW()  GROUP BY Lead_ID ORDER BY Follow_Ups.ID DESC");
												$row_count_planedfollow_ups = mysqli_num_rows($planed_ups_query);
											?>
											<button id="missed_follow_ups" onclick="add_active(this.value);getList()" value="0" class="btn btn-light user_tabs">Missed Follow-Ups (<?php echo $row_count_missed ?>)</button>
	                                    	
	                                    	<button id="planned_follow_ups" onclick="add_active(this.value);getList()" value="1" class="btn btn-light user_tabs">Planned Follow-Ups (<?php echo $row_count_planedfollow_ups ?>)</button>
                                    	</div>
                                    </div>
                                    <!-- <label class="col-lg-1 col-form-label"
                                        for="followup-date">Select Date</label>
                                    <div class="col-lg-2">
                                        <input type="text" id="range-datepicker" class="form-control datepicker_range" placeholder="yyyy-mm-dd to yyyy-mm-dd" onchange="getList()">
									</div> -->
                                </div>

                                <!-- <div class="col-lg-2">
                                	<select class="form-control" id="counsellor_id" onchange="getList()">
                                		<option value="">Select Counsellor</option>
                                		<?php $counsellor = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '".$_SESSION['useremployeeid']."' AND Role = 'Counsellor'");
                                		while($data= mysqli_fetch_assoc($counsellor)) {?>
                                		<option value="<?php echo $data['ID'];?>"><?php echo $data['Name'];?></option>
                                	<?php }?>
                                	</select>
                                </div>
 -->
                                <div id="divLoader" class="col-12" style="display: none; height: 100%;">
									<center>
										<div class="spinner-grow text-primary m-2" role="status">
											<span class="sr-only">Loading...</span>
										</div>
									</center>
								</div>
								<br><br>
								<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                                <form id="checkbox-form" method="POST">
                                <table class="table table-striped nowrap">
                                    <tbody id="followup_team">
                                    	
                                    </tbody>
								</table>
								</form>
								</div>
<script>
	function pop(){
		$('[data-toggle="popover"]').popover();
	};
</script>
                            </div>
                        </div>
                    </div>
                </div>

			</div> <!-- content -->

<!--------------------ICON MODAL-------------->

<script type='text/javascript'>

		function followupmodal(id) {
			
			var userid = id;
			console.log(userid);

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"userid": userid},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}
</script>

<script type='text/javascript'>

		function responsesmodal(id) {
			
			var responseid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"responseid": responseid},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}
</script>

<script type='text/javascript'>

		function re_enquiredmodal(id) {
			
			var renquiredid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"renquiredid": renquiredid},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}
</script>

<div class="modal fade" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-slideout modal-md">
    <div class="modal-content" id="right-modal-body">
      
    </div>
  </div>
</div>
<!--------------------ICON MODAL-------------->

<script type='text/javascript'>
		function checkbox_function() {
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide1").css({display: "block"});
			$("#divShowHide2").css({display: "block"});
			$("#divShowHide3").css({display: "block"});
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide1").css({display: "none"});
			$("#divShowHide2").css({display: "none"});
			$("#divShowHide3").css({display: "none"});
			$("#divShowHide4").css({display: "none"});
		}
    }          	
</script>
			
<script type='text/javascript'>
	
		function addfollowupmodal(id) {		
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/next_followup.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-addfollowup').html(response); 

					// Display Modal
					$('#addfollowup').modal('show'); 
				}
			});
		}
</script>												
<!-------Add followup modal-------->
<div class="modal fade" id="addfollowup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Schedule Next Followup</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-addfollowup">
				</div>
			</div>
		</div>
	</div>
<!-------Add Followup modal-------->

<script type='text/javascript'>
	function leadhistory(id) {
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/followup_history.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-history').html(response); 

					// Display Modal
					$('#viewhistory').modal('show'); 
				}
			});
		}
</script>
<!-------History modal-------->
<div class="modal fade" id="viewhistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">History</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-history">
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-------End History modal-------->


<script>
	$(document).ready(function(){
    var buttonCommon = {
        init: function (dt, node, config) {
          var table = dt.table().context[0].nTable;
          if (table) config.title = $(table).data('export-title')
        },
        title: 'default title'
      };
    
    $("#basic-datatable").DataTable({language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}});
    
    var a=$("#followup_table").DataTable({lengthChange:!1,dom: 'Brfrtip',buttons:[$.extend( true, {}, buttonCommon, {
    extend: 'csv',
    exportOptions: {
      columns: 'th:not(:last-child)'
    }
} ),
$.extend( true, {}, buttonCommon, {
    extend: 'copy',
    orientation: 'landscape',
    exportOptions: {
      columns: 'th:not(:last-child)'
    }
} ),
$.extend( true, {}, buttonCommon, {
    extend: 'pdf',
    exportOptions: {
      columns: 'th:not(:last-child)'
    },
    orientation: 'landscape'
} )],initComplete: function() {
    var $buttons = $('.dt-buttons').hide();
    $('#exportLink').on('change', function() {
      var btnClass = $(this).find(":selected")[0].id 
        ? '.buttons-' + $(this).find(":selected")[0].id 
        : null;
      if (btnClass) $buttons.find(btnClass).click(); 
    })
  },"aaSorting": [],
    "bSortable":false,
	"orderable": false,
	"ordering": false,
  'columnDefs': [{
    'targets': 0,
    'width': '1%',
 }],"pageLength": 50,language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}});$("#selection-datatable").DataTable({select:{style:"multi"},language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}}),$("#key-datatable").DataTable({keys:!0,language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}}),a.buttons().container().appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)")});


</script>

<!-- <script>
	$(document).ready(function() {
		var table = $('#followup_table').DataTable();
 
		$('#followup_date').on('change', function () {
		    table.search(this.value).draw();
		} );
	});
</script> -->

<!-------Refer Selected modal-------->
<div class="modal fade" id="selected_refer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Refer Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-refer-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Refer Selected modal-------->
<!-------SMS Selected modal-------->
<div class="modal fade" id="selected_sms" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Send SMS to Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-sms-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End SMS Selected modal-------->
<!-------SMS Selected modal-------->
<div class="modal fade" id="selected_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Send Mail to Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-mail-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End SMS Selected modal-------->
<!-------Delete Selected modal-------->
<div class="modal fade" id="selected_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
		<div class="modal-dialog modal-xs">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel2">Delete Selected Leads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body-delete-selected">
					
				</div>
			</div>
		</div>
	</div>
<!-------End Delete Selected modal-------->
<script>
function whatsapp(id)
		{
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/whatsapp.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-whatsapp').html(response); 

					// Display Modal
					$('#whatsappmessage').modal('show'); 
				}
			});
			}
		

</script>
<!-------whatsapp modal-------->
<div class="modal fade" id="whatsappmessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
	<div class="modal-dialog modal-xs" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Send WhatsApp Message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body" id="modal-body-whatsapp">
			</div>	
		</div>
	</div>
</div>
<!-------whatsapp modal-------->
<script type="text/javascript">
	$(document).ready(function() {
		$(document).ajaxStart(function(){
		    $('#divLoader').css("display", "block");
		});
		$(document).ajaxStop(function(){
		    $('#divLoader').css("display", "none");
		});
	});
</script>

<script type="text/javascript">

	function add_active(val){
    
    var oldURL = window.location.href.split("?")[0];

	var newURL = oldURL+"?tabIndex="+val;

    history.pushState(null, '', newURL);

    $('.user_tabs').removeClass('active');
    if(val ==0){
      
		$('#missed_follow_ups').addClass('active');
    }
    else if(val ==1){
		$('#planned_follow_ups').addClass('active');

    }

  

  }
	function getList(vars="") {
			$("#divShowHide1").css({display: "none"});
			$("#divShowHide2").css({display: "none"});
			$("#divShowHide3").css({display: "none"});
			$("#divShowHide4").css({display: "none"});
			var searchstring = $('#searchbox').val();
			var tabValue = $('.user_tabs.active').val();
			var lead_date = $('#range-datepicker').val();
			var counsellor_id = $('#counsellor_id').val();
	

			var itemPerPage = 25;
      var page =1;
      var totalPages = 0;
      if($("#pagination_info").attr("data-totalpages") != undefined ){
          totalPages = $("#pagination_info").attr("data-totalpages");
      }

      if($("#pagination_info").attr("data-currentpage") != undefined){
          page = $("#pagination_info").attr("data-currentpage");
      }

      if(vars == 'next' && page > 0 && totalPages != page ){
         page = parseInt(page)+1;
        // $('#next').attr('data-page',page);
         $("#pagination_info").attr("data-currentPage",page)
      }else{
        //$('#next').attr('data-page',1);
        $('#next').attr('disabled','disabled');
      }

      if(vars == 'pre' && page > 1 ){
         page = parseInt(page)-1 ;
         $('#pre').attr('data-page',page);
        // value='';
      }else{
        $('#pre').attr('data-page',1);
        $('#pre').attr('disabled','disabled');
      }

			$.ajax({
				url:"fetch_myfollowups.php",
				type: "GET",
				global: true, 
				data: {"counsellor_id":counsellor_id,"lead_date":lead_date,"tabValue":tabValue,"searchstring":searchstring,"page":page,"itemPerPage":itemPerPage},
				success: function(data) {
					//console.log(data);
					$("#followup_team").html(data);
					$('html, body').animate({ scrollTop: 0 }, 'slow');
					
				}
			});
		}
		getList();
</script>
<!-- Modal -->
	<div class="modal right fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
		<div class="modal-dialog" role="document" style="margin-right: 303px;">
			<div class="modal-content">

				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel2">Follow Ups Filter</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					
				</div>

				<div class="modal-body">

					
					<div class="form-group">

					    <label for="Counsellor">Counsellor</label>
					   	<select class="form-control" id="counsellor_id" multiple data-plugin="customselect">
                                		
                                		<?php $counsellor = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '".$_SESSION['useremployeeid']."' AND Role = 'Counsellor'");
                                		while($data= mysqli_fetch_assoc($counsellor)) {?>
                                		<option value="<?php echo $data['ID'];?>"><?php echo $data['Name'];?></option>
                                	<?php } ?>
                                	</select>
					  </div>
					  <div class="form-group">
					    <label for="Course">Course</label>
						<select data-plugin="customselect" multiple class="form-control" id="Course_filter"> 
                                		<option value="">Select Course</option>
                                		<?php $course = $conn->query("SELECT * FROM Courses WHERE Institute_ID = '".$users_data['Institute_ID']."'");
                                		while($course_name= mysqli_fetch_assoc($course)) {?>
                                		<option value="<?php echo $course_name['ID'];?>"><?php echo $course_name['Name'];?></option>
                                	<?php }?>
									</select>
					  </div>
					  
					  <div class="form-group">
					    <label for="From_Date">Date</label>
					    <input type="text"  class="form-control datepicker_range" placeholder="yyyy-mm-dd to yyyy-mm-dd" id="creation_date_filter">
					  
					  </div>

					  

					  <div class="form-group">
					  	<label for="number_of_rows">Number of Rows</label>
					  	<input type="text" class="form-control" placeholder="Enter Rows" id="number_rows">
					  </div>

					  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="FollowUpsFilter();">Search</button>
      </div>

					  

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->

	<script type="text/javascript">
	function FollowUpsFilter(vars="")
	{
		$('#searchbox').val('');
		var counsellor_id = $('#counsellor_id').val();
		var Course_filter = $('#Course_filter').val();
		
		var date = $('#creation_date_filter').val();
		var tabValue = $('.user_tabs.active').val();
	var itemPerPage = 25;
	var page =1;
	var totalPages = 0;
	var number_rows = $('#number_rows').val();
	if(number_rows > 0) {
		var itemPerPage = number_rows;
	}
	else {
		var itemPerPage = 25;
	}
	if($("#pagination_info").attr("data-totalpages") != undefined ){
	  totalPages = $("#pagination_info").attr("data-totalpages");
	}

	if($("#pagination_info").attr("data-currentpage") != undefined){
	  page = $("#pagination_info").attr("data-currentpage");
	}

	if(vars == 'next' && page > 0 && totalPages != page ){
	 page = parseInt(page)+1;
	// $('#next').attr('data-page',page);
	 $("#pagination_info").attr("data-currentPage",page)
	}else{
	//$('#next').attr('data-page',1);
	$('#next').attr('disabled','disabled');
	}

	if(vars == 'pre' && page > 1 ){
	 page = parseInt(page)-1 ;
	 $('#pre').attr('data-page',page);
	// value='';
	}else{
	$('#pre').attr('data-page',1);
	$('#pre').attr('disabled','disabled');
	}

		$.ajax({
			url: "fetch_filter_followup.php",
			type: "POST",
			data: {"Course_filter":Course_filter,"counsellor_id":counsellor_id,"date":date,"tabValue":tabValue,"page":page,"itemPerPage":itemPerPage},
			success:function(res)
			{
				$('#filterModal').modal('hide');
				$('#FollowUpFil').empty();
				$('#FollowUpFil').html(res);
			}
		})
	}
</script>

<script type="text/javascript">
  function copyName(id) {
    var copyText = document.getElementById(id);
    var textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    textArea.value = textArea.value.split(": ")[1];
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();

    if(id.includes("person_name")) {
      toastr.info("Name copied");
    }
    else if(id.includes("person_email")) {
      toastr.info("Email copied");
    }
    else if(id.includes("person_mobile")) {
      toastr.info("Mobile copied");
    }
  }
</script>

<?php

if(isset($_GET['tabIndex'])) {
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			var tabVal = '<?php echo $_GET['tabIndex']; ?>';

			add_active(tabVal);getList();
		});
	</script>
	<?php
}

?>


<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>