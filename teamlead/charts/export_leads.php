<?php
//include database configuration file
if(session_status() === PHP_SESSION_NONE) session_start();
include '../filestobeincluded/db_config.php';

if(!empty($_POST['manager'])){
    $manager = $_POST['manager'];
    $exp = explode('(', $manager);
    $manager_id = rtrim($exp[1], ')');
    $manager_query = "AND (U1.ID = '".$_SESSION['useremployeeid']."' OR Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."')";
}else{
    $manager_query = "AND (U1.ID = '".$_SESSION['useremployeeid']."' OR Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."')";
}

if(!empty($_POST['stage'])){
    $stage = $_POST['stage'];
    $get_stage_id = $conn->query("SELECT ID FROM Stages WHERE Name like '$stage'");
    $gsi = mysqli_fetch_assoc($get_stage_id);
    $stage_query = "AND Leads.Stage_ID = '".$gsi['ID']."'";
}else{
    $stage_query = ' ';
}

if(!empty($_POST['counsellor'])){
    $couns = $_POST['counsellor'];
    $exp = explode('(', $couns);
    $couns_id = rtrim($exp[1], ')');
    $couns_query = "AND Leads.Counsellor_ID = '$couns_id'";
}else{
    $couns_query = ' ';
}

if(!empty($_POST['course'])){
    $course = $_POST['course'];
    $exp = explode('-', $course);
    $new_course = $exp[0];
    $new_univ = $exp[1];
    $get_newuniv_id = $conn->query("SELECT ID FROM Institutes WHERE Name like '$new_univ'");
    $nui = mysqli_fetch_assoc($get_newuniv_id);
    $get_course_id = $conn->query("SELECT ID FROM Courses WHERE Name like '$new_course' AND Institute_ID = '".$nui['ID']."'");
    $gci = mysqli_fetch_assoc($get_course_id);
    $course_query = "AND Leads.Course_ID = '".$gci['ID']."'";
}else{
    $course_query = ' ';
}

if(!empty($_POST['university'])){
    $univ = $_POST['university'];
    $get_univ_id = $conn->query("SELECT ID FROM Institutes WHERE Name like '$univ'");
    $gui = mysqli_fetch_assoc($get_univ_id);
    $univ_query = "AND Leads.Institute_ID = '".$gui['ID']."'";
}else{
    $univ_query = ' ';
}


if(!empty($_POST['state'])){
    $state = $_POST['state'];
    $get_state_id = $conn->query("SELECT ID FROM States WHERE Name like '$state'");
    if($get_state_id->num_rows>0){
        $gstate = mysqli_fetch_assoc($get_state_id);
        $state_query = "AND Leads.State_ID = '".$gstate['ID']."'";
    }else{
        $state_query = "AND (Leads.State_ID = '' OR Leads.State_ID = ' ')";
    }
}else{
    $state_query = ' ';
}

if(!empty($_POST['source'])){
    $source = $_POST['source'];
    $get_source_id = $conn->query("SELECT ID FROM Sources WHERE Name like '$source'");
    $gsourcei = mysqli_fetch_assoc($get_source_id);
    $source_query = "AND Leads.Source_ID = '".$gsourcei['ID']."'";
}else{
    $source_query = ' ';
}

if(!empty($_POST['attempt'])){
    $attempt = $_POST['attempt'];
    $attempt_query = "attempt = '$attempt'";
}else{
    $attempt_query = ' ';
}



if(!empty($_POST['date'])){
    if(strpos($_POST['date'],'to')>0)
    {
      $date = explode('to', $_POST['date']);
      $from_date = date($date[0]);
      $to_date = date($date[1]);
      $date_query = "AND (Leads.Created_at BETWEEN '$from_date' AND '$to_date')";
    }
    else{
      $from_date = date($_POST['date']);
      $date_query = "AND (Leads.Created_at BETWEEN '".$from_date." 00:00:00' AND '".$from_date." 23:59:59')";
    }
}else{
    $date_query = ' ';
}

if(!empty($_POST['update_date'])){
    if(strpos($_POST['update_date'],'to')>0)
    {
      $date = explode('to', $_POST['update_date']);
      $from_date = date($date[0]);
      $to_date = date($date[1]);
      $update_date_query = "AND (Leads.TimeStamp BETWEEN '$from_date' AND '$to_date')";
    }
    else{
      $from_date = date($_POST['date']);
      $update_date_query = "AND (Leads.TimeStamp BETWEEN '".$from_date." 00:00:00' AND '".$from_date." 23:59:59')";
    }
}else{
    $update_date_query = ' ';
}


if(!empty($_POST['attempt'])){
                                                                                                                           
    $query = $conn->query("SELECT ID, Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID,Created_at, TimeStamp FROM Leads WHERE ID IN (SELECT leads_id FROM (SELECT leads_id, attempt FROM (SELECT DISTINCT(Leads.ID) as leads_id, COUNT(History.Lead_ID) as attempt FROM Leads LEFT JOIN History ON Leads.ID = History.Lead_ID LEFT JOIN users ON Leads.Counsellor_ID = users.ID LEFT JOIN users as U1 ON users.Reporting_To_User_ID = U1.ID LEFT JOIN Institutes ON Leads.Institute_ID = Institutes.ID LEFT JOIN Courses ON Leads.Course_ID = Courses.ID LEFT JOIN Stages ON Leads.Stage_ID = Stages.ID LEFT JOIN States ON Leads.State_ID = States.ID LEFT JOIN Sources ON Leads.Source_ID = Sources.ID WHERE Leads.ID = History.Lead_ID AND users.Role != 'Administrator' ".$course_query.$univ_query.$stage_query.$state_query.$source_query.$manager_query.$couns_query.$date_query.$update_date_query." GROUP BY Leads.ID ORDER BY attempt DESC) Leads WHERE ".$attempt_query.") Leads)");
}else{
    $query = $conn->query("SELECT ID, Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID,Created_at, TimeStamp FROM Leads WHERE ID IN (SELECT Leads.ID FROM Leads LEFT JOIN users ON Leads.Counsellor_ID = users.ID LEFT JOIN users as U1 ON users.Reporting_To_User_ID = U1.ID LEFT JOIN States ON Leads.State_ID = States.ID LEFT JOIN Institutes ON Leads.Institute_ID = Institutes.ID LEFT JOIN Courses ON Leads.Course_ID = Courses.ID LEFT JOIN Stages ON Leads.Stage_ID = Stages.ID LEFT JOIN Sources ON Leads.Source_ID = Sources.ID WHERE users.Role != 'Administrator' ".$manager_query.$univ_query.$course_query.$stage_query.$state_query.$source_query.$couns_query.$date_query.$update_date_query.") ORDER BY ID DESC");
}



if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "Leads_" . date('Y-m-d') . ".csv";
    
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Name',	'Email', 'Mobile', 'Alt Mobile', 'Stage', 'Reason', 'Remarks', 'University', 'Course', 'Specialization', 'State', 'City', 'Source', 'Subsource', 'Counsellor', 'Updated Date', 'Creation Date');

    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$row['Stage_ID']."'");
        if($stage_query->num_rows > 0) {
            $stage_fet = mysqli_fetch_assoc($stage_query);
            $stage_Id = $stage_fet['Name'];
        }else{
            $stage_Id = ' ';
        }
        $reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$row['Reason_ID']."'");
        if($reason_query->num_rows > 0) {
            $reason_fet = mysqli_fetch_assoc($reason_query);
            $reason_Id = $reason_fet['Name'];
        }else{
            $reason_Id = ' ';
        }
        $institute_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$row['Institute_ID']."'");
        if($institute_query->num_rows > 0) {
            $institute_fet = mysqli_fetch_assoc($institute_query);
            $institute_Id = $institute_fet['Name'];
        }else{
            $institute_Id = ' ';
        }
        $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$row['Course_ID']."' AND Institute_ID = '".$row['Institute_ID']."'");
        if($course_query->num_rows > 0) {
            $course_fet = mysqli_fetch_assoc($course_query);
            $course_Id = $course_fet['Name'];
        }else{
            $course_Id = ' ';
        }
        $specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$row['Specialization_ID']."' AND Institute_ID = '".$row['Institute_ID']."' AND Course_ID = '".$row['Course_ID']."'");
        if($specialization_query->num_rows > 0) {
            $specialization_fet = mysqli_fetch_assoc($specialization_query);
            $specialization_Id = $specialization_fet['Name'];
        }else{
            $specialization_Id = ' ';
        }
        $state_query = $conn->query("SELECT * FROM States WHERE ID = '".$row['State_ID']."'");
        if($state_query->num_rows > 0) {
            $state_fet = mysqli_fetch_assoc($state_query);
            $state_Id = $state_fet['Name'];
        }else{
            $state_Id = ' ';
        }
        $city_query = $conn->query("SELECT * FROM Cities WHERE ID = '".$row['City_ID']."'");
        if($city_query->num_rows > 0) {
            $city_fet = mysqli_fetch_assoc($city_query);
            $city_Id = $city_fet['Name'];
        }else{
            $city_Id = ' ';
        }
        $source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$row['Source_ID']."'");
        if($source_query->num_rows > 0) {
            $source_fet = mysqli_fetch_assoc($source_query);
            $source_Id = $source_fet['Name'];
        }else{
            $source_Id = ' ';
        }
        $subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$row['Subsource_ID']."'");
        if($subsource_query->num_rows > 0) {
            $subsource_fet = mysqli_fetch_assoc($subsource_query);
            $subsource_Id = $subsource_fet['Name'];
        }else{
            $subsource_Id = ' ';
        }
        $employee_Id = $row['Counsellor_ID'];
        
        $created_date = $row['Created_at'];
        $updated_date = $row['TimeStamp'];
        $lineData = array($row['Name'], $row['Email'], $row['Mobile'], $row['Alt_Mobile'], $stage_Id, $reason_Id, $row['Remarks'], $institute_Id, $course_Id, $specialization_Id, $state_Id, $city_Id, $source_Id, $subsource_Id, $employee_Id, $updated_date, $created_date);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>