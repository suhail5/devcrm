var manager = $('#manager').val();
var counsellor = $('#counsellor').val();
var course = $('#course').val();
var stage = $('#stage').val();
var source = $('#source').val();
var university = $('#university').val();
var date = $('#lead_date').val();
var update_date = $('#update_date').val();
var state = $('#state').val();
var month = $('#month').val();
var attempt = $('#attempt').val();




    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart_stage);

    function drawChart_stage() {
        var manager = $('#manager').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var source = $('#source').val();
        var university = $('#university').val();
        var date = $('#lead_date').val();
        var update_date = $('#update_date').val();
        var state = $('#state').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/stage_chart.php',
            dataType: "json",
            data:{"manager":manager, "university":university, "counsellor":counsellor, "course":course, "source":source, "date":date, "state":state, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        var data = new google.visualization.arrayToDataTable(Data);
    var options = {
        legend: {
            position: 'none'
        },
        bars: 'horizontal',
        animation: {
            duration: 1000,
            easing: 'out'
        }
    };

    var chart = new google.charts.Bar(document.getElementById('stage_chart'));
    chart.draw(data, google.charts.Bar.convertOptions(options));
    $(window).resize(function() {
      chart.draw(data, google.charts.Bar.convertOptions(options));
    });
    google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selection = chart.getSelection();
                var message = '';
                for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                var category = data
                .getValue(chart.getSelection()[0].row, 0)
                message += '' + category + '';
                }
                }
                if (message == '') {
                message = '';
                }
                $('#stage').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }
                if(($('#state').val()).length==0){
                    drawTable_state();
                }
                if(($('#month').val()).length==0){
                    drawChart_Month();
                }
                drawChart_attempt();
            }
    }



      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChartcourse);
      function drawChartcourse() {
        var manager = $('#manager').val();
        var counsellor = $('#counsellor').val();
        var stage = $('#stage').val();
        var source = $('#source').val();
        var university = $('#university').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var state = $('#state').val();
        var CourseData = $.ajax({
            type:'POST',
            url:'charts/course_chart.php',
            dataType: "json",
            data:{"manager":manager, "university":university, "counsellor":counsellor, "stage":stage, "source":source, "date":date, "state":state, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(CourseData);
        var data = new google.visualization.arrayToDataTable(CourseData);

        var options = {
          //is3D:true,
          pieSliceText: 'label',
          slices: {  4: {offset: 0.2},
                    12: {offset: 0.3},
                    14: {offset: 0.4},
                    15: {offset: 0.5},
          },
          legend: {position: 'none'},
          animation: {
            duration: 1000,
            easing: 'out'
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
        $(window).resize(function() {
            chart.draw(data, options);
        });
        google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selectedItem = chart.getSelection()[0];

                if (selectedItem) 
                {
                    var topping = data.getValue(selectedItem.row, 0);
                    $('#course').val(topping);
                }else{
                    $('#course').val('');
                }
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }
                if(($('#state').val()).length==0){
                    drawTable_state();
                }
                if(($('#month').val()).length==0){
                    drawChart_Month();
                }
                drawChart_attempt();
            }
      }




      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(counsellor_chart_draw);
      function counsellor_chart_draw() {  
        var manager = $('#manager').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var source = $('#source').val();
        var university = $('#university').val();
        var state = $('#state').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();        
            var Data = $.ajax({
            type:'POST',
            url:'charts/counsellor_chart.php',
            dataType: "json",
            data:{"manager":manager, "university":university, "course":course, "stage":stage, "source":source, "date":date, "state":state, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        var data = new google.visualization.arrayToDataTable(Data);
        

        var options = {
            legend: {
                position: 'none'
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('counsellor_chart'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
        $(window).resize(function() {
            chart.draw(data, google.charts.Bar.convertOptions(options));
        });
        google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selection = chart.getSelection();
                var message = '';
                for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                var category = data
                .getValue(chart.getSelection()[0].row, 0)
                message += '' + category + '';
                }
                }
                if (message == '') {
                    message = '';
                    var manager = $('#manager').val('');
                    var counsellor = $('#counsellor').val('');
                    var course = $('#course').val('');
                    var stage = $('#stage').val('');
                    var source = $('#source').val('');
                    var university = $('#university').val('');
                    counsellor_chart_draw();
                }
                $('#counsellor').val(message);
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }
                if(($('#manager').val()).length==0){
                    drawTable_manager();
                }
                if(($('#state').val()).length==0){
                    drawTable_state();
                }
                if(($('#month').val()).length==0){
                    drawChart_Month();
                }
                drawChart_attempt();
            }
        }


      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart_univ);

      function drawChart_univ() {
        var manager = $('#manager').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var source = $('#source').val();
        var state = $('#state').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/university_chart.php',
            dataType: "json",
            data:{"manager":manager, "counsellor":counsellor, "course":course, "stage":stage, "source":source, "date":date, "state":state, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        var data = new google.visualization.arrayToDataTable(Data);

        var options = {
            legend: {
                position: 'none'
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('university_chart'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
        $(window).resize(function() {
            chart.draw(data, google.charts.Bar.convertOptions(options));
        });
        google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler() {
                var selection = chart.getSelection();
                var message = '';
                for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                var category = data
                .getValue(chart.getSelection()[0].row, 0)
                message += '' + category + '';
                }
                }
                if (message == '') {
                message = '';
                }
                $('#university').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#manager').val()).length==0){
                    drawTable_manager();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }
                if(($('#state').val()).length==0){
                    drawTable_state();
                }
                if(($('#month').val()).length==0){
                    drawChart_Month();
                }
                drawChart_attempt();
            }
      }


      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable_manager);

      function drawTable_manager() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Manager');
        data.addColumn('number', 'Lead Count');
        var university = $('#university').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var state = $('#state').val();
        var source = $('#source').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/manager_chart.php',
            dataType: "json",
            data:{"university":university, "counsellor":counsellor, "course":course, "stage":stage, "source":source, "date":date, "state":state, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        //var data = new google.visualization.arrayToDataTable(Data);


        data.addRows(Data);
        //data.sort({column: 1, desc: true});


        var manager_table = new google.visualization.Table(document.getElementById('manager_chart'));
        manager_table.draw(data, {width: '100%', height: '100%'});
        google.visualization.events.addListener(manager_table, 'select', selectHandler);

            // The selection handler.
            // Loop through all items in the selection and concatenate
            // a single message from all of them.
            function selectHandler() {
            var selection = manager_table.getSelection();
            var message = '';
            for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                message += '' + str + '';
                } else if (item.row != null) {
                var str = data.getFormattedValue(item.row, 0);
                message += '' + str + '';
                } else if (item.column != null) {
                var str = data.getFormattedValue(0, item.column);
                message += '' + str + '';
                }
            }
            if (message == '') {
                message = '';
            }
                $('#manager').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#source').val()).length==0){
                    drawTable_source(); 
                }
                if(($('#state').val()).length==0){
                    drawTable_state();
                }
                if(($('#month').val()).length==0){
                    drawChart_Month();
                }
                drawChart_attempt();
            }
        }
      



      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable_source);

      function drawTable_source() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Sources');
        data.addColumn('number', 'Lead Count');
        var university = $('#university').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var manager = $('#manager').val();
        var state = $('#state').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/source_chart.php',
            dataType: "json",
            data:{"university":university, "counsellor":counsellor, "course":course, "stage":stage, "manager":manager, "date":date, "state":state, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        data.addRows(Data);
        //data.sort({column: 1, desc: true});


        var table = new google.visualization.Table(document.getElementById('source_chart'));
        table.draw(data, {width: '100%', height: '100%'});
        google.visualization.events.addListener(table, 'select', selectHandler);

            // The selection handler.
            // Loop through all items in the selection and concatenate
            // a single message from all of them.
            function selectHandler() {
            var selection = table.getSelection();
            var message = '';
            for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                message += '' + str + '';
                } else if (item.row != null) {
                var str = data.getFormattedValue(item.row, 0);
                message += '' + str + '';
                } else if (item.column != null) {
                var str = data.getFormattedValue(0, item.column);
                message += '' + str + '';
                }
            }
            if (message == '') {
                message = '';
            }
                $('#source').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#state').val()).length==0){
                    drawTable_state();
                }
                if(($('#month').val()).length==0){
                    drawChart_Month();
                }   
                drawChart_attempt();   
        }
      }

      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable_state);

      function drawTable_state() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'States');
        data.addColumn('number', 'Lead Count');
        var university = $('#university').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var stage = $('#stage').val();
        var manager = $('#manager').val();
        var source = $('#source').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/state_chart.php',
            dataType: "json",
            data:{"university":university, "counsellor":counsellor, "course":course, "stage":stage, "manager":manager, "date":date, "source":source, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        data.addRows(Data);
        //data.sort({column: 1, desc: true});


        var table = new google.visualization.Table(document.getElementById('state_chart'));
        table.draw(data, {width: '100%', height: '100%'});
        google.visualization.events.addListener(table, 'select', selectHandler);

            // The selection handler.
            // Loop through all items in the selection and concatenate
            // a single message from all of them.
            function selectHandler() {
            var selection = table.getSelection();
            var message = '';
            for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                message += '' + str + '';
                } else if (item.row != null) {
                var str = data.getFormattedValue(item.row, 0);
                message += '' + str + '';
                } else if (item.column != null) {
                var str = data.getFormattedValue(0, item.column);
                message += '' + str + '';
                }
            }
            if (message == '') {
                message = '';
            }
                $('#state').val(message);
                if(($('#counsellor').val()).length==0){
                    counsellor_chart_draw();
                }
                if(($('#course').val()).length==0){
                    drawChartcourse();
                }
                if(($('#stage').val()).length==0){
                    drawChart_stage();
                }
                if(($('#university').val()).length==0){
                    drawChart_univ();
                }
                if(($('#source').val()).length==0){
                    drawTable_source();
                }
                if(($('#month').val()).length==0){
                    drawChart_Month();
                }
                drawChart_attempt();
                
        }
      }


      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart_Month);

      function drawChart_Month() {
        var manager = $('#manager').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var source = $('#source').val();
        var university = $('#university').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var state = $('#state').val();
        var stage = $('#stage').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/month_chart.php',
            dataType: "json",
            data:{"manager":manager, "university":university, "counsellor":counsellor, "course":course, "source":source, "date":date, "state":state, "stage":stage, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        var data = new google.visualization.arrayToDataTable(Data);
        

        var options = {
          legend : {position: 'none'}
        };

        var chart = new google.charts.Bar(document.getElementById('month_chart'));

        chart.draw(data, options);
        $(window).resize(function() {
            chart.draw(data, options);
        });
        // google.visualization.events.addListener(chart, 'select', selectHandler);
      
        // function selectHandler() {
        //     var selection = chart.getSelection();
        //     var message = '';
        //     for (var i = 0; i < selection.length; i++) {
        //     var item = selection[i];
        //     if (item.row != null && item.column != null) {
        //     var str = data.getFormattedValue(item.row, item.column);
        //     var category = data
        //     .getValue(chart.getSelection()[0].row, 0)
        //     message += '' + category + '';
        //     }
        //     }
        //     if (message == '') {
        //     message = '';
        //     }
        //     $('#month').val(message);
        //     if(($('#counsellor').val()).length==0){
        //         counsellor_chart_draw();
        //     }
        //     if(($('#course').val()).length==0){
        //         drawChartcourse();
        //     }
        //     if(($('#university').val()).length==0){
        //         drawChart_univ();
        //     }
        //     if(($('#source').val()).length==0){
        //         drawTable_source();
        //     }
        //     if(($('#state').val()).length==0){
        //         drawTable_state();
        //     }
        //     if(($('#manager').val()).length==0){
        //         drawTable_manager();
        //     }
        //     if(($('#stage').val()).length==0){
        //         drawChart_stage();
        //     }

        // }
      }


      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart_attempt);

      function drawChart_attempt() {
        var manager = $('#manager').val();
        var counsellor = $('#counsellor').val();
        var course = $('#course').val();
        var source = $('#source').val();
        var university = $('#university').val();
        var date = $('#lead_date').val(); var update_date = $('#update_date').val();
        var state = $('#state').val();
        var stage = $('#stage').val();
        var Data = $.ajax({
            type:'POST',
            url:'charts/attempt_chart.php',
            dataType: "json",
            data:{"manager":manager, "university":university, "counsellor":counsellor, "course":course, "source":source, "date":date, "state":state, "stage":stage, "update_date":update_date},
            async: false
            }).responseJSON;
        
        //console.log(Data);
        var data = new google.visualization.arrayToDataTable(Data);
        

        var options = {
          legend : {position: 'none'},
          bars: 'horizontal',
        };

        var chart = new google.charts.Bar(document.getElementById('attempt_chart'));

        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);
      
        function selectHandler() {
            var selection = chart.getSelection();
            var message = '';
            for (var i = 0; i < selection.length; i++) {
            var item = selection[i];
            if (item.row != null && item.column != null) {
            var str = data.getFormattedValue(item.row, item.column);
            var category = data
            .getValue(chart.getSelection()[0].row, 0)
            message += '' + category + '';
            }
            }
            if (message == '') {
            message = '';
            }
            $('#attempt').val(message);
            
        }
        
      }

      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable_logout);

      function drawTable_logout() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'User ID');
        data.addColumn('string', 'Last Log-out Time');
        var Data = $.ajax({
            url:'charts/logout_chart.php',
            dataType: "json",
            async: false
            }).responseJSON;
        
        
        data.addRows(Data);


        var table = new google.visualization.Table(document.getElementById('logout_chart'));
        table.draw(data, {width: '100%', height: '100%'});
      }


      function date_select(){
        var manager = $('#manager').val('');
        var counsellor = $('#counsellor').val('');
        var course = $('#course').val('');
        var stage = $('#stage').val('');
        var source = $('#source').val('');
        var university = $('#university').val('');
        var state = $('#state').val('');
        var month = $('#month').val('');
        var update_date = $('#update_date').val('');
    
        counsellor_chart_draw();
        drawChartcourse();
        drawChart_stage();
        drawChart_univ();
        drawTable_manager();
        drawTable_source();
        drawTable_state();
        drawChart_Month();
        drawChart_attempt();
    }
    
    function update_date_select(){
        var manager = $('#manager').val('');
        var counsellor = $('#counsellor').val('');
        var course = $('#course').val('');
        var stage = $('#stage').val('');
        var source = $('#source').val('');
        var university = $('#university').val('');
        var state = $('#state').val('');
        var month = $('#month').val('');
        var date = $('#lead_date').val('');
    
    
        counsellor_chart_draw();
        drawChartcourse();
        drawChart_stage();
        drawChart_univ();
        drawTable_manager();
        drawTable_source();
        drawTable_state();
        drawChart_Month();
        drawChart_attempt();
    }
