<?php
if(session_status() === PHP_SESSION_NONE) session_start();
require '../filestobeincluded/db_config.php';

$all_array = array();
$coun_array = array();
$couns_array = array();
$get_tree = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '". $_SESSION['useremployeeid'] ."'");
while($row = $get_tree->fetch_assoc()) {
    $all_array[] = $row;
}
foreach($all_array as $univ){
    $university = $univ['Institute_ID'];
    $get_counsellors = $conn->query("SELECT * FROM users WHERE Institute_ID = $university AND Role = 'Counsellor'");
    while($rows = $get_counsellors->fetch_assoc()) {
        $coun_array[] = $rows;
    }
}
foreach($coun_array as $cuniv){
    $couns_array[]=$cuniv['ID'];                                                     
}
$imp = "'" . implode( "','", ($couns_array) ) . "'";
$tree_ids = $imp.",'".$_SESSION['USERS_ID']."'";

?>



<?php

if(isset($_POST['userid'])){
    $leadid = $_POST['userid'];

    $all_leads = array();
    $all_followup = array();
    
    $leads_query_res = $conn->query("SELECT * FROM Leads WHERE ID = $leadid AND Counsellor_ID in ($tree_ids)");
    while($row = $leads_query_res->fetch_assoc()) {
        $all_leads[] = $row;
    }
    $followup_query_res = $conn->query("SELECT * FROM Follow_Ups WHERE Lead_ID = $leadid AND Counsellor_ID in ($tree_ids) ORDER BY ID DESC");
    while($frow = $followup_query_res->fetch_assoc()) {
        $all_followup[] = $frow;
    }
    
    foreach($all_leads as $lead){
    ?>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Counsellor Followup for <?php echo $lead['Name']; ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="custom-accordion accordion ml-4" id="customaccordion_exa">
                <?php
                    foreach($all_followup as $af){
                ?>
                
                    <div class="card mb-1">
                        <span class="text-dark" aria-expanded="true">
                            <div class="card-header">
                                <h5 class="m-0 font-size-14">
                                    <i class="uil uil-notes h3 text-primary icon"></i>
                                        Follow-up added on
                                </h5>
                            </div>
                        </span>

                        <div class="show">
                            <div class="card-body text-muted">
                                <p><font style="font-weight: 600;"><b><?php echo date("F j, Y g:i a", strtotime($af['Followup_Timestamp'])) ?></b></font></p>
                                <p style="font-weight: 500;">Comment : <?php echo $af['Remark']; ?></p>
                                <p style="font-weight: 500;">Counsellor: <?php $get_counsellor = $conn->query("SELECT Name FROM users WHERE ID = '".$af['Counsellor_ID']."' LIMIT 1");
                                                while($crow = $get_counsellor->fetch_assoc()) { echo $crow["Name"];}   ?></p>
                            </div>
                        </div>
                    </div>

                <?php
                    }
                ?>
            </div>
        </div>
    <?php
    }
}
?>

<?php

if(isset($_POST['responseid'])){
    $leadid = $_POST['responseid'];

    $all_leads = array();
    $all_mail = array();
    
    $leads_query_res = $conn->query("SELECT * FROM Leads WHERE ID = $leadid AND Counsellor_ID in ($tree_ids)");
    while($row = $leads_query_res->fetch_assoc()) {
        $all_leads[] = $row;
    }
    $mail_log_res = $conn->query("SELECT * FROM Email_Logs WHERE Lead_ID = $leadid AND Employee_ID in ($tree_ids)");
    while($mrow = $mail_log_res->fetch_assoc()) {
        $all_mail[] = $mrow;
    }
    
    foreach($all_leads as $lead){
    ?>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Prospect Responses for <?php echo $lead['Name']; ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
        <div class="custom-accordion accordion ml-4" id="customaccordion_exa">
                <?php
                    foreach($all_mail as $am){
                ?>
                
                    <div class="card mb-1">
                        <span class="text-dark" aria-expanded="true">
                            <div class="card-header">
                                <h5 class="m-0 font-size-14">
                                    <i class="uil uil-envelope-upload h3 text-primary icon"></i>
                                        Delivery Report (Mail)
                                </h5>
                            </div>
                        </span>

                        <div class="show">
                            <div class="card-body text-muted">
                                <p>Dropped on: <font style="font-weight: 600;"><b><?php echo date("F j, Y g:i a", strtotime($am['TimeStamp'])) ?></b></font></p>
                                <p style="font-weight: 500;">Counsellor: <?php $get_counsellor = $conn->query("SELECT Name FROM users WHERE ID = '".$am['Employee_ID']."' LIMIT 1");
                                                while($crow = $get_counsellor->fetch_assoc()) { echo $crow["Name"];}   ?></p>
                                                
                                <button class="btn btn-link" style="font-weight: 600;" onclick="view_mail(<?php echo $am['ID'] ?>);">View</button>
                            </div>
                        </div>
                    </div>

                <?php
                    }
                ?>
            </div>
        </div>
        <script>
            function view_mail(id) {
                var mailid = id;

                // AJAX request
                $.ajax({
                    url: 'ajax_leads/get_email_temp.php',
                    type: 'post',
                    data: {mailid: mailid},
                    success: function(response){ 
                        // Add response in Modal body
                        $('#modal-body-mail').html(response); 

                        // Display Modal
                        $('#modal_mail').modal('show'); 
                    }
                });
            }
        </script>
        



    <?php
    }
}
?>

<?php

if(isset($_POST['renquiredid'])){
    $leadid = $_POST['renquiredid'];
    $all_leads = array();
    $all_enquired = array();
    
    $leads_query_res = $conn->query("SELECT * FROM Leads WHERE ID = '$leadid' LIMIT 1");
    while($row = $leads_query_res->fetch_assoc()) {
        $all_leads[] = $row;
    }
    
        
    foreach($all_leads as $lead){
    ?>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Re-enquired Followup for <?php echo $lead['Name']; ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
        <div class="custom-accordion accordion ml-4" id="customaccordion_exa">
            <?php
                

                $re_enquired_query_res = $conn->query("SELECT * FROM Re_Enquired WHERE Name = '".$lead['Name']."' AND Email = '".$lead['Email']."' AND Mobile = '".$lead['Mobile']."' AND Institute_ID = '".$lead['Institute_ID']."' ORDER BY ID DESC");
                while($rerow = $re_enquired_query_res->fetch_assoc()) {
                    $all_enquired[] = $rerow;
                }
                foreach($all_enquired as $ae){
            ?>
                
                    <div class="card mb-1">
                        <span class="text-dark" aria-expanded="true">
                            <div class="card-header">
                                <h5 class="m-0 font-size-14">
                                    <i class="uil uil-award h3 text-primary icon"></i>
                                        Re-Enquired on
                                </h5>
                            </div>
                        </span>

                        <div class="show">
                            <div class="card-body text-muted">
                                <p><font style="font-weight: 600;"><b><?php echo date("F j, Y g:i a", strtotime($ae['TimeStamp'])) ?></b></font></p>
                                <p style="font-weight: 500;">Comment : <?php echo $ae['Remarks']; ?></p>
                                <p style="font-weight: 500;">Counsellor: <?php $get_counsellor = $conn->query("SELECT Name FROM users WHERE ID = '".$ae['Counsellor_ID']."' LIMIT 1");
                                                while($crow = $get_counsellor->fetch_assoc()) { echo $crow["Name"];}   ?></p>
                            </div>
                        </div>
                    </div>

                <?php
                    }}
                ?>
            </div>
        </div>
<?php
}
?>