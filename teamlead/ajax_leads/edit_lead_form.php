<?php 
require '../filestobeincluded/db_config.php';
if(session_status() === PHP_SESSION_NONE) if(session_status() === PHP_SESSION_NONE) session_start();
$userid = $_POST['userid'];


$sql = "select * from Leads where ID=".$userid;
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);
?>
<form method="POST">
<div class="row">
    <input type="hidden" id="new_leadID" value="<?php echo $row['ID']; ?>">
    <div class="form-group col-lg-6 d-none">
        <label>Stage<span style="color: #F64744; font-size: 12px">*</span></label>
        <select data-plugin="customselect" class="form-control" id="new_stage_select" name="new_stage_select">
            <option selected value="<?php echo $row['Stage_ID'] ?>"><?
            $get_stage_name = $conn->query("SELECT * FROM Stages WHERE ID = '".$row['Stage_ID']."'");
            $stage_name = mysqli_fetch_assoc($get_stage_name);
            if($get_stage_name->num_rows>0){
                echo $stage_name['Name'];
            }else{
                $stage_name['Name'] = 'Select Stage';
                echo $stage_name['Name'];
            }
            ?></option>
            <?php
                if($_SESSION['User_Type']=="b2b"){
                    $result_stage = $conn->query("SELECT * FROM Stages WHERE User_Type = 'b2b' AND Status = 'Y' or ID in ('1','2','8')");
                }else{
                    $result_stage = $conn->query("SELECT * FROM Stages WHERE User_Type is NULL AND Status = 'Y'");
                }
                while($stages = $result_stage->fetch_assoc()) {
            ?>
                <option value="<?php echo $stages['ID']; ?>"><?php echo $stages['Name']; ?></option>
            <?php } ?>
        </select>                                                            
    </div>
    <div class="form-group col-lg-6 d-none">
        <label>Reason<span style="color: #F64744; font-size: 12px">*</span></label>
        <select data-plugin="customselect" class="form-control" id="new_reason_select">
            <option selected value="<?php echo $row['Reason_ID'] ?>"><?
            $get_reason_name = $conn->query("SELECT * FROM Reasons WHERE ID = '".$row['Reason_ID']."'");
            $reason_name = mysqli_fetch_assoc($get_reason_name);
            if($get_reason_name->num_rows>0){
                echo $reason_name['Name'];
            }else{
                $reason_name['Name'] = 'Select Reason';
                echo $reason_name['Name'];
            }
            ?></option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-lg-6">
        <label for="sw-arrows-first-name">Full Name<span style="color: #F64744; font-size: 12px">*</span></label>
        <input type="text" id="new_full_name" name="full_name" class="form-control" placeholder="Full Name" required="required" value="<?php echo $row['Name'] ?>">
    </div>
    <div class="form-group col-lg-6 d-none">
        <label for="sw-arrows-first-name">Email</label>
        <input type="email" id="new_email_id" name="email_id" class="form-control" placeholder="Email" value="<?php echo $row['Email'] ?>">
    </div>
    <div class="form-group col-lg-6 d-none">
        <label for="sw-arrows-first-name">Mobile Number<span style="color: #F64744; font-size: 12px">*</span></label>
        <input type="number" id="new_mobile_number" name="mobile_number" maxlength="10" class="form-control" placeholder="Mobile Number" value="<?php echo $row['Mobile'] ?>">
    </div>
    <div class="form-group col-lg-6 d-none">
        <label for="new_alt_mobile_number">Alternate Number<span style="color: #F64744; font-size: 12px">*</span></label>
        <input type="number" id="new_alt_mobile_number" name="alt_mobile_number" maxlength="10" class="form-control" placeholder="Mobile Number" value="<?php echo $row['Alt_Mobile'] ?>">
    </div>
    <div class="form-group col-lg-6">
        <label for="sw-arrows-first-name">Dob<span style="color: #F64744; font-size: 12px">*</span></label>
        <input type="date" id="dob" name="dob" class="form-control" placeholder="Date of birth" value="<?php echo $row['dob'] ?>">
    </div>
    <div class="form-group col-lg-6">
        <label for="sw-arrows-first-name">Remarks<span style="color: #F64744; font-size: 12px">*</span></label>
        <input type="text" id="new_remarks" name="remarks" class="form-control" placeholder="Remarks" value="<?php echo $row['Remarks'] ?>">
    </div>
</div>
<div class="row">
    <div class="form-group col-lg-6">
        <label for="sw-arrows-first-name">Address</label>
        <input type="text" id="new_address" class="form-control" placeholder="Address" value="<?php echo $row['Address'] ?>">
    </div>
    <div class="form-group col-lg-6">
        <label>State<span style="color: #F64744; font-size: 12px">*</span></label>
        <select data-plugin="customselect" class="form-control" id="new_state_select" name="state_select">
            <option selected value="<?php echo $row['State_ID'] ?>"><?
            $get_state_name = $conn->query("SELECT * FROM States WHERE ID = '".$row['State_ID']."'");
            $state_name = mysqli_fetch_assoc($get_state_name);
            if($get_state_name->num_rows>0){
                echo $state_name['Name'];
            }else{
                $state_name['Name'] = 'Select State';
                echo $state_name['Name'];
            }
            ?></option>
            <?php
                $result_state = $conn->query("SELECT * FROM States WHERE Country_ID = 101");
                while($states = $result_state->fetch_assoc()) {
            ?>
                <option value="<?php echo $states['ID']; ?>"><?php echo $states['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group col-lg-6">
        <label>City<span style="color: #F64744; font-size: 12px">*</span></label>
        <select data-plugin="customselect" class="form-control" id="new_city_select">
            <option selected value="<?php echo $row['City_ID'] ?>"><?
            $get_city_name = $conn->query("SELECT * FROM Cities WHERE ID = '".$row['City_ID']."'");
            $city_name = mysqli_fetch_assoc($get_city_name);
            if($get_city_name->num_rows > 0){
                echo $city_name['Name'];
            }else{
                $city_name['Name']= 'Choose City';
                echo $city_name['Name'];
            }
            
            ?></option>
            <?php
                $result_city = $conn->query("SELECT * FROM Cities WHERE State_ID = '".$row['State_ID']."'");
                while($cities = $result_city->fetch_assoc()) {
            ?>
                <option value="<?php echo $cities['ID']; ?>"><?php echo $cities['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group col-lg-6">
        <label for="sw-arrows-first-name">Pincode</label>
        <input type="text" pattern="\d*" maxlength="6" id="new_pin_code" name="pin_code" class="form-control" placeholder="Pincode" value="<?php echo $row['Pincode']?>">
    </div>
</div>

<div class="row d-none">
    <div class="form-group col-lg-6">
        <label>Source<span style="color: #F64744; font-size: 12px">*</span></label>
        <select data-plugin="customselect" class="form-control" id="new_source_select" name="source_select">
            <option selected value="<?php echo $row['Source_ID'] ?>"><?
            $get_source_name = $conn->query("SELECT * FROM Sources WHERE ID = '".$row['Source_ID']."'");
            $source_name = mysqli_fetch_assoc($get_source_name);
            if($get_source_name->num_rows > 0){
                echo $source_name['Name'];
            }else{
                $source_name['Name']= 'Choose Source';
                echo $source_name['Name'];
            }
            ?></option>
            <?php
                $result_source = $conn->query("SELECT * FROM Sources WHERE Status = 'Y'");
                while($sources = $result_source->fetch_assoc()) {
            ?>
                <option value="<?php echo $sources['ID']; ?>"><?php echo $sources['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
    
    <div class="form-group col-lg-6">
        <label>Sub-Source<span style="color: #F64744; font-size: 12px">*</span></label>
        <select data-plugin="customselect" class="form-control" id="new_subsource_select">
            <option selected value="<?php echo $row['Subsource_ID'] ?>"><?
            $get_subsource_name = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$row['Subsource_ID']."'");
            $subsource_name = mysqli_fetch_assoc($get_subsource_name);
            if($get_subsource_name->num_rows > 0){
                echo $subsource_name['Name'];
            }else{
                $subsource_name['Name']= 'Choose Sub-Source';
                echo $subsource_name['Name'];
            }
            ?></option>
            <?php
                $result_subsource = $conn->query("SELECT * FROM Sub_Sources WHERE Source_ID = '".$row['Source_ID']."'");
                while($subsources = $result_subsource->fetch_assoc()) {
            ?>
                <option value="<?php echo $subsources['ID']; ?>"><?php echo $subsources['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group col-lg-6">
        <label>Counsellor</label>
        <select data-plugin="customselect" class="form-control" id="new_select_counsellor">
            <option selected value="<?php echo $row['Counsellor_ID']?>"><?php  
            $result_user = $conn->query("SELECT * FROM users WHERE ID = '".$row['Counsellor_ID']."'");
            $result = mysqli_fetch_assoc($result_user);
            if($result_user->num_rows > 0){
                echo $result['Name'];
            }else{
                $result['Name']= 'Choose Counsellor';
                echo $result['Name'];
            }
            ?></option>
            <?php
                $result_users = $conn->query("SELECT * FROM users WHERE Status = 'Y' and Reporting_To_User_ID = '".$_SESSION['useremployeeid']."'");
                while($users = $result_users->fetch_assoc()) {
            ?>
                <option value="<?php echo $users['ID']; ?>"><?php echo $users['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group col-lg-6">
        <label>University</label>
        <select data-plugin="customselect" class="form-control" id="new_select_institute" name="select_institute">
            <option selected value="<?php echo $row['Institute_ID'] ?>"><?
            $get_institute_name = $conn->query("SELECT * FROM Institutes WHERE ID = '".$row['Institute_ID']."'");
            $institute_name = mysqli_fetch_assoc($get_institute_name);
            if($get_institute_name->num_rows > 0){
                echo $institute_name['Name'];
            }else{
                $institute_name['Name']= 'Choose Institute';
                echo $institute_name['Name'];
            }
            ?></option>
        </select>
    </div>
</div>
<div class="row d-none">
    <div class="form-group col-lg-6">
        <label>Course</label>
        <select data-plugin="customselect" class="form-control" id="new_select_course">
            <option selected value="<?php echo $row['Course_ID'] ?>"><?
            $get_course_name = $conn->query("SELECT * FROM Courses WHERE ID = '".$row['Course_ID']."'");
            $course_name = mysqli_fetch_assoc($get_course_name);
            if($get_course_name->num_rows > 0){
                echo $course_name['Name'];
            }else{
                $course_name['Name']= 'Choose Course';
                echo $course_name['Name'];
            }
            ?></option>
            <?php
                $result_courses = $conn->query("SELECT * FROM Courses WHERE Institute_ID = '".$row['Institute_ID']."'");
                while($courses = $result_courses->fetch_assoc()) {
            ?>
                <option value="<?php echo $courses['ID']; ?>"><?php echo $courses['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group col-lg-6">
        <label>Specialization</label>
        <select data-plugin="customselect" class="form-control" id="new_select_specialization">
            <option selected value="<?php echo $row['Specialization_ID'] ?>"><?
            $get_specialization_name = $conn->query("SELECT * FROM Specializations WHERE ID = '".$row['Specialization_ID']."'");
            $specialization_name = mysqli_fetch_assoc($get_specialization_name);
            if($get_specialization_name->num_rows > 0){
                echo $specialization_name['Name'];
            }else{
                $specialization_name['Name']= 'Choose Specialization';
                echo $specialization_name['Name'];
            }
            ?></option>
            <?php
                $result_specializations = $conn->query("SELECT * FROM Specializations WHERE Institute_ID = '".$row['Institute_ID']."' AND Course_ID = '".$row['Course_ID']."'");
                while($specializations = $result_specializations->fetch_assoc()) {
            ?>
                <option value="<?php echo $specializations['ID']; ?>"><?php echo $specializations['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-lg-10"></div>
    <div class="col-lg-2">
        <button class="btn btn-light" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" type="button" id="new_add_lead" onclick="updateLead(<?php echo $row['ID']; ?>);" style="float: right;">Update</button>
    </div>
    
</div>


</form>

<script>
    $(document).ready(function() {
        $('#new_stage_select').change(function() {

            $('#new_reason_select').html("<option disabled selected>Select Reason</option>");

            var new_selected_stage = $('#new_stage_select').val();
            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "stage_select": "", "selected_stage": new_selected_stage },
                success: function(data) {
                    if(data != "") {
                        $('#new_reason_select').html(data);
                    }
                    else {
                        $('#new_reason_select').html("<option disabled selected>Select Reason</option>");
                    }
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#new_state_select').change(function() {

            $('#new_city_select').html("<option disabled selected>Select City</option>");

            var new_selected_state = $('#new_state_select').val();
            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "state_select": "", "selected_state": new_selected_state },
                success: function(data) {
                    if(data != "") {
                        $('#new_city_select').html(data);
                    }
                    else {
                        $('#new_city_select').html("<option disabled selected>Select City</option>");
                    }
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#new_source_select').change(function() {

            $('#new_subsource_select').html("<option disabled selected>Select Sub-Source</option>");

            var new_selected_source = $('#new_source_select').val();
            $.ajax
            ({
                type: "POST",
                url: "onselect/onSelect.php",
                data: { "source_select": "", "selected_source": new_selected_source },
                success: function(data) {
                    if(data != "") {
                        $('#new_subsource_select').html(data);
                    }
                    else {
                        $('#new_subsource_select').html("<option disabled selected>Select Sub-Source</option>");
                    }
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#new_select_institute').change(function() {
            $('#new_select_course').html("<option disabled selected>Select Course</option>");
            var selected_institute = $('#new_select_institute').val();
            $.ajax
            ({
                type: "POST",
                url: "onselect/onSelect.php",
                data: { "select_institute": "", "selected_institute": selected_institute },
                success: function(data) {
                    if(data != "") {
                        $('#new_select_course').html(data);
                        jQuery('#new_select_course').trigger('change');
                    }
                    else {
                        $('#new_select_course').html("<option disabled selected>Select Course</option>");
                    }
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#new_select_course').change(function() {

            $('#new_select_specialization').html("<option disabled selected>Select Specialization</option>");
            var selected_course = $('#new_select_course').val();
            var selected_institute = $('#new_select_institute').val();
            console.log(selected_institute);
            
            $.ajax
            ({
                type: "POST",
                url: "onselect/onSelect.php",
                data: { "select_course": "", "selected_course": selected_course, "selected_institute": selected_institute },
                success: function(data) {
                    console.log(data);
                    if(data != "") {
                        $('#new_select_specialization').html(data);
                    }
                    else {
                        $('#new_select_specialization').html("<option disabled selected>Select Specialization</option>");
                    }
                }
            });
        });
    });
</script>

<?php
exit;
?>