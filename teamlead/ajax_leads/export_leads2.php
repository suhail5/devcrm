<?php
//include database configuration file
ini_set('max_input_time','600');
if(session_status() === PHP_SESSION_NONE) if(session_status() === PHP_SESSION_NONE) session_start();
include '../filestobeincluded/db_config.php';

$all_array = array();
$coun_array = array();
$couns_array = array();
$get_tree = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '". $_SESSION['useremployeeid'] ."'");
while($row = $get_tree->fetch_assoc()) {
    $all_array[] = $row;
}
foreach($all_array as $univ){
    $get_counsellors = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '". $_SESSION['useremployeeid'] ."' AND Role = 'Counsellor'");
    while($rows = $get_counsellors->fetch_assoc()) {
        $coun_array[] = $rows;
    }
}
foreach($coun_array as $cuniv){
    $couns_array[]=$cuniv['ID'];                                                     
}
$imp = "'" . implode( "','", ($couns_array) ) . "'";
$tree_ids = $imp.",'".$_SESSION['USERS_ID']."'";

$stage = $_POST['stage_id'];
$header = trim($_POST['header'], '[]');
$a = str_replace('"', '', $header);
$selected = explode(',',$a);
foreach($selected as $sl){
    $selected_header[] = $sl;
}


$stage = $_POST['stage_id'];
if($stage == '0'){

    $query = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID in ($tree_ids) ORDER BY ID DESC ");
    
    
}else{
    $query = $conn->query("SELECT ID, Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID,Created_at, TimeStamp FROM Leads WHERE Stage_ID = $stage and Counsellor_ID in ($tree_ids) ORDER BY ID DESC");
}



if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "Leads_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Name',	'Email', 'Mobile', 'Alt Mobile', 'Stage', 'Reason', 'Remarks', 'University', 'Course', 'Specialization', 'State', 'City', 'Source', 'Subsource', 'Counsellor','Manager', 'Updated Date', 'Creation Date', 'Follow Up Remark');

    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$row['Stage_ID']."'");
        if($stage_query->num_rows > 0) {
            $stage_fet = mysqli_fetch_assoc($stage_query);
            $stage_Id = $stage_fet['Name'];
        }else{
            $stage_Id = ' ';
        }
        $reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$row['Reason_ID']."'");
        if($reason_query->num_rows > 0) {
            $reason_fet = mysqli_fetch_assoc($reason_query);
            $reason_Id = $reason_fet['Name'];
        }else{
            $reason_Id = ' ';
        }
        $institute_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$row['Institute_ID']."'");
        if($institute_query->num_rows > 0) {
            $institute_fet = mysqli_fetch_assoc($institute_query);
            $institute_Id = $institute_fet['Name'];
        }else{
            $institute_Id = ' ';
        }
        $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$row['Course_ID']."' AND Institute_ID = '".$row['Institute_ID']."'");
        if($course_query->num_rows > 0) {
            $course_fet = mysqli_fetch_assoc($course_query);
            $course_Id = $course_fet['Name'];
        }else{
            $course_Id = ' ';
        }
        $specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$row['Specialization_ID']."' AND Institute_ID = '".$row['Institute_ID']."' AND Course_ID = '".$row['Course_ID']."'");
        if($specialization_query->num_rows > 0) {
            $specialization_fet = mysqli_fetch_assoc($specialization_query);
            $specialization_Id = $specialization_fet['Name'];
        }else{
            $specialization_Id = ' ';
        }
        $state_query = $conn->query("SELECT * FROM States WHERE ID = '".$row['State_ID']."'");
        if($state_query->num_rows > 0) {
            $state_fet = mysqli_fetch_assoc($state_query);
            $state_Id = $state_fet['Name'];
        }else{
            $state_Id = ' ';
        }
        $city_query = $conn->query("SELECT * FROM Cities WHERE ID = '".$row['City_ID']."'");
        if($city_query->num_rows > 0) {
            $city_fet = mysqli_fetch_assoc($city_query);
            $city_Id = $city_fet['Name'];
        }else{
            $city_Id = ' ';
        }
        $source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$row['Source_ID']."'");
        if($source_query->num_rows > 0) {
            $source_fet = mysqli_fetch_assoc($source_query);
            $source_Id = $source_fet['Name'];
        }else{
            $source_Id = ' ';
        }
        $subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$row['Subsource_ID']."'");
        if($subsource_query->num_rows > 0) {
            $subsource_fet = mysqli_fetch_assoc($subsource_query);
            $subsource_Id = $subsource_fet['Name'];
        }else{
            $subsource_Id = ' ';
        }
        $employee_Id = $row['Counsellor_ID'];
        $manager = $conn->query("SELECT Reporting_To_User_ID from users WHERE ID ='".$row['Counsellor_ID']."'");
        $manager_name = mysqli_fetch_row($manager);
        $manager_name = $conn->query("SELECT Name from users WHERE ID ='".$manager_name[0]."'");
        $manager_name = mysqli_fetch_row($manager_name);


        
        $created_date = $row['Created_at'];
        $updated_date = $row['TimeStamp'];

        $followup_sql  = $conn->query("SELECT * FROM Follow_Ups WHERE Lead_ID='".$row['ID']."' ORDER BY ID DESC LIMIT 1");
        if($followup_sql->num_rows > 0)
        {
            $followup_get = mysqli_fetch_assoc($followup_sql);
            $follow_up_remark = $followup_get['Remark'];
        }
        else {
            $follow_up_remark = '';
        }

        $lineData = array($row['Name'], $row['Email'], $row['Mobile'], $row['Alt_Mobile'], $stage_Id, $reason_Id, $row['Remarks'], $institute_Id, $course_Id, $specialization_Id, $state_Id, $city_Id, $source_Id, $subsource_Id, $employee_Id,$manager_name[0], $updated_date, $created_date, $follow_up_remark);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>