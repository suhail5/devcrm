<?php 
require '../filestobeincluded/db_config.php';
$selected_leads = $_POST['data_id'];

$selected_leads = str_replace("%5B", "", $selected_leads);
$selected_leads = str_replace("%5D", "", $selected_leads);
$selected_leads = str_replace("id=", "", $selected_leads);
$selected_leads = str_replace("&", ",", $selected_leads);

?>

<form method="POST">
<center><button class="btn btn-danger textS-center" type="button" onclick="deleteSelectedLeads()">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
</form>
<script>
    function deleteSelectedLeads() {    
        var all_selected_leads = '<?php echo $selected_leads ?>';
        $.ajax
            ({
                type: "POST",
                url: "ajax_leads/ajax_delete_selected.php",
                data: {"all_selected_leads": all_selected_leads },
                success: function (data) {
                console.log(data);
                if(data.match("true")) {
                    toastr.success('Selected leads deleted successfully');
                    location.reload();
                }
                else {
                    toastr.error('Unable to delete leads');
                }
                }
            });
        }
</script>


<?php
exit;
?>