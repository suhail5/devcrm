<?php 

if(session_status() === PHP_SESSION_NONE) session_start();
require '../filestobeincluded/db_config.php';
$selected_leads = $_POST['data_id'];

$selected_leads = str_replace("%5B", "", $selected_leads);
$selected_leads = str_replace("%5D", "", $selected_leads);
$selected_leads = str_replace("id=", "", $selected_leads);
$selected_leads = str_replace("&", ",", $selected_leads);
?>
<form method="POST">
<div class="form-group row">
    <div class="col-lg-12">
        <select data- plugin="customselect" class="form-control" id="email_templates">
            <option disabled selected>Choose Template</option>
            <?php
                $result_email_template = $conn->query("SELECT * FROM Email_Templates WHERE Institute_ID = '".$_SESSION['INSTITUTE_ID']."'");
                while($email_temp = $result_email_template->fetch_assoc()) {
            ?>
                <option value="<?php echo $email_temp['id']; ?>"><?php echo $email_temp['template_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12">
        <div id="mailshow" style="border: 1px solid #e6e6e6; border-radius:5px;">
            <div class="controls">
                <textarea class="summernote input-block-level" id="email_textareas" required>
                        
                </textarea>
            </div>
        </div>
    </div>
</div>
    

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" id="mail_btn" onclick="sendMailSelected();" class="btn btn-primary">&nbsp;&nbsp;Send&nbsp;&nbsp;</button>
</div>
</form>

<script>
function sendMailSelected() {
    
    var mail_id = $('#email_templates').val();
    var leadid = '<?php echo $selected_leads ?>';
    var url = window.location.pathname;
    $.ajax
        ({
            type: "POST",
            url: "Mailer/ajax_send_mail_selected.php",
            beforeSend: disableSendButton,
            complete: enableSendButton,
            data: { "mail_id" :mail_id, "leadid" :leadid },
            success: function (data) {
            if(data.match("true")) {
                $('.modal').modal('hide');
                $("#divShowHide1").css({display: "none"});
                $("#divShowHide2").css({display: "none"});
                $("#divShowHide3").css({display: "none"});
                $("#divShowHide4").css({display: "none"});
                toastr.success('Mails send successfully');
                $('#checkbox-form').find('input[name="id[]"]:checked').each(function () {
                    var id = $(this).val();
                    if(url=='/teamlead/myfollowup'){
                        $.ajax({
                            url:'/ajax_leads/fetch_followup_lead_data.php',
                            data:{id:id},
                            type:'POST',
                            success:function(data){
                                $('#row'+id).html(data);
                            }
                        })
                    }else{
                        $.ajax({
                            url:'/ajax_leads/fetch_lead_data.php',
                            data:{id:id},
                            type:'POST',
                            success:function(data){
                                $('#row'+id).html(data);
                            }
                        })        
                    }
                });

                $(".checkbox-function").prop('checked', false);

            }
            else {
                toastr.error('Unable to send Mails');
            }
            }
        });
    
}
</script>

<script>
    function enableSendButton() {
        $("#mail_btn").prop("disabled", false);
        $('#mail_btn').css('cursor', 'pointer');
        $('#mail_btn').text('Send');
    }

    function disableSendButton() {
        $("#mail_btn").prop("disabled", true);
        $('#mail_btn').css('cursor', 'no-drop');
        $('#mail_btn').text('Sending...');
    }
</script>


<script>
    $(document).ready(function() {
        $('#email_templates').change(function() {

            $('#email_textareas').summernote('pasteHTML', "<b>Select Template</b>");
            var template_id = $('#email_templates').val();

            $.ajax
            ({
                type: "POST",
                url: "ajax_leads/ajax_email.php",
                data: { "email_template_id": template_id },
                success: function(data) {
                    if(data != "") {
                        $('#email_textareas').summernote('code', data);
                    }
                    else {
                        $('#email_textareas').summernote('pasteHTML', "<b>Select Template</b>");
                    }
                }
            });
        });
    });
</script>
<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 330,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
    });
</script>
<?php
exit;
?>