<?php 
if(session_status() === PHP_SESSION_NONE) session_start();
require '../filestobeincluded/db_config.php';

$userid = $_POST['userid'];
 
$sql = "select * from Leads where ID=".$userid;
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);
?>
<form method="POST">
    <div class="row col-lg-12">
        <p>Refer Lead <b><?php echo $row['Name'];?></b> to</p>
    </div><br>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label">Stage<span style="color: #F64744; font-size: 12px">*</span></label>
            <div class="col-lg-10">
                <select data-plugin="customselect" class="form-control" id="follow_stage_select" name="follow_stage_select">
                    <option disabled selected>Select Stage</option>
                        <?php
                        if($_SESSION['User_Type'] != 'b2b'){
                            $result_stage = $conn->query("SELECT * FROM Stages WHERE User_Type is NULL AND Status = 'Y'");
                        }else{
                            $result_stage = $conn->query("SELECT * FROM Stages WHERE User_Type = 'b2b' AND Status = 'Y' or ID = '1'");
                        }
                        while($stages = $result_stage->fetch_assoc()) {
                    ?>
                        <option value="<?php echo $stages['ID']; ?>"><?php echo $stages['Name']; ?></option>
                    <?php } ?>
                </select>
            </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label">Reason<span style="color: #F64744; font-size: 12px">*</span></label>
        <div class="col-lg-10">
            <select data-plugin="customselect" class="form-control" id="follow_reason" name="follow_reason">
                <option disabled selected>Select Reason</option>
            </select>
        </div>
    </div>
<div class="form-group row">
    <input type="hidden" value="<?php echo $row['ID']; ?>" id="lead_id">
    <input type="hidden" value="<?php echo $row['Counsellor_ID']; ?>" id="lead_owner">
    <label class="col-lg-2 col-form-label">Select Lead Owner</label>
    <div class="col-lg-10">
        <select data-plugin="customselect" class="form-control" id="refer_lead_owner">
            <option disabled selected>Choose</option>
            <?php
                $result_refer_lead = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '".$_SESSION['useremployeeid']."' AND Status='Y' ");
                while($refer_leads = $result_refer_lead->fetch_assoc()) {
            ?>
                <option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Add Comment</label>
    <div class="col-lg-10">
        <input type="text" class="form-control" id="referal_comment"
            placeholder="">
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="updateOwner();">&nbsp;&nbsp;Refer&nbsp;&nbsp;</button>
</div>
</form>

<script>
	$(document).ready(function() {
		$('#follow_stage_select').change(function() {

			$('#follow_reason').html("<option disabled selected>Select Reason</option>");

			var new_selected_stage = $('#follow_stage_select').val();
			$.ajax
			({
				type: "POST",
				url: "../onselect/onSelect.php",
				data: { "stage_select": "", "selected_stage": new_selected_stage },
				success: function(data) {
					if(data != "") {
						$('#follow_reason').html(data);
					}
					else {
						$('#follow_reason').html("<option disabled selected>Select Reason</option>");
					}
				}
			});
		});
	});
</script>

<?php
exit;
?>