<?php
if(session_status() === PHP_SESSION_NONE) session_start();
if(isset($_POST['id'])){
	$lead_id = $_POST['id'];
	require '../filestobeincluded/db_config.php';
}
$all_array = array();
$coun_array = array();
$get_tree = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '". $_SESSION['useremployeeid'] ."'");
while($row = $get_tree->fetch_assoc()) {
	$all_array[] = $row;
}
foreach($all_array as $univ){
	$university = $univ['Institute_ID'];
	$get_counsellors = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID = '". $_SESSION['useremployeeid'] ."' AND Role = 'Counsellor'");
	while($rows = $get_counsellors->fetch_assoc()) {
		$coun_array[] = $rows;
	}
}
foreach($coun_array as $cuniv){
	$couns_array[]=$cuniv['ID'];                                                     
}
// $imp = "'" . implode( "','", ($couns_array) ) . "'";
// $tree_ids = $imp.",'".$_SESSION['USERS_ID']."'";

if($get_counsellors->num_rows>0){
	$imp = "'" . implode( "','", ($couns_array) ) . "'";
	$tree_ids = $imp.",'".$_SESSION['USERS_ID']."'";
}else{
	$tree_ids = "'".$_SESSION['USERS_ID']."'";
}

$result = $conn->query("SELECT * FROM Leads WHERE ID = '$lead_id'");
while($lead = $result->fetch_assoc())
 {
 
  echo '
		<div class="row" style="padding-top: 10px;" id="row'.$lead['ID'].'">
		<div class="col-lg-1">
		<div class="custom-control custom-checkbox">
		<input type="checkbox" onclick="checkbox()" class="custom-control-input checkbox-function" name="id[]" id="customCheck2'. $lead['ID'] .'" value="'. $lead['ID'] .'">
		<label class="custom-control-label" for="customCheck2'. $lead['ID'] .'"></label>
		</div>
	</div>
	<div class="col-lg-2">
		<div class="row">
			<div class="col-lg-12 col-md-3 col-sm-12">
				<p id="person_name'. $lead['ID'] .'" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy name"><b>Name:</b> '. $lead['Name'] .'</p>
			</div>
			<div class="col-lg-12 col-md-3 col-sm-12">
				<p id="person_email'. $lead['ID'] .'" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy email"><b>Email:</b> '. $lead['Email'].'</p>
			</div>
			<div class="col-lg-12 col-md-3 col-sm-12">
				<p id="person_mobile'. $lead['ID'] .'" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy mobile"><b>Mobile:</b> <a href="tel:'. $lead['Mobile'] .'">'. $lead['Mobile'] .'</a></p>
			</div>
			<div class="col-lg-12 col-md-3 col-sm-12">';?>
					<?php 
					$get_followup_remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' ORDER BY ID DESC LIMIT 1");
					if($get_followup_remark->num_rows > 0){
						$re_mark = mysqli_fetch_assoc($get_followup_remark);
						if(strlen($re_mark['Remark'])>40){
							echo '<p><b>Remark:</b>&nbsp;' . substr($re_mark['Remark'],0,40).'...<button type="button" onclick="pop();" class="btn btn-link btn-sm" data-container="body" title=""
							data-toggle="popover" data-placement="left"
							data-content= "'.$re_mark['Remark'].'"
							data-original-title="Remark">
							Read More
						</button>';
						}else{
							echo '<p><b>Remark:</b>&nbsp;' . $re_mark['Remark'];
						}
						
					}else{
						echo '';
					}

				?>
				
				
				
				<?php echo '</p>
</div>
<div class="col-lg-12 col-md-3 col-sm-12">'; ?>
<?php 
if(strlen($lead['Remarks'])>0){


if(strlen($lead['Remarks'])>40){
echo '<p><b>Lead Remark:</b>&nbsp;' . substr($lead['Remarks'],0,40).'...<button type="button" onclick="pop();" class="btn btn-link btn-sm" data-container="body" title=""
data-toggle="popover" data-placement="left"
data-content= "'.$lead['Remarks'].'"
data-original-title="Lead Remark">
Read More
</button>';
}else{
echo '<p><b>Lead Remark:</b>&nbsp;' . $lead['Remarks'];
}
}else{
echo '';
} 


?>
<?php echo '</p>
</div>
		</div>
	</div>
	<div class="col-lg-2">
		<div class="row">
			<div class="col-lg-12 col-md-3 col-sm-12">
				<ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu mb-0">
					<li class="nav-link" role="button" aria-haspopup="false" aria-expanded="false">';
					 $fsql = "SELECT COUNT(Lead_ID) as Leadid FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' AND Counsellor_ID in ($tree_ids) GROUP BY Lead_ID"; $fresult = $conn->query($fsql); if ($fresult->num_rows > 0) { while($frow = $fresult->fetch_assoc()) { $gfc = $frow["Leadid"]; }} else { $gfc = "0";} 
						echo '<font style="font-size: 24px; cursor:pointer;" onclick="followupmodal('. $lead['ID'] .');"><i class="fas fa-user-tie"></i></font>
						<span><mark class="mark1">&nbsp;'. $gfc .'&nbsp;</mark></span>
					</a></li>
					<li class="nav-link" role="button" aria-haspopup="false"
						aria-expanded="false">';
						 $elsql = "SELECT COUNT(Lead_ID) as Leadid FROM Email_Logs WHERE Lead_ID = '".$lead['ID']."' AND Employee_ID in ($tree_ids) GROUP BY Lead_ID"; $elresult = $conn->query($elsql); if ($elresult->num_rows > 0) { while($elrow = $elresult->fetch_assoc()) { $gelc = $elrow["Leadid"]; }} else { $gelc = "0";} 
							$slsql = "SELECT COUNT(Lead_ID) as Leadid FROM SMS_Logs WHERE Lead_ID = '".$lead['ID']."' GROUP BY Lead_ID"; $slresult = $conn->query($slsql); if ($slresult->num_rows > 0) { while($slrow = $slresult->fetch_assoc()) { $gslc = $slrow["Leadid"]; }} else { $gslc = "0";} 
							$add_both = $gelc + $gslc;
						echo '<font style="font-size: 24px; cursor:pointer;" onclick="responsesmodal('. $lead['ID'] .');"><i class="fas fa-user-graduate"></i></font>
						<span><mark class="mark2">&nbsp;'. $add_both .'&nbsp;</mark></span>
					</a></li>
					<li class="nav-link" role="button" aria-haspopup="false"
						aria-expanded="false">';
						 $rsql = "SELECT COUNT(ID) as Leadid FROM Re_Enquired WHERE Name = '".$lead['Name']."' AND Email = '".$lead['Email']."' AND Mobile = '".$lead['Mobile']."' AND Institute_ID = '".$lead['Institute_ID']."' AND Counsellor_ID in ($tree_ids)"; $rresult = $conn->query($rsql); if ($rresult->num_rows > 0) { while($rrow = $rresult->fetch_assoc()) { $grc = $rrow["Leadid"]; }} else { $grc = "0";} 
						echo '<font style="font-size: 24px; cursor:pointer;"><i onclick="re_enquiredmodal('. $lead['ID'] .');" class="fas fa-user-tie"></i></font>
						<span><mark class="mark3">&nbsp;'. $grc .'&nbsp;</mark></span>
					</a></li>
				</ul>
			</div>
			<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">';
				
					$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$lead['Counsellor_ID']."'");
					$counsellor = mysqli_fetch_assoc($counsellor_query);
					if($counsellor_query->num_rows > 0){
						$couns = $counsellor['Name'];
					}else{
						$counsellor['Name'] = ' ';
						$couns = $counsellor['Name'];
					}
			
				echo '<p><b>Counsellor:</b> '. $couns .'</p>
			</div>

			<div class="col-lg-12 col-md-3 col-sm-12">';
				
					$counsellor_query = $conn->query("SELECT Previous_Owner_ID FROM Leads WHERE ID = '".$lead['ID']."' AND Previous_Owner_ID != '".$lead['Counsellor_ID']."'");
					$counsellor = mysqli_fetch_assoc($counsellor_query);
					if($counsellor_query->num_rows > 0){
						$get_counsellor_name = $conn->query("SELECT Name FROM users WHERE ID = '".$counsellor['Previous_Owner_ID']."' AND Reporting_To_User_ID = '".$_SESSION['useremployeeid']."'");
						$couns_name = mysqli_fetch_assoc($get_counsellor_name);
						if($get_counsellor_name->num_rows > 0){
							echo '<p><b>Previous Owner:</b>&nbsp; '.$couns_name['Name'].' </p>';
						}else{
							echo '';
						}
					}	
					
			
				echo '
			</div>';
			if($lead['Alt_Mobile']!=''){
				echo '<div class="col-lg-12 col-md-3 col-sm-12">
				<p><b>Alternate Mobile:</b> <a href="tel:'. $lead['Alt_Mobile'] .'">'. $lead['Alt_Mobile'] .'</a></p>
				</div>';
			}
			echo '
		</div>
	</div>
	<div class="col-lg-3">
		<div class="row">
			<div class="col-lg-12 col-md-3 col-sm-12">
				<p><b>University:</b>';
						$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$lead['Institute_ID']."'");
						$univ = mysqli_fetch_assoc($univ_query);
						if($univ_query->num_rows > 0){
							if(strcasecmp($univ['Name'], 'Admin')==0){
								$univ['Name'] = '';
								echo $univ['Name'];
							}else{
								echo $univ['Name'];
							}
						}else{
							$univ['Name'] = ' ';
							echo $univ['Name'];
						}
					
				echo '</p>
			</div>
			<div class="col-lg-12 col-md-3 col-sm-12">
				<p><b>Course:</b>';
						$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$lead['Course_ID']."'");
						$course = mysqli_fetch_assoc($course_query);
						if($course_query->num_rows > 0){
							echo $course['Name'];
						}else{
							$course['Name'] = ' ';
							echo $course['Name'];
						}
				
				echo '</p>
			</div>
			<div class="col-lg-12 col-md-3 col-sm-12">
				<p><b>Specialization:</b>';
						$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$lead['Specialization_ID']."'");
						$specialization = mysqli_fetch_assoc($specialization_query);
						if($specialization_query->num_rows > 0){
							echo substr($specialization['Name'],0,27);
						}else{
							$specialization['Name'] = ' ';
							echo $specialization['Name'];
						}

						if($univ['ID']==64){
							echo '<div class="col-lg-12 col-md-3 col-sm-12">
							<p><b>SVU ID:</b>'; 
							  $suv_id_query = $conn->query("SELECT urnno from Leads where ID ='".$lead['ID']."'");
							  $suv = mysqli_fetch_assoc($suv_id_query);
							  if($suv['urnno']){
								echo $suv['urnno'];
							  }
							  else{
								echo '<button type="button" onclick="callSVUApi(&#39;'.$lead['ID'].'&#39;,&#39;'.$lead['dob'].'&#39;,&#39;'.$lead['Specialization_ID'].'&#39;)" class="btn btn-sm btn-outline-primary mx-1">Generate SVU ID</button>';
							  }
							echo '</p>
							</div>';
						  }else if($univ['ID']==51 || $univ['ID']==60){
							echo '<div class="col-lg-12 col-md-3 col-sm-12" id="idolid'.$lead['ID'].'">
							<p><b>IDOL ID:</b>'; 
							  $suv_id_query = $conn->query("SELECT urnno from Leads where ID ='".$lead['ID']."'");
							  $suv = mysqli_fetch_assoc($suv_id_query);
							  if($suv['urnno']){
								echo $suv['urnno'];
							  }
							  else{
								echo '<button type="button" onclick="callCuApi(&#39;'.$lead['ID'].'&#39;);" class="btn btn-sm btn-outline-primary mx-1">Generate IDOL ID</button>';
							  }
							echo '</p>
							</div>';
						  }   
				
				echo '</p>
			</div>
			<div class="col-lg-12 col-md-3 col-sm-12">
				';?>
				<?php 
					$get_followup_date = $conn->query("SELECT Followup_Timestamp FROM Follow_Ups WHERE Lead_ID = '".$lead['ID']."' ORDER BY ID DESC LIMIT 1");
					if($get_followup_date->num_rows > 0){
						$current_timestamp = date('Y-m-d h:i:s');
						$date = mysqli_fetch_assoc($get_followup_date);
						if($current_timestamp < $date['Followup_Timestamp']){
							echo '<p><b>Next Follow-up Date:</b>&nbsp;'. date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
						}else{
							echo '<p><b>Previous Follow-up Date:</b>&nbsp;'. date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
						}
						
					}else{
						echo '';
					}

				?>
				
				<?php echo '</p>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="row">
			<div class="col-lg-12 col-md-3 col-sm-12">';
					$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$lead['Stage_ID']."'");
					$stage = mysqli_fetch_assoc($stage_query);
					if($stage_query->num_rows > 0){
						$lead_stage = $stage['Name'];
					}else{
						$stage['Name'] = ' ';
						$lead_stage = $stage['Name'];
					}
				
				echo '<p><b>Stage:</b> '. $lead_stage .'</p>
			</div>
			<div class="col-lg-12 col-md-3 col-sm-12">';
					$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$lead['Reason_ID']."'");
					$reason = mysqli_fetch_assoc($reason_query);
					if(strcasecmp($stage['Name'], "NEW")==0 || strcasecmp($stage['Name'], "FRESH")==0) {
						$badge = "success";
					}
					else if(strcasecmp($stage['Name'], "COLD")==0) {
						$badge = "warning";
					}
					else {
						$badge = "danger";
					}
				
				echo '<p><b>Reason:</b> <span class="badge badge-soft-'. $badge .' py-1">';
						if($reason_query->num_rows > 0){
							echo $reason['Name'];
						}else{
							$reason['Name'] = ' ';
							echo $reason['Name'];
						}
					echo '</span>
				</p>
			</div>
<div class="col-lg-12 col-md-3 col-sm-12">';
$state_query = $conn->query("SELECT * FROM States WHERE ID = '".$lead['State_ID']."'");
$state = mysqli_fetch_assoc($state_query);
if($state_query->num_rows > 0){
$lead_state = $state['Name'];
}else{
$state['Name'] = ' ';
$lead_state = $state['Name'];
}
echo '<p><b>State:</b>'. $lead_state .'</p>
</div>';

if($lead['Stage_ID'] == 8)
{
echo '<div class="col-lg-12 col-md-3 col-sm-12">';

$source_query = $conn->query("SELECT * FROM Sources WHERE ID = '".$lead['Source_ID']."'");
$source = mysqli_fetch_assoc($source_query);
if($source_query->num_rows > 0){
$lead_source = $source['Name'];
}else{
$source['Name'] = ' ';
$lead_source = $source['Name'];
}

echo '<p><b>Source:</b> '. $lead_source .'</p>
</div>';
}
			echo '<div class="col-lg-12 col-md-3 col-sm-12">';
					$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '".$lead['Subsource_ID']."'");
					$subsource = mysqli_fetch_assoc($subsource_query);
					if($subsource_query->num_rows > 0){
						$lead_sub = $subsource['Name'];
					}else{
						$subsource['Name'] = ' ';
						$lead_sub = $subsource['Name'];
					}
				
				echo '<p><b>Sub-Source:</b> '. $lead_sub .'</p>
</div>';
if($lead['Stage_ID'] == 8)
{
$renquired_date = $conn->query("SELECT Sources.Name,Re_Enquired.TimeStamp FROM Re_Enquired LEFT JOIN Sources ON Re_Enquired.Source_ID = Sources.ID  WHERE Re_Enquired.Lead_ID = '".$lead['ID']."' ORDER BY Re_Enquired.TimeStamp DESC");
$renquired = mysqli_fetch_assoc($renquired_date);
echo '<div class="col-lg-12 col-md-3 col-sm-12">
<p><b>Renquired Date:</b>';?>
<?php 


//echo $renquired["TimeStamp"];
if(isset($renquired["TimeStamp"])!='') {
echo date("F j, Y g:i a", strtotime($renquired["TimeStamp"]));
}
else {
echo '';
}

?>

<?php echo '</p>
</div><div class="col-lg-12 col-md-3 col-sm-12">
<p><b>Renquired Source:</b>';?>
<?php 


//echo $renquired["TimeStamp"];
if(isset($renquired["Name"])!='') {
echo $renquired["Name"];
}
else {
echo '';
}

?>

<?php echo '</p>
</div>';
}
			echo '<div class="col-lg-12 col-md-3 col-sm-12">
				<p><b>Last Update Date:</b>';?>
				<?php 
					
						echo date("F j, Y g:i a", strtotime($lead["TimeStamp"]));
					

				?>
				
				<?php echo '</p>
			</div>
		</div>
	</div>
	<div class="col-lg-1">
		<div class="row">
			<div class="col-lg-12 col-md-3 col-sm-12">
				<div class="btn-group">
					<span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" onclick="whatsapp('. $lead['ID'] .')" style="cursor: pointer;" aria-hidden="true"></i></p></span>&nbsp;&nbsp;
					
					<a href="tel:'. $lead['Mobile'] .'"><span data-toggle="tooltip" data-placement="top" data-original-title="Call" title=""><p style="font-size: 20px;"><i class="fa fa-phone" data-toggle="modal" style="cursor: pointer;" data-target="#leadcallmodal" aria-hidden="true"></i></p></span></a>&nbsp;&nbsp;
					<span data-toggle="dropdown"><p style="font-size: 20px;"><i class="fa fa-ellipsis-v re_hide" style="cursor: pointer;" aria-hidden="true"></i></p></span>
					<div class="dropdown-menu dropdown-menu-right">
						<span class="dropdown-item"><i class="fas fa-notes-medical" style="font-size: 16px; color: #6C757D;"></i> <font class="addfollowupmodal" onclick="addfollowupmodal('. $lead['ID'] .')"  style="cursor: pointer;">Add Followup</font></span>
						<span class="dropdown-item"><i class="fas fa-user-edit" style="font-size: 16px; color: #6C757D;"></i> <font onclick="editlead('. $lead['ID'] .')" class="editlead" style="cursor: pointer;">Edit Lead</font></span>
						<span class="dropdown-item"><i class="fas fa-history" style="font-size: 16px; color: #6C757D;"></i> <font onClick="leadhistory('. $lead['ID'] .')" class="leadhistory" style="cursor: pointer;">View History</font></span>
						<span class="dropdown-item"><i class="fas fa-share-alt" style="font-size: 16px; color: #6C757D;"></i> <font class="referlead" onclick="referlead('. $lead['ID'] .')" style="cursor: pointer;">Refer Lead</font></span>
						
					</div>
				</div>
			</div>
		</div>
		
		
	</div>

		</div>
	';
}
?>