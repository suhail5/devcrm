<?php
    require '../filestobeincluded/db_config.php';

    $emailid = $_POST['emailid'];


    $all_institutes = array();
    $institute_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> 0");
    while ($row = $institute_query_res->fetch_assoc()) {
        $all_institutes[] = $row;
    }
    $all_emails = array();
    $email_query_res = $conn->query("SELECT * FROM Email_Templates WHERE id = $emailid LIMIT 1");
    while ($row = $email_query_res->fetch_assoc()) {
        $all_emails[] = $row;
    }
    foreach($all_emails as $em){
        $ID = $em['id'];
        $emails_temp_name = $em['template_name'];
        $emails_temp_subject = $em['subject'];
        $emails_temp_content = $em['email_template'];
        $email_temp_institute = $em['Institute_ID'];
    }
?>

<form method="POST" action="">
    <input type="hidden" value="<?php echo $ID ?>" id="temp_email_id<?php echo $ID ?>" required>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label"
            for="temp_name_email">Template Name</label>
        <div class="col-lg-4">
            <input type="text" class="form-control" id="temp_name_email<?php echo $ID ?>"
                placeholder="Template Name" value="<?php echo $emails_temp_name ?>" required>
        </div>
        <div class="col-lg-1"></div>
        <label class="col-lg-1 col-form-label"
            for="temp_subject_email">Subject</label>
        <div class="col-lg-4">
            <input type="text" class="form-control" id="temp_subject_email<?php echo $ID ?>"
                placeholder="Subject" value="<?php echo $emails_temp_subject ?>" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label">Institute</label>
        <div class="col-lg-10">
            <select class="form-control custom-select" id="email_institute_id<?php echo $ID ?>">
            <option selected value="<?php echo $email_temp_institute ?>"><?
            $get_institute_name = $conn->query("SELECT * FROM Institutes WHERE ID = '$email_temp_institute' LIMIT 1");
            $institute_name = mysqli_fetch_assoc($get_institute_name);
            if($get_institute_name->num_rows > 0){
                echo $institute_name['Name'];
            }else{
                $institute_name['Name']= 'Choose Institute';
                echo $institute_name['Name'];
            }
            ?></option>
            <?php
                $result_institute = $conn->query("SELECT * FROM Institutes WHERE Status = 'Y' AND ID <> '0'");
                while($institutes = $result_institute->fetch_assoc()) {
            ?>
                <option value="<?php echo $institutes['ID']; ?>"><?php echo $institutes['Name']; ?></option>
            <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-2 col-form-label">Choose Variable</label>
        <div class="col-lg-10">
            <select class="form-control custom-select" id="choose_var_emails<?php echo $ID ?>">
                <option value="">Choose</option>
                <option value="$lead_name">Lead Name</option>
                <option value="$counsellor_name">Counsellor Name</option>
                <option value="$counsellor_contact_no">Counsellor Contact No</option>
                <option value="$counsellor_email">Counsellor Email</option>
            </select>
        </div>
    </div>
    <br><br>
    <div id="emailEditor" style="border: 1px solid #e6e6e6; border-radius:5px;">
        <div class="controls">
            <textarea class="summernote input-block-level" id="email_contents_update<?php echo $ID ?>" required maxlength="500000">
                <?php echo $emails_temp_content ?>
            </textarea>
        </div>
    </div>
    <br><br>
    
    <button class="btn btn-primary float-right" type="button" onclick="updateemailtemp(<?php echo $ID ?>)">Update</button>
    
</form>
<script>
    $(document).ready(function() {
        $("#choose_var_emails<?php echo $ID ?>").change(function(){
            var choose_variable = $('#choose_var_emails<?php echo $ID ?> option:selected').val();
            $('#email_contents_update<?php echo $ID ?>').summernote('editor.saveRange');
            $('#email_contents_update<?php echo $ID ?>').summernote('editor.focus');
            $('#email_contents_update<?php echo $ID ?>').summernote('editor.insertText', choose_variable);
        })
    });
</script>


<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 330,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,                 // set focus to editable area after initializing summernote
            callbacks: {
                onImageUpload: function(image) {
                    uploadImage(image[0]);
                }
            }
        });
    });
</script>

<script type="text/javascript">
    function uploadImage(image) {
        var data = new FormData();
        data.append("image", image);
        $.ajax({
                url: 'ajax_leads/upload_email_image.php',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "post",
                success: function(url) {
                    var image = $('<img>').attr('src', url);
                    $('#email_contents_update<?php echo $ID ?>').summernote("insertNode", image[0]);
                },
                error: function(data) {
                    console.log(data);
                }
         });
}
</script>