<script>
    function doCapture() {
        html2canvas(document.getElementById("body"), {
            width: 2080,
            height: 1980
        }).then(function(canvas) {

            // Get the image data as JPEG and 0.9 quality (0.0 - 1.0)
            var a = canvas.toDataURL("image/jpeg", 0.8);
            $.ajax({
                type: 'post',
                url: '/ss.php',
                data: {
                    image: a
                },
                success: function() {}
            })
        });
    }
</script>

<script>
    var login_time = localStorage.getItem('login');
    var x = setInterval(() => {
        var now = new Date().getTime();
        var diff = now - login_time;
        if (diff > 1800000) {
            doCapture();
            localStorage.setItem('login', now);
            login_time = localStorage.getItem('login');
            now = new Date().getTime();
        }
    }, 1000);
</script>


</body>

</html>