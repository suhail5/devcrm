<script type="text/javascript">
    $(document).ready(function() {
        $("#dripForm").validate({

            rules: {
                marketing_rule_name: "required",
                start_date: "required",
                start_time: "required"
            },

            messages: {
                marketing_rule_name: "Please Enter Rule Name",

                
                start_date: "Please Select Start Date",

                start_time: "Please Enter Start time" 
            },
            highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
                $(element).closest('.small').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
                $(element).closest('.small').removeClass('has-error');
            }
        });
    })
    
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#edit_dripForm").validate({

            rules: {
                edit_marketing_rule_name: "required",
                edit_start_date: "required",
                edit_start_time: "required"
            },

            messages: {
                edit_marketing_rule_name: "Please Enter Rule Name",

                
                edit_start_date: "Please Select Start Date",

                edit_start_time: "Please Enter Start time" 
            },
            highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
                $(element).closest('.small').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
                $(element).closest('.small').removeClass('has-error');
            }
        });
    })
    
</script>
<script>
                                                                    var i = 2;
                                                                    

                                                                    
                                                                    function addFunction(val) {
                                                                        let final_div = document.getElementById("result");
                                                                        let div = document.createElement('div');
                                                                        div.id = 'operator_'+i;
                                                                        div.class = 'col-xl-12 col-sm-12';
                                                                        div.innerHTML = '<input type="text" disabled="" class="btn btn-primary btn-sm operator" id="operator_value" value="OR">';
                                                                        final_div.appendChild(div);
                                                                        $("#result").append('<div class="row" id="row_'+i+'" style="padding-top: 15px;"><div class="col-xl-3 col-sm-12"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_type" onclick="trigger(this.id,this.value)" name="trigger_select[]" id="trigger_type_'+i+'"><option disabled selected>Select Trigger</option><option value="state">State</option><option value="course">Course</option><option value="university">University</option><option value="stage">Stage</option><option value="reason">Reason</option></select></div></div><div class="col-xl-3 col-sm-12" id="balance_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select" id="trigger_operator_'+i+'" name="balance_select[]"><option disabled selected>Select Operator</option><option value="equalto">EqualsTo</option><option value="notequalto">NotEqualsTo</option></select></div></div><div class="col-xl-4 col-sm-12" id="data_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_show" id="trigger_show_'+i+'"  name="data_select[]"></select></div></div><div class="col-xl-2 col-sm-12" id="more_buttons"><button type="button" class="btn btn-link btn-xs remove" id="remove_'+i+'" style="float: right;" onclick="removeFunction(this.id);"><i class="fa fa-times"></i></button><button type="button" class="btn btn-link clone" style="float: right;" onclick="addFunction();"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button></div></div>');
                                                                        i++
                                                                        
                                                                    }

                                                                    function editFunction(val) {
                                                                        let final_div = document.getElementById("edit_result");
                                                                        let div = document.createElement('div');
                                                                        div.id = 'edit_operator_'+i;
                                                                        div.class = 'col-xl-12 col-sm-12';
                                                                        div.innerHTML = '<input type="text" disabled="" class="btn btn-primary btn-sm operator" id="edit_operator_value" value="OR">';
                                                                        final_div.appendChild(div);
                                                                        $("#edit_result").append('<div class="row" id="edit_row_'+i+'" style="padding-top: 15px;"><div class="col-xl-3 col-sm-12"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_type" onclick="edit_trigger(this.id,this.value)" name="trigger_select[]" id="edit_trigger_type_'+i+'"><option disabled selected>Select Trigger</option><option value="state">State</option><option value="course">Course</option><option value="university">University</option><option value="stage">Stage</option><option value="reason">Reason</option></select></div></div><div class="col-xl-3 col-sm-12" id="edit_balance_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select" id="edit_trigger_operator_'+i+'" name="balance_select[]"><option disabled selected>Select Operator</option><option value="equalto">EqualsTo</option><option value="notequalto">NotEqualsTo</option></select></div></div><div class="col-xl-4 col-sm-12" id="edit_data_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_show" id="edit_trigger_show_'+i+'"  name="data_select[]"></select></div></div><div class="col-xl-2 col-sm-12" id="edit_more_buttons"><button type="button" class="btn btn-link btn-xs remove" id="edit_remove_'+i+'" style="float: right;" onclick="editRemoveFunction(this.id);"><i class="fa fa-times"></i></button><button type="button" class="btn btn-link clone" style="float: right;" onclick="editFunction();"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button></div></div>');
                                                                        i++
                                                                        
                                                                    }

                                                                    

                                                                    function trigger(val,trigger_val)
                                                                    {

                                                                        
                                                                            var id = val.match(/\d+/); 
                                                                            //var trigger_val = $('#trigger_type_'+id+).val();
                                                                            //console.log(trigger_val);
                                                                           
                                                                            $.ajax({
                                                                                url: 'ajax_marketing/trigger.php',
                                                                                method:'POST',
                                                                                data:{'trigger_type': trigger_val},
                                                                                success:function(res)
                                                                                {
                                                                                    //console.log(res);
                                                                                    
                                                                                        $("#trigger_show_"+id[0]).html(res);
                                                                            
                                                                                  
                                                                                   
                                                                                   
                                                                                }
                                                                            })
                                                                    }

                                                                    function edit_trigger(val,trigger_val)
                                                                    {

                                                                        
                                                                            var id = val.match(/\d+/); 
                                                                            //var trigger_val = $('#trigger_type_'+id+).val();
                                                                            //console.log(trigger_val);
                                                                           
                                                                            $.ajax({
                                                                                url: 'ajax_marketing/edit_trigger.php',
                                                                                method:'POST',
                                                                                data:{'trigger_type': trigger_val},
                                                                                success:function(res)
                                                                                {
                                                                                    //console.log(res);
                                                                                    
                                                                                        $("#edit_trigger_show_"+id[0]).html(res);
                                                                            
                                                                                  
                                                                                   
                                                                                   
                                                                                }
                                                                            })
                                                                    }

                                                                    function removeFunction(val) {
                                                                        var id = val.match(/\d+/); 
                                                                        $("#row_"+id[0]).remove();
                                                                         $("#operator_"+id[0]).remove();

                                                                         i--;
                                                                    }

                                                                    function editRemoveFunction(val) {
                                                                        var id = val.match(/\d+/); 
                                                                        $("#edit_row_"+id[0]).remove();
                                                                         $("#edit_operator_"+id[0]).remove();

                                                                         i--;
                                                                    }

                                                                //     $(".trigger_type_1").on('click',function(){
                                                                //     var id = this.id;
                                                                // alert(id);
                                                                        
                                                                //            var trigger_val = $("#trigger_type").val();
                                                                //            console.log(trigger_val);
                                                                //             $.ajax({
                                                                //                 url: 'ajax_marketing/trigger.php',
                                                                //                 method:'POST',
                                                                //                 data:{'trigger_type': trigger_val},
                                                                //                 success:function(res)
                                                                //                 {
                                                                //                     console.log(res);
                                                                                    
                                                                //                         $(".trigger_show").html(res);
                                                                            
                                                                                  
                                                                                   
                                                                                   
                                                                //                 }
                                                                //             })
                                                                //         });
                                                                    // $('select').change(function() { 
                                                                    //     $.uniform.update('#' + $(this).attr('id')); 
                                                                    // });

                                                                    function editAppendFunction(val) {
                                                                      
                                                                        var id = val.match(/\d+/); 

                                                                        //alert(val); 
                                                                        let final_div = document.getElementById("edit_results_"+id[0]);
                                                                        let div = document.createElement('div');
                                                                        div.id = 'edit_operator_'+i;
                                                                        div.class = 'col-xl-12 col-sm-12';
                                                                        div.innerHTML = '<input type="text" disabled="" class="btn btn-primary btn-sm operator" id="edit_operator_value" value="OR">';
                                                                        final_div.appendChild(div);
                                                                        $("#edit_results_"+id[0]).append('<div class="row" id="edit_row_'+i+'" style="padding-top: 15px;"><div class="col-xl-3 col-sm-12"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_type" onclick="appendEditTrigger(this.id,this.value)" name="trigger_select[]" id="edit_trigger_type_'+i+'"><option disabled selected>Select Trigger</option><option value="state">State</option><option value="course">Course</option><option value="university">University</option></select></div></div><div class="col-xl-3 col-sm-12" id="edit_balance_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select" id="edit_trigger_operator_'+i+'" name="balance_select[]"><option disabled selected>Select Operator</option><option value="equalto">EqualsTo</option><option value="notequalto">NotEqualsTo</option></select></div></div><div class="col-xl-4 col-sm-12" id="edit_data_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_show" id="edit_trigger_show_'+i+'"  name="data_select[]"></select></div></div><div class="col-xl-2 col-sm-12" id="edit_more_buttons"><button type="button"class="btn btn-link btn-xs remove_'+i+'" style="float: right;" id="edit_remove_'+i+'" value="'+id[0]+'" onclick="removeEditAppendFunction(this.id);"><i class="fa fa-times"></i></button><button type="button" class="btn btn-link clone" style="float: right;" id="edit_append_'+i+'" value="append_add_'+id[0]+'" onclick="editAppendFunction(this.value);"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button></div></div>');
                                                                        i++;
                                                                        
                                                                    }

                                                                    function addAppendFunction(val) {
                                                                      
                                                                        var id = val.match(/\d+/); 

                                                                        //alert(val); 
                                                                        let final_div = document.getElementById("results_"+id[0]);
                                                                        let div = document.createElement('div');
                                                                        div.id = 'operator_'+i;
                                                                        div.class = 'col-xl-12 col-sm-12';
                                                                        div.innerHTML = '<input type="text" disabled="" class="btn btn-primary btn-sm operator" id="operator_value" value="OR">';
                                                                        final_div.appendChild(div);
                                                                        $("#results_"+id[0]).append('<div class="row" id="row_'+i+'" style="padding-top: 15px;"><div class="col-xl-3 col-sm-12"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_type" onclick="appendTrigger(this.id,this.value)" name="trigger_select[]" id="trigger_type_'+i+'"><option disabled selected>Select Trigger</option><option value="state">State</option><option value="course">Course</option><option value="university">University</option></select></div></div><div class="col-xl-3 col-sm-12" id="balance_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select" id="trigger_operator_'+i+'" name="balance_select[]"><option disabled selected>Select Operator</option><option value="equalto">EqualsTo</option><option value="notequalto">NotEqualsTo</option></select></div></div><div class="col-xl-4 col-sm-12" id="data_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_show" id="trigger_show_'+i+'"  name="data_select[]"></select></div></div><div class="col-xl-2 col-sm-12" id="more_buttons"><button type="button"class="btn btn-link btn-xs remove_'+i+'" style="float: right;" id="remove_'+i+'" value="'+id[0]+'" onclick="removeAppendFunction(this.id);"><i class="fa fa-times"></i></button><button type="button" class="btn btn-link clone" style="float: right;" id="append_'+i+'" value="append_add_'+id[0]+'" onclick="addAppendFunction(this.value);"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button></div></div>');
                                                                        i++;
                                                                        
                                                                    }

                                                                    function removeAppendFunction(val) {
                                                                        //alert(val.match(/(\d+)/)); 
                                                                        var id = val.match(/\d+/); 
                                                                    //alert(id);
                                                                         $("#row_"+id[0]).remove();
                                                                         $("#operator_"+id[0]).remove();

                                                                  
                                                                        
                                                                    }

                                                                      

                                                                    function appendTrigger(val,trigger_val)
                                                                    {

                                                                        var id = val.match(/\d+/); 
                                                                        
                                                                            //var id = val.slice(-1);
                                                                            //var trigger_val = $('#trigger_type_'+id+).val();
                                                                            //console.log(trigger_val);
                                                                           
                                                                            $.ajax({
                                                                                url: 'ajax_marketing/trigger.php',
                                                                                method:'POST',
                                                                                data:{'trigger_type': trigger_val},
                                                                                success:function(res)
                                                                                {
                                                                                    console.log(res);
                                                                                    
                                                                                        $("#trigger_show_"+id[0]).html(res);
                                                                            
                                                                                  
                                                                                   
                                                                                   
                                                                                }
                                                                            })
                                                                    }

                                                                    function removeEditAppendFunction(val) {
                                                                        //alert(val.match(/(\d+)/)); 
                                                                        var id = val.match(/\d+/); 
                                                                    //alert(id);
                                                                         $("#edit_row_"+id[0]).remove();
                                                                         $("#edit_operator_"+id[0]).remove();

                                                                  
                                                                        
                                                                    }

                                                                      

                                                                    function appendEditTrigger(val,trigger_val)
                                                                    {

                                                                        var id = val.match(/\d+/); 
                                                                        
                                                                            //var id = val.slice(-1);
                                                                            //var trigger_val = $('#trigger_type_'+id+).val();
                                                                            //console.log(trigger_val);
                                                                           
                                                                            $.ajax({
                                                                                url: 'ajax_marketing/trigger.php',
                                                                                method:'POST',
                                                                                data:{'trigger_type': trigger_val},
                                                                                success:function(res)
                                                                                {
                                                                                    console.log(res);
                                                                                    
                                                                                        $("#edit_trigger_show_"+id[0]).html(res);
                                                                            
                                                                                  
                                                                                   
                                                                                   
                                                                                }
                                                                            })
                                                                    }

                                                                    // Form Validation

        function editDrip()
        {
           if($('#editDripForm').valid())
            {
                var if_sentence = "";
                var then_sentence = "";

                var marketing_rule_name = $('#edit_marketing_rule_name').val();
                var start_date = $('#edit_basic-datepicker').val();
                var start_time = $('#edit_basic-timepicker').val();
                var communication_action = $('#edit_communication_action').val();
                var communication_mode = $('#edit_communication_mode').val();

                    
                    for(j=1; j<i; j++)
                    {
                        var trigger_type = $('#edit_trigger_type_'+j).val();
                        var trigger_operator =$('#edit_trigger_operator_'+j).val();
                        var trigger_show = $('#edit_trigger_show_'+j).val();

                        var operator_value = $('#edit_operator_value').val();
                        
                        if_sentence = if_sentence+" $"+trigger_type+" "+trigger_operator+" "+trigger_show+" "+operator_value;
                        
                    }

                    for(j=1; j<k; j++) {
                        var lead = $('#edit_lead_'+j).val();
                        var lead_show = $('#edit_lead_show_'+j).val();

                        then_sentence = then_sentence+" $"+lead+" SHOULD BE "+lead_show+" AND";
                    }

                    if_sentence = if_sentence.substring(0, if_sentence.lastIndexOf(" "));
                    then_sentence = then_sentence.substring(0, then_sentence.lastIndexOf(" "));

                    if_sentence = if_sentence.replaceAll("AND", "&&");
                    if_sentence = if_sentence.replaceAll("OR", "||");
                    if_sentence = if_sentence.replaceAll("notequalto", "<>");
                    if_sentence = if_sentence.replaceAll("equalto", "=");

                    then_sentence = then_sentence.replace("SHOULD BE", "=");
                    then_sentence = then_sentence.replaceAll("AND", "&&");

                    var sms_ok, email_ok, whatsapp_ok = false;
                    var comm_mode_final_text = "";

                    if(trigger_type == null || trigger_operator == null || trigger_show == null || lead == null || lead_show == null) {
                        toastr.error("Some fields are empty!");
                    }
                    else {
                        if(communication_mode.includes("SMS")) {
                            var sms_template = $("#edit_selected_sms_template").val();
                            var sms_contacts = $("#edit_sms_primary").val()+$("#edit_sms_alt").val();
                            var sms_time = $('#edit_sms_time').val();

                            comm_mode_final_text = comm_mode_final_text+"SMS"+"~"+sms_template+"~"+sms_contacts+"~"+sms_time+"@";

                            if(sms_template == null) {
                                toastr.error("Please select an SMS Template");
                            }
                            else {
                                sms_ok = true;
                            }

                            if(sms_time == null) {
                                toastr.error("Please select SMS Time");
                            }
                            else {
                                sms_ok = true;
                            }
                        }
                        else {
                            sms_ok = true;
                        }
                        if(communication_mode.includes("Email")) {
                            var email_template = $("#edit_selected_email_template").val();
                            var email_contact = $('#edit_email_primary').val();
                            var email_time = $('#edit_email_time').val();

                            comm_mode_final_text = comm_mode_final_text+"EMAIL"+"~"+email_template+"~"+email_contact+"~"+email_time+"@";

                            if(email_template == null) {
                                toastr.error("Please select an Email Template");
                            }
                            else {
                                email_ok = true;
                            }

                            if(email_time == null) {
                                toastr.error("Please select Email Time");
                            }
                            else {
                                email_ok = true;
                            }
                        }
                        else {
                            email_ok = true;
                        }
                        if(communication_mode.includes("WhatsApp")) {
                            var whatsapp_template = $("#edit_selected_whatsapp_template").val();
                            var whatsapp_contacts = $("#edit_whatsapp_primary").val()+$("#edit_whatsapp_alt").val();
                            var whatsapp_time = $("#edit_whatsapp_time").val();

                            comm_mode_final_text = comm_mode_final_text+"WHATSAPP"+"~"+whatsapp_template+"~"+whatsapp_contacts+"~"+whatsapp_time+"@";

                            if(whatsapp_template == null) {
                                toastr.error("Please select a WhatsApp Template");
                            }
                            else {
                                whatsapp_ok = true;
                            }

                            if(whatsapp_time == null) {
                                toastr.error("Please select WhatsApp Time");
                            }
                            else {
                                whatsapp_ok = true;
                            }
                        }
                        else {
                            whatsapp_ok = true;
                        }

                        if(sms_ok && email_ok && whatsapp_ok) {
                            console.log(sms_contacts);
                            if(email_contact === '') {
                                toastr.error("Primary Email is required for Email");
                            }
                            if(sms_contacts === '') {
                                toastr.error("Please select atleast one number for SMS");
                            }
                            if(whatsapp_contacts === '') {
                                toastr.error("Please select atleast one number for WhatsApp");
                            }
                            if(email_contact != '' && sms_contacts != '' && whatsapp_contacts != '') {
                                comm_mode_final_text = comm_mode_final_text.substring(0, comm_mode_final_text.length-1);
                                

                                $.ajax({
                                    url: 'ajax_marketing/edit_ajax_drip_marketing.php',
                                    type: 'post',
                                    data: {"Name": marketing_rule_name, "startTime": start_date+" "+start_time, "ifStatement": if_sentence.trim(), "thenStatement": then_sentence.trim(), "finalCommModes": comm_mode_final_text},
                                    success: function(response){ 
                                        console.log(response);
                                        if(response.match("true")) {
                                            toastr.success("Rule updated successfully");
                                            location.reload();
                                        }
                                        else {
                                            toastr.error("Unable to add rule");
                                        }
                                    }
                                });
                            }
                        }
                    }
            }
            else
            {
                event.preventDefault();
            }
        }



        function submitDrip()
        {
           if($('#dripForm').valid())
            {
                var if_sentence = "";
                var then_sentence = "";

                var marketing_rule_name = $('#marketing_rule_name').val();
                var start_date = $('#basic-datepicker').val();
                var start_time = $('#basic-timepicker').val();
                var communication_action = $('#communication_action').val();
                var communication_mode = $('#communication_mode').val();

                    
                    for(j=1; j<i; j++)
                    {
                        var trigger_type = $('#trigger_type_'+j).val();
                        var trigger_operator =$('#trigger_operator_'+j).val();
                        var trigger_show = $('#trigger_show_'+j).val();

                        var operator_value = $('#operator_value').val();
                        
                        if_sentence = if_sentence+" $"+trigger_type+" "+trigger_operator+" "+trigger_show+" "+operator_value;
                        
                    }

                    for(j=1; j<k; j++) {
                        var lead = $('#lead_'+j).val();
                        var lead_show = $('#lead_show_'+j).val();

                        then_sentence = then_sentence+" $"+lead+" SHOULD BE "+lead_show+" AND";
                    }

                    if_sentence = if_sentence.substring(0, if_sentence.lastIndexOf(" "));
                    then_sentence = then_sentence.substring(0, then_sentence.lastIndexOf(" "));

                    if_sentence = if_sentence.replaceAll("AND", "&&");
                    if_sentence = if_sentence.replaceAll("OR", "||");
                    if_sentence = if_sentence.replaceAll("notequalto", "<>");
                    if_sentence = if_sentence.replaceAll("equalto", "=");

                    then_sentence = then_sentence.replace("SHOULD BE", "=");
                    then_sentence = then_sentence.replaceAll("AND", "&&");

                    var sms_ok, email_ok, whatsapp_ok = false;
                    var comm_mode_final_text = "";

                    if(trigger_type == null || trigger_operator == null || trigger_show == null || lead == null || lead_show == null) {
                        toastr.error("Some fields are empty!");
                    }
                    else {
                    	if(communication_mode.includes("SMS")) {
                    		var sms_template = $("#selected_sms_template").val();
                    		var sms_contacts = $("#sms_primary").val()+$("#sms_alt").val();
                    		var sms_time = $('#sms_time').val();

                    		comm_mode_final_text = comm_mode_final_text+"SMS"+"~"+sms_template+"~"+sms_contacts+"~"+sms_time+"@";

                    		if(sms_template == null) {
                    			toastr.error("Please select an SMS Template");
                    		}
                    		else {
                    			sms_ok = true;
                    		}

                    		if(sms_time == null) {
                    			toastr.error("Please select SMS Time");
                    		}
                    		else {
                    			sms_ok = true;
                    		}
                    	}
                    	else {
                    		sms_ok = true;
                    	}
                    	if(communication_mode.includes("Email")) {
                    		var email_template = $("#selected_email_template").val();
                    		var email_contact = $('#email_primary').val();
                    		var email_time = $('#email_time').val();

                    		comm_mode_final_text = comm_mode_final_text+"EMAIL"+"~"+email_template+"~"+email_contact+"~"+email_time+"@";

                    		if(email_template == null) {
                    			toastr.error("Please select an Email Template");
                    		}
                    		else {
                    			email_ok = true;
                    		}

                    		if(email_time == null) {
                    			toastr.error("Please select Email Time");
                    		}
                    		else {
                    			email_ok = true;
                    		}
                    	}
                    	else {
                    		email_ok = true;
                    	}
                    	if(communication_mode.includes("WhatsApp")) {
                    		var whatsapp_template = $("#selected_whatsapp_template").val();
                    		var whatsapp_contacts = $("#whatsapp_primary").val()+$("#whatsapp_alt").val();
                    		var whatsapp_time = $("#whatsapp_time").val();

                    		comm_mode_final_text = comm_mode_final_text+"WHATSAPP"+"~"+whatsapp_template+"~"+whatsapp_contacts+"~"+whatsapp_time+"@";

                    		if(whatsapp_template == null) {
                    			toastr.error("Please select a WhatsApp Template");
                    		}
                    		else {
                    			whatsapp_ok = true;
                    		}

                    		if(whatsapp_time == null) {
                    			toastr.error("Please select WhatsApp Time");
                    		}
                    		else {
                    			whatsapp_ok = true;
                    		}
                    	}
                    	else {
                    		whatsapp_ok = true;
                    	}

                    	if(sms_ok && email_ok && whatsapp_ok) {
                    		console.log(sms_contacts);
                    		if(email_contact === '') {
                    			toastr.error("Primary Email is required for Email");
                    		}
                    		if(sms_contacts === '') {
                    			toastr.error("Please select atleast one number for SMS");
                    		}
                    		if(whatsapp_contacts === '') {
                    			toastr.error("Please select atleast one number for WhatsApp");
                    		}
                    		if(email_contact != '' && sms_contacts != '' && whatsapp_contacts != '') {
                    			comm_mode_final_text = comm_mode_final_text.substring(0, comm_mode_final_text.length-1);
                    			

                    			$.ajax({
                    				url: 'ajax_marketing/ajax_drip_marketing.php',
									type: 'post',
									data: {"Name": marketing_rule_name, "startTime": start_date+" "+start_time, "ifStatement": if_sentence.trim(), "thenStatement": then_sentence.trim(), "finalCommModes": comm_mode_final_text},
									success: function(response){ 
										console.log(response);
										if(response.match("true")) {
											toastr.success("Rule added successfully");
											location.reload();
										}
										else {
											toastr.error("Unable to add rule");
										}
									}
                    			});
                    		}
                    	}
                    }
            }
            else
            {
                event.preventDefault();
            }
        }
                                                                </script>

                                                                <script type="text/javascript">
                                                                    var k=2;
                                                                    var opts = [];
                                                                    function addLead(val) {
                                                                        
                                                                        $("#then_lead_result").append('<div class="wrapper" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px; margin-top: 10px;" id="wrapper_'+k+'"><div class="element" style="display: block;" ><div class="row" style="padding-top: 15px;"><div class="col-xl-3 col-sm-12"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select trigger_type" name="lead_select[]" id="lead_'+k+'" onchange="Lead(this.id,this.value)"><option disabled selected>Select Lead Attribute</option><option value="state">State</option><option value="course">Course</option><option value="university">University</option></select></div></div><div class="col-xl-3 col-sm-12" id="balance_select"><div class="form-group mt-3 mt-sm-0" style="text-align: center;"><label>Should be</label></div></div><div class="col-xl-4 col-sm-12" id="data_select"><div class="form-group mt-3 mt-sm-0"><select class="form-control custom-select lead_show" id="lead_show_'+k+'"  name="lead_data_select[]"></select></div></div><div class="col-xl-2 col-sm-12" id="more_buttons"><button type="button" class="btn btn-link remove" id="remove_lead_'+k+'" style="float: right;" onclick="removeLead(this.id);"><i class="fa fa-times" style="margin-top: 0px;"></i></button><button type="button" class="btn btn-link clone" style="float: right;" onclick="addLead();"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button></div></div></div></div>');
                                                                        k++;
                                                                        
                                                                    }

                                                                    function Lead(val,trigger_val)
                                                                    {

                                                                        var id = val.match(/\d+/); 
                                                                        
                                                                            //var id = val.slice(-1);
                                                                            //var trigger_val = $('#trigger_type_'+id+).val();
                                                                            //console.log(trigger_val);
                                                                           
                                                                            $.ajax({
                                                                                url: 'ajax_marketing/trigger.php',
                                                                                method:'POST',
                                                                                data:{'trigger_type': trigger_val},
                                                                                success:function(res)
                                                                                {
                                                                                    console.log(res);
                                                                                    
                                                                                        $("#lead_show_"+id[0]).html(res);
                                                                            
                                                                                  
                                                                                   
                                                                                   
                                                                                }
                                                                            })
                                                                    }

                                                                    function editLead(val,trigger_val)
                                                                    {

                                                                        var id = val.match(/\d+/); 
                                                                        
                                                                            //var id = val.slice(-1);
                                                                            //var trigger_val = $('#trigger_type_'+id+).val();
                                                                            //console.log(trigger_val);
                                                                           
                                                                            $.ajax({
                                                                                url: 'ajax_marketing/edit_trigger.php',
                                                                                method:'POST',
                                                                                data:{'trigger_type': trigger_val},
                                                                                success:function(res)
                                                                                {
                                                                                    $("#edit_lead_show_"+id[0]).html(res);
                                                                                }
                                                                            })
                                                                    }

                                                                    function removeLead(val) {
                                                                        //alert(val.match(/(\d+)/)); 
                                                                        var id = val.match(/\d+/); 
                                                                    //alert(id);
                                                                         $("#wrapper_"+id[0]).remove();
                                                                         k--;
                                                                    }

                                                                    function commAction(val)
                                                                    {
                                                                    	$('#sms_block').css('display','none');
                                                                        	$('#email_block').css('display', 'none');
                                                                        	$('#whatsapp_block').css('display','none');

                                                                            $('#sms_primary').prop("disabled", false);
                                                                            $('#sms_alt').prop("disabled", false);

                                                                            $('#email_primary').prop("disabled", false);
                                                                            $('#email_alt').prop("disabled", false);               
                                                                            
                                                                            $('#whatsapp_primary').prop("disabled", false);
                                                                            $('#whatsapp_alt').prop("disabled", false);
                                                                                                                         

                                                                        if(val=="Immediate")
                                                                        {
                                                                        


                                                                            $('#communi_number').empty();
                                                                            $('#communi_count').empty();
                                                                            $('#communi_number').html('<div class="form-group mt-3 mt-sm-0"><select data-plugin="customselect" class="form-control custom-select2" name="communication_mode[]"  onchange="commMode(this)" multiple id="communication_mode"><option value="SMS">SMS</option><option value="Email">Email</option><option value="WhatsApp">WhatsApp</option></select></div>');


                                                                                $('.custom-select2').select2({
		                                                                        	placeholder: "Select Communication Modes",
		                                                                        });

                                                                        }
                                                                        
                                                                    }

                                                                    function editcommAction(val)
                                                                    {
                                                                        $('#edit_sms_block').css('display','none');
                                                                            $('#edit_email_block').css('display', 'none');
                                                                            $('#edit_whatsapp_block').css('display','none');

                                                                            $('#edit_sms_primary').prop("disabled", false);
                                                                            $('#edit_sms_alt').prop("disabled", false);

                                                                            $('#edit_email_primary').prop("disabled", false);
                                                                            $('#edit_email_alt').prop("disabled", false);               
                                                                            
                                                                            $('#edit_whatsapp_primary').prop("disabled", false);
                                                                            $('#edit_whatsapp_alt').prop("disabled", false);
                                                                                                                         

                                                                        if(val=="Immediate")
                                                                        {
                                                                        


                                                                            $('#edit_communi_number').empty();
                                                                            $('#edit_communi_count').empty();
                                                                            $('#edit_communi_number').html('<div class="form-group mt-3 mt-sm-0"><select data-plugin="customselect" class="form-control custom-select2" name="communication_mode[]" onchange="editcommMode(this)" multiple id="edit_communication_mode"><option value="SMS">SMS</option><option value="Email">Email</option><option value="WhatsApp">WhatsApp</option></select></div>');


                                                                                $('.custom-select2').select2({
                                                                                    placeholder: "Select Communication Modes",
                                                                                });

                                                                        }
                                                                        
                                                                    }

                                                                    // function showModes(val)
                                                                    // {
                                                                    	
                                                                    	
                                                                    	
                                                                    //     if(val>0)
                                                                    //     {
                                                                    //         $('#communi_count').html('<div class="form-group mt-3 mt-sm-0"><select data-plugin="customselect" class="form-control custom-select2" name="communication_mode[]"  onchange="commMode(this)" multiple><option value="SMS">SMS</option><option value="Email">Email</option><option value="WhatsApp">WhatsApp</option></select></div>');
                                                                    //         $('#sms_communi').empty();
                                                                    //         $('#email_communi').empty();
                                                                    //         $('#whatsapp_communi').empty();

                                                                    //         for(j=1; j<=val; j++)
                                                                    //         {
                                                                            	
                                                                    //         	$('#sms_communi').append('<div class="col-md-12" style="margin-top: 10px;"><p>'+j+'st Communication</p></div><div class="row" style="padding: 5px 14px;"><div class="col-md-4"><select class="form-control custom-select sms_template" name="sms_template[]" disabled><option disabled selected>Select Templates</option><?php $sql = $conn->query("SELECT ID,sms_template_name FROM SMS_Templates");while($result=mysqli_fetch_assoc($sql)){ ?><option value="<?php echo $result['ID']?>"><?php echo $result['sms_template_name']; ?></option><?php }?></select></div><div class="col-md-4"><select class="form-control custom-select sms_time" name="sms_time[]"><option disabled selected>Time Gap</option><option value="1 hr">1 hr</option><option value="4 hrs">4 hrs</option><option value="8 hrs">8 hrs</option><option value="1 day">1 day</option></select></div></div>');


                                                                    //         	$('#email_communi').append('<div class="col-md-12" style="margin-top: 10px;"><p>'+j+'st Communication</p></div><div class="row" style="padding: 5px 14px;"><div class="col-md-4"><select class="form-control custom-select email_template" name="email_template" disabled><option disabled selected>Select Templates</option><?php $sql = $conn->query("SELECT ID,template_name FROM Email_Templates");while($result=mysqli_fetch_assoc($sql)){ ?><option value="<?php echo $result['ID']?>"><?php echo $result['template_name']; ?></option><?php }?></select></div><div class="col-md-4"><select class="form-control custom-select email_time" name="email_time[]" disabled><option disabled selected>Time Gap</option><option value="1 hr">1 hr</option><option value="4 hrs">4 hrs</option><option value="8 hrs">8 hrs</option><option value="1 day">1 day</option></select></div></div>');

                                                                    //         	$('#whatsapp_communi').append('<div class="col-md-12" style="margin-top: 10px;"><p>'+j+'st Communication</p></div><div class="row" style="padding: 5px 14px;"><div class="col-md-4"><select class="form-control custom-select whatsapp_template" name="whatsapp_template" disabled><option disabled selected>Select Templates</option><?php $sql = $conn->query("SELECT ID,wa_template_name FROM WhatsApp_Templates");while($result=mysqli_fetch_assoc($sql)){ ?><option value="<?php echo $result['ID']?>"><?php echo $result['wa_template_name']; ?></option><?php }?></select></div><div class="col-md-4"><select class="form-control custom-select whatsapp_time" name="whatsapp_time[]" disabled><option disabled selected>Time Gap</option><option value="1 hr">1 hr</option><option value="4 hrs">4 hrs</option><option value="8 hrs">8 hrs</option><option value="1 day">1 day</option></select></div></div>');
                                                                            	
                                                                    //         }
                                                                            
                                                                    	
                                                                            

                                                                    //     }
                                                                    //     else{
                                                                    //     	$('#communi_count').empty();
                                                                    //     	$('#sms_communi').empty();
                                                                    //     	$('#sms_block').css('display','none');
                                                                    //     	$('#email_block').css('display', 'none');
                                                                    //     	$('#whatsapp_block').css('display','none');

                                                                    //         $('#sms_sender_id').prop("disabled", false);
                                                                    //         $('#sms_primary').prop("disabled", false);
                                                                    //         $('#sms_father').prop("disabled", false);
                                                                    //         $('#sms_mother').prop("disabled", false);
                                                                    //         $('#sms_alt').prop("disabled", false);

                                                                    //         $('#email_primary').prop("disabled", false);
                                                                    //         $('#email_father').prop("disabled", false);
                                                                    //         $('#email_mother').prop("disabled", false);
                                                                    //         $('#email_alt').prop("disabled", false);               
                                                                           
                                                                    //         $('#whatsapp_primary').prop("disabled", false);
                                                                    //         $('#whatsapp_father').prop("disabled", false);
                                                                    //         $('#whatsapp_mother').prop("disabled", false);
                                                                    //         $('#whatsapp_alt').prop("disabled", false);
                                                                                                                         
                                                                    //     }
                                                                    //     $('.custom-select2').select2({
                                                                    //     	placeholder: "Select Communication Modes",
                                                                    //     });
                                                                    // }
                                                                    function editcommMode(sel) {
                                                                      var opt;
                                                                      var opts = [];
                                                                      var len = sel.options.length;
                                                                      for (var i = 0; i < len; i++) {
                                                                        opt = sel.options[i];

                                                                        if (opt.selected) {
                                                                          opts.push(opt.value);
                                                                        }
                                                                      }
                                                                            console.log(opts);
                                                                          if(opts.includes("Email")) {
                                                                            $('#edit_email_block').css('display','block');
                                                                            $('#edit_email_primary').prop("disabled", false);
                                                                            $('#edit_email_alt').prop("disabled", false);
                                                                            $('.email_template').prop("disabled", false);
                                                                            $('.email_time').prop("disabled", false);
                                                                          }
                                                                          else {
                                                                            $('#edit_email_block').css('display', 'none');
                                                                            $('#edit_email_primary').prop("disabled", true);
                                                                            $('#edit_email_alt').prop("disabled", true);
                                                                            $('.email_template').prop("disabled", true);
                                                                            $('.email_time').prop("disabled", true);
                                                                          }
                                                                          
                                                                          if(opts.includes("SMS")) {
                                                                          	console.log("ok");
                                                                            $('#edit_sms_block').css('display','block');
                                                                            $('#edit_sms_primary').prop("disabled", false);
                                                                            $('#edit_sms_alt').prop("disabled", false);
                                                                            $('.edit_sms_template').prop("disabled", false);
                                                                            $('.edit_sms_time').prop("disabled", false);

                                                                          }
                                                                          else {
                                                                          	console.log("what");
                                                                            $('#edit_sms_block').css('display', 'none');
                                                                            $('#edit_sms_primary').prop("disabled", true);
                                                                            $('#edit_sms_alt').prop("disabled", true);
                                                                            $('.sms_template').prop("disabled", true);
                                                                            $('.sms_time').prop("disabled", true);
                                                                          }
                                                                          
                                                                          if(opts.includes("WhatsApp"))
                                                                          {
                                                                            $('#edit_whatsapp_block').css('display','block');
                                                                            $('#edit_whatsapp_primary').prop("disabled", false);
                                                                            $('#edit_whatsapp_alt').prop("disabled", false);
                                                                            $('.whatsapp_template').prop("disabled", false);
                                                                            $('.whatsapp_time').prop("disabled", false);
                                                                          }
                                                                          else {
                                                                            $('#edit_whatsapp_block').css('display', 'none');
                                                                            $('#edit_whatsapp_primary').prop("disabled", true);
                                                                            $('#edit_whatsapp_alt').prop("disabled", true);
                                                                            $('.whatsapp_template').prop("disabled", true);
                                                                            $('.whatsapp_time').prop("disabled", true);
                                                                          }
                                                                        }
                                                                    
                                                                    function commMode(sel) {
                                                                      var opt;
                                                                      var opts = [];
                                                                      var len = sel.options.length;
                                                                      for (var i = 0; i < len; i++) {
                                                                        opt = sel.options[i];

                                                                        if (opt.selected) {
                                                                          opts.push(opt.value);
                                                                      	}
                                                                      }
                                                                      		//console.log(opts);
                                                                          if(opts.includes("Email")) {
                                                                          	$('#email_block').css('display','block');
                                                                            $('#email_primary').prop("disabled", false);
                                                                            $('#email_alt').prop("disabled", false);
                                                                            $('.email_template').prop("disabled", false);
                                                                            $('.email_time').prop("disabled", false);
                                                                          }
                                                                          else {
                                                                          	$('#email_block').css('display', 'none');
                                                                            $('#email_primary').prop("disabled", true);
                                                                            $('#email_alt').prop("disabled", true);
                                                                            $('.email_template').prop("disabled", true);
                                                                            $('.email_time').prop("disabled", true);
                                                                          }
                                                                          
                                                                          if(opts.includes("SMS")) {
                                                                          	$('#sms_block').css('display','block');
                                                                            $('#sms_primary').prop("disabled", false);
                                                                            $('#sms_alt').prop("disabled", false);
                                                                            $('.sms_template').prop("disabled", false);
                                                                            $('.sms_time').prop("disabled", false);

                                                                          }
                                                                          else {
                                                                          	$('#sms_block').css('display', 'none');
                                                                            $('#sms_primary').prop("disabled", true);
                                                                            $('#sms_alt').prop("disabled", true);
                                                                            $('.sms_template').prop("disabled", true);
                                                                            $('.sms_time').prop("disabled", true);
                                                                          }
                                                                          
                                                                          if(opts.includes("WhatsApp"))
                                                                          {
                                                                            $('#whatsapp_block').css('display','block');
                                                                            $('#whatsapp_primary').prop("disabled", false);
                                                                            $('#whatsapp_alt').prop("disabled", false);
                                                                            $('.whatsapp_template').prop("disabled", false);
                                                                            $('.whatsapp_time').prop("disabled", false);
                                                                          }
                                                                          else {
                                                                          	$('#whatsapp_block').css('display', 'none');
                                                                            $('#whatsapp_primary').prop("disabled", true);
                                                                            $('#whatsapp_alt').prop("disabled", true);
                                                                            $('.whatsapp_template').prop("disabled", true);
                                                                            $('.whatsapp_time').prop("disabled", true);
                                                                          }
                                                                        }

                                                                  
                                                                </script>