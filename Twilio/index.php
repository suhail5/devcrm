<?php require "../filestobeincluded/db_config.php" ?>

<?php

if(session_status() === PHP_SESSION_NONE) session_start();

$sms_temp_query = $conn->query("SELECT * FROM SMS_Templates WHERE sms_template_name = 'Welcome Message'");
$sms_body_res = mysqli_fetch_assoc($sms_temp_query);

$sms_body = $sms_body_res['sms_template'];

$sms_body = str_replace('$lead_name', $lead_name, $sms_body);
$sms_body = str_replace('$counsellor_name', $counsellor_name, $sms_body);
$sms_body = str_replace('$counsellor_contact_no', $counsellor_mobile, $sms_body);
$sms_body = str_replace('$counsellor_email', $counsellor_email, $sms_body);

?>

<?php

require_once '/DialerAPI/vendor/autoload.php';

$client = new \GuzzleHttp\Client();

if(isset($_POST['phoneNumber']) && isset($_POST['leadName'])) {

	$phoneNumber = $_POST['phoneNumber'];
	$leadName = $_POST['leadName'];

	$sender_id_query = $conn->query("SELECT Sender_ID FROM Institutes LEFT JOIN users ON Institutes.ID = users.Institute_ID WHERE users.ID = '".$_SESSION['useremployeeid']."'");

	if($sender_id_query->num_rows > 0) {
		$sender_id_dets = mysqli_fetch_assoc($sender_id_query);

		$sender_id = $sender_id_dets['Sender_ID'];

		$key = "010TH37S00jcoqsW5hBKpgRe0mJEA";
		$profile_id = "624897";

		$response = $client->request('GET', 'http://sms.venetsmedia.com/shn/api/pushsms.php?usr='.$profile_id.'&key='.$key.'&sndr='.$sender_id.'&ph='.$phoneNumber.'&text='.$sms_body.'&rpt=1');

		$resp = $response->getBody();
		if (strpos($resp, 'Message Sent.') !== false) {
			echo "true";
		}
		else {
			echo "false";
		}
	}
	else {
		echo "false";
		exit;
	}
}

else {
	echo "false";
}

?>