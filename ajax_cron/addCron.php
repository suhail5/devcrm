<?php require '../filestobeincluded/db_config.php'; ?>

<?php

require_once '/opt/lampp/htdocs/CronTabManager/vendor/autoload.php';

use TiBeN\CrontabManager\CrontabJob;
use TiBeN\CrontabManager\CrontabRepository;
use TiBeN\CrontabManager\CrontabAdapter;

$crontabRepository = new CrontabRepository(new CrontabAdapter());

$rule_name = $_POST['Name'];
$time_gap = $_POST['TimeGap'];
$start_time = $_POST['StartTime'];
$stage = $_POST['Stage'];
$reason = $_POST['Reason'];
$finalCommModes = $_POST['finalCommModes'];

$hour = explode(":", date("G:i", strtotime($start_time)))[0];
$minute = explode(":", date("G:i", strtotime($start_time)))[1];

$crontabJob = new CrontabJob();
$crontabJob
    ->setMinutes($minute)
    ->setHours($hour)
    ->setDayOfMonth("*")
    ->setMonths("*")
    ->setDayOfWeek("*")
    ->setTaskCommandLine("/opt/lampp/bin/php /opt/lampp/htdocs/ajax_cron/customCron.php ".$stage." ".$reason." ".$time_gap." ".$finalCommModes." > /home/ubuntu/log.txt")
    ->setComments($rule_name);

$crontabRepository->addJob($crontabJob);
$crontabRepository->persist();

$add_cron_job = $conn->query("INSERT INTO CronJobs(Rule_Name, Stage_ID, Reason_ID, TimeGap, StartTime, CommModes) VALUES ('$rule_name', '$stage', '$reason', '$time_gap', '$start_time', '$finalCommModes')");

if($add_cron_job) {
	echo "true";
}
else {
	echo "false";
}


?>