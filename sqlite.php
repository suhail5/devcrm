<?php
   class MyDB extends SQLite3 {
      function __construct() {
         print_r(__DIR__);
         $this->open('drip.db');
      }
   }

   $db = new MyDB();
   if(!$db) {
      echo $db->lastErrorMsg();
   } else {
      echo "Opened database successfully\n";
   }

   $table = $db->exec("CREATE TABLE `drip_leads` 
   ( `ID` INTEGER NOT NULL,
   `lead_ID` VARCHAR(30) ,
   `counsellor_ID` VARCHAR(255) NULL ,
   `stage_ID` INT(8) NULL ,
   `mobile` VARCHAR(20) NULL ,
   `alt_mobile` VARCHAR(20) NULL ,
   `email` VARCHAR(100) NULL ,
   `name` VARCHAR(100) NULL ,
   `institute_ID` INT(8) NULL ,
   `course_name` VARCHAR(100) NULL ,
   `reason_ID` INT(8) NULL ,
   `state_ID` INT(8) NULL ,
   `status` VARCHAR(10) NOT NULL DEFAULT 'N' ,
   PRIMARY KEY (`ID`))");

$ret = $db->exec($sql);
   if(!$ret){
      echo $db->lastErrorMsg();
   } else {
      echo "Table created successfully\n";
   }
   $db->close();
