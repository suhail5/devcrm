<?php include 'filestobeincluded/header-top.php' ?>
<script src="https://kit.fontawesome.com/3212b33ef4.js" crossorigin="anonymous"></script>
<?php include 'filestobeincluded/header-bottom.php' ?>

<style>
	th,
	td,
	p,
	input {
		font: 14px Verdana;
	}

	table,
	th,
	td {
		border: solid 1px #DDD;
		border-collapse: collapse;
		padding: 2px 3px;
		text-align: center;
	}

	th {
		font-weight: bold;
		text-transform: capitalize;
	}





	.col-sm-2.call-dtlmain {
		padding: 0px 3px;
	}

	.call-dtlbox {
		box-shadow: 0 1px 5px #00000040;
		padding: 10px;
	}

	.call-dtlbox input.form-control {
		height: 30px;
		border: none;
		font-weight: 600;
		color: #000000;
		padding-left: 0;
	}

	.call-dtlbox input.form-control:focus {
		box-shadow: none;
		outline: none;
	}

	.call-dtlbox p {
		font-size: 14px;
		text-transform: uppercase;
		font-weight: 600;
		color: #404040;
		margin: 0;
	}

	.call-dtlbox span {
		display: inline-block;
		/* margin-left: 12px; */
	}

	div#pills-tabContent button.btn.btn-primary {
		width: 160px;
		height: 47px;
	}

	ul#pills-tab li.nav-item {
		background: #eee;
		border: 1px solid #eee;
	}


	ul#pills-tab {
		margin: 0 !important;
	}

	div#pills-tabContent {
		border: 1px solid #eee;
		padding: 20px 10px;
	}

	ul#pills-tab li.nav-item a {
		padding: 13px 35px;
		font-weight: 600;
		border-radius: 0;
	}

	.dataTables_filter {
		display: block !important;
	}

	.imggallery-opn {
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 99;
		overflow-y: scroll;
		cursor: pointer;
	}

	.imggallery-opn img {
		width: 80%;
	}

	.clospop-gallery {
		display: none;
	}

	.imggallery-opn .clospop-gallery {
		position: absolute;
		top: 30px;
		font-size: 37px;
		right: 18px;
		color: #fff;
		font-weight: 100;
		background: #333;
		width: 50px;
		height: 50px;
		border-radius: 30px;
		transition: all .5s ease-in-out;
		cursor: pointer;
	}

	.imggallery-opn .clospop-gallery:hover {
		transform: rotate(90deg);
		box-shadow: 7px 0px 11px #0202026e;
	}
</style>

<!-- Pre-loader -->
<!-- <div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div> -->
<!-- End Preloader -->
<?php include 'filestobeincluded/navigation.php' ?>
<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
<div class="container" style="margin-top: 96px;background: white;">
	<div class="table-responsive">
		<table id="basic-datatable1" class="table table-hover table-striped">
			<thead>
				<tr>
					<!-- <th>#</th> -->
					<th>Counsellor Name</th>
					<th>Screenshots</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$users = $conn->query("SELECT * FROM users");


				?>
				<?php while ($user = mysqli_fetch_assoc($users)) {
					$image = $conn->query("SELECT * FROM Screenshots WHERE User_ID='" . $user['ID'] . "'");
				?>
					<tr>
						<td><?php echo $user['Name'] ?></td>
						<td>
							<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#picmodal<?php echo $user['ID'] ?>">Details</button>
							<div class="modal fade" id="picmodal<?php echo $user['ID'] ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="myCenterModalLabel">Screenshots</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="container">
												<div class="row">
													<?php while ($img = mysqli_fetch_assoc($image)) { ?>
														<div class="col-md-3 mb-3">
															<div class="md-imggallery">
																<span class="clospop-gallery">&times;</span>
																<img width="100%" src="<?php echo $img['image'] ?>" class="modallery">
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>





<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#basic-datatable1").DataTable({
			"pageLength": 10000,

			"aoColumns": [{
					"bSearchable": true
				},
				{
					"bSearchable": false
				},

			],
			language: {
				paginate: {
					previous: "<i class='uil uil-angle-left'>",
					next: "<i class='uil uil-angle-right'>"
				}
			},
			drawCallback: function() {
				$(".dataTables_paginate > .pagination").addClass("pagination-rounded")
			}
		});
	});
</script>
<script>
	$(document).ready(function() {


		// ============================= //

		$(".md-imggallery").click(function() {
			$(this).toggleClass("imggallery-opn");
		});

		$(".clospop-gallery").click(function() {
			$(".md-imggallery").removeClass("imggallery-opn");
		});
	});
</script>