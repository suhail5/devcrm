<?php 
require '../filestobeincluded/db_config.php';

$userid = $_POST['userid'];
 
?>
<div class="modal-body text-center">
        <form method="POST">
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-warning" onclick="deleteCampaign(<?php echo $userid ?>);">&nbsp;Yes&nbsp;</button>
        </form>
    </div>

    <script>
        function deleteCampaign(id){
            var delete_campaign = id;
                $.ajax
                ({
                type: "POST",
                url: "ajax_campaign/delete_sql.php",
                data: {"delete_campaign":delete_campaign},
                success: function (data) {
                            
                    if(data.match("true")) {
                        $('.modal').modal('hide');
                        $("#basic-datatable").load(location.href + " #basic-datatable");
                        toastr.success('Campaign deleted successfully');

                    }
                    else {
                        toastr.error('Unable to delete campaign');
                    }
                }
                });
            }

    </script>
<?php
exit;
?>